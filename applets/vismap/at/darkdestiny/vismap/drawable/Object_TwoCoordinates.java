/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.drawable;

import javax.swing.JPanel;
import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.I_Object_TwoCoordinates;


public abstract class Object_TwoCoordinates extends Object_OneCoordinate implements I_Object_TwoCoordinates {

    protected String identifier;
    private int x2;
    private int y2;
    protected int actCoord_x2;
    protected int actCoord_y2;

    public Object_TwoCoordinates(int id, int x1, int y1, int x2, int y2) {

        super(id, "", x1, y1, null);
        this.x2 = x2;
        this.y2 = y2;
    }


    @Override
    public JPanel getPanel() {
        return new JPanel();
    }

 

    @Override
    public int getX2() {
        return x2;
    }

    @Override
    public void setX2(int x2) {
        this.x2 = x2;
    }

    @Override
    public int getY2() {
        return y2;
    }

    @Override
    public void setY2(int y2) {
        this.y2 = y2;
    }

    @Override
    public boolean isToShow(int i) {
        return false;
    }
    
    @Override
    public void init(MainPainter painter) {
        this.painter = painter;
    }

    
    @Override
    public int getActCoord_x2() {
        return this.actCoord_x2;
    }

    @Override
    public int getActCoord_y2() {
        return this.actCoord_y2;
    }

    @Override
    public void setActCoord_x2(int actCoord_x2) {
        this.actCoord_x2 = actCoord_x2;
    }

    @Override
    public void setActCoord_y2(int actCoord_y2) {
        this.actCoord_y2 = actCoord_y2;


    }

}
