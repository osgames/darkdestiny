/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.drawable;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import javax.swing.border.Border;
import at.darkdestiny.vismap.logic.DisplayManagement;
import java.awt.Color;

/**
 *
 * @author HorstRabe
 */
public class GraphPanel extends JPanel {

    public GraphPanel(String picName, int width, int height) {
        this.setPreferredSize(new Dimension(width, height));
        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic(picName), picName);
        final Border background = new CentredBackgroundBorder(bi);
        this.setBorder(background);

    }
    
    public GraphPanel(String picName, int width, int height, String tooltext) {
        this.setPreferredSize(new Dimension(width, height));
        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic(picName), picName, width, height);
        final Border background = new CentredBackgroundBorder(bi);
        this.setBorder(background);
        setToolTipText(tooltext);
    }

    public GraphPanel(String picName, int width, int height, String tooltext, boolean opaque) {
        this.setPreferredSize(new Dimension(width, height));
        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic(picName), picName, width, height);
       this.setBackground(Color.red);
        final Border background = new CentredBackgroundBorder(bi);
        this.setBorder(background);
        setToolTipText(tooltext);
        setOpaque(opaque);
    }
}
