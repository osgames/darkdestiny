/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.techtree;

import at.darkdestiny.vismap.dto.AbstractAppletData;
import at.darkdestiny.vismap.techtree.objects.ResDesc;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class AppletData extends AbstractAppletData{


    private List<String> techRelations = new ArrayList();
    private Map<String, ResDesc> researches = new TreeMap();
    private Map<Integer, List<Technology>> unlockedTech = new HashMap();
    public Map<Integer, String> idToName = new HashMap();
    public Map<String, Integer> nameToId = new HashMap();
    public AppletData() {
    }


    /**
     * @return the techRelations
     */
    public List<String> getTechRelations() {
        return techRelations;
    }


    /**
     * @return the researches
     */
    public Map<String, ResDesc> getResearches() {
        return researches;
    }

    /**
     * @param researches the researches to set
     */
    public void setResearches(Map<String, ResDesc> researches) {
        this.researches = researches;
    }

    /**
     * @return the unlockedTech
     */
    public Map<Integer, List<Technology>> getUnlockedTech() {
        return unlockedTech;
    }

    /**
     * @param unlockedTech the unlockedTech to set
     */
    public void setUnlockedTech(Map<Integer, List<Technology>> unlockedTech) {
        this.unlockedTech = unlockedTech;
    }

}
