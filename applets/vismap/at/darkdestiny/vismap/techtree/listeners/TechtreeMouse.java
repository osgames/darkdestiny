/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.techtree.listeners;

import at.darkdestiny.vismap.techtree.PainterData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.drawable.Object_TwoCoordinates;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.AbstractMouse;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import at.darkdestiny.vismap.techtree.Painter;
import at.darkdestiny.vismap.techtree.ResDesc_ToolTip;
import at.darkdestiny.vismap.techtree.Techtree;
import at.darkdestiny.vismap.techtree.buttons.ToolBox;
import at.darkdestiny.vismap.techtree.objects.ResDesc;
import at.darkdestiny.vismap.techtree.objects.TechRelation;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class TechtreeMouse extends AbstractMouse {

    private static final boolean SNAP_TO_GRID = true;

    private PainterData painterData;
    private at.darkdestiny.vismap.techtree.Painter painter;
    private AppletMain main;
    int lastMousePressedX = 0;
    int lastMousePressedY = 0;
    boolean mouseDragged = false;
    boolean mousePressed = true;
    float x_start = Float.NaN;
    float y_start = Float.NaN;
    private int tmpx;
    private int tmpy;
    private int x_int;
    private int y_int;
    public boolean moveMap = false;
    public boolean selectedRect = false;

    ResDesc sd = null;
    ResDesc conn1 = null;
    ResDesc conn2 = null;
    boolean connectingObjects = false;

    public TechtreeMouse(AppletMain main, IPainter painter, IPainterData painterData) {
        this.painterData = (PainterData) painterData;
        this.painter = (at.darkdestiny.vismap.techtree.Painter) painter;
        this.main = main;

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

        int zoom = e.getWheelRotation() * 1;

        painter.mouseWheelChanged(zoom, e);

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (painter.getMode().equals(EStarmapMode.DEFAULT)) {
            if (selectedRect) {
                tmpx = e.getX();
                tmpy = e.getY();
                if (Math.abs(tmpx - x_int) > 2 && Math.abs(tmpy - y_int) > 2) {
                    active = true;
                    painter.repaint();
                }
            } else if (moveMap) {

                painter.setMouseEvent(e);

                tmpx = e.getX();
                tmpy = e.getY();
                int diff_x = x_int - tmpx;
                int diff_y = y_int - tmpy;

                float weight = 0.3f;

                if (Math.abs(diff_x) > 2) {
                    if (diff_x > 0) {
                        painter.moveImage("left", weight);
                        diff_x--;
                    } else if (diff_x < 0) {
                        painter.moveImage("right", weight);
                        diff_x++;
                    }
                }
                if (Math.abs(diff_y) > 2) {
                    if (diff_y > 0) {
                        painter.moveImage("up", weight);
                        diff_y--;
                    } else if (diff_y < 0) {
                        painter.moveImage("down", weight);
                        diff_y++;
                    }
                }
                x_int = painter.getPreferredSize().width / 2;
                y_int = painter.getPreferredSize().height / 2;
                try {
                    Robot r = new java.awt.Robot();
                    r.mouseMove(main.getLocationOnScreen().x + x_int, main.getLocationOnScreen().y + y_int);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        ResDesc rd = ((Painter) painter).findResearch(e.getX(), e.getY());

        Painter p = (Painter) painter;
        if (rd != null) {
            if ((p.getShowToolTipForRes() == null) || (p.getShowToolTipForRes().getRd() != rd)) {
                p.setShowToolTipForRes(new ResDesc_ToolTip(rd, e.getX(), e.getY(), painter.getWidth(), painter.getHeight(), main));
                // showToolTipFor = new SystemDesc_ToolTip(sd, e.getX1(), e.getY1());
                painter.repaint();
            } else {
                p.getShowToolTipForRes().setPos(e.getX(), e.getY());
                painter.repaint();

            }

        } else if (p.getShowToolTipForRes() != null) {
            p.setShowToolTipForRes(null);
            painter.repaint();
        }
        if (!isAltPressed()) {
            painterData.setShowRaster(false);
        } else {
            painterData.setShowRaster(true);
        }
        if (sd != null) {
            if (main.getAppletData().isAdmin() && !connectingObjects && isAltPressed()) {
                getInformationData().remove(sd.getIdentifier());
                int[] con = new int[2];
                int rasterSize = 500;
                painter.convertCoordinatesBack(e.getX(), e.getY(), con);
                sd.setX1Normalized(con[0] - (con[0] % rasterSize));
                sd.setY1(con[1] - (con[1] % rasterSize));
                int[] pos = new int[2];
                int[] pos2 = new int[2];
                painter.convertCoordinates(sd, pos);
                getInformationData().put(sd.getName(), sd);

                List<I_Object_OneCoordinate> toRemove = new ArrayList();

                for (Map.Entry<String, I_Object_OneCoordinate> informationEntity : getInformationData().entrySet()) {
                    if (informationEntity.getValue() instanceof Object_TwoCoordinates) {
                        if (informationEntity.getValue() instanceof TechRelation) {
                            TechRelation tr = (TechRelation) informationEntity.getValue();

                            int r = tr.getReqResearchId();
                            int s = tr.getSourceId();
                            if (sd.getId() == r || sd.getId() == s) {
                                tr.updatePosition();
                            }
                        }
                    }
                }
                for (I_Object_OneCoordinate o : toRemove) {
                    TechRelation tr = (TechRelation) o;
                    int r = tr.getReqResearchId();
                    int s = tr.getSourceId();
                    getInformationData().remove(tr.getIdentifier());
                    TechRelation newTr = new TechRelation(r, s, getInformationData(), main);
                    getInformationData().put(newTr.getIdentifier(), newTr);
                    painter.convertCoordinates(tr, pos, pos2);
                }

                forceRefresh();
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

        //Zoom Rect
        if (painter.getMode().equals(EStarmapMode.DEFAULT)) {
            if (selectedRect) {
                float tmpx = painter.convertToFloatX(e.getX(), e.getY());
                float tmpy = painter.convertToFloatY(e.getX(), e.getY());

                if (Math.abs(x_int - e.getX()) > 5 && Math.abs(y_int - e.getY()) > 5) {

                    painter.zoomRect(x_start, y_start, tmpx, tmpy);
                }
                x_start = Float.NaN;
                y_start = Float.NaN;
            }

            active = false;
            moveMap = false;
            selectedRect = false;
            painter.setDefaultCursor(true);
            painter.forceRefresh();

        }
        if ((lastMousePressedX == e.getX()) && (lastMousePressedY == e.getY()) && !connectingObjects && !isCtrlPressed() && !isAltPressed()) {

            sd = painter.findResearch(e.getX(), e.getY());

            if (sd != null) {

                ((PainterData) painter.getPainterData()).unmark();
                sd.setMarked(true);
                Techtree techtree = (Techtree) main.getPlugin();
                ToolBox toolBox = (ToolBox) techtree.getToolBox();
                toolBox.getSearchTextField().setText(sd.getName());
                main.mainFrame.updateInformationPanel(sd);
                forceRefresh();
            } else {
                main.mainFrame.clearInformationPanel();
            }

        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

        mousePressed = true;
        lastMousePressedX = e.getX();
        lastMousePressedY = e.getY();
        if (painter.getMode().equals(EStarmapMode.DEFAULT)) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                x_start = painter.convertToFloatX(e.getX(), e.getY());
                y_start = painter.convertToFloatY(e.getX(), e.getY());
                x_int = e.getX();
                y_int = e.getY();
                selectedRect = true;
            }
            if (e.getButton() == MouseEvent.BUTTON3) {
                moveMap = true;
                x_int = painter.getPreferredSize().width / 2;
                y_int = painter.getPreferredSize().height / 2;
            }
        }
        if (conn1 == null) {
            conn1 = ((Painter) painter).findResearch(e.getX(), e.getY());
            if (conn1 != null) {
                if (e.getButton() == MouseEvent.BUTTON1 && !isCtrlPressed()) {
                    connectingObjects = false;

                    conn1.setMarked(false);

                    conn1 = null;
                    conn2 = null;
                    forceRefresh();
                }
                if (main.getAppletData().isAdmin()) {
                    if (e.getButton() == MouseEvent.BUTTON1 && isCtrlPressed()) {
                        System.out.println("LMB");
                        System.out.println("WANNA CONNECT: " + conn1.getIdentifier());
                        connectingObjects = true;
                        conn1.setMarked(true);
                        forceRefresh();

                    }
                    if (e.getButton() == MouseEvent.BUTTON3 && isCtrlPressed()) {
                        List<I_Object_OneCoordinate> toRemove = new ArrayList();

                        for (Map.Entry<String, I_Object_OneCoordinate> informationEntity : getInformationData().entrySet()) {
                            if (informationEntity.getValue() instanceof Object_TwoCoordinates) {
                                if (informationEntity.getValue() instanceof TechRelation) {
                                    TechRelation tr = (TechRelation) informationEntity.getValue();

                                    int r = tr.getReqResearchId();
                                    int s = tr.getSourceId();
                                    if (conn1.getId() == r || conn1.getId() == s) {
                                        toRemove.add(tr);
                                    }
                                }
                            }
                        }
                        for (I_Object_OneCoordinate o : toRemove) {
                            TechRelation tr = (TechRelation) o;
                            getInformationData().remove(tr.getIdentifier());
                        }

                        System.out.println("RMB");
                        conn1 = null;
                        forceRefresh();
                    }
                }
            }
        } else if (e.getButton() == MouseEvent.BUTTON1 && isCtrlPressed() && conn1 != null) {
            conn2 = ((Painter) painter).findResearch(e.getX(), e.getY());
            if (conn2 != null && !conn1.getIdentifier().equals(conn2.getIdentifier())) {
                //Found object to connect

                TechRelation tr = new TechRelation(conn1.getId(), conn2.getId(), getInformationData(), main);
                conn1.setMarked(false);
                getInformationData().put(tr.getIdentifier(), tr);
                painterData.getDrawObjects().get(AppletConstants.DRAW_LEVEL_TECHRELATION).add(tr);
                connectingObjects = false;

                conn1 = null;
                conn2 = null;
                forceRefresh();

            }
        }
    }

    private void forceRefresh() {
        painter.forceRefresh();
    }

    private Map<String, I_Object_OneCoordinate> getInformationData() {

        return painter.getPainterData().getInformationData();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void paint(IPainter painter, Graphics g) {

        if (selectedRect) {
            int w = Math.abs(tmpx - x_int);
            int h = Math.abs(tmpy - y_int);
            g.setColor(new Color(0.3f, 0.3f, 0.8f, 0.3f));
            g.fillRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w, h);

            g.setColor(new Color(0.3f, 0.3f, 1f));
            g.drawRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w, h);
        } else if (moveMap) {
        }
    }

    @Override
    public void update(AppletMain main, MainPainter aThis, IPainterData painterData) {
        this.painterData = (PainterData) painterData;
        this.painter = (at.darkdestiny.vismap.techtree.Painter) painter;
        this.main = main;

    }

}
