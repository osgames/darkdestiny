/**
 *
 */
package at.darkdestiny.vismap.techtree.listeners;

import at.darkdestiny.vismap.gui.MainPainter;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * @author martin
 *
 */
public class SelectedRect implements MouseListener, MouseMotionListener {

    float x_start = Float.NaN;
    float y_start = Float.NaN;
    private MainPainter painter;
    private int tmpx;
    private int tmpy;
    private int x_int;
    private int y_int;
    private boolean active = false;

  
    public SelectedRect(MainPainter painter) {
        this.painter = painter;
        painter.addMouseListener(this);
        painter.addMouseMotionListener(this);
    }


    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent e) {
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent e) {

        float tmpx = painter.convertToFloatX(e.getX(), e.getY());
        float tmpy = painter.convertToFloatY(e.getX(), e.getY());




        if (Math.abs(x_int - e.getX()) > 5 && Math.abs(y_int - e.getY()) > 5) {
            //parent.setSystemSize(tmpx/x_start, tmpy/y_start);

            painter.zoomRect(x_start, y_start, tmpx, tmpy);
        }
        x_start = Float.NaN;
        y_start = Float.NaN;


        active = false;
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        tmpx = e.getX();
        tmpy = e.getY();
        active = true;
        painter.repaint();
    }

    public boolean isActive() {
        return active;
    }

    public void paint(MainPainter painter, Graphics g) {

        g.setColor(Color.white);
        int w = Math.abs(tmpx - x_int);
        int h = Math.abs(tmpy - y_int);

        g.drawRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w, h);
    }
}


