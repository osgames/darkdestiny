/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.techtree;

import at.darkdestiny.vismap.buttons.NavigationPanel;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.gui.LoaderScreen;
import at.darkdestiny.vismap.interfaces.IDataLoader;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.IVisMapPlugin;
import at.darkdestiny.vismap.logic.AbstractMouse;
import at.darkdestiny.vismap.techtree.buttons.ToolBox;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLayeredPane;
import javax.swing.SpringLayout;

/**
 *
 * @author Admin
 */
public class Techtree implements IVisMapPlugin {

    private IPainter painter;
    private AbstractMouse mouse;
    private ToolBox toolBox;

    @Override
    public IPainter getPainter(AppletMain main) {
        if (painter == null) {
            painter = new Painter(main);
        }
        return painter;
    }

    @Override
    public IDataLoader getDataLoader(AppletMain main, IAppletData appletData, IPainterData painterData, LoaderScreen l) {
        return new at.darkdestiny.vismap.techtree.DataLoader(main, appletData, painterData, l);
    }

    @Override
    public List<String> getImages() {
        List<String> images = new ArrayList<>();

        images.add("cancelP.png");
        images.add("cancel.jpg");
        images.add("zoomOut.png");
        images.add("zoomIn.png");
        images.add("arrright.png");
        images.add("arrleft.png");
        images.add("arrup.png");
        images.add("arrdown.png");

        return images;
    }

    @Override
    public void addPanels(JLayeredPane panel, AppletMain appletMain, IPainter painter) {

       toolBox = new ToolBox(painter, appletMain);
        SpringLayout layout = (SpringLayout) panel.getLayout();
        panel.add(getToolBox(), 2, 1);
        layout.putConstraint(SpringLayout.NORTH, getToolBox(), 15, SpringLayout.NORTH, panel);
        layout.putConstraint(SpringLayout.WEST, getToolBox(), 15, SpringLayout.WEST, panel);

        NavigationPanel np = new NavigationPanel(painter);
        panel.add(np, 6, 1);
        layout.putConstraint(SpringLayout.SOUTH, np, -20, SpringLayout.SOUTH, panel);
        layout.putConstraint(SpringLayout.WEST, np, panel.getPreferredSize().width / 2 - np.getPreferredSize().width / 2, SpringLayout.WEST, panel);
    }

    @Override
    public AbstractMouse getMouse() {

        return mouse;
    }

    @Override
    public AbstractMouse getMouse(AppletMain main, IPainter p, IPainterData painterData) {
        mouse = new at.darkdestiny.vismap.techtree.listeners.TechtreeMouse(main, p, painterData);

        return mouse;
    }

    /**
     * @return the toolBox
     */
    public ToolBox getToolBox() {
        return toolBox;
    }
}
