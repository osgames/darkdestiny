/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.techtree;

import at.darkdestiny.vismap.dto.AbstractPainterData;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.techtree.objects.ResDesc;
import at.darkdestiny.vismap.techtree.objects.TechRelation;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class PainterData extends AbstractPainterData {

    private int[][] maixma = new int[40][4];
    private boolean showMarkers = false;

    public PainterData() {

    }

    public void unmark() {
        for (Map.Entry<Integer, List<I_Object_OneCoordinate>> entry : this.drawObjects.entrySet()) {
            for (I_Object_OneCoordinate o : entry.getValue()) {
                if (o instanceof ResDesc) {
                    ResDesc res = (ResDesc) o;

                    res.setMarked(false);
                    o.setHide(false);
                } else if (o instanceof TechRelation) {
                    TechRelation tr = (TechRelation) o;
                    tr.setMarked(false);
                    o.setHide(false);
                }
            }
        }

    }

    /**
     * @return the maixma
     */
    public int[][] getMaixma() {
        return maixma;
    }

    /**
     * @param maixma the maixma to set
     */
    public void setMaixma(int[][] maixma) {
        this.maixma = maixma;
    }

    void resetMaxima() {
       this.maixma = new int[40][4];
    }

    public void switchShowMarkers() {
       this.showMarkers = !this.showMarkers;
    }

    /**
     * @return the showMarkers
     */
    public boolean isShowMarkers() {
        return showMarkers;
    }

    /**
     * @param showMarkers the showMarkers to set
     */
    public void setShowMarkers(boolean showMarkers) {
        this.showMarkers = showMarkers;
    }
}
