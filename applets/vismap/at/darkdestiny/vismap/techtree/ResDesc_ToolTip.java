/**
 *
 */
package at.darkdestiny.vismap.techtree;

import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.logic.ML;
import java.awt.*;

import java.text.NumberFormat;
import at.darkdestiny.vismap.techtree.objects.ResDesc;

/**
 * @author martin
 *
 */
public class ResDesc_ToolTip {

    private int x;
    private int y;
    private ResDesc rd;
    private int width;
    private int height;
    private int globalWidth;
    private int globalHeight;
    private AppletMain main;
   
    public ResDesc_ToolTip(ResDesc rd, int x, int y, int globalWidth, int globalHeight, AppletMain main) {
        this.x = x;
        this.y = y;
        this.rd = rd;
        this.globalWidth = globalWidth;
        this.globalHeight = globalHeight;
        this.main = main;

    }
        public ResDesc_ToolTip(ResDesc rd, int x, int y) {
        this.x = x;
        this.y = y;
        this.rd = rd;
    }
    public void paint(Painter painter, Graphics g) {

        int rectWidth = 20;
        int rectHeight = 20;

        String resName = ML.getMLStr("lbl_research", main.getAppletData().getLocale()) + ": "+getRd().getName()+"";
         String points = ML.getMLStr("lbl_abbr_researchpoints", main.getAppletData().getLocale()) + ":"+getFormattedNumber((long)getRd().getRp())+"|"+ ML.getMLStr("lbl_abbr_computerresearchpoints", main.getAppletData().getLocale()) + ":"+getFormattedNumber((long)getRd().getCrp());
        int longestLine = Math.max(g.getFontMetrics().stringWidth(resName),g.getFontMetrics().stringWidth(points));

        Font normalFont = g.getFont();
        Font boldFont = new Font(normalFont.getFontName(),Font.BOLD,normalFont.getSize());
        Font smallFont = new Font(normalFont.getFontName(),Font.PLAIN,normalFont.getSize()-1);
        Font verySmallFont = new Font(normalFont.getFontName(),Font.BOLD,normalFont.getSize()-3);

        // Loop through all textLines and determine longest line
        g.setFont(smallFont);
        int w = longestLine;
        int h = 31 ;
        this.width = w;
        this.height = h;

        if ((x+(w+rectWidth)) > globalWidth)
            x = x-(w+rectWidth);
        if((y+(h+rectHeight)) > globalHeight) {
            y = y-(h+rectHeight);

            // Check if new y will be lower than zero if yes move down till zero
            if (y < 0) {
                y = y + Math.abs(y);
            }
        }

        g.setFont(boldFont);

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, longestLine, 35);
        g.setColor(Color.black);
        g.drawRect(x, y, longestLine-1, 34);

        g.setFont(verySmallFont);
        g.drawString(resName, x+10, y+15);
        g.drawString(points, x+10, y+30);

        g.setFont(normalFont);

    }

    public void setPos(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
        public static String getFormattedNumber(long number) {
        NumberFormat nf = NumberFormat.getInstance();
        return nf.format(number);
    }

    /**
     * @return the rd
     */
    public ResDesc getRd() {
        return rd;
    }

    /**
     * @param rd the rd to set
     */
    public void setRd(ResDesc rd) {
        this.rd = rd;
    }
}
