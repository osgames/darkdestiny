package at.darkdestiny.vismap.techtree.objects;

import at.darkdestiny.vismap.gui.AppletMain;
import javax.swing.JPanel;
import at.darkdestiny.vismap.techtree.panels.research.ResearchListPanel;

/**
 * Ein System, f�r die Anzeige
 *
 * @author martin
 *
 */
public class ResearchList{

    /**
     * Anzeige ob Feind Freund usw;
     */
    /**
     * Daten f�r farbliche Hinterlegung im Join Modus
     */
    private AppletMain main;

     public ResearchList(AppletMain main) {
         this.main = main;
    }



    //ovdbg @Override
    public String getIdentifier() {
        return "researchlist";
    }

    //ovdbg @Override
    public JPanel getPanel() {
        return new ResearchListPanel(main);

    }

}
