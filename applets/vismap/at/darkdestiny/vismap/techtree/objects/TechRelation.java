/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.techtree.objects;

import at.darkdestiny.vismap.drawable.Object_TwoCoordinates;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.techtree.PainterData;
import at.darkdestiny.vismap.techtree.AppletData;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.Map;

/**
 *
 * @author Bullet
 */
public class TechRelation extends Object_TwoCoordinates {

    private int reqResearchStatus;
    private int sourceStatus;
    private int reqResearchId;
    private int sourceId;
    private static int STARPICTURE_SIZE = 150;
    private static final int MAXIMG_SIZE = 150;
    private AppletMain main;
    private boolean hide = false;

    public TechRelation(int reqResearchId, int sourceId, Map<String, I_Object_OneCoordinate> researches, AppletMain main) {

        super(0, 0, 0, 0, 0);
        this.reqResearchId = reqResearchId;
        this.sourceId = sourceId;
        this.main = main;

        this.identifier = reqResearchId + "  -  " + sourceId;
        this.updatePosition();

        ((AppletData) main.getAppletData()).getTechRelations().add(identifier);
    }

    public void updatePosition() {
        ResDesc sourceResearch = null;
        ResDesc requResearch = null;

        for (I_Object_OneCoordinate o : main.mainFrame.getPainter().getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_RESEARCH)) {
            if (o instanceof ResDesc) {
                ResDesc rd = (ResDesc) o;
                if (rd.getId() == sourceId) {
                    sourceResearch = rd;
                } else if (rd.getId() == reqResearchId) {
                    requResearch = rd;
                }
                if ((sourceResearch != null && requResearch != null)) {
                    break;
                }
            }

        }

        if ((sourceResearch != null && requResearch != null)) {

            this.setX1((int) requResearch.getX1());
            this.setY1((int) requResearch.getY1());
            this.setX1((int) requResearch.getX1());
            this.setY1((int) requResearch.getY1());
            this.reqResearchStatus = requResearch.getStatus();
            this.setX2((int) sourceResearch.getX1());
            this.setY2((int) sourceResearch.getY1());
            this.sourceStatus = sourceResearch.getStatus();

            sourceResearch.setTotalRequRes(sourceResearch.getTotalRequRes() + 1);
            if (requResearch.getStatus() == 3) {
                sourceResearch.setRequResDone(sourceResearch.getRequResDone() + 1);
            }

        }

        if (requResearch == null && sourceResearch != null) {
            this.setX1((int) sourceResearch.getX1());
            this.setY1((int) sourceResearch.getY1());
        }

    }

    /**
     * @return the reqResearchId
     */
    public int getReqResearchId() {
        return reqResearchId;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public void draw(IAppletData appletData_, Graphics gc, float[] delta) {

        AppletData appletData = ((AppletData) appletData_);
        PainterData painterData = (PainterData) main.mainFrame.getPainter().getPainterData();
        String sourceIdent = appletData.idToName.get(getSourceId());
        String reqIdent = appletData.idToName.get(getReqResearchId());

        ResDesc rd1 = (ResDesc) painterData.getInformationData().get(sourceIdent);
        ResDesc rd2 = (ResDesc) painterData.getInformationData().get(reqIdent);
        int x1 = 0;
        int x2 = 0;
        if (rd1 != null) {
            x1 = rd1.width;
        }
        if (rd2 != null) {
            x2 = rd2.width;
        }
        if (getReqResearchStatus() == 0) {

            gc.setColor(Color.RED);
        } else if (getReqResearchStatus() == 1) {
            gc.setColor(Color.getHSBColor(0.051f, 0.973f, 1.000f));
        } else if (getReqResearchStatus() == 2) {

            gc.setColor(Color.YELLOW);
        } else if (getReqResearchStatus() == 3) {
            gc.setColor(Color.GREEN);
        }

        if (isMarked()) {
            gc.setColor(Color.WHITE);
        }
        int[] o = new int[2];
        o[0] = (int) (actCoord_y);
        o[1] = (int) ((actCoord_x + x2 / 2) - 5);

        int[] p = new int[2];
        p[0] = actCoord_y + ((actCoord_y2 - actCoord_y) / 2);
        p[1] = (int) ((actCoord_x2 + x1 / 2) - 5);

        int[] q = new int[2];
        q[1] = (int) ((actCoord_x2 + x1 / 2) - 5);
        q[0] = (int) (actCoord_y2 - 10);

        //Normalize
//        o[1] -= (o[1] % 30);
//        q[1] -= (p[1] % 30);

        if (o[1] == q[1]) {
            gc.drawLine(o[1],
                    o[0],
                    q[1],
                    q[0]);
        } else {

            gc.drawLine(o[1],
                    o[0],
                    o[1],
                    p[0]);

            //p[1] = actCoord_x + ((actCoord_x2 - actCoord_x) / 2);
            gc.drawLine(o[1],
                    p[0],
                    p[1],
                    p[0]);

            gc.drawLine(p[1],
                    p[0],
                    q[1],
                    q[0]);
        }
        int inversion = 1;
        if (q[1] < p[1]) {
            inversion = -1;
        }

        double a = 140 * delta[0] * inversion;
//        System.out.println("o[" + o[0] + "][" + o[1] + "]");
//        System.out.println("p[" + p[0] + "][" + p[1] + "]");
//        System.out.println("q[" + q[0] + "][" + q[1] + "]");

        int[] r = calculateFirstPoint(a, p, q);

//        System.out.println("r[" + r[0] + "][" + r[1] + "]");
        double b = 20 * delta[0] * inversion;

        double c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

//        System.out.println("c[" + c + "]");
        double cosBeta = (Math.pow(b, 2) + Math.pow(c, 2) - Math.pow(a, 2)) / (2 * b * c);
//        System.out.println("cosBeta[" + cosBeta + "]");
        double beta = Math.cos(cosBeta);
//        System.out.println("beta[" + beta + "]");
        double s = b * Math.sin(beta);
//        System.out.println("s[" + s + "]");
        double t = Math.sqrt(Math.pow(b, 2) + Math.pow(s, 2));
//        System.out.println("t[" + t + "]");

        int[] u1 = new int[2];

        u1[0] = (int) (r[0] - s);
        u1[1] = (int) (r[1] + t);
//        System.out.println("u[" + u1[0] + "][" + u1[1] + "]");

        int[] u2 = new int[2];
        u2[0] = (int) (r[0] + s);
        u2[1] = (int) (r[1] - t);

//        gc.setColor(Color.yellow);
//        gc.fillOval((int) u2, (int) u1, 20, 20);
//        gc.setColor(Color.green);
//        gc.fillOval((int) u2_2, (int) u2_1, 20, 20);
//
        Polygon poly = new Polygon();
        poly.addPoint(q[1], q[0]);
        poly.addPoint(u1[1], u1[0]);
        poly.addPoint(u2[1], u2[0]);
        gc.fillPolygon(poly);

//        gc.setColor(Color.red);
//        gc.fillOval(r[1], r[0], 4, 4);
//        gc.setColor(Color.yellow);
//        gc.fillOval(u1[1], u1[0], 4, 4);
//        gc.setColor(Color.green);
//        gc.fillOval(u2[1], u2[0], 4, 4);
//        gc.drawLine((int) ((actCoord_x + x2 / 2) - 5),
//                (int) (actCoord_y),
//                (int) ((actCoord_x2 + x1 / 2) - 5),
//                (int) (actCoord_y2 - 10));
    }

    public int[] calculateFirstPoint(double a, int[] p, int[] q) {

        int[] r = new int[2];

        int v1 = q[0] - p[0];
        int v2 = q[1] - p[1];
//        System.out.println("v[" + v1 + "][" + v2 + "]");
        double k = 0d;

        if (v1 != 0) {
            k = (double) v2 / (double) v1;
        } else {
            k = v2;
        }
//        System.out.println("k[" + k + "]");
        double d1 = Math.sqrt(Math.pow(a, 2) / (Math.pow(k, 2) + 1));
        double d2 = Math.sqrt(Math.pow(a, 2) - (Math.pow(d1, 2)));

//        System.out.println("d[" + d1 + "][" + d2 + "]");
        if (a < 0) {
            r[0] = (int) (q[0] - d1);
            r[1] = (int) (q[1] + d2);
        } else {

            r[0] = (int) (q[0] - d1);
            r[1] = (int) (q[1] - d2);
        }
        return r;

    }

    /**
     * @return the sourceId
     */
    public int getSourceId() {
        return sourceId;
    }

    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > MAXIMG_SIZE) {
            return MAXIMG_SIZE;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    /**
     * @return the reqResearchStatus
     */
    public int getReqResearchStatus() {
        return reqResearchStatus;
    }

    /**
     * @return the sourceStatus
     */
    public int getSourceStatus() {
        return sourceStatus;
    }

    /**
     * @return the hide
     */
    @Override
    public boolean isHide() {
        return hide;
    }

    /**
     * @param hide the hide to set
     */
    @Override
    public void setHide(boolean hide) {
        this.hide = hide;
    }

}
