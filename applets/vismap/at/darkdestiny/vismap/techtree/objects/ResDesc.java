package at.darkdestiny.vismap.techtree.objects;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.techtree.PainterData;
import at.darkdestiny.vismap.techtree.AppletData;
import at.darkdestiny.vismap.techtree.Technology;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.JPanel;

import at.darkdestiny.vismap.techtree.panels.research.ResearchInformationPanel;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

/**
 * Ein System, f�r die Anzeige
 *
 * @author martin
 *
 */
public class ResDesc extends Object_OneCoordinate {

    private int STARPICTURE_SIZE = 30;
    /**
     * Wie weit sind Planeten vom eigenen System aus zu sehen
     */
    private static final int VISIBLE_RADIUS = 100;
    private static final int MAXIMG_SIZE = 19;
    /**
     * Anzeige ob Feind Freund usw;
     */
    private int status = -1;
    /**
     * Daten f�r farbliche Hinterlegung im Join Modus
     */
    private int totalRequRes;
    private int requResDone;
    private int popPoints = 0;
    private int scorePoints = 0;
    private AppletMain mainApplet;
    private String identifier;
    private int rp;
    private int crp;
    private String description;
    private boolean marked = false;
    private boolean hide = false;
    public int width = 0;
    public int totalWidth = 0;
    boolean totalResPointsCalculated = false;
    int[] totalpoints = new int[2];

    public ResDesc(String sysName, int id, int x, int y, int status, Image imgName, AppletMain mainApplet, int rp, int crp, String description) {
        super(id, sysName, x, y, imgName);
        this.rp = rp;
        this.crp = crp;
        this.description = description;
        this.status = status;
        this.mainApplet = mainApplet;
        width = 0;

        this.identifier = sysName;
        ((AppletData) mainApplet.getAppletData()).getResearches().put(sysName, this);
    }

    public ResDesc(String sysName, int sysId, int x, int y, Image img, AppletMain mainApplet) {
        super(sysId, sysName, x, y, img);
        this.mainApplet = mainApplet;
        this.identifier = sysName;
    }

    @Override
    public boolean isToShow(int i) {
        return true;
    }

    //ovdbg @Override
    public void drawVisibleOvals(Graphics gc, int level, float[] delta) {
    }

    public void updateWidth(Graphics gc) {

        String resName = getName();
        int longestLine = gc.getFontMetrics().stringWidth(resName);
        width = longestLine;
    }
    //ovdbg @Override

    @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {
        int height = 20;

        List<Technology> techs = ((AppletData) mainApplet.getAppletData()).getUnlockedTech().get(getId());

        ArrayList<Integer> processed = new ArrayList();
        int techCount = 0;
        if (techs != null) {
            for (Technology tech : techs) {
                if (tech.getResearchType() == 1) {
                    continue;
                }
                if (!processed.contains(tech.getResearchType())) {
                    techCount++;
                    processed.add(tech.getResearchType());
                }
            }
        }
        processed = new ArrayList();
        gc.setColor(Color.WHITE);
        if (isMarked()) {

            gc.fillRect(this.getActCoord_x1() - 22, this.getActCoord_y1() - height / 2 - 2, width + 24 + techCount * 20, height + 4);

        }
        if (status == 0) {

            gc.setColor(Color.RED);
            if (this.requResDone == this.totalRequRes) {
                gc.setColor(Color.YELLOW);
            }
        } else if (status == 1) {

            gc.setColor(Color.getHSBColor(0.051f, 0.973f, 1.000f));
        } else if (status == 2) {

            gc.setColor(Color.YELLOW);
        } else if (status == 3) {
            gc.setColor(Color.GREEN);
        }

        if (getRequResDone() == getTotalRequRes()) {

            if (status == 1) {

                gc.setColor(Color.getHSBColor(0.051f, 0.703f, 1.00f));
            } else if (status == 3) {
                gc.setColor(Color.GREEN);
            } else {
                gc.setColor(Color.YELLOW);
            }

        } else if (getRequResDone() != getTotalRequRes()) {

            gc.setColor(Color.RED);

        } else {
            gc.setColor(Color.GREEN);
        }

        totalWidth = width + techCount;
        if (this.totalRequRes > 0) {
            int vx = ((((this.getActCoord_x1() - 10 / 2))) + width / 2) - 5;
           gc.fillOval(vx, (int) (this.getActCoord_y1() - 10 / 2) - 10, 10, 10);
        }
        //Main
        gc.fillRect(this.getActCoord_x1(), this.getActCoord_y1() - height / 2, width + techCount * 20, height);
        //Links
        gc.fillRect(this.getActCoord_x1() - 20, this.getActCoord_y1() - height / 2, 20, 20);
        int count = 0;
        if (techs != null) {
            for (Technology tech : techs) {
                if (tech.getResearchType() == 1) {
                    continue;
                }
                if (!processed.contains(tech.getResearchType())) {
                    String picName = DisplayManagement.getGraphicName(tech.getResearchType(), tech.getDescription());
                    Image img = DisplayManagement.getGraphic(picName);

                    gc.fillRect(this.getActCoord_x1() + width + count, this.getActCoord_y1() - height / 2, 20, 20);
                    gc.drawImage(img, this.getActCoord_x1() + width + count, this.getActCoord_y1() - height / 2, null);

                    processed.add(tech.getResearchType());
                    count += 20;
                }
            }
        }
        gc.setColor(Color.BLACK);
        //Punkt
        // gc.fillOval(this.getActCoord_x1() - 15, (this.getActCoord_y1() - height / 2) + 5, 10, 10);

        Image img = DisplayManagement.getGraphic("infoT.png");
        gc.drawImage(img, this.getActCoord_x1() - 20, (this.getActCoord_y1() - height / 2), null);

        gc.setColor(Color.BLACK);
        gc.drawString(this.getName(), this.getActCoord_x1(), this.getActCoord_y1() + 4);
        gc.setColor(Color.WHITE);

        if (((PainterData) this.mainApplet.mainFrame.getPainter().getPainterData()).isShowMarkers()) {
            if (!totalResPointsCalculated) {
                this.totalpoints = findTotalResPoints(this.getName());
                totalResPointsCalculated = true;
            }
            PainterData tmpPainterData = (PainterData) mainApplet.mainFrame.getPainter().getPainterData();
            int[][] maxima = tmpPainterData.getMaixma();
            int y = this.getY1() - (this.getY1() % 500);
            int ySkipped = 0;
            if (y > 0) {
                ySkipped = (y / 500) - 1;
            }
            if (maxima[ySkipped][1] < totalpoints[0]) {
                maxima[ySkipped][1] = totalpoints[0];
                maxima[ySkipped][0] = this.id;
            }
            if (maxima[ySkipped][2] < totalpoints[1]) {
                maxima[ySkipped][2] = totalpoints[1];
                maxima[ySkipped][3] = this.id;
            }
            tmpPainterData.setMaixma(maxima);
            NumberFormat formatter = NumberFormat.getInstance(new Locale("en_US"));
            gc.drawString("[" + id + "]" + formatter.format(totalpoints[0]) + "/" + formatter.format(totalpoints[1]), this.getActCoord_x1() + width + (20 * techCount), this.getActCoord_y1());
        }

    }

    public int[] findTotalResPoints(String resName) {
        Queue<String> queue = new LinkedList<>();
        queue.add(resName);
        PainterData painterData = (PainterData) mainApplet.mainFrame.getPainter().getPainterData();
        AppletData appletData = (AppletData) mainApplet.getAppletData();
        int[] totalResPoints = new int[2];
        while (queue.size() != 0) {
            String res = queue.poll();
            for (String tr : (appletData).getTechRelations()) {
                TechRelation techrl = (TechRelation) painterData.getInformationData().get(tr);
                Object_OneCoordinate oo = (Object_OneCoordinate) painterData.getInformationData().get((appletData).idToName.get(techrl.getSourceId()));

                if (oo != null) {
                    String ident = oo.getIdentifier();
                    if (ident.equals(res)) {
                        ResDesc rd = (ResDesc) oo;

                        String ident2 = painterData.getInformationData().get(((AppletData) appletData).idToName.get(techrl.getReqResearchId())).getIdentifier();

                        queue.add(ident2);
                        totalResPoints[0] += rd.getRp();
                        totalResPoints[1] += rd.getCrp();

                    }
                }
            }

        }

        return totalResPoints;
    }

    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > MAXIMG_SIZE) {
            return MAXIMG_SIZE;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    public int getPopPoints() {
        return popPoints;
    }

    public void setPopPoints(int popPoints) {
        this.popPoints = popPoints;
    }

    public int getScorePoints() {
        return scorePoints;
    }

    public void setScorePoints(int scorePoints) {
        this.scorePoints = scorePoints;
    }

    @Override
    public String getIdentifier() {
        return this.identifier;
    }

    @Override
    public JPanel getPanel() {

        return new ResearchInformationPanel(this, mainApplet);

    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the totalRequRes
     */
    public int getTotalRequRes() {
        return totalRequRes;
    }

    /**
     * @param totalRequRes the totalRequRes to set
     */
    public void setTotalRequRes(int totalRequRes) {
        this.totalRequRes = totalRequRes;
    }

    /**
     * @return the requResDone
     */
    public int getRequResDone() {
        return requResDone;
    }

    /**
     * @param requResDone the requResDone to set
     */
    public void setRequResDone(int requResDone) {
        this.requResDone = requResDone;
    }

    /**
     * @return the rp
     */
    public int getRp() {
        return rp;
    }

    /**
     * @param rp the rp to set
     */
    public void setRp(int rp) {
        this.rp = rp;
    }

    /**
     * @return the crp
     */
    public int getCrp() {
        return crp;
    }

    /**
     * @param crp the crp to set
     */
    public void setCrp(int crp) {
        this.crp = crp;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public void setX1Normalized(int i) {

      this.setX1(i - (int)(totalWidth / 2));
    }

}
