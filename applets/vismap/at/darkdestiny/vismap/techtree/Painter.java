package at.darkdestiny.vismap.techtree;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.interfaces.I_Object_TwoCoordinates;
import at.darkdestiny.vismap.logic.AppletConstants;
import java.util.ArrayList;
import at.darkdestiny.vismap.techtree.objects.ResDesc;
import at.darkdestiny.vismap.techtree.objects.TechRelation;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import javax.swing.JPanel;

/**
 * Hier wird das Universum auch ObjectData-Objekten gezeichnet und kann
 * entsprechend manipuliert werden
 *
 * @author martin
 */
public class Painter extends MainPainter {

    int lastMousePressedX = 0;
    int lastMousePressedY = 0;
    boolean initialized = false;
    private static final int TOOLTIP_HEIGHT = 10;
    private static final int TOOLTIP_WIDTH = 10;
    private ResDesc_ToolTip showToolTipForRes;
    /**
     * Damit der Compiler friedlich ist ;)
     */
    private static final long serialVersionUID = 4354204667927554366L;
    //Radius eines Pixels f�r den der Tooltip angezeigt werden soll
    /**
     * Minimale Gr��e des Ausschnittes
     */
    boolean drawRelations = true;
    boolean editMode = false;
    boolean connectingObjects = false;
    boolean initWidth = true;
    /**
     * Maximale Gr��e des Ausschnittes: (Werte reichen von -2000 bis 2000,
     * => MAX 4*so gro�er Ausschnitt)
     */

    private SelectedRect selectedRect = null;
    boolean mouseDragged = false;
    boolean mousePressed = true;

    private String tmpSearchWord = "";
    private int lastSearchId = -1;
    ResDesc sd = null;
    ResDesc conn1 = null;
    ResDesc conn2 = null;
    boolean ctrlPressed = false;

    private ArrayList<Object_OneCoordinate> tmpSearchResult;

    public Painter(AppletMain main) {

        super(main);
    }

    @Override
    public void paint(Graphics g) {

        if (initWidth) {

            for (Map.Entry<Integer, List<I_Object_OneCoordinate>> entry : painterData.getDrawObjects().entrySet()) {
                for (I_Object_OneCoordinate res : entry.getValue()) {
                    if (res instanceof ResDesc) {
                        ((ResDesc) res).updateWidth(g);
                    }
                }
            }
            initWidth = false;
        }
        super.prePaint(g);

        float[] res = new float[2];
        float[] delta = new float[2]; //Die L�nge von jeweils einem LJ (d.h. 1), jeweils in Richtung X und Y

        convertCoordinates(0, 0, res);
        convertCoordinates(1, 1, delta);
        delta[0] -= res[0];
        delta[1] -= res[1];

        if (dataChanged) {

            if (backBuffer == null) {
                backBuffer = createImage(getWidth(), getHeight());
            }
            Graphics gc = backBuffer.getGraphics();

            gc.setColor(Color.black);
            gc.fillRect(0, 0, this.getWidth(), this.getHeight());

            super.paint(g);

            dataChanged = false;

            if (((PainterData) painterData).isShowMarkers()) {
                int[][] maxima = ((PainterData) painterData).getMaixma();

                for (int i = 0; i < maxima.length; i++) {
                    if (maxima[i][1] <= 0 && maxima[i][2] <= 0) {
                        continue;
                    }
                    gc.setColor(Color.RED);
                    gc.setFont(new Font("Tahoma", Font.BOLD, 9));
                    NumberFormat formatter = NumberFormat.getInstance(new Locale("en_US"));

                    int[] coord = new int[2];
                    coord[0] = 0;
                    coord[1] = i * 500;
                    convertCoordinates(0, i * 500, coord);

                    int y = coord[1];

                    String tip = String.valueOf(formatter.format(maxima[i][1])) + " / " + String.valueOf(formatter.format(maxima[i][2]) + "[" + maxima[i][0] + "/" + maxima[i][3] + "]");
                    gc.drawString(tip, 60, y - 4);
                }
            }
        }

        g.drawImage(backBuffer, 0, 0, getWidth(), getHeight(), null);

        if (getShowToolTipForRes() != null) {
            getShowToolTipForRes().paint(this, g);
        }

        globalRatio = (float) getWidth() / (float) getHeight();

        super.postPaint(g);

        if (mouse.active) {
            mouse.paint(this, g);
        }

        if (!main.isInitialized() && !main.isReloaded()) {

            switchToCoordinates(at.darkdestiny.vismap.logic.AppletConstants.UNIVERSE_SIZE_X / 2, at.darkdestiny.vismap.logic.AppletConstants.UNIVERSE_SIZE_Y / 2);
            main.setInitialized(true);
            main.setReloaded(false);
            dataChanged = true;
            repaint();

        }
        ((PainterData) painterData).resetMaxima();
    }

    public void switchToResearchName(String name, ResDesc toMark) {

        ResDesc tmp = null;
        boolean foundLastId = false;
        boolean sameWord = false;

        if (tmpSearchWord.equals(name)) {
            sameWord = true;
        } else {
            tmpSearchResult = new ArrayList<Object_OneCoordinate>();
        }

        if (!sameWord) {
            ((PainterData) painterData).unmark();
            for (I_Object_OneCoordinate object : painterData.getDrawObjects().get(AppletConstants.DRAW_LEVEL_RESEARCH)) {

                if (object instanceof ResDesc) {
                    tmp = (ResDesc) object;

                    if (toMark != null) {
                        if (object.getId() == toMark.getId()) {
                            tmp.setMarked(true);
                        }
                    }
                    if (tmp.getName().equalsIgnoreCase(name)) {
                        tmp.setMarked(true);
                        tmpSearchResult.add(tmp);
                    }
                }
            }
            forceRefresh();
        }
        if (tmpSearchResult.size() >= 1 && !sameWord) {
            Object_OneCoordinate sd = tmpSearchResult.get(0);
            ResDesc tmp2 = (ResDesc) sd;
            switchToCoordinates(Math.round(tmp2.getX1()), Math.round(tmp2.getY1()));
            lastSearchId = tmp2.getId();
        } else if (tmpSearchResult.size() == 1 && sameWord) {
            Object_OneCoordinate sd = tmpSearchResult.get(0);
            ResDesc tmp2 = (ResDesc) sd;
            switchToCoordinates(Math.round(tmp2.getX1()), Math.round(tmp2.getY1()));
            lastSearchId = tmp2.getId();
        } else if (tmpSearchResult.size() > 1 && sameWord) {

            for (int i = 0; i < tmpSearchResult.size(); i++) {

                Object_OneCoordinate tmpSystemResults = tmpSearchResult.get(i);

                if (tmpSystemResults instanceof ResDesc) {
                    ResDesc tmp2 = (ResDesc) tmpSystemResults;

                    if (foundLastId == false) {
                        if (tmp2.getId() == this.lastSearchId) {
                            foundLastId = true;

                        } else {
                            switchToCoordinates(Math.round(tmp2.getX1()), Math.round(tmp2.getY1()));
                            lastSearchId = tmp2.getId();

                            break;
                        }
                    }

                }
            }

        }
        tmpSearchWord = name;
    }

    public boolean findTreeFor(String resName, boolean hideOthers) {
        Queue<String> queue = new LinkedList<>();
        queue.add(resName);
        ArrayList<String> toActivate = new ArrayList<>();
        boolean found = false;
        while (queue.size() != 0) {
            String res = queue.poll();
            for (String tr : ((AppletData) appletData).getTechRelations()) {
                TechRelation techrl = (TechRelation) painterData.getInformationData().get(tr);
                Object_OneCoordinate oo = (Object_OneCoordinate) painterData.getInformationData().get(((AppletData) appletData).idToName.get(techrl.getSourceId()));

                if (oo != null) {
                    String ident = oo.getIdentifier();
                    if (ident.equals(res)) {
                        if (!toActivate.contains(res)) {
                            toActivate.add(res);
                        }
                        if (!toActivate.contains(techrl.getIdentifier())) {
                            toActivate.add(techrl.getIdentifier());
                        }
                        String ident2 = painterData.getInformationData().get(((AppletData) appletData).idToName.get(techrl.getReqResearchId())).getIdentifier();

                        queue.add(ident2);
                        found = true;

                    }
                }
            }
            if (!found) {
                if (!toActivate.contains(res)) {
                    toActivate.add(res);
                }
            }
        }

        for (Map.Entry<Integer, List<I_Object_OneCoordinate>> o : painterData.getDrawObjects().entrySet()) {
            for (I_Object_OneCoordinate obj : o.getValue()) {
                if (toActivate.contains(obj.getIdentifier())) {
                    obj.setMarked(true);
                } else if (hideOthers) {
                    obj.setHide(true);
                } else if (!hideOthers) {
                    obj.setHide(false);
                }
            }
        }

        forceRefresh();
        return found;
    }

    @Override
    public void convertCoordinates(Object_OneCoordinate sd, int[] res) {

        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;

        float x = (sd.getX1() - visible_startx) / width_fact;
        float y = (sd.getY1() - visible_starty) / height_fact;

        res[0] = Math.round(x);
        res[1] = Math.round(y);
        sd.setActCoord_x1(res[0]);
        sd.setActCoord_y1(res[1]);
    }

    public void convertCoordinates(int x_old, int y_old, int[] res) {

        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;

        float x = (x_old - visible_startx) / width_fact;
        float y = (y_old - visible_starty) / height_fact;

        res[0] = Math.round(x);
        res[1] = Math.round(y);
    }

    public void convertCoordinatesBack(float x1, float y1, int[] res) {
        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;

        float x = x1 * width_fact;
        float y = y1 * height_fact;
        x += visible_startx;
        y += visible_starty;

        res[0] = Math.round(x);
        res[1] = Math.round(y);
    }

    public ResDesc findResearch(int x, int y) {
        float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 4 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 4);

        ResDesc res = null;

        for (int i = 0; i < painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_RESEARCH).size(); i++) {
            I_Object_OneCoordinate rd = painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_RESEARCH).get(i);
            float dist = ((x - (rd.getActCoord_x1() - 10)) * (x - (rd.getActCoord_x1() - 10)) + (y - rd.getActCoord_y1()) * (y - rd.getActCoord_y1()));
            if ((rd instanceof ResDesc) && (dist < maxdist)) {
                ResDesc sys = (ResDesc) rd;
                if (sys.isHide()) {
                    return null;
                }
                res = sys;

            }
        }
        return res;
    }

    @Override
    public void convertCoordinates(I_Object_TwoCoordinates object_twoCoordinates, int[] res1, int[] res2) {
        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;

        float startX = (object_twoCoordinates.getX1() - visible_startx) / width_fact;
        float startY = (object_twoCoordinates.getY1() - visible_starty) / height_fact;
        float endX = (object_twoCoordinates.getX2() - visible_startx) / width_fact;
        float endY = (object_twoCoordinates.getY2() - visible_starty) / height_fact;

        res1[0] = Math.round(startX);
        res1[1] = Math.round(startY);
        res2[0] = Math.round(endX);
        res2[1] = Math.round(endY);
        object_twoCoordinates.setActCoord_x1(res1[0]);
        object_twoCoordinates.setActCoord_y1(res1[1]);
        object_twoCoordinates.setActCoord_x2(res2[0]);
        object_twoCoordinates.setActCoord_y2(res2[1]);
    }

    /**
     * @return the showToolTipForRes
     */
    public ResDesc_ToolTip getShowToolTipForRes() {
        return showToolTipForRes;
    }

    /**
     * @param showToolTipForRes the showToolTipForRes to set
     */
    public void setShowToolTipForRes(ResDesc_ToolTip showToolTipForRes) {
        this.showToolTipForRes = showToolTipForRes;
    }

}
