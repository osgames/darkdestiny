package at.darkdestiny.vismap.techtree;

import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.gui.LoaderScreen;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.logic.AbstractDataLoader;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.ArrayList;

import at.darkdestiny.vismap.techtree.objects.ResDesc;
import at.darkdestiny.vismap.techtree.objects.TechRelation;
import at.darkdestiny.vismap.xml.XMLMemo;
import java.net.HttpURLConnection;
import java.net.URLConnection;

/**
 * Das DataLoader dient dazu den XML Stream auszulesen für - Systeme u.
 * Planeten - Flotten - Interne Handelsflotten - Hyperraumscanner
 *
 *
 * @author martin
 *
 */
public class DataLoader extends AbstractDataLoader {

    private static AppletMain main;
    private static boolean joinMode = false;
    private static int marked = 0;
    private static String basePath = "";
    private static LoaderScreen loader;
    private static int timer = 2;
    int height;
    int width;
    public static final boolean showViewSystemCircles = false;

    static AppletData appletData;
    static PainterData painterData;

    public DataLoader(AppletMain main, IAppletData appletData, IPainterData painterData, LoaderScreen l) {
        DataLoader.appletData = (AppletData) appletData;
        DataLoader.painterData = (PainterData) painterData;
        loader = l;

        DataLoader.main = main;
    }

    public void init() {

        loadXMLData(main);
    }

    public void reload(AppletMain main) {

        boolean showRaster = main.mainFrame.getPainter().getPainterData().isShowRaster();
        boolean rasterInverted = main.mainFrame.getPainter().getPainterData().isRasterInverted();

        painterData = new PainterData();
        loadXMLData(main);
        //p.fitView(height, width);
        for (int i = timer; i >= 0; i--) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            loader.addLine("Rebuilding UI in : " + i + " seconds");
        }
        painterData.setShowRaster(showRaster);
        painterData.setRasterInverted(rasterInverted);

        main.mainFrame.getPainter().setPainterData(painterData);

    }

    private static XMLMemo getXMLfromFile(InputStream is, AppletMain main) {
        String source = main.getParameter("source");
        String join = main.getParameter("join");

        if (join != null) {
            //dbg//dbg System.out.println("Join Mode active");
            joinMode = true;
        } else {
            joinMode = false;
        }

        if (source == null) {
            // joinMode = true;
            source = AppletConstants.FILE_PATH + "techtree.xml";
            //dbg//dbg System.out.println("SOURCE=" + source);

            loader.addLine("Getting XML file from: " + source);
            try {
                is = new FileInputStream(source);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                //dbg//dbg System.out.println("e2=" + e);
                return null;
            }
        } else {
            URL url;

            // Get base path
            if (joinMode) {
                basePath = source.replace("createJoinMapInfo.jsp", "");
            } else {
                basePath = source.replace("createStarMapInfo.jsp", "");

            }
            loader.addLine("Getting XML file from: " + basePath);
            String sessionId = main.getParameter("sessionId");
            main.getAppletData().setSessionId(sessionId);
            loader.addLine("SessionId: " + sessionId);
            try {

                long t1 = System.currentTimeMillis();

                url = new URL(basePath);
                URLConnection uc = url.openConnection();
                HttpURLConnection conn = (HttpURLConnection) uc;
                conn.setRequestProperty("Cookie", "JSESSIONID=" + sessionId);

                loader.addLine("Loading Data - Requesting StarMapInfo - Done in " + (System.currentTimeMillis() - t1) + " ms");

                is = conn.getInputStream();
                loader.addLine("Loading Data - Parsing Inputstream - Done in " + (System.currentTimeMillis() - t1) + " ms");

            } catch (IOException e) {
                e.printStackTrace();
                //dbg//dbg System.out.println("e4=" + e);
                return null;
            }
        }

        //dbg//dbg System.out.println("mem");
        XMLMemo in;
        try {
            long t1 = System.currentTimeMillis();
            //     loader.addLine("Loading Data - Parse to XML - Start");
            in = XMLMemo.parse(is);
            loader.addLine("Loading Data - Parse to XML - Done in " + (System.currentTimeMillis() - t1) + " ms");

            //dbg//dbg System.out.println("is");
        } catch (Exception e) {
            e.printStackTrace();
            //dbg//dbg System.out.println("e5=" + e);
            loader.addLine("Could not read XML - Mostlikely your session has expired. Relogin and retry pls");
            return null;
        }
        try {

            is.close();
            //dbg//dbg System.out.println("closed Stream");
        } catch (Exception e) {
            e.printStackTrace();
            //dbg//dbg System.out.println("Cant close Stream : " + e);
        }
        return in;
    }

    //Load Data from XML
    private static void loadXMLData(AppletMain main) {
        InputStream is = null;
        XMLMemo in = getXMLfromFile(is, main);

        //Reading all Systems
        XMLMemo constants = in.getElements("Constants").get(0);

        parseConstants(main, constants);

        XMLMemo unlockedTechnologies = in.getElements("UnlockedTechnologies").get(0);

        for (Iterator i = unlockedTechnologies.getElements("UnlockedTechnology").iterator(); i.hasNext();) {
            XMLMemo technology = (XMLMemo) i.next();
            Technology tech = parseTechnology(technology);
            if (tech.getResearchType() != 1) {
                List<Technology> ts = appletData.getUnlockedTech().get(tech.getId());
                if (ts == null) {
                    ts = new ArrayList<Technology>();
                }
                ts.add(tech);
                appletData.getUnlockedTech().put(tech.getId(), ts);
            }
        }
        //Reading all Systems
        XMLMemo allSystems = in.getElements("Researches").get(0);

        int maxX = 0;
        int maxY = 0;
        for (Iterator i = allSystems.getElements("Research").iterator(); i.hasNext();) {
            XMLMemo res = (XMLMemo) i.next();
            ResDesc rd = parseResearch(res);
            maxX = Math.max(maxX, rd.getX1());
            maxY = Math.max(maxY, rd.getY1());
            painterData.getInformationData().put(rd.getIdentifier(), rd);
            if (rd.getId() != 999) {
                painterData.addDisplayObject(AppletConstants.DRAW_LEVEL_RESEARCH, rd);
                painterData.addDrawObject(AppletConstants.DRAW_LEVEL_RESEARCH, rd);
            }
            appletData.idToName.put(rd.getId(), rd.getIdentifier());
            appletData.nameToId.put(rd.getIdentifier(), rd.getId());
        }
        AppletConstants.UNIVERSE_SIZE_X = maxX;
        AppletConstants.UNIVERSE_SIZE_Y = maxY;
        //Reading all Systems
        XMLMemo techRelations = in.getElements("TechRelations").get(0);

        for (Iterator i = techRelations.getElements("TechRelation").iterator(); i.hasNext();) {
            XMLMemo techRelation = (XMLMemo) i.next();
            TechRelation tr = parseTechRelation(techRelation);
            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_TECHRELATION, tr);
            painterData.getInformationData().put(tr.getIdentifier(), tr);
        }
    }

    private static void parseConstants(AppletMain main, XMLMemo constants) {
        XMLMemo Language = constants.getElements("Language").get(0);
        String locale = Language.getAttribute("Locale");
        String language = locale.substring(0, locale.indexOf("_"));
        String country = locale.substring(locale.indexOf("_") + 1, locale.length());
        appletData.setLocale(new Locale(language, country));
    }

    private static Technology parseTechnology(XMLMemo technology) {

        int id = Integer.parseInt(technology.getAttribute("resId"));
        int resType = Integer.parseInt(technology.getAttribute("resType"));
        int sourceId = Integer.parseInt(technology.getAttribute("sourceId"));

        String name = technology.getAttribute("name");
        String description = technology.getAttribute("description");

        return new Technology(id, resType, name, description, sourceId);
    }

    public PainterData getPainterData() {
        return painterData;
    }

    @Override
    public AppletData getAppletData() {
        return appletData;
    }

    private static ResDesc parseResearch(XMLMemo res) {
        String name = res.getAttribute("name");
        int id = Integer.parseInt(res.getAttribute("id").trim());
        int x = Integer.parseInt(res.getAttribute("x").trim());
        int y = Integer.parseInt(res.getAttribute("y").trim());

        String description = res.getAttribute("description");
        int rp = Integer.parseInt(res.getAttribute("rp"));
        int crp = Integer.parseInt(res.getAttribute("crp"));
        int status = 0;
        if (res.getAttribute("status") != null) {
            status = Integer.parseInt(res.getAttribute("status"));
        }
        Image img = null;
        return new ResDesc(name, id, x, y, status, img, main, rp, crp, description);
    }

    private static TechRelation parseTechRelation(XMLMemo res) {
        int reqResearchId = Integer.parseInt(res.getAttribute("reqResearchId"));
        int sourceId = Integer.parseInt(res.getAttribute("sourceId"));
        return new TechRelation(reqResearchId, sourceId, painterData.getInformationData(), main);
    }
}
