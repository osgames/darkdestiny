/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.techtree.panels;

import at.darkdestiny.vismap.drawable.CentredBackgroundBorder;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.logic.ML;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Locale;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;

/**
 *
 * @author Bullet
 */
public class Glossar extends JPanel implements ActionListener{

    private JButton close;
    private AppletMain main;

    public Glossar(AppletMain main) {
        this.main = main;

        SpringLayout layout = new SpringLayout();
        this.setLayout(layout);
        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic("helpBackground.png"), "helpBackground.png");
        final Border background = new CentredBackgroundBorder(bi);
        Locale l = main.getLocale();
        close = new JButton(ML.getMLStr("but_close", l));
        close.addActionListener(this);

        JLabel descResearch = new JLabel(ML.getMLStr("lbl_researches", l));
        JLabel descResearched = new JLabel(ML.getMLStr("lbl_alreadyresearched", l));
        JLabel descResearchAble = new JLabel(ML.getMLStr("lbl_researchable", l));
        JLabel descInResearch = new JLabel(ML.getMLStr("lbl_inresearch", l));
        JLabel descNotResearchable = new JLabel(ML.getMLStr("lbl_notresearchable", l));
        JLabel icons = new JLabel(ML.getMLStr("lbl_icons", l));
        JLabel technologies = new JLabel(ML.getMLStr("lbl_technologies", l));
        JLabel iconSearch = new JLabel(ML.getMLStr("lbl_searchresearch", l));
        JLabel iconSearchTree = new JLabel(ML.getMLStr("lbl_searchpreresearches", l));
        JLabel iconSearchTreeHidden = new JLabel(ML.getMLStr("lbl_searchpreresearches_hideothers1", l));
        JLabel iconSearchTreeHidden2 = new JLabel(ML.getMLStr("lbl_searchpreresearches_hideothers2", l));
        JLabel listAll = new JLabel(ML.getMLStr("lbl_showresearchlist", l));
        JLabel cancelSearch = new JLabel(ML.getMLStr("lbl_resetsearch", l));
        JLabel help = new JLabel(ML.getMLStr("lbl_help", l));
        JLabel armor = new JLabel(ML.getMLStr("lbl_armor", l));
        JLabel weapon = new JLabel(ML.getMLStr("lbl_weapon", l));
        JLabel construction = new JLabel(ML.getMLStr("lbl_construction", l));
        JLabel shield = new JLabel(ML.getMLStr("lbl_shield", l));
        JLabel module = new JLabel(ML.getMLStr("lbl_module", l));
        JLabel groundtroop = new JLabel(ML.getMLStr("lbl_groundtroop", l));
        JLabel chassis = new JLabel(ML.getMLStr("lbl_chassis", l));

        armor.setFont(new Font("Tahoma", 1, 9));
        weapon.setFont(new Font("Tahoma", 1, 9));
        construction.setFont(new Font("Tahoma", 1, 9));
        shield.setFont(new Font("Tahoma", 1, 9));
        module.setFont(new Font("Tahoma", 1, 9));
        groundtroop.setFont(new Font("Tahoma", 1, 9));
        chassis.setFont(new Font("Tahoma", 1, 9));
        iconSearch.setFont(new Font("Tahoma", 1, 9));
        iconSearchTree.setFont(new Font("Tahoma", 1, 9));
        iconSearchTreeHidden.setFont(new Font("Tahoma", 1, 9));
        iconSearchTreeHidden2.setFont(new Font("Tahoma", 1, 9));
        listAll.setFont(new Font("Tahoma", 1, 9));
        cancelSearch.setFont(new Font("Tahoma", 1, 9));
        help.setFont(new Font("Tahoma", 1, 9));

        armor.setForeground(Color.WHITE);
        construction.setForeground(Color.WHITE);
        shield.setForeground(Color.WHITE);
        module.setForeground(Color.WHITE);
        groundtroop.setForeground(Color.WHITE);
        chassis.setForeground(Color.WHITE);
        weapon.setForeground(Color.WHITE);
        descResearch.setForeground(Color.WHITE);
        iconSearchTreeHidden2.setForeground(Color.WHITE);
        descResearched.setForeground(Color.WHITE);
        descResearchAble.setForeground(Color.WHITE);
        descInResearch.setForeground(Color.WHITE);
        descNotResearchable.setForeground(Color.WHITE);
        technologies.setForeground(Color.WHITE);
        icons.setForeground(Color.WHITE);
        iconSearch.setForeground(Color.WHITE);
        iconSearchTree.setForeground(Color.WHITE);
        iconSearchTreeHidden.setForeground(Color.WHITE);
        listAll.setForeground(Color.WHITE);
        help.setForeground(Color.WHITE);
        cancelSearch.setForeground(Color.WHITE);

        this.setBorder(background);
        this.setPreferredSize(new Dimension(341, 388));
        this.add(descResearch);
        this.add(iconSearchTreeHidden2);
        this.add(descResearched);
        this.add(descResearchAble);
        this.add(descInResearch);
        this.add(descNotResearchable);

        this.add(technologies);
        this.add(icons);
        this.add(iconSearch);
        this.add(iconSearchTree);
        this.add(iconSearchTreeHidden);
        this.add(listAll);
        this.add(cancelSearch);
        this.add(armor);
        this.add(weapon);
        this.add(construction);
        this.add(shield);
        this.add(module);
        this.add(groundtroop);
        this.add(chassis);

        this.add(close);
        this.add(help);


        layout.putConstraint(SpringLayout.WEST, descResearch, 15, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, descResearch, 20, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, descResearched, 80, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, descResearched, 11, SpringLayout.SOUTH, descResearch);

        layout.putConstraint(SpringLayout.WEST, descResearchAble, 0, SpringLayout.WEST, descResearched);
        layout.putConstraint(SpringLayout.NORTH, descResearchAble, 6, SpringLayout.SOUTH, descResearched);

        layout.putConstraint(SpringLayout.WEST, descInResearch, 0, SpringLayout.WEST, descResearchAble);
        layout.putConstraint(SpringLayout.NORTH, descInResearch, 7, SpringLayout.SOUTH, descResearchAble);

        layout.putConstraint(SpringLayout.WEST, descNotResearchable, 0, SpringLayout.WEST, descInResearch);
        layout.putConstraint(SpringLayout.NORTH, descNotResearchable, 7, SpringLayout.SOUTH, descInResearch);


        layout.putConstraint(SpringLayout.WEST, icons, 15, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, icons, 17, SpringLayout.SOUTH, descNotResearchable);

        layout.putConstraint(SpringLayout.WEST, iconSearch, 45, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, iconSearch, 10, SpringLayout.SOUTH, icons);

        layout.putConstraint(SpringLayout.WEST, iconSearchTree, 0, SpringLayout.WEST, iconSearch);
        layout.putConstraint(SpringLayout.NORTH, iconSearchTree, 17, SpringLayout.SOUTH, iconSearch);

        layout.putConstraint(SpringLayout.WEST, iconSearchTreeHidden, 0, SpringLayout.WEST, iconSearchTree);
        layout.putConstraint(SpringLayout.NORTH, iconSearchTreeHidden, 15, SpringLayout.SOUTH, iconSearchTree);

        layout.putConstraint(SpringLayout.WEST, iconSearchTreeHidden2, 0, SpringLayout.WEST, iconSearchTreeHidden);
        layout.putConstraint(SpringLayout.NORTH, iconSearchTreeHidden2, 2, SpringLayout.SOUTH, iconSearchTreeHidden);

        layout.putConstraint(SpringLayout.WEST, cancelSearch, 0, SpringLayout.WEST, iconSearchTreeHidden);
        layout.putConstraint(SpringLayout.NORTH,cancelSearch, 26, SpringLayout.SOUTH, iconSearchTreeHidden);

        layout.putConstraint(SpringLayout.WEST, listAll, 0, SpringLayout.WEST, cancelSearch);
        layout.putConstraint(SpringLayout.NORTH,listAll, 21, SpringLayout.SOUTH, cancelSearch);

        layout.putConstraint(SpringLayout.WEST, help, 0, SpringLayout.WEST, listAll);
        layout.putConstraint(SpringLayout.NORTH,help, 21, SpringLayout.SOUTH, listAll);


        layout.putConstraint(SpringLayout.EAST, close, -15, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.SOUTH,close, -15, SpringLayout.SOUTH, this);

                layout.putConstraint(SpringLayout.WEST, technologies, 210, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, technologies, 17, SpringLayout.SOUTH, descNotResearchable);

        layout.putConstraint(SpringLayout.WEST, armor, 233, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, armor, 7, SpringLayout.SOUTH, technologies);

        layout.putConstraint(SpringLayout.WEST, chassis, 0, SpringLayout.WEST, armor);
        layout.putConstraint(SpringLayout.NORTH, chassis, 15, SpringLayout.SOUTH, armor);

        layout.putConstraint(SpringLayout.WEST, construction, 0, SpringLayout.WEST, armor);
        layout.putConstraint(SpringLayout.NORTH, construction, 16, SpringLayout.SOUTH, chassis);

        layout.putConstraint(SpringLayout.WEST, groundtroop, 0, SpringLayout.WEST, armor);
        layout.putConstraint(SpringLayout.NORTH, groundtroop, 15, SpringLayout.SOUTH, construction);

        layout.putConstraint(SpringLayout.WEST, module, 0, SpringLayout.WEST, armor);
        layout.putConstraint(SpringLayout.NORTH, module, 16, SpringLayout.SOUTH, groundtroop);

        layout.putConstraint(SpringLayout.WEST, shield, 0, SpringLayout.WEST, armor);
        layout.putConstraint(SpringLayout.NORTH, shield, 17, SpringLayout.SOUTH, module);

        layout.putConstraint(SpringLayout.WEST, weapon, 0, SpringLayout.WEST, armor);
        layout.putConstraint(SpringLayout.NORTH, weapon, 14, SpringLayout.SOUTH, shield);



    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == close){

            System.out.println("wanna close");
            main.setShowHelp(false);
        }
    }
}
