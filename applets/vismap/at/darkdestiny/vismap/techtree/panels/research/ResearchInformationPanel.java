/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.techtree.panels.research;

import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.I_InformationPanel;
import at.darkdestiny.vismap.logic.ML;
import at.darkdestiny.vismap.techtree.AppletData;
import at.darkdestiny.vismap.techtree.Technology;
import java.awt.Color;
import java.awt.Font;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.techtree.objects.ResDesc;
import java.awt.Dimension;
import java.util.List;

/**
 *
 * @author Horst
 */
public class ResearchInformationPanel extends JPanel implements I_InformationPanel {

    private SpringLayout layout;
    private String identifier;
    
    public ResearchInformationPanel(ResDesc resDesc, AppletMain mainApplet) {
        
        this.identifier = resDesc.getIdentifier();
        this.setBackground(Color.BLACK);


        this.layout = new SpringLayout();
        this.setLayout(layout);
        Locale l = mainApplet.getAppletData().getLocale();
        NumberFormat nf = NumberFormat.getInstance();
        JLabel researchId = new JLabel(ML.getMLStr("lbl_research", l) + ":");
        JLabel neededPoints = new JLabel(ML.getMLStr("lbl_neededrp", l) + ": ");
        JLabel points = new JLabel(ML.getMLStr("lbl_abbr_researchpoints", l) + ": " + nf.format(resDesc.getRp()) + " | " + ML.getMLStr("lbl_abbr_computerresearchpoints", l) + " :" + nf.format(resDesc.getCrp()));
        JLabel researchName = new JLabel(resDesc.getName());
        JLabel unlockedTech = new JLabel(ML.getMLStr("lbl_unlockedtech", l));
        JTextArea description = new JTextArea();

        description = new JTextArea(20, 20);
        description.setLineWrap(true);
        description.setWrapStyleWord(true);
        description.setText(resDesc.getDescription().replace("<BR>", ""));

        description.setWrapStyleWord(true);
        description.setBackground(Color.BLACK);
        description.setForeground(Color.WHITE);
        researchId.setForeground(Color.WHITE);
        researchName.setForeground(Color.WHITE);
        points.setForeground(Color.WHITE);
        neededPoints.setForeground(Color.WHITE);
        unlockedTech.setForeground(Color.WHITE);
        researchId.setFont(new Font("Tahoma", 1, 12));
        researchName.setFont(new Font("Tahoma", 1, 9));
        description.setFont(new Font("Tahoma", 1, 9));
        points.setFont(new Font("Tahoma", 1, 9));
        unlockedTech.setFont(new Font("Tahoma", 1, 9));
        neededPoints.setFont(new Font("Tahoma", 1, 12));
        this.add(researchName);
        this.add(unlockedTech);

        unlockedTech.setVisible(false);
        this.add(researchId);
        this.add(description);
        this.add(neededPoints);
        this.add(points);

        layout.putConstraint(SpringLayout.WEST, researchId, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, researchId, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, researchName, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, researchName, 2, SpringLayout.SOUTH, researchId);


        layout.putConstraint(SpringLayout.WEST, neededPoints, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, neededPoints, 2, SpringLayout.SOUTH, researchName);


        layout.putConstraint(SpringLayout.WEST, points, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, points, 2, SpringLayout.SOUTH, neededPoints);


        layout.putConstraint(SpringLayout.WEST, unlockedTech, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, unlockedTech, 15, SpringLayout.SOUTH, points);
        TechnologyListEntry tletmp = null;
        boolean first = true;
        List<Technology> techs = ((AppletData)mainApplet.getAppletData()).getUnlockedTech().get(resDesc.getId());
        if (techs != null) {

            unlockedTech.setVisible(true);
        }
        if (techs != null) {
            for (Technology t : techs) {
                if (t.getResearchType() == 1) {
                    continue;
                }
                TechnologyListEntry tle = new TechnologyListEntry(t);
                this.add(tle);
                if (first) {
                    layout.putConstraint(SpringLayout.WEST, tle, 2, SpringLayout.WEST, this);
                    layout.putConstraint(SpringLayout.NORTH, tle, 2, SpringLayout.SOUTH, unlockedTech);
                    first = false;
                } else {

                    layout.putConstraint(SpringLayout.WEST, tle, 2, SpringLayout.WEST, this);
                    layout.putConstraint(SpringLayout.NORTH, tle, 2, SpringLayout.SOUTH, tletmp);
                }
                tletmp = tle;

            }
        }
        if (tletmp == null) {

            layout.putConstraint(SpringLayout.WEST, description, 2, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.NORTH, description, 7, SpringLayout.SOUTH, points);

        } else {
            layout.putConstraint(SpringLayout.WEST, description, 2, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.NORTH, description, 7, SpringLayout.SOUTH, tletmp);


        }
        this.setPreferredSize(new Dimension(182, mainApplet.mainFrame.getPainter().getPainterData().getHeight() - 35));

        this.setVisible(true);

    }

    @Override
    public String getIdentifier() {
        return this.identifier;
    }
}
