/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.techtree.panels.research;

import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.techtree.Painter;
import at.darkdestiny.vismap.techtree.Techtree;
import at.darkdestiny.vismap.techtree.buttons.ToolBox;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import at.darkdestiny.vismap.techtree.objects.ResDesc;

/**
 *
 * @author Bullet
 */
public class ResearchListEntry extends JPanel implements ActionListener {

    private SpringLayout layout;
    private ResDesc rd;
    private AppletMain main;

    JButton research;

    public ResearchListEntry(AppletMain main, ResDesc rd, ResearchListPanel rlp) {

        this.setBackground(Color.BLACK);
        layout = new SpringLayout();
        this.setLayout(layout);
        this.rd = rd;
        this.main = main;
        research = new JButton(rd.getName());
        research.setForeground(Color.WHITE);
        research.setBackground(Color.BLACK);
        research.setHorizontalAlignment(SwingConstants.LEFT);
        research.setMargin(new Insets(-1, -1, -1, -1));
        research.addActionListener(this);

        research.setFont(new Font("Tahoma", 1, 9));
        this.add(research);

        layout.putConstraint(SpringLayout.WEST, research, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, research, 0, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, research, 0, SpringLayout.NORTH, this);

        this.setPreferredSize(new Dimension(182, (int) research.getPreferredSize().getHeight()));
        this.setVisible(true);
        this.validate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == research) {
            ((Painter) main.mainFrame.getPainter()).switchToResearchName(rd.getName(), rd);
            Techtree techtree = (Techtree) main.getPlugin();
            ToolBox toolBox = (ToolBox) techtree.getToolBox();
            toolBox.getSearchTextField().setText(rd.getName());

        }

    }
}
