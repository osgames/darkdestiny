/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.techtree.panels.research;

import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.I_InformationPanel;
import at.darkdestiny.vismap.techtree.AppletData;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.techtree.objects.ResDesc;

/**
 *
 * @author Horst
 */
public class ResearchListPanel extends JPanel implements I_InformationPanel {

    private SpringLayout layout;
    private String identifier;

    public ResearchListPanel(AppletMain mainApplet) {
        this.identifier = "researchlist";
        this.setBackground(Color.YELLOW);

        this.layout = new SpringLayout();
        this.setLayout(layout);
        boolean isFirst = true;
        ResearchListEntry rletmp = null;
        for (Map.Entry<String, ResDesc> entry : ((AppletData)mainApplet.getAppletData()).getResearches().entrySet()) {
            ResearchListEntry rle = new ResearchListEntry(mainApplet, entry.getValue(), this);
            this.add(rle);

            if (isFirst) {


                layout.putConstraint(SpringLayout.WEST, rle, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, rle, 0, SpringLayout.NORTH, this);
                isFirst = false;
            } else {

                layout.putConstraint(SpringLayout.WEST, rle, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, rle, 0, SpringLayout.SOUTH, rletmp);
            }
            rletmp = rle;
        }
        this.setPreferredSize(new Dimension(182,
               ((AppletData)mainApplet.getAppletData()).getResearches().size()
                * (int) rletmp.getPreferredSize().getHeight()));

        this.setVisible(true);

    }

    @Override
    public String getIdentifier() {
        return this.identifier;
    }
}
