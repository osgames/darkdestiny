/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.techtree.buttons;

import at.darkdestiny.vismap.buttons.BlueToggleButton;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.techtree.AppletData;
import at.darkdestiny.vismap.techtree.Painter;
import at.darkdestiny.vismap.techtree.PainterData;
import at.darkdestiny.vismap.techtree.Technology;
import at.darkdestiny.vismap.techtree.objects.ResDesc;
import at.darkdestiny.vismap.techtree.objects.ResearchList;
import at.darkdestiny.vismap.techtree.objects.TechRelation;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.ToolTipManager;

/**
 *
 * @author Admin
 */
public class ToolBox extends JPanel implements ActionListener {

    private Painter painter;
    SpringLayout layout = new SpringLayout();
    List<JComponent> buttons = new ArrayList();
    private AppletMain appletMain;

    private JLabel searchLabel;
    private JTextField searchTextField;
    private JButton searchButton;
    private JButton rootButton;
    private JButton rootButtonHidden;
    private JButton unmark;
    private JButton list;
    private JButton help;
    private JButton printSQL;
    private JButton showMarkers;

    public ToolBox(IPainter painter, AppletMain appletMain) {
        this.setLayout(layout);
        ToolTipManager ttm = ToolTipManager.sharedInstance();
        ttm.setInitialDelay(100);
        this.appletMain = appletMain;
        this.painter = (Painter) painter;

        buildPanel();
    }

    private void buildPanel() {

        initializeComponents();
        addComponents();
        orderComponents();

        if (buttons.size() > 0) {

            for (JComponent button : buttons) {
                this.add(button);
            }
        }

        this.setPreferredSize(new Dimension(760, 20));
        this.setOpaque(true);
        this.setVisible(true);
        this.setBackground(Color.BLACK);
    }

    public IPainter getPainter() {
        return painter;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String name = "";
        ResDesc rd = null;
        name = "";
        try {
            name = getSearchTextField().getText();
            for (Map.Entry<String, ResDesc> entry : ((AppletData) appletMain.getAppletData()).getResearches().entrySet()) {
                if (entry.getKey().equalsIgnoreCase(name)) {
                    rd = entry.getValue();
                }
            }
        } catch (Exception ex) {
            System.out.println("NAN");
        }
        if (e.getSource() == searchButton) {

            if (!name.equals("")) {
                ((Painter) painter).switchToResearchName(name, null);
            }
        } else if (e.getSource() == rootButton) {
            if (!name.equals("")) {
                if (rd != null) {
                    boolean found = ((Painter) painter).findTreeFor(rd.getIdentifier(), false);
                    if (found) {
                        getSearchTextField().setText(rd.getName());
                    }
                }
            }
            if (!name.equals("")) {
                painter.switchToResearchName(name, null);
            }
        } else if (e.getSource() == rootButtonHidden) {

            if (!name.equals("")) {
                if (rd != null) {
                    painter.findTreeFor(rd.getIdentifier(), true);
                }
            }
            if (!name.equals("")) {
                painter.switchToResearchName(name, null);
            }
        } else if (e.getSource() == unmark) {
            this.getSearchTextField().setText("");
            ((PainterData) painter.getPainterData()).unmark();
            painter.forceRefresh();

        } else if (e.getSource() == list) {

            ResearchList rl = new ResearchList(appletMain);
            appletMain.mainFrame.updateInformationPanelResearch(rl);
        } else if (e.getSource() == help) {
            appletMain.setShowHelp(true);

        } else if (e.getSource() == printSQL) {
            System.out.println("Wanna print sql");
            for (I_Object_OneCoordinate informationEntity : painter.getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_RESEARCH)) {

                ResDesc red = (ResDesc) informationEntity;
                System.out.println("UPDATE resalignment SET x='" + (int) red.getX1() + "' , y='" + (int) red.getY1() + "' WHERE id=" + red.getId() + ";");

            }
            for (I_Object_OneCoordinate informationEntity : painter.getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_TECHRELATION)) {
                TechRelation tr = (TechRelation) informationEntity;
                //INSERT INTO `techrelation` VALUES ('9', '0', '8', '1');
                System.out.println("INSERT INTO techrelation VALUES(" + tr.getSourceId() + ",0," + tr.getReqResearchId() + "," + 1 + ");");

            }

            for (Map.Entry<Integer, List<Technology>> informationEntity : ((AppletData) appletMain.getAppletData()).getUnlockedTech().entrySet()) {
                int id = informationEntity.getKey();
                for (Technology t : informationEntity.getValue()) {
                    int sourceId = t.getSourceId();
                    int type = t.getResearchType();
                    System.out.println("INSERT INTO techrelation VALUES(" + sourceId + ",0," + id + "," + type + ");");
                }

            }

        } else if (e.getSource() == showMarkers) {
            ((PainterData)painter.getPainterData()).switchShowMarkers();
            painter.forceRefresh();

        }
    }

    private void setEnabled(ArrayList<JComponent> components, boolean enabled) {
        for (JComponent c : components) {
            c.setEnabled(enabled);
        }
    }

    private void addComponents() {
        this.add(searchLabel);
        this.add(searchButton);
        this.add(rootButton);
        this.add(rootButtonHidden);
        this.add(unmark);
        this.add(getSearchTextField());
        if (appletMain.getAppletData().isAdmin()) {
            this.add(printSQL);
            this.add(showMarkers);
        }

        unmark.addActionListener(this);
        rootButtonHidden.addActionListener(this);
        rootButton.addActionListener(this);
        searchButton.addActionListener(this);
        printSQL.addActionListener(this);
        showMarkers.addActionListener(this);
    }

    private void initializeComponents() {

        list = new JButton();
        help = new JButton();
        Image helpImage = DisplayManagement.getGraphic("help.png");
        Image listImage = DisplayManagement.getGraphic("list.png");
        ImageIcon helpIcon = new ImageIcon(helpImage);
        ImageIcon listIcon = new ImageIcon(listImage);
        list.setIcon(listIcon);
        list.setPreferredSize(new Dimension(20, 20));
        help.setIcon(helpIcon);
        help.setPreferredSize(new Dimension(20, 20));

        list.addActionListener(this);
        help.addActionListener(this);

        this.add(list);
        this.add(help);

        searchButton = new JButton();
        rootButton = new JButton();
        unmark = new JButton();
        rootButtonHidden = new JButton();
        Image buttonImage = DisplayManagement.getGraphic("search.png");
        Image unmarkImage = DisplayManagement.getGraphic("cancelP.png");
        Image treeImage = DisplayManagement.getGraphic("tree.png");
        Image treehImage = DisplayManagement.getGraphic("treeh.png");
        ImageIcon treeIcon = new ImageIcon(treeImage);
        ImageIcon treehIcon = new ImageIcon(treehImage);
        ImageIcon searchButtonIcon = new ImageIcon(buttonImage);
        ImageIcon unmarkImageIcon = new ImageIcon(unmarkImage);

        rootButton.setIcon(treeIcon);
        rootButton.setPreferredSize(new Dimension(20, 20));
        rootButtonHidden.setIcon(treehIcon);
        rootButtonHidden.setPreferredSize(new Dimension(20, 20));
        unmark.setIcon(unmarkImageIcon);
        unmark.setPreferredSize(new Dimension(20, 20));
        searchButton.setIcon(searchButtonIcon);
        searchButton.setPreferredSize(new Dimension(20, 20));
        searchLabel = new JLabel("Suche nach Forschung" + " :");
        searchLabel.setForeground(Color.WHITE);

        searchTextField = new JTextField();
        printSQL = new JButton("p");
        printSQL.setPreferredSize(new Dimension(20, 20));
        printSQL.setFont(new Font("Tahoma", Font.PLAIN, 8));

        showMarkers = new JButton("m");
        showMarkers.setPreferredSize(new Dimension(20, 20));
        showMarkers.setFont(new Font("Tahoma", Font.PLAIN, 6));


        getSearchTextField().setPreferredSize(new Dimension(400, 18));
    }

    private void orderComponents() {

        layout.putConstraint(SpringLayout.NORTH, searchLabel, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, searchLabel, 2, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.WEST, getSearchTextField(), 0, SpringLayout.EAST, searchLabel);
        layout.putConstraint(SpringLayout.NORTH, getSearchTextField(), 0, SpringLayout.NORTH, searchLabel);

        layout.putConstraint(SpringLayout.WEST, searchButton, 5, SpringLayout.EAST, getSearchTextField());
        layout.putConstraint(SpringLayout.NORTH, searchButton, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, rootButton, 5, SpringLayout.EAST, searchButton);
        layout.putConstraint(SpringLayout.NORTH, rootButton, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, rootButtonHidden, 5, SpringLayout.EAST, rootButton);
        layout.putConstraint(SpringLayout.NORTH, rootButtonHidden, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, unmark, 5, SpringLayout.EAST, rootButtonHidden);
        layout.putConstraint(SpringLayout.NORTH, unmark, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, list, 5, SpringLayout.EAST, unmark);
        layout.putConstraint(SpringLayout.NORTH, list, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, help, 5, SpringLayout.EAST, list);
        layout.putConstraint(SpringLayout.NORTH, help, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, printSQL, 25, SpringLayout.EAST, help);
        layout.putConstraint(SpringLayout.NORTH, printSQL, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, showMarkers, 5, SpringLayout.EAST, printSQL);
        layout.putConstraint(SpringLayout.NORTH, showMarkers, 0, SpringLayout.NORTH, this);

    }

    /**
     * @return the searchTextField
     */
    public JTextField getSearchTextField() {
        return searchTextField;
    }
}
