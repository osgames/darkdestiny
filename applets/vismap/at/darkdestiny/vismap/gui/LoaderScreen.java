/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.logic.DisplayManagement;

/**
 *
 * @author Aion
 */
public class LoaderScreen extends JPanel {

    JTextArea text;

    public LoaderScreen(final int width, final int height) {
        SpringLayout layout = new SpringLayout();
        this.setLayout(layout);
        this.setBackground(Color.BLACK);
        this.setPreferredSize(new Dimension(width, height));
        text = new JTextArea("Applet Loaded....") {
            //Image grayImage = GrayFilter.createDisabledImage(image);

            {
                setOpaque(false);
            }

            public void paintComponent(Graphics g) {
                Image image = DisplayManagement.getGraphic("10.jpg");
                int imgHeight = 400;
                g.drawImage(image, (width - imgHeight) / 2, (height - imgHeight) / 2, imgHeight, imgHeight, this);
                //g.drawImage(grayImage, 0, 0, (int)getSize().getWidth(), (int)getSize().getHeight(), this);
                super.paintComponent(g);
            }
        };

        text.setForeground(Color.white);
        text.setBackground(Color.BLACK);
        text.setEditable(false);



        this.add(text);
        layout.putConstraint(SpringLayout.NORTH, text, 3, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, text, 3, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, text, -3, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.SOUTH, text, -3, SpringLayout.SOUTH, this);



    }

    public void addLine(String line) {
        text.append(line);
        text.append("\n");


    }
    public void clearText() {
        text.setText("");
    }
}
