/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.gui;

import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import javax.swing.*;

import java.awt.*;
import java.util.List;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.drawable.Transmitter;
import at.darkdestiny.vismap.starmap.panels.fleet.ChassisToolTip;
import at.darkdestiny.vismap.starmap.panels.fleet.FleetMovementPanel;
import at.darkdestiny.vismap.starmap.panels.fleet.FleetsPanel;
import at.darkdestiny.vismap.techtree.objects.ResearchList;
import java.util.ArrayList;

/*
 * LayeredPaneDemo.java requires
 * images/dukeWaveRed.gif.
 */
public class MainPanel extends JPanel {

    private JLayeredPane layeredPane;
    public JScrollPane informationScrollPane = new JScrollPane();
    public JPanel informationPanel = new JPanel(new CardLayout());
    private IPainter painter = null;
    private List<String> identifiers;
    public FleetMovementPanel fmp;
    protected boolean showInformationPanel = false;
    SpringLayout layout;
    SpringLayout panelLayout;
    private AppletMain am;
    public ChassisToolTip chassisToolTip;

    public MainPanel(IPainter painter, Dimension d, final AppletMain am) {


        this.setPreferredSize(d);
        this.am = am;
        this.painter = painter;
        //A List of all invoked and added Panels to the informationScrollPanel
        identifiers = new ArrayList();

        //Create and set up the layered pane.
        layeredPane = new JLayeredPane();
        layeredPane.setPreferredSize(d);
        panelLayout = new SpringLayout();
        this.setLayout(panelLayout);


        //Add several overlapping, colored labels to the layered pane
        //using absolute positioning/sizing.

        layout = new SpringLayout();
        layeredPane.setLayout(layout);
        informationScrollPane = new JScrollPane(informationPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);


        layout.putConstraint(SpringLayout.NORTH, informationScrollPane, 15, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.SOUTH, informationScrollPane, -15, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.EAST, informationScrollPane, -15, SpringLayout.EAST, this);

        layeredPane.add(informationScrollPane, 4, 1);
        informationScrollPane.setVisible(false);



        fmp = new FleetMovementPanel(am);
        layout.putConstraint(SpringLayout.NORTH, fmp, am.getHeight() / 2 - 200, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, fmp, am.getWidth() / 2 - 200, SpringLayout.WEST, this);
        layeredPane.add(fmp, 3, 1);

        fmp.setVisible(false);

        am.getPlugin().addPanels(layeredPane, am, painter);
        MovementArea maTop = new MovementArea(MovementArea.AreaSide.TOP, this, painter);
        layeredPane.add(maTop, 6, 1);
        MovementArea maLeft = new MovementArea(MovementArea.AreaSide.LEFT, this, painter);
        layeredPane.add(maLeft, 6, 1);
        MovementArea maRight = new MovementArea(MovementArea.AreaSide.RIGHT, this, painter);
        layeredPane.add(maRight, 6, 1);
        MovementArea maBottom = new MovementArea(MovementArea.AreaSide.BOTTOM, this, painter);
        layeredPane.add(maBottom, 6, 1);

        //  new Thread(maTop).start();
        //   new Thread(maLeft).start();
        //    new Thread(maRight).start();
        //    new Thread(maBottom).start();

        MovementArea maTopLeft = new MovementArea(MovementArea.AreaSide.TOP_LEFT, this, painter);
        layeredPane.add(maTopLeft, 7, 1);
        MovementArea maTopRight = new MovementArea(MovementArea.AreaSide.TOP_RIGHT, this, painter);
        layeredPane.add(maTopRight, 7, 1);
        MovementArea maBottomLeft = new MovementArea(MovementArea.AreaSide.BOTTOM_LEFT, this, painter);
        layeredPane.add(maBottomLeft, 7, 1);
        MovementArea maBottomRight = new MovementArea(MovementArea.AreaSide.BOTTOM_RIGHT, this, painter);
        layeredPane.add(maBottomRight, 67, 1);

        layout.putConstraint(SpringLayout.NORTH, maTopLeft, 0, SpringLayout.NORTH, layeredPane);
        layout.putConstraint(SpringLayout.WEST, maTopLeft, 0, SpringLayout.WEST, layeredPane);

        layout.putConstraint(SpringLayout.NORTH, maTopRight, 0, SpringLayout.NORTH, layeredPane);
        layout.putConstraint(SpringLayout.EAST, maTopRight, 0, SpringLayout.EAST, layeredPane);

        layout.putConstraint(SpringLayout.SOUTH, maBottomLeft, 0, SpringLayout.SOUTH, layeredPane);
        layout.putConstraint(SpringLayout.WEST, maBottomLeft, 0, SpringLayout.WEST, layeredPane);

        layout.putConstraint(SpringLayout.SOUTH, maBottomRight, 0, SpringLayout.SOUTH, layeredPane);
        layout.putConstraint(SpringLayout.EAST, maBottomRight, 0, SpringLayout.EAST, layeredPane);


        layout.putConstraint(SpringLayout.NORTH, maTop, 0, SpringLayout.NORTH, layeredPane);
        layout.putConstraint(SpringLayout.WEST, maTop, 0, SpringLayout.EAST, maTopLeft);

        layout.putConstraint(SpringLayout.NORTH, maLeft, 0, SpringLayout.SOUTH, maTopLeft);
        layout.putConstraint(SpringLayout.WEST, maLeft, 0, SpringLayout.WEST, layeredPane);

        layout.putConstraint(SpringLayout.NORTH, maRight, 0, SpringLayout.SOUTH, maTopRight);
        layout.putConstraint(SpringLayout.EAST, maRight, 0, SpringLayout.EAST, layeredPane);

        layout.putConstraint(SpringLayout.SOUTH, maBottom, 0, SpringLayout.SOUTH, layeredPane);
        layout.putConstraint(SpringLayout.WEST, maBottom, 0, SpringLayout.EAST, maBottomLeft);



        layout.putConstraint(SpringLayout.NORTH, painter.getPanel(), 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, painter.getPanel(), 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.SOUTH, painter.getPanel(), 0, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.EAST, painter.getPanel(), 0, SpringLayout.EAST, this);

        layeredPane.add(painter.getPanel(), 1, 1);


        add(layeredPane);
        panelLayout.putConstraint(SpringLayout.NORTH, layeredPane, 0, SpringLayout.NORTH, this);
        panelLayout.putConstraint(SpringLayout.WEST, layeredPane, 0, SpringLayout.WEST, this);
    }

    public void updateInformationPanelResearch(ResearchList informationEntity) {


        if (!showInformationPanel) {
            this.setInfoVisibility(true);
        }

        setBackground(new Color(0f, 0f, 1f, 1f));
        informationPanel.setOpaque(false);
        informationPanel.setBackground(new Color(0f, 1f, 0f, 1f));
        informationScrollPane.setOpaque(false);
        informationScrollPane.setBackground(new Color(1f, 0f, 0f, 1f));
        CardLayout c1 = (CardLayout) informationPanel.getLayout();
        try {

            informationScrollPane.getViewport().setView(informationEntity.getPanel());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (identifiers.indexOf(informationEntity.getIdentifier()) > -1) {

            c1.show(informationPanel, informationEntity.getIdentifier());
        } else {
            identifiers.add(informationEntity.getIdentifier());
            informationPanel.add(informationEntity.getPanel(), informationEntity.getIdentifier());
            c1.show(informationPanel, informationEntity.getIdentifier());
        }
        informationPanel.revalidate();
        informationPanel.repaint();
        informationScrollPane.revalidate();
        informationScrollPane.repaint();
        painter.revalidate();
        painter.repaint();

        am.mainFrame.revalidate();
        am.mainFrame.repaint();
        am.reValidate();
        am.repaint();



    }

    public void updateInformationPanel(List<FleetEntry> informationEntity) {


        if (!showInformationPanel) {
            this.setInfoVisibility(true);
        }

        setBackground(new Color(0f, 0f, 1f, 1f));
        informationPanel.setOpaque(false);
        informationPanel.setBackground(new Color(0f, 1f, 0f, 1f));
        informationScrollPane.setOpaque(false);
        informationScrollPane.setBackground(new Color(1f, 0f, 0f, 1f));
        CardLayout c1 = (CardLayout) informationPanel.getLayout();
        try {

            informationScrollPane.getViewport().setView(new FleetsPanel(informationEntity, am));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (identifiers.indexOf(informationEntity.get(0).getIdentifier()) > -1) {

            c1.show(informationPanel, informationEntity.get(0).getIdentifier());
        } else {
            identifiers.add(informationEntity.get(0).getIdentifier());
            informationPanel.add(new FleetsPanel(informationEntity, am), informationEntity.get(0).getIdentifier());
            c1.show(informationPanel, informationEntity.get(0).getIdentifier());
        }
        informationPanel.revalidate();
        informationPanel.repaint();
        informationScrollPane.revalidate();
        informationScrollPane.repaint();
        painter.revalidate();
        painter.repaint();

        am.mainFrame.revalidate();
        am.mainFrame.repaint();
        am.reValidate();
        am.repaint();



    }

    //Displaying and Adding an InformationPanel about the Entity to the informationPanel
    public void updateInformationPanel(I_Object_OneCoordinate informationEntity) {

        if (!showInformationPanel) {
            this.setInfoVisibility(true);
        }
        informationScrollPane.setBackground(new Color(1f, 1f, 1f, 0f));

        CardLayout c1 = (CardLayout) informationPanel.getLayout();

        informationScrollPane.getViewport().setView(informationEntity.getPanel());
        if (identifiers.indexOf(informationEntity.getIdentifier()) > -1) {

            c1.show(informationPanel, informationEntity.getIdentifier());

        } else {
            identifiers.add(informationEntity.getIdentifier());
            informationPanel.add(informationEntity.getPanel(), informationEntity.getIdentifier());
            c1.show(informationPanel, informationEntity.getIdentifier());

        }
        informationPanel.revalidate();
        informationPanel.repaint();
        informationScrollPane.revalidate();
        informationScrollPane.repaint();
        painter.revalidate();
        painter.repaint();

        am.mainFrame.revalidate();
        am.mainFrame.repaint();
        am.reValidate();
        am.repaint();


    }

    public void setInfoVisibility(boolean show) {

        if (show == false) {
            if (am.mainFrame.getPainter() instanceof at.darkdestiny.vismap.starmap.Painter) {
                ((at.darkdestiny.vismap.starmap.Painter) am.mainFrame.getPainter()).setSendSystem(null);
                fmp.setVisible(false);
                if (informationScrollPane.isVisible()) {

                    informationScrollPane.setVisible(show);
                }
            }
        } else {
            if (!informationScrollPane.isVisible()) {

                informationScrollPane.setVisible(show);
            }
        }

    }

    public void setInformationPanel(JPanel informationPanel) {
        this.informationPanel = informationPanel;
    }

    /**
     * @return the informationScrollPane
     */
    public JScrollPane getInformationScrollPane() {
        return informationScrollPane;
    }

    public void updateFleetEntry(FleetEntry fe) {
        am.mainFrame.revalidate();
        am.mainFrame.repaint();
        am.reValidate();
        am.repaint();
    }

    public void sendFleetToPlanet(int id, SystemDesc sd, String name) {

        sendFleetTo(id, sd, FleetMovementPanel.ELocationType.PLANET, name);
    }

    public void sendFleetToSystem(int id, SystemDesc sd, String name) {
        sendFleetTo(id, sd, FleetMovementPanel.ELocationType.SYSTEM, name);
    }

    public void sendFleetTo(int id, SystemDesc sd, FleetMovementPanel.ELocationType type, String name) {
        FleetEntry fe = ((at.darkdestiny.vismap.starmap.Painter) am.mainFrame.getPainter()).fleetToSend;
        fmp.setValues(fe, id, sd, type, name, null, null);
        fmp.setVisible(true);
    }

    public void sendFleetToTransmitter(FleetEntry fe, Transmitter transmitter) {
        fmp.setValues(fe, transmitter);
        fmp.setVisible(true);
    }

    /**
     * @return the painter
     */
    public IPainter getPainter() {
        if (painter == null) {
            painter = am.getPlugin().getPainter(am);
        }
        return painter;
    }

    /**
     * @param painter the painter to set
     */
    public void setPainter(IPainter painter) {
        this.painter = painter;
    }

    public void clearInformationPanel() {
        informationPanel.removeAll();
        identifiers.clear();
        CardLayout c1 = new CardLayout();
        informationPanel.setLayout(c1);

    }
}
