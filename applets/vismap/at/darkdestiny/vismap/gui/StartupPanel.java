/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.gui;

import at.darkdestiny.vismap.enumerations.EVismapType;
import at.darkdestiny.vismap.logic.AppletConstants;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;

public class StartupPanel extends JPanel {

    private JComboBox<EVismapType> dropdown;

    private static final Map<EVismapType, String> typeToFile = new HashMap();

    private SpringLayout layout = new SpringLayout();
    private Component tmp;

    static {
        typeToFile.put(EVismapType.STARMAP, "starmap.xml");
        typeToFile.put(EVismapType.JOINMAP, "joinmap.xml");
        typeToFile.put(EVismapType.PLANETLOGVIEWER, "planetlog.xml");
        typeToFile.put(EVismapType.TECHTREE, "techtree.xml");

    }

    private JButton startButton;
    private AppletMain appletMain;

    public StartupPanel(final AppletMain appletMain) {

        this.appletMain = appletMain;
        this.setLayout(layout);

        this.add(new JLabel("Current sourcedoc folder is : " + AppletConstants.FILE_PATH));
        this.dropdown = new JComboBox();
        this.dropdown.setPreferredSize(new Dimension(300, 20));
        for (Map.Entry<EVismapType, String> entry : typeToFile.entrySet()) {
            File f = new File(AppletConstants.FILE_PATH, entry.getValue());
            if (f.exists()) {
                dropdown.addItem(entry.getKey());
                this.add(new JLabel("[" + entry.getKey().toString() + "] Found file [" + entry.getValue() + "]"));
            } else {
                this.add(new JLabel("[" + entry.getKey().toString() + "] Could not find file [" + entry.getValue() + "]"));
            }
        }
        this.add(dropdown);
        this.setPreferredSize(new Dimension(appletMain.getWidth(), appletMain.getHeight()));
        this.setVisible(true);
        this.startButton = new JButton("Start");
        this.add(startButton);
        startButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                appletMain.startWithType((EVismapType) dropdown.getSelectedItem());
            }
        });

    }

    @Override
    public Component add(Component comp) {

        if (tmp == null) {
            layout.putConstraint(SpringLayout.WEST, comp, appletMain.getWidth()/2 - 120, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.NORTH, comp, 0, SpringLayout.NORTH, this);
        }else{

            layout.putConstraint(SpringLayout.WEST, comp, 0, SpringLayout.WEST, tmp);
            layout.putConstraint(SpringLayout.NORTH, comp, 10, SpringLayout.SOUTH, tmp);
        }
            tmp = comp;
        return super.add(comp);
    }
}
