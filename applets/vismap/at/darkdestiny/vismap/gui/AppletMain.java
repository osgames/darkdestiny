package at.darkdestiny.vismap.gui;

import at.darkdestiny.vismap.joinmap.AppletData;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.enumerations.EVismapType;
import at.darkdestiny.vismap.interfaces.IDataLoader;
import at.darkdestiny.vismap.interfaces.IVisMapPlugin;
import at.darkdestiny.vismap.joinmap.Joinmap;
import at.darkdestiny.vismap.logic.LoadThread;
import at.darkdestiny.vismap.planetlogviewer.PlanetLogViewer;
import at.darkdestiny.vismap.starmap.Starmap;
import at.darkdestiny.vismap.techtree.Techtree;
import at.darkdestiny.vismap.techtree.panels.Glossar;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import javax.swing.JApplet;
import javax.swing.SpringLayout;

public class AppletMain extends JApplet implements KeyEventDispatcher {

    public static final String version = "Version 4.03";
    private SpringLayout layout;
    private static JApplet instance = null;

    public MainPanel mainFrame;
    private LoaderScreen loader;
    int width;
    int height;
    public LoadThread lt;
    private IAppletData appletData;
    private EVismapType vismapType;
    private boolean initialized = false;
    private boolean reloaded = false;
    private IVisMapPlugin plugin;
    private StartupPanel startupPanel;
    private boolean admin = false;
    private static final boolean showSelection = true;

    private Glossar help;

    public AppletMain() throws HeadlessException {
        instance = this;
    }

    public static JApplet getApplet() {
        return instance;
    }

    /**
     * This method initializes this
     *
     */
    @Override
    public void init() {
        boolean startAsApplication = false;

        vismapType = EVismapType.STARMAP;

        try {
            vismapType = EVismapType.valueOf(this.getParameter("MODE"));
        } catch (Exception e) {
            startAsApplication = true;
        }

        layout = new SpringLayout();
        this.setLayout(layout);

        width = (int) this.getSize().getWidth();
        height = (int) this.getSize().getHeight();

        //Need fixed with if not coming from html
        if (startAsApplication) {
            width = 1600;
            height = 900;
            admin = true;
        }
        this.setSize(width, height);
        this.setPreferredSize(new Dimension(width, height));
        this.setBackground(Color.BLACK);

        if (startAsApplication && showSelection) {
            startupPanel = new StartupPanel(this);
            this.add(startupPanel);
        }else{
            startup();
        }
    }

    public void startup() {

        //Loader Screen
        loader = new LoaderScreen(width, height);

        if (vismapType.equals(EVismapType.JOINMAP)) {
            plugin = new Joinmap();
        } else if (vismapType.equals(EVismapType.STARMAP)) {
            plugin = new Starmap();
        } else if (vismapType.equals(EVismapType.PLANETLOGVIEWER)) {
            plugin = new PlanetLogViewer();
        } else if (vismapType.equals(EVismapType.TECHTREE)) {
            plugin = new Techtree();
        }

        //create Loadthread to load data
        lt = new LoadThread(this, plugin, vismapType, admin);

        //Add Loader Screen
        this.add(getLoader());
        //Add Version
        getLoader().addLine(version);

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);

        //Start loading
        lt.start();

    }

    //Invoked by the LoadThread to build up the main screen
    public void setReloaded(boolean reloaded) {
        this.reloaded = reloaded;
        if (reloaded) {
            addComponents();
            orderComponents();
            getLoader().setVisible(false);
        }
    }

    //Add main components
    private void addComponents() {

        this.add(mainFrame);
    }
    //Align main components

    private void orderComponents() {

        layout.putConstraint(SpringLayout.NORTH, mainFrame, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, mainFrame, 0, SpringLayout.WEST, this);

    }

    public void reValidate() {

        this.validate();
    }

    //Reloads the Applet
    public void reload() {
        IPainterData painterData = null;
        if (vismapType.equals(EVismapType.JOINMAP)) {
            this.appletData = new at.darkdestiny.vismap.joinmap.AppletData();
            painterData = new at.darkdestiny.vismap.joinmap.PainterData();
        } else if (vismapType.equals(EVismapType.STARMAP)) {
            this.appletData = new at.darkdestiny.vismap.starmap.AppletData();
            painterData = new at.darkdestiny.vismap.starmap.PainterData();
        } else if (vismapType.equals(EVismapType.TECHTREE)) {
            this.appletData = new at.darkdestiny.vismap.techtree.AppletData();
            painterData = new at.darkdestiny.vismap.techtree.PainterData();
        } else if (vismapType.equals(EVismapType.PLANETLOGVIEWER)) {
            this.appletData = new at.darkdestiny.vismap.planetlogviewer.AppletData();
            painterData = new at.darkdestiny.vismap.planetlogviewer.PainterData();
        }
        IDataLoader dataLoader = plugin.getDataLoader(this, appletData, painterData, loader);
        dataLoader.reload(this);
        mainFrame.clearInformationPanel();

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {

        if (e.getKeyCode() == 40 && e.getID() == 401) {
            mainFrame.getPainter().moveImage("down", 1f);
            return true;
        } else if (e.getKeyCode() == 39 && e.getID() == 401) {
            mainFrame.getPainter().moveImage("right", 1f);
            return true;
        } else if (e.getKeyCode() == 38 && e.getID() == 401) {
            mainFrame.getPainter().moveImage("up", 1f);
            return true;
        } else if (e.getKeyCode() == 37 && e.getID() == 401) {
            mainFrame.getPainter().moveImage("left", 1f);
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_CONTROL && e.getID() == KeyEvent.KEY_PRESSED && !plugin.getMouse().isCtrlPressed()) {
            plugin.getMouse().setCtrlPressed(true);
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_CONTROL && e.getID() == KeyEvent.KEY_RELEASED && plugin.getMouse().isCtrlPressed()) {
            plugin.getMouse().setCtrlPressed(false);
        } else if (e.getKeyCode() == KeyEvent.VK_ALT && e.getID() == KeyEvent.KEY_PRESSED && !plugin.getMouse().isAltPressed()) {
            plugin.getMouse().setAltPressed(true);
            return true;
        } else if (e.getKeyCode() == KeyEvent.VK_ALT && e.getID() == KeyEvent.KEY_RELEASED && plugin.getMouse().isAltPressed()) {
            plugin.getMouse().setAltPressed(false);
        }
        return false;
    }

    /**
     * @return the loader
     */
    public LoaderScreen getLoader() {
        return loader;
    }

    /**
     * @return the appletData
     */
    public IAppletData getAppletData() {
        return appletData;
    }

    /**
     * @param appletData the appletData to set
     */
    public void setAppletData(IAppletData appletData) {
        this.appletData = appletData;
    }

    /**
     * @return the pugin
     */
    public IVisMapPlugin getPlugin() {
        return plugin;
    }

    /**
     * @return the initialized
     */
    public boolean isInitialized() {
        return initialized;
    }

    /**
     * @param initialized the initialized to set
     */
    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    /**
     * @return the reloaded
     */
    public boolean isReloaded() {
        return reloaded;
    }

    public void initialize() {
        addComponents();
        orderComponents();
        getLoader().setVisible(false);
    }

    public void setShowHelp(boolean show) {
        if (help == null) {
            help = new Glossar(this);
        }
        if (!show) {
            help.setVisible(false);
            mainFrame.setVisible(true);
        } else {
            help.setVisible(true);
            mainFrame.setVisible(false);
        }
        validate();
    }

    void startWithType(EVismapType selectedItem) {
        this.startupPanel.setVisible(false);
        this.remove(startupPanel);
        this.vismapType = selectedItem;
        startup();
    }
}  //  @jve:decl-index=0:visual-constraint="10,10"

