package at.darkdestiny.vismap.gui;

import at.darkdestiny.vismap.logic.AppletConstants;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JPanel;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.drawable.Object_TwoCoordinates;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.enumerations.EVismapType;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.interfaces.I_Object_TwoCoordinates;
import at.darkdestiny.vismap.logic.AbstractMouse;
import at.darkdestiny.vismap.starmap.drawable.Raster;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import java.awt.Cursor;
import java.util.List;
import javax.swing.SpringLayout;

public class MainPainter extends JPanel implements IPainter {

    private static final long serialVersionUID = 4354204667927554366L;
    //Radius eines Pixels f�r den der Tooltip angezeigt werden soll
    private static final float ZOOMSTRENGTH = 1.1f;
    private static final float MINXDIFF = 10;
    private static final float MINYDIFF = 10;
    protected IPainterData painterData;
    protected float visible_startx, visible_starty;
    protected float visible_endx, visible_endy;
    protected Image backBuffer = null;
    protected boolean dataChanged = true;
    protected float x_start = Float.NaN;
    protected float y_start = Float.NaN;
    protected int w_o = 100, h_o = 100;
    protected float globalRatio;
    protected boolean showCoordinates = true;
    private static int rasterDetail = AppletConstants.RASTER_STANDARD_SIZE;
    private List<Raster> rasterLines;
    protected IAppletData appletData;
    protected AbstractMouse mouse;
    protected AppletMain main;
    public boolean sendFleet = false;
    protected MouseEvent mouseEvent;
    public boolean setDefaultCursor = false;
    public EStarmapMode mode = EStarmapMode.DEFAULT;
    private SpringLayout layout = new SpringLayout();
    boolean buttonsAdded = false;
    int width;
    int height;
    protected Cursor defaultCursor;

    /**
     *
     * @param main
     */
    public MainPainter(AppletMain main) {

        this.main = main;
        width = (int) main.getPreferredSize().getWidth();
        height = (int) main.getPreferredSize().getHeight();
        this.setPreferredSize(new Dimension(width, height));
        layout = new SpringLayout();
        this.setLayout(layout);

        visible_startx = -2000;
        visible_endx = 2000;
        visible_starty = -2000;
        visible_endy = 2000;
    }

    @Override
    public void update(Graphics g) {
        paint(g);
    }

    public void prePaint(Graphics g) {
        if (mouse == null) {

            mouse = main.getPlugin().getMouse(main, this, painterData);

            this.addMouseMotionListener(mouse);
            this.addMouseListener(mouse);
            this.addMouseWheelListener(mouse);
        }
        if (main.isReloaded()) {
            mouse.update(main, this, painterData);
        }

        if ((backBuffer == null) || (getWidth() != backBuffer.getWidth(null)) || (getHeight() != backBuffer.getHeight(null))) {

            //Die Ratio der dinge soll beibehalten bleiben
            if (backBuffer != null) {
                w_o = backBuffer.getWidth(null);
                h_o = backBuffer.getHeight(null);
            }

            int w_n = getWidth();
            int h_n = getHeight();

            //Aspect-Ratio berechnen:
            float a_o = w_o * 1.0f / h_o;
            float a_n = w_n * 1.0f / h_n;

            if (a_n > a_o) {
                //Die Breite ist gr��er geworden, oder die H�he kleiner =>
                //Die neue Breite muss gr��er werdenf
                float delta = (a_n / a_o - 1) * (visible_endx - visible_startx);
                visible_endx += delta / 2;
                visible_startx -= delta / 2;
            } else {
                //Die H�he muss gr��er werden
                float delta = (a_o / a_n - 1) * (visible_endy - visible_starty);
                visible_endy += delta / 2;
                visible_starty -= delta / 2;
            }
            dataChanged = true;
            backBuffer = null;
        }

        if (dataChanged) {
            if (backBuffer == null) {
                backBuffer = createImage(getWidth(), getHeight());
            }
            Graphics gc = backBuffer.getGraphics();

            gc.setColor(Color.black);
            gc.fillRect(0, 0, this.getWidth(), this.getHeight());
        }
    }

    @Override
    public void paint(Graphics g) {

        //  Alle Systeme in denen der User ein System besitzt bekommen einen wei�en Kreis
        float[] res = new float[2];
        float[] delta = new float[2]; //Die L�nge von jeweils einem LJ (d.h. 1), jeweils in Richtung X und Y

        convertCoordinates(0, 0, res);
        convertCoordinates(1, 1, delta);
        delta[0] -= res[0];
        delta[1] -= res[1];

        int[] pos2 = new int[2];
        int[] pos = new int[2];

        if (dataChanged) {
            if (backBuffer == null) {
                backBuffer = createImage(getWidth(), getHeight());
            }
            Graphics gc = backBuffer.getGraphics();

            /* gc.setColor(Color.black);
             gc.fillRect(0, 0, this.getWidth(), this.getHeight());*/
            //Main Draw procedure Loop all items
            boolean hyperScannerDrawn = false;
            for (Map.Entry<Integer, List<I_Object_OneCoordinate>> entry : painterData.getDrawObjects().entrySet()) {

                if (!hyperScannerDrawn && appletData.getVismapType().equals(EVismapType.STARMAP) && entry.getKey() > AppletConstants.DRAW_LEVEL_HYPERSCANNER) {
                    at.darkdestiny.vismap.starmap.Painter painter = (at.darkdestiny.vismap.starmap.Painter) main.getPlugin().getPainter(main);
                    painter.drawHyperScannerArea();
                    hyperScannerDrawn = true;
                }
                List<I_Object_OneCoordinate> objects = entry.getValue();

                for (I_Object_OneCoordinate ob : objects) {
                    if(ob.isHide()){
                        continue;
                    }

                    if (painterData.isContinueOnObject(ob, this, sendFleet)) {
                        continue;
                    }

                    if (ob instanceof Object_TwoCoordinates) {

                        Object_TwoCoordinates object_TwoCoordinates = (Object_TwoCoordinates) ob;
                        convertCoordinates(object_TwoCoordinates, pos, pos2);
                        object_TwoCoordinates.draw(appletData, gc, delta);

                    } else if (ob instanceof Object_OneCoordinate) {

                        Object_OneCoordinate conOb = (Object_OneCoordinate) ob;
                        convertCoordinates(conOb, pos);

                        /*     if(ob instanceof at.viswars.vismap.starmap.drawable.SystemDesc && !isVisible(conOb)){
                         continue;
                         }*/
                        //System needs special handling
                        if (appletData.getVismapType().equals(EVismapType.JOINMAP)) {
                            if (ob instanceof at.darkdestiny.vismap.joinmap.drawable.SystemDesc) {
                                at.darkdestiny.vismap.joinmap.drawable.SystemDesc sys = (at.darkdestiny.vismap.joinmap.drawable.SystemDesc) ob;
                                if (entry.getKey() == AppletConstants.DRAW_LEVEL_SYSTEMS_OVALS) {
                                    sys.draw(appletData, gc, 8, delta);
                                } else {
                                    sys.draw(appletData, gc, 9, delta);
                                }
                            } else {
                                conOb.draw(appletData, gc, delta);

                            }
                        } else if (appletData.getVismapType().equals(EVismapType.STARMAP) || appletData.getVismapType().equals(EVismapType.PLANETLOGVIEWER)) {
                            if (ob instanceof at.darkdestiny.vismap.starmap.drawable.SystemDesc) {
                                at.darkdestiny.vismap.starmap.drawable.SystemDesc sys = (at.darkdestiny.vismap.starmap.drawable.SystemDesc) ob;
                                if (entry.getKey() == AppletConstants.DRAW_LEVEL_SYSTEMS_OVALS) {
                                    sys.draw(appletData, gc, 8, delta);
                                } else {
                                    sys.draw(appletData, gc, 9, delta);
                                }
                            } else {
                                conOb.draw(appletData, gc, delta);

                            }
                        } else if (appletData.getVismapType().equals(EVismapType.TECHTREE)) {
                            conOb.draw(appletData, gc, delta);
                        } else {
                            System.err.println("Failure not specified vismap type  in mainPainter : [" + appletData.getVismapType().toString() + "]");
                            System.exit(1);
                        }
                    } else {
                        //Object not recognized
                    }
                }
            }

            if (painterData.isShowRaster()) {
                if (rasterLines == null) {
                    addRaster(AppletConstants.RASTER_STANDARD_SIZE);
                }
                for (Raster raster : rasterLines) {
                    convertCoordinates(raster, pos, pos2);
                    raster.setInverted(painterData.isRasterInverted());
                    raster.draw(appletData, gc, delta);
                }
            }
            dataChanged = false;

        }

        globalRatio = (float) getWidth() / (float) getHeight();

    }

    public void postPaint(Graphics g) {
        //Draw buttons and components on the layer
    }

    public float convertToFloatX(int x, int y) {
        int width = getWidth();

        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;

        return x * width_fact + visible_startx;
    }

    public float convertToFloatY(int x, int y) {
        int height = getHeight();
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;

        return y * height_fact + visible_starty;
    }

    public void convertCoordinates(float sx, float sy, float[] res) {

        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;

        float x = (sx - visible_startx) / width_fact;
        float y = (sy - visible_starty) / height_fact;

        res[0] = x;
        res[1] = y;
    }

    public void convertCoordinates(Object_OneCoordinate sd, int[] res) {

        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;

        float x = (sd.getX1() - visible_startx) / width_fact;
        float y = (sd.getY1() - visible_starty) / height_fact;

        res[0] = Math.round(x);
        res[1] = Math.round(y);
        sd.setActCoord_x1(res[0]);
        sd.setActCoord_y1(res[1]);
    }

    public int[] getXYOffset(float[] pos) {
        int[] offset = new int[2];

        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;

        float x = (pos[0] - visible_startx) / width_fact;
        float y = (pos[1] - visible_starty) / height_fact;

        offset[0] = Math.round(x);
        offset[1] = Math.round(y);

        return offset;
    }

    public void zoomRectScaled(float x_start, float y_start, float width, float height) {

        float factor = 1f;
        float originalWidth = width;
        float originalHeight = height;
        float endx = x_start;
        float endy = y_start;
        if (painterData.getWidth() > painterData.getHeight()) {
            factor = (float) painterData.getWidth() / (float) painterData.getHeight();
            width *= factor;
        } else {
            factor = (float) painterData.getHeight() / (float) painterData.getWidth();
            height *= factor;
        }
        endx += width;
        endy += height;
        zoomRect(x_start, y_start, endx, endy);
        switchToCoordinates((int) (x_start + originalWidth / 2f), (int) (y_start + originalHeight / 2f));

    }

    public void zoomRect(float x_start, float y_start, float endx, float endy) {

        if ((endx == x_start) || (endy == y_start)) {
            return;
            // Check if shorter side will exceed zoomfactor if yes rescale selection
        }

        float xDiff = Math.abs(endx - x_start);
        float yDiff = Math.abs(endy - y_start);
        float currXDiff = Math.abs(visible_endx - visible_startx);
        float currYDiff = Math.abs(visible_endy - visible_starty);

        if (xDiff < MINXDIFF) {
            endx = x_start + MINXDIFF;
        }

        if (yDiff < MINYDIFF) {
            endy = y_start + MINYDIFF;
        }

        float ratio = currXDiff / currYDiff;

        // Select main side for resize
        float equalToX = (currXDiff / Math.abs(x_start - endx)) - 1;
        float equalToY = (currYDiff / Math.abs(y_start - endy)) - 1;

        boolean scaleByX = true;

        if (Math.abs(equalToX) > Math.abs(equalToY)) {
            scaleByX = false;
        }

        if (endx > x_start && endy > y_start) {
            visible_startx = x_start;
            visible_starty = y_start;

            if (scaleByX) {
                visible_endx = endx;
                visible_endy = visible_starty + (visible_endx - visible_startx) / ratio;
            } else {
                visible_endy = endy;
                visible_endx = visible_startx + (visible_endy - visible_starty) * ratio;
            }
        } else if (endx < x_start && endy < y_start) {
            visible_startx = endx;
            visible_starty = endy;

            if (scaleByX) {
                visible_endx = x_start;
                visible_endy = visible_starty + (visible_endx - visible_startx) / ratio;
            } else {
                visible_endy = y_start;
                visible_endx = visible_startx + (visible_endy - visible_starty) * ratio;
            }
        } else if (endx > x_start && endy < y_start) {
            visible_startx = x_start;
            visible_starty = endy;

            if (scaleByX) {
                visible_endx = endx;
                visible_endy = visible_starty + (visible_endx - visible_startx) / ratio;
            } else {
                visible_endy = y_start;
                visible_endx = visible_startx + (visible_endy - visible_starty) * ratio;
            }
        } else if (endx < x_start && endy > y_start) {
            visible_startx = endx;
            visible_starty = y_start;

            if (scaleByX) {
                visible_endx = x_start;
                visible_endy = visible_starty + (visible_endx - visible_startx) / ratio;
            } else {
                visible_endy = endy;
                visible_endx = visible_startx + (visible_endy - visible_starty) * ratio;
            }
        }

        dataChanged = true;
        //fitView((int)tmp1x,(int)tmp1y);
        repaint();
    }

    @Override
    public void moveImage(String direction, float weight) {
        float x = 0;
        float y = 0;
        float dx = convertToFloatX(getWidth() / 10, 0) - convertToFloatX(0, 0);
        float dy = convertToFloatY(0, getHeight() / 10) - convertToFloatY(0, 0);
        if (direction.equals("left")) {
            x = -dx;
        } else if (direction.equals("right")) {
            x = dx;
        } else if (direction.equals("down")) {
            y = dy;
        } else if (direction.equals("up")) {
            y = -dy;
        }
        visible_startx = visible_startx + (x * weight);
        visible_endx = visible_endx + (x * weight);
        visible_starty = visible_starty + y * weight;
        visible_endy = visible_endy + y * weight;

        forceRefresh();
    }

    @Override
    public void zoomOut() {
        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;

        w *= ZOOMSTRENGTH;
        h *= ZOOMSTRENGTH;

        if (w > h) {
            if (w > (AppletConstants.MAXXDIFF)) {
                w = (AppletConstants.MAXXDIFF);
            }
            if (h > (AppletConstants.MAXYDIFF / globalRatio)) {
                h = (AppletConstants.MAXYDIFF / globalRatio);
            }
        } else {
            if (w > (AppletConstants.MAXXDIFF / globalRatio)) {
                w = (AppletConstants.MAXXDIFF / globalRatio);
            }
            if (h > (AppletConstants.MAXYDIFF)) {
                h = (AppletConstants.MAXYDIFF);
            }
        }

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        forceRefresh();
    }

    @Override
    public void zoomIn() {
        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;

        w /= ZOOMSTRENGTH;
        h /= ZOOMSTRENGTH;

        if (w > h) {
            if (w < (MINXDIFF)) {
                w = (MINXDIFF);
            }
            if (h < (MINYDIFF / globalRatio)) {
                h = (MINYDIFF / globalRatio);
            }
        } else {
            if (w < (MINXDIFF / globalRatio)) {
                w = (MINXDIFF / globalRatio);
            }
            if (h < (MINYDIFF)) {
                h = (MINYDIFF);
            }
        }

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        forceRefresh();
    }

    public void mouseWheelChanged(int zoom, MouseEvent mouseEvent) {
        if (zoom < 0) {

            zoomIn(mouseEvent, this.getHeight(), this.getWidth());

        }
        if (zoom > 0) {

            zoomOut(mouseEvent, this.getHeight(), this.getWidth());

        }
        dataChanged = true;
        repaint();

    }

    @Override
    public void forceRefresh() {
        dataChanged = true;
        repaint();
    }


    public void addRaster(float rasterDetail) {

        rasterLines = new ArrayList();

        for (int i = 0; i < AppletConstants.UNIVERSE_SIZE_X + 1; i++) {
            if (i % rasterDetail == 0) {

                rasterLines.add(new Raster(i, 0, i, AppletConstants.UNIVERSE_SIZE_X));
            }
        }
        for (int i = 0; i < AppletConstants.UNIVERSE_SIZE_Y + 1; i++) {
            if (i % rasterDetail == 0) {
                rasterLines.add(new Raster(0, i, AppletConstants.UNIVERSE_SIZE_Y, i));
            }
        }
    }

    protected void convertCoordinates(I_Object_TwoCoordinates object_twoCoordinates, int[] res1, int[] res2) {
        int width = getWidth();
        int height = getHeight();
        float visWidth = visible_endx - visible_startx;
        float width_fact = visWidth / width;
        // float width_fact = 1;
        float visHeight = visible_endy - visible_starty;
        float height_fact = visHeight / height;
        // float height_fact = 1;

        float startX = (object_twoCoordinates.getX1() - visible_startx) / width_fact;
        float startY = (object_twoCoordinates.getY1() - visible_starty) / height_fact;
        float endX = (object_twoCoordinates.getX2() - visible_startx) / width_fact;
        float endY = (object_twoCoordinates.getY2() - visible_starty) / height_fact;

        res1[0] = Math.round(startX);
        res1[1] = Math.round(startY);
        res2[0] = Math.round(endX);
        res2[1] = Math.round(endY);
        object_twoCoordinates.setActCoord_x1(res1[0]);
        object_twoCoordinates.setActCoord_y1(res1[1]);
        object_twoCoordinates.setActCoord_x2(res2[0]);
        object_twoCoordinates.setActCoord_y2(res2[1]);
    }

    public void setRaster(boolean value, int size) {

        painterData.setShowRaster(value);
        if (rasterDetail != size) {
            this.addRaster(size);
            rasterDetail = size;
        }
        dataChanged = true;

        repaint();
    }

    @Override
    public void setRasterDetail(int size) {

        if (rasterDetail != size) {
            this.addRaster(size);
            rasterDetail = size;
        }
        dataChanged = true;

        repaint();
    }

    @Override
    public int getRasterDetail() {

        return rasterDetail;
    }

    protected void moveImage(float y, float x) {

        /*-y/-x        -y/+x
         *        |
         *    1   |   2
         *        |
         * -----center----
         *        |
         *    3   |   4
         *        |
         * +y/-x      +y/+x
         */
        //     y = Math.round(y / 10) * 10;
        //    x = Math.round(x / 10) * 10;
        visible_startx = visible_startx + x;
        visible_endx = visible_endx + x;
        visible_starty = visible_starty + y;
        visible_endy = visible_endy + y;

        forceRefresh();

    }

    protected void zoomIn(int xMouse, int yMouse, int height, int width) {

        float visDifX = visible_endx - visible_startx;
        float visDifY = visible_endy - visible_starty;

        float widthF = (float) xMouse / (float) width;
        float heightF = (float) yMouse / (float) height;

        float coordX1 = (visible_startx + visDifX * widthF);
        float coordY1 = (visible_starty + visDifY * heightF);

        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;

        w /= ZOOMSTRENGTH;
        h /= ZOOMSTRENGTH;

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        visDifX = visible_endx - visible_startx;
        visDifY = visible_endy - visible_starty;

        float coordX2 = (visible_startx + visDifX * widthF);
        float coordY2 = (visible_starty + visDifY * heightF);

        float moveImageX = coordX2 - coordX1;
        float moveImageY = coordY2 - coordY1;

        moveImage(-moveImageY, -moveImageX);

        forceRefresh();

    }

    protected void zoomIn(MouseEvent mouseEvent, int height, int width) {

        if (mouseEvent == null) {
            return;
        }
        float visDifX = visible_endx - visible_startx;
        float visDifY = visible_endy - visible_starty;

        float widthF = (float) mouseEvent.getX() / (float) width;
        float heightF = (float) mouseEvent.getY() / (float) height;

        float coordX1 = (visible_startx + visDifX * widthF);
        float coordY1 = (visible_starty + visDifY * heightF);

        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;

        w /= ZOOMSTRENGTH;
        h /= ZOOMSTRENGTH;

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        visDifX = visible_endx - visible_startx;
        visDifY = visible_endy - visible_starty;

        float coordX2 = (visible_startx + visDifX * widthF);
        float coordY2 = (visible_starty + visDifY * heightF);

        float moveImageX = coordX2 - coordX1;
        float moveImageY = coordY2 - coordY1;

        moveImage(-moveImageY, -moveImageX);

        forceRefresh();

    }

    protected void zoomOut(MouseEvent mouseEvent, int height, int width) {
        if (mouseEvent == null) {
            return;
        }
        float visDifX = visible_endx - visible_startx;
        float visDifY = visible_endy - visible_starty;

        float widthF = (float) mouseEvent.getX() / (float) width;
        float heightF = (float) mouseEvent.getY() / (float) height;

        float coordX1 = (visible_startx + visDifX * widthF);
        float coordY1 = (visible_starty + visDifY * heightF);

        float x = (visible_endx + visible_startx) / 2;
        float y = (visible_endy + visible_starty) / 2;

        float w = visible_endx - visible_startx;
        float h = visible_endy - visible_starty;

        w *= ZOOMSTRENGTH;
        h *= ZOOMSTRENGTH;

        visible_startx = x - w / 2;
        visible_endx = x + w / 2;
        visible_starty = y - h / 2;
        visible_endy = y + h / 2;

        visDifX = visible_endx - visible_startx;
        visDifY = visible_endy - visible_starty;

        float coordX2 = (visible_startx + visDifX * widthF);
        float coordY2 = (visible_starty + visDifY * heightF);

        float moveImageX = coordX2 - coordX1;
        float moveImageY = coordY2 - coordY1;

        moveImage(-moveImageY, -moveImageX);

        forceRefresh();
    }

    public void switchToCoordinates(int x, int y) {

        float diffVisibleX = this.visible_endx - this.visible_startx;
        float diffVisibleY = this.visible_endy - this.visible_starty;

        this.visible_startx = x - Math.round(diffVisibleX / 2);

        this.visible_endx = x + Math.round(diffVisibleX / 2);

        this.visible_starty = y - Math.round(diffVisibleY / 2);

        this.visible_endy = y + Math.round(diffVisibleY / 2);

        forceRefresh();
    }

    @Override
    public IPainterData getPainterData() {
        return painterData;
    }

    @Override
    public void setPainterData(IPainterData painterData) {
        this.painterData = painterData;
        this.painterData.setWidth(width);
        this.painterData.setHeight(height);
    }

    /**
     * @param appletData the appletData to set
     */
    @Override
    public void setAppletData(IAppletData appletData) {
        this.appletData = appletData;
    }

    public void setMouseEvent(MouseEvent e) {
        mouseEvent = e;
    }

    public int getZFactorAdjust(float inValue, int maxSize) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > maxSize) {
            return maxSize;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    @Override
    public JPanel getPanel() {
        return this;
    }

    public boolean isVisible(Object_OneCoordinate object) {

        if (visible_startx < object.getX1() && visible_endx > object.getX1()
                && visible_starty < object.getY1() && visible_endy > object.getY1()) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public EStarmapMode getMode() {
        return mode;
    }

    @Override
    public void setMode(EStarmapMode mode) {
        this.mode = mode;
    }

    @Override
    public void setDefaultCursor(boolean defaultCursor) {
        setDefaultCursor = defaultCursor;

    }

    @Override
    public void printBondarys() {
        System.out.println("visible_startx : " + visible_startx);
        System.out.println("visible_starty : " + visible_starty);
        System.out.println("visible_endx : " + visible_endx);
        System.out.println("visible_endy : " + visible_endy);

    }

}
