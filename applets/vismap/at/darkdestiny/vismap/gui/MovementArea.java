/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.gui;

import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.logic.DisplayManagement;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author Admin
 */
public class MovementArea extends JPanel {

    private final IPainter painter;
    boolean inside = false;
    private final AreaSide side;
    boolean exited = false;

    public MovementArea(AreaSide side, JComponent parent, final IPainter painter) {
        this.painter = painter;
        this.side = side;
        this.setOpaque(false);
        if (side.equals(AreaSide.TOP)) {
            this.setPreferredSize(new Dimension(parent.getPreferredSize().width - 30, 15));
        } else if (side.equals(AreaSide.LEFT)) {
            this.setPreferredSize(new Dimension(15, parent.getPreferredSize().height - 30));
        } else if (side.equals(AreaSide.RIGHT)) {
            this.setPreferredSize(new Dimension(15, parent.getPreferredSize().height - 30));
        } else if (side.equals(AreaSide.BOTTOM)) {
            this.setPreferredSize(new Dimension(parent.getPreferredSize().width - 30, 15));
        } else {
            this.setPreferredSize(new Dimension(15, 15));
        }

        this.setVisible(true);

    }

    public void setCursor(String imageString) {

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image raw = DisplayManagement.getGraphic(imageString);
        BufferedImage i = DisplayManagement.toBufferedImage(raw, imageString);
        int h = i.getHeight(null);
        int w = i.getWidth(null);

        BufferedImage resultImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

// assume the upperleft corner of the original image is a transparent pixel
        int transparentColor = i.getRGB(0, 0);
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                int color = i.getRGB(x, y);
                if (color == transparentColor) {
                    color = color & 0x00FFFFFF; // clear the alpha flag
                }
                resultImage.setRGB(x, y, color);
            }
        }
        java.awt.Point hotSpot = new java.awt.Point(0, 0);
        if (side.equals(AreaSide.RIGHT)) {

            hotSpot = new java.awt.Point(0, 17);
        } else if (side.equals(AreaSide.BOTTOM)) {

            hotSpot = new java.awt.Point(0, 17);
        }
        Cursor cursor = toolkit.createCustomCursor(resultImage, hotSpot, "Arrow");

        setCursor(cursor);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (side.equals(AreaSide.TOP)) {
            Image i = DisplayManagement.getGraphic("borderTop.png");
            g.drawImage(i, 0, 0, this.getPreferredSize().width, 15, this);
        } else if (side.equals(AreaSide.LEFT)) {
            Image i = DisplayManagement.getGraphic("borderLeft.png");
            g.drawImage(i, 0, 0, 15, this.getPreferredSize().height, this);

        } else if (side.equals(AreaSide.RIGHT)) {
            Image i = DisplayManagement.getGraphic("borderRight.png");
            g.drawImage(i, 0, 0, 15, this.getPreferredSize().height, this);

        } else if (side.equals(AreaSide.BOTTOM)) {
            Image i = DisplayManagement.getGraphic("borderBottom.png");
            g.drawImage(i, 0, 0, this.getPreferredSize().width, 15, this);

        } else if (side.equals(AreaSide.BOTTOM_LEFT)) {

            Image i = DisplayManagement.getGraphic("borderBottomLeft.png");
            g.drawImage(i, 0, 0, 15, 15, this);
        } else if (side.equals(AreaSide.BOTTOM_RIGHT)) {

            Image i = DisplayManagement.getGraphic("borderBottomRight.png");
            g.drawImage(i, 0, 0, 15, 15, this);
        } else if (side.equals(AreaSide.TOP_LEFT)) {

            Image i = DisplayManagement.getGraphic("borderTopLeft.png");
            g.drawImage(i, 0, 0, 15, 15, this);
        } else if (side.equals(AreaSide.TOP_RIGHT)) {

            Image i = DisplayManagement.getGraphic("borderTopRight.png");
            g.drawImage(i, 0, 0, 15, 15, this);
        }
    }

    @Override
    public void setCursor(Cursor cursor) {
        super.setCursor(cursor);
    }

    public enum AreaSide {

        TOP, LEFT, BOTTOM, RIGHT, TOP_LEFT, BOTTOM_LEFT, TOP_RIGHT, BOTTOM_RIGHT
    }
}
