package at.darkdestiny.vismap.buttons;

import at.darkdestiny.vismap.logic.DisplayManagement;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;

/**
 *
 * @author Stefan
 */
public class BlueToggleButton extends JToggleButton {

    /**
     * 
     */
    private static final long serialVersionUID = -2485759902418256130L;
    private static final int defaultWidth = 20;
    private static final int defaultHeight = 20;

    public BlueToggleButton(String tooltip, String image, boolean pressed) {
        this.setMargin(new Insets(0, 0, 0, 0));
        this.setPreferredSize(new Dimension(defaultWidth, defaultHeight));
        this.setToolTipText(tooltip);
        this.createToolTip();
        this.getModel().setPressed(pressed);
        setOpaque(false);
        setBackground(new Color(0, 0, 0, 0));
        this.setForeground(Color.WHITE);
        this.setBorder(null);
        if (image != null) {
            setIcon(new ImageIcon(DisplayManagement.getGraphic(image)));
        }
    }

    public BlueToggleButton(String tooltip, String image, boolean pressed, int width, int height) {
        this.setMargin(new Insets(0, 0, 0, 0));
        this.setPreferredSize(new Dimension(width, height));
        this.setToolTipText(tooltip);
        this.createToolTip();
        this.getModel().setPressed(pressed);
        this.getModel().setSelected(pressed);
        setOpaque(false);
        setBackground(new Color(0, 0, 0, 0));
        this.setForeground(Color.WHITE);
        this.setBorder(null);
        if (image != null) {
            setIcon(new ImageIcon(DisplayManagement.getGraphic(image)));
        }
    }
}
