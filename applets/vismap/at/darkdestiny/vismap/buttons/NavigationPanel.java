/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.buttons;

import at.darkdestiny.vismap.interfaces.IPainter;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

/**
 *
 * @author Admin
 */
public class NavigationPanel extends JPanel{
    
    private DirectionButton button_left = null;
    private DirectionButton button_up = null;
    private DirectionButton button_right = null;
    private DirectionButton button_down = null;
    private DirectionButton button_zoomIn = null;
    private DirectionButton button_zoomOut = null;
    
    
    public NavigationPanel(IPainter painter){
           SpringLayout layout = new SpringLayout();
        this.setLayout(layout);
        button_left = new DirectionButton(DirectionButton.BUTTON_LEFT, painter);
        button_up = new DirectionButton(DirectionButton.BUTTON_UP, painter);
        button_right = new DirectionButton(DirectionButton.BUTTON_RIGHT, painter);
        button_down = new DirectionButton(DirectionButton.BUTTON_DOWN, painter);
        button_zoomIn = new DirectionButton(DirectionButton.ZOOM_IN, painter);
        button_zoomOut = new DirectionButton(DirectionButton.ZOOM_OUT, painter);
        
        
        int size = button_left.getPreferredSize().height;
        
        this.add(button_left);
        this.add(button_up);
        this.add(button_right);
        this.add(button_down);
        
        this.add(button_zoomIn);
        this.add(button_zoomOut);
        
        

        layout.putConstraint(SpringLayout.WEST, button_zoomIn, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, button_zoomIn, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, button_up, 0, SpringLayout.EAST, button_zoomIn);
        layout.putConstraint(SpringLayout.NORTH, button_up, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, button_zoomOut, 0, SpringLayout.EAST, button_up);
        layout.putConstraint(SpringLayout.NORTH, button_zoomOut, 0, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, button_left, 0, SpringLayout.WEST, button_zoomIn);
        layout.putConstraint(SpringLayout.NORTH, button_left, 0, SpringLayout.SOUTH, button_zoomIn);

        layout.putConstraint(SpringLayout.WEST, button_down, 0, SpringLayout.EAST, button_left);
        layout.putConstraint(SpringLayout.NORTH, button_down, 0, SpringLayout.NORTH, button_left);

        layout.putConstraint(SpringLayout.WEST, button_right, 0, SpringLayout.EAST, button_down);
        layout.putConstraint(SpringLayout.NORTH, button_right, 0, SpringLayout.NORTH, button_left);

        
        this.setPreferredSize(new Dimension(size*3, size*2));
        this.setOpaque(false);
        this.setVisible(true);
    }
}
