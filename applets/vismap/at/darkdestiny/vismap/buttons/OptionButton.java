/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.buttons;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;


import at.darkdestiny.vismap.drawable.CentredBackgroundBorder;
import at.darkdestiny.vismap.logic.DisplayManagement;

/**
 *
 * @author Eobane
 */
public class OptionButton extends JButton {

    private final SpringLayout layout;
    private final JComboBox comboBox;

    public OptionButton(String name, JComboBox checkBox) {

        this.comboBox = checkBox;
        this.setPreferredSize(new Dimension(224, 21));
        JLabel nameLabel = new JLabel(name);
        nameLabel.setForeground(Color.white);

        this.layout = new SpringLayout();
        this.setLayout(layout);

        comboBox.setBackground(Color.WHITE);
        this.add(checkBox);
        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic("checkButton.png"), "checkButton.png");
        final Border background = new CentredBackgroundBorder(bi);
        this.add(nameLabel);
        this.setBorder(background);
        this.repaint();

        layout.putConstraint(SpringLayout.NORTH, nameLabel, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, nameLabel, 4, SpringLayout.WEST, this);


        layout.putConstraint(SpringLayout.NORTH, checkBox, 1, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.EAST, checkBox, -1, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, checkBox, 4, SpringLayout.EAST, nameLabel);


    }





      public static boolean hasAlpha(Image image) {

             // If buffered image, the color model is readily available

             if (image instanceof BufferedImage) {return ((BufferedImage)image).getColorModel().hasAlpha();}



             // Use a pixel grabber to retrieve the image's color model;

             // grabbing a single pixel is usually sufficient

             PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);

             try {pg.grabPixels();} catch (InterruptedException e) {}



             // Get the image's color model

             return pg.getColorModel().hasAlpha();

         }
   



    public JComboBox getComboBox() {
        return comboBox;
    }
}
