package at.darkdestiny.vismap.buttons;

import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.logic.DisplayManagement;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.ImageIcon;

/**
 * 
 *
 *
 * @author Stefan
 */
public class BlueButton extends JButton {
    
    
	private static final long serialVersionUID = 2512144058801786723L;

        IPainter painter;
           
    public BlueButton(int width, int height, String img, IPainter painter, String toolTip) {
        this.painter = painter;
        this.setMargin(new Insets(0,0,0,0));
        this.setToolTipText(toolTip);
        this.setPreferredSize(new Dimension(width,height));
        setOpaque(false);
        setBackground(new Color(0,0,0,0));
        this.setForeground(Color.WHITE);
        this.setBorder(null);
        if(img != null){
        setIcon(new ImageIcon(DisplayManagement.getGraphic(img)));
        }
    }   
    public BlueButton(int width, int height, IPainter painter, String toolTip) {
        this.painter = painter;
        this.setMargin(new Insets(0,0,0,0));
        this.setToolTipText(toolTip);
        this.setPreferredSize(new Dimension(width,height));
        setOpaque(false);
        setBackground(new Color(0,0,0,0));
        this.setForeground(Color.WHITE);
        this.setBorder(null);
    }    
    public BlueButton(String tooltip, String image, int width, int height, IPainter painter) {
        this.painter = painter;
        this.setMargin(new Insets(0,0,0,0));
        this.setToolTipText(tooltip);
        this.setPreferredSize(new Dimension(width,height));
        setOpaque(false);
        setBackground(new Color(0,0,0,0));
        this.setForeground(Color.WHITE);
        this.setBorder(null);
        if(image != null){
        setIcon(new ImageIcon(DisplayManagement.getGraphic(image)));
        }
    }    
}
