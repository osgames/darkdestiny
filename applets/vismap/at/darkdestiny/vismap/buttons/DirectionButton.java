/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.buttons;

import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.logic.DisplayManagement;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import javax.swing.ImageIcon;

/**
 *
 * @author Admin
 */
public class DirectionButton extends BlueButton{
    public static final int BUTTON_UP = 1;
    public static final int BUTTON_DOWN = 2;
    public static final int BUTTON_LEFT = 3;
    public static final int BUTTON_RIGHT = 4;
    
    public static final int ZOOM_IN = 5;
    public static final int ZOOM_OUT = 6;
    
    public DirectionButton(int direction, IPainter painter_){
        super(30,30, painter_, "");
            setPreferredSize(new Dimension(30, 30));
            setMargin(new Insets(0, 0, 0, 0));
        switch(direction){
            case(BUTTON_LEFT):
            {
            setName("switchLeft");
            Image buttonImage = DisplayManagement.getGraphic("arrleft.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            setIcon(sendFleetIcon);
            addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    painter.moveImage("left", 1f);
                }
            });
            }
                break;
            case(BUTTON_RIGHT):
            {
            setName("switchRight");
            Image buttonImage = DisplayManagement.getGraphic("arrright.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            setIcon(sendFleetIcon);
            addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    painter.moveImage("right", 1f);
                }
            });
            }
            
                break;
            case(BUTTON_UP):
            {
            setName("switchUp");
            Image buttonImage = DisplayManagement.getGraphic("arrup.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            setIcon(sendFleetIcon);
            addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    painter.moveImage("up", 1f);
                }
            });
            }
                break;
            case(BUTTON_DOWN):
            {
            setName("swithLeft");
            Image buttonImage = DisplayManagement.getGraphic("arrdown.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            setIcon(sendFleetIcon);
            addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    painter.moveImage("down", 1f);
                }
            });
            
            }
                break;
            case(ZOOM_IN):
            {
            setName("zoomIn");
            Image buttonImage = DisplayManagement.getGraphic("zoomIn.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            setIcon(sendFleetIcon);
            addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    painter.zoomIn();
                }
            });
            
            }
                break;
            case(ZOOM_OUT):
            {
            setName("zoomOut");
            Image buttonImage = DisplayManagement.getGraphic("zoomOut.png");
            ImageIcon sendFleetIcon = new ImageIcon(buttonImage);
            setIcon(sendFleetIcon);
            addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    painter.zoomOut();
                }
            });
            
            }
                break;
        }
      
    }
}
