package at.darkdestiny.vismap.starmap;

import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.gui.AppletMain;

import at.darkdestiny.vismap.logic.AppletConstants;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.enumerations.EHyperAreaState;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc_ToolTip;
import at.darkdestiny.vismap.starmap.drawable.Point;
import at.darkdestiny.vismap.starmap.dto.PlanetEntry;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.drawable.Territory;
import at.darkdestiny.vismap.starmap.dto.LineIntersection;
import at.darkdestiny.vismap.starmap.enumerations.ESearchPopupLocation;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import at.darkdestiny.vismap.starmap.panels.SearchPopup;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.geom.*;
import java.awt.image.MemoryImageSource;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Painter extends MainPainter {

    private static final long serialVersionUID = 4354204667927554366L;
    //Radius eines Pixels f�r den der Tooltip angezeigt werden soll
    private static final int MAX_PAINTLEVEL = 10;
    private Area hyperArea;
    private Area hyperAreaBase = null;
    private EHyperAreaState hyperAreaState = EHyperAreaState.NOT_INITIALIZED;
    float[] deltaBase = new float[2];
    float[] offSetBase = new float[2];
    private List<Object_OneCoordinate> tmpSearchResult;
    private String tmpSearchWord = "";
    private int lastSearchId = -1;
    public SystemDesc sendSystem;
    SearchPopup searchPopup = null;
    public boolean intersecting = false;
    public SystemDesc_ToolTip showToolTipFor;
    private static final int TOOLTIP_HEIGHT = 10;
    private static final int TOOLTIP_WIDTH = 10;
    public List<I_Object_OneCoordinate> createdTerritory = new ArrayList();
    public List<I_Object_OneCoordinate> foundTerritory;
    private boolean closePolygon = false;
    public FleetEntry fleetToSend = null;
    public LineIntersection lineIntersection = null;
    private int lastId = -1;
    private boolean grayedOut = false;
    boolean moved = false;
    public boolean drawTerritoryInfo = false;
    public Territory infoTerritory = null;

    /**
     * t
     *
     */
    public Painter(AppletMain main) {

        super(main);
        hyperAreaState = EHyperAreaState.NOT_INITIALIZED;

        if (foundTerritory == null) {
            foundTerritory = new ArrayList();
        }
        this.painterData = new PainterData();
        setCursor(getDefaultCursor());
    }

    @Override
    public void paint(Graphics g) {

        super.prePaint(g);

        float[] res = new float[2];
        float[] delta = new float[2]; //Die L�nge von jeweils einem LJ (d.h. 1), jeweils in Richtung X und Y

        convertCoordinates(0, 0, res);
        convertCoordinates(1, 1, delta);
        delta[0] -= res[0];
        delta[1] -= res[1];

        int[] pos2 = new int[2];
        int[] pos = new int[2];

        if (mode.equals(EStarmapMode.TERRITORY_ADD) && !closePolygon && isStartPoint()) {
            dataChanged = true;
            closePolygon = true;
        }
        if (mode.equals(EStarmapMode.TERRITORY_ADD) && closePolygon && !isStartPoint()) {
            dataChanged = true;
            closePolygon = false;
        }
        if (dataChanged) {

            ((PainterData) painterData).clearFleetsDrawn();

            if (backBuffer == null) {
                backBuffer = createImage(getWidth(), getHeight());
            }
            Graphics gc = backBuffer.getGraphics();

            gc.setColor(Color.black);
            gc.fillRect(0, 0, this.getWidth(), this.getHeight());

            if (fleetToSend != null) {

                convertCoordinates(fleetToSend, pos, pos2);
            }
            if (sendSystem != null) {

                convertCoordinates(sendSystem, pos);
            }

            drawHyperScannerArea();

            //Main Draw procedure Loop all items
            super.paint(g);

            if (createdTerritory != null) {
                Object_OneCoordinate lastPoint = null;
                for (I_Object_OneCoordinate iPoint : createdTerritory) {
                    String territoryImage = "territory.png";
                    Image img = DisplayManagement.getGraphic(territoryImage);

                    Object_OneCoordinate point = (Object_OneCoordinate) iPoint;
                    convertCoordinates(point, pos);

                    if (lastPoint != null) {

                        if (isStartPoint() || isMoved()) {
                            gc.setColor(Color.YELLOW);
                        } else {
                            gc.setColor(Color.GREEN);
                        }
                        gc.drawLine(lastPoint.getActCoord_x1(), lastPoint.getActCoord_y1(), point.getActCoord_x1(), point.getActCoord_y1());
                    }

                    int flagSize = 16;

                    int wx = getZFactorAdjust(flagSize * delta[0], flagSize);
                    int wy = getZFactorAdjust(flagSize * delta[1], flagSize);

                    int width = wx;
                    int height = wy;
                    gc.drawImage(img, point.getActCoord_x1() - getZFactorAdjust(32 * delta[0], 32) / 2, point.getActCoord_y1() - getZFactorAdjust(32 * delta[0], 32) / 2, width, height, null);

                    lastPoint = point;
                }
            }

            dataChanged = false;

        }

        g.drawImage(backBuffer, 0, 0, getWidth(), getHeight(), null);

        if (showToolTipFor != null) {
            showToolTipFor.paint(appletData, g);
        }

        globalRatio = (float) getWidth() / (float) getHeight();

        if (drawTerritoryInfo && infoTerritory != null && mouseEvent != null) {

            String name = "Name : " + infoTerritory.name;
            String description = "Beschreibung : " + infoTerritory.description;

            int longestLine = g.getFontMetrics().stringWidth(name);
            longestLine = Math.max(g.getFontMetrics().stringWidth(description), longestLine);

            g.setColor(Color.LIGHT_GRAY);
            g.fillRect(mouseEvent.getX() + 13, mouseEvent.getY() - 10, longestLine, 20);
            if (infoTerritory.getStatus() == 0) {
                g.setColor(new Color(0f, 0.9f, 0f, 0.8f));
            } else {
                Color c = Color.decode(((AppletData) appletData).getRelations().get(infoTerritory.getStatus()).getColor());
                float[] colors = new float[4];
                c.getRGBColorComponents(colors);
                g.setColor(new Color(colors[0], colors[1], colors[2], 0.8f));
            }
            g.drawRect(mouseEvent.getX() + 13, mouseEvent.getY() - 10, longestLine, 20);
            g.setColor(Color.BLACK);
            g.drawString(name, mouseEvent.getX() + 14, mouseEvent.getY());
            g.drawString(description, mouseEvent.getX() + 14, mouseEvent.getY() + 10);
        }

        if (mode.equals(EStarmapMode.DEFAULT) && ((at.darkdestiny.vismap.starmap.listeners.StarmapMouse) mouse).moveMap && mouseEvent != null) {
            String territoryImage = "arr_move.png";

            Image img = DisplayManagement.getGraphic(territoryImage);

            int width = 40;
            int height = 40;

            g.drawImage(img, this.getPreferredSize().width / 2, (this.getPreferredSize().height / 2), width, height, null);

            int[] pixels = new int[16 * 16];
            Image image = Toolkit.getDefaultToolkit().createImage(
                    new MemoryImageSource(16, 16, pixels, 0, 16));
            Cursor transparentCursor
                    = Toolkit.getDefaultToolkit().createCustomCursor(image, new java.awt.Point(0, 0), "invisibleCursor");
            setCursor(transparentCursor);
        }
        if (mode.equals(EStarmapMode.TERRITORY_ADD) && mouseEvent != null) {
            g.setColor(Color.GREEN);
            int targetX = mouseEvent.getX();
            int targetY = mouseEvent.getY();
            I_Object_OneCoordinate p = getLastPoint();
            if (p != null) {
                Object_OneCoordinate point = (Object_OneCoordinate) p;
                g.drawLine(point.getActCoord_x1(), point.getActCoord_y1(), targetX, targetY);
            }
            String territoryImage = "territory.png";

            if (intersecting) {
                territoryImage = "territory_invalid.png";
            }

            Image img = DisplayManagement.getGraphic(territoryImage);

            int wx = getZFactorAdjust(32 * delta[0], 32);
            int wy = getZFactorAdjust(32 * delta[1], 32);

            int width = wx;
            int height = wy;
            g.drawImage(img, targetX - 25, targetY - 30, width, height, null);
            int[] pixels = new int[16 * 16];
            Image image = Toolkit.getDefaultToolkit().createImage(
                    new MemoryImageSource(16, 16, pixels, 0, 16));
            Cursor transparentCursor
                    = Toolkit.getDefaultToolkit().createCustomCursor(image, new java.awt.Point(0, 0), "invisibleCursor");
            setCursor(transparentCursor);

        }
        if (mode.equals(EStarmapMode.TERRITORY_ADD_POINT) && mouseEvent != null) {
            lineIntersection = findLineIntersection(mouseEvent.getX(), mouseEvent.getY());

            if (lineIntersection != null && findTerritoryEntry(mouseEvent.getX(), mouseEvent.getY()).isEmpty()) {
                int targetX = mouseEvent.getX();
                int targetY = mouseEvent.getY();
                String territoryImage = "territory.png";

                Image img = DisplayManagement.getGraphic(territoryImage);

                int wx = getZFactorAdjust(32 * delta[0], 32);
                int wy = getZFactorAdjust(32 * delta[1], 32);

                int width = wx;
                int height = wy;
                g.drawImage(img, targetX - 25, targetY - 30, width, height, null);

                int[] pixels = new int[16 * 16];
                Image image = Toolkit.getDefaultToolkit().createImage(
                        new MemoryImageSource(16, 16, pixels, 0, 16));
                Cursor transparentCursor
                        = Toolkit.getDefaultToolkit().createCustomCursor(image, new java.awt.Point(0, 0), "invisibleCursor");
                setCursor(transparentCursor);
            } else {

                setDefaultCursor = true;
            }

            int x = (int) convertToFloatX((int) mouseEvent.getX(), (int) mouseEvent.getY());
            int y = (int) convertToFloatY((int) mouseEvent.getX(), (int) mouseEvent.getY());
            g.drawString("Sternenkoordinaten : " + x + "/" + y, mouseEvent.getX() + 14, mouseEvent.getY());
        }
        if (mode.equals(EStarmapMode.SEND_FLEET) && mouseEvent != null) {
            g.setColor(new Color(0.3f, 0.3f, 1f));
            int targetX = mouseEvent.getX();
            int targetY = mouseEvent.getY();
            if (main.mainFrame.getInformationScrollPane() != null && main.mainFrame.getInformationScrollPane().isVisible() && sendSystem != null) {

                targetX = sendSystem.getActCoord_x1();
                targetY = sendSystem.getActCoord_y1();
            } else if (showToolTipFor != null) {
                targetX = showToolTipFor.sd.getActCoord_x1();
                targetY = showToolTipFor.sd.getActCoord_y1();
            }
            double starMapX = convertToFloatX(targetX, targetY);
            double starMapY = convertToFloatY(targetX, targetY);
            double flightDistance = Math.sqrt(Math.pow(fleetToSend.getX1() - starMapX, 2d) + Math.pow(fleetToSend.getY1() - starMapY, 2d));
            double flightDuration = (int) Math.ceil(flightDistance / fleetToSend.getSpeed());

            int diameter = 2 * (int) Math.round(Math.sqrt(Math.pow(Math.abs(fleetToSend.getActCoord_x1() - targetX), 2) + Math.pow(Math.abs(fleetToSend.getActCoord_y1() - targetY), 2)));
            if (flightDistance > fleetToSend.getRange()) {
                g.setColor(Color.red);
            }
            g.drawLine(fleetToSend.getActCoord_x1(), fleetToSend.getActCoord_y1(), targetX, targetY);
            g.drawOval(fleetToSend.getActCoord_x1() - diameter / 2, fleetToSend.getActCoord_y1() - diameter / 2, diameter, diameter);

            String fleetImage = AppletConstants.FLEET_IMAGE_OWN;
            fleetImage += ".gif";
            Image img = DisplayManagement.getGraphic(fleetImage);

            int wx = getZFactorAdjust(32 * delta[0], 32);
            int wy = getZFactorAdjust(32 * delta[1], 32);

            int width = wx;
            int height = wy;
            g.drawImage(img, targetX - 32 / 2, targetY - 32 / 2, width, height, null);
            if (sendSystem == null) {
                int[] pixels = new int[16 * 16];
                Image image = Toolkit.getDefaultToolkit().createImage(
                        new MemoryImageSource(16, 16, pixels, 0, 16));
                Cursor transparentCursor
                        = Toolkit.getDefaultToolkit().createCustomCursor(image, new java.awt.Point(0, 0), "invisibleCursor");
                setCursor(transparentCursor);
            } else {

                setDefaultCursor = true;
            }
            g.drawString("Entfernung : " + Math.round(flightDistance * 100d) / 100d, targetX + 14, targetY);
            g.drawString("Flugdauer : " + flightDuration, targetX + 14, targetY + 12);
        }

        if (setDefaultCursor) {
            setCursor(getDefaultCursor());
            setDefaultCursor = false;
        }
        super.postPaint(g);
//g.drawImage(backBuffer, 0, 0, getWidth(), getHeight(), null);
        if (mouse.active) {
            mouse.paint(this, g);
        }
        if (grayedOut) {

            g.setColor(new Color(1f, 1f, 1f, 0.5f));
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
        }
        if (!main.isInitialized() && !main.isReloaded()) {
            if (((PainterData) painterData).getHomeSystem() != null) {
                /* for (int i = 0; i < 30; i++) {
                 zoomIn((int) painterData.getHomegetActCoord_x1ctCoord_x(), (int) painterData.getHomegetActCoord_y1ctCoord_y(), this.getHeight(), this.getWidth());
                 }*
                 *
                 */
                zoomIn((int) ((PainterData) painterData).getHomeSystem().getActCoord_x1(), (int) ((PainterData) painterData).getHomeSystem().getActCoord_y1(), this.getHeight(), this.getWidth());

                switchToCoordinates((int) ((PainterData) painterData).getHomeSystem().getX1(), (int) ((PainterData) painterData).getHomeSystem().getY1());

                main.setInitialized(true);
                main.setReloaded(false);
                dataChanged = true;
                repaint();
            } else {

                switchToCoordinates(AppletConstants.UNIVERSE_SIZE_X / 2, AppletConstants.UNIVERSE_SIZE_Y / 2);
                main.setInitialized(true);
                main.setReloaded(false);
                dataChanged = true;
                repaint();
            }
        }

    }



    public void drawHyperScannerArea() {

        float[] res = new float[2];
        float[] delta = new float[2]; //Die L�nge von jeweils einem LJ (d.h. 1), jeweils in Richtung X und Y

        Graphics gc = backBuffer.getGraphics();
        convertCoordinates(0, 0, res);
        convertCoordinates(1, 1, delta);
        delta[0] -= res[0];
        delta[1] -= res[1];

        int[] pos2 = new int[2];
        int[] pos = new int[2];

        if (((PainterData)main.mainFrame.getPainter().getPainterData()).isShowHyperScanner()) {
            for (I_Object_OneCoordinate od : ((PainterData) painterData).getScanners()) {
                convertCoordinates((Object_OneCoordinate) od, pos);
            }
            this.buildScannerArea(delta);
            Graphics2D g2 = (Graphics2D) gc;
            g2.setColor(Color.RED);
            g2.draw(hyperArea);

        }

    }

    private void initializeScannerArea(float[] delta) {
        /*
         float[] delta = new float[2];
         delta[0] = 1f;
         delta[1] = 1f;
         */

        hyperAreaState = EHyperAreaState.INITIALIZING;

        deltaBase[0] = delta[0];
        deltaBase[1] = delta[1];

        offSetBase[0] = visible_startx;
        offSetBase[1] = visible_starty;

        buildScannerArea(delta);
        hyperAreaBase = (Area) hyperArea.clone();

        hyperAreaState = EHyperAreaState.INITIALIZED;
    }

    private void buildScannerArea(float[] delta) {
        if (hyperAreaState == EHyperAreaState.NOT_INITIALIZED) {
            initializeScannerArea(delta);

            AffineTransform at = new AffineTransform();
            at.scale(delta[0] / deltaBase[0], delta[1] / deltaBase[1]);

            hyperArea = hyperAreaBase.createTransformedArea(at);
        } else if (hyperAreaState == EHyperAreaState.INITIALIZING) {
            hyperArea = new Area();

            Area workingArea = new Area();
            // Area workingAreaInner = new Area();
            // Area combinedAreaInner = new Area();

            int workCounter = 0;

            for (int i = 0; i < ((PainterData) painterData).getScanners().size(); i++) {
                if (workCounter > 10) {
                    hyperArea.add(workingArea);
                    // combinedAreaInner.add(workingAreaInner);

                    workingArea = new Area();
                    // workingAreaInner = new Area();

                    workCounter = 0;
                }

                workCounter++;

                Ellipse2D currEllipse = new Ellipse2D.Double(Math.round(
                        ((PainterData) painterData).getScanners().get(i).getActCoord_x1() - delta[0] * AppletConstants.HYPERSCANNER_RANGE - 1),
                        Math.round(((PainterData) painterData).getScanners().get(i).getActCoord_y1() - delta[1] * AppletConstants.HYPERSCANNER_RANGE - 1),
                        Math.round(delta[0] * 2 * AppletConstants.HYPERSCANNER_RANGE + 1),
                        Math.round(delta[1] * 2 * AppletConstants.HYPERSCANNER_RANGE + 1));

                Area tmpArea1 = new Area(currEllipse);
                workingArea.add(tmpArea1);
            }

            hyperArea.add(workingArea);
        } else {
            int[] offSet = getXYOffset(offSetBase);

            hyperArea = (Area) hyperAreaBase.clone();
            // System.out.println("deltaBase[0] = " + deltaBase[0] + " delta[0] = " + delta[0]);
            AffineTransform at = new AffineTransform();
            at.translate(offSet[0], offSet[1]);

            AffineTransform at2 = new AffineTransform();
            at2.scale(delta[0] / deltaBase[0], delta[1] / deltaBase[1]);

            hyperArea.transform(at2);
            hyperArea.transform(at);
        }

        // hyperArea.subtract(combinedAreaInner);
    }

    public Cursor getDefaultCursor() {

        if (defaultCursor != null) {
            return defaultCursor;
        }

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image raw = DisplayManagement.getGraphic("mouse_default.png");
        java.awt.Point hotSpot = new java.awt.Point(0, 0);
        //  defaultCursor = toolkit.createCustomCursor(raw, hotSpot, "Arrow");
        defaultCursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);

        return defaultCursor;
    }

    public void switchToSystemName(String name) {
        SystemDesc tmp;
        boolean sameWord = false;
        if (tmpSearchWord.equals(name)) {
            sameWord = true;
        } else {
            tmpSearchResult = new ArrayList();
        }
        tmpSearchWord = name;

        if (!sameWord) {

            for (I_Object_OneCoordinate entry : painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_SYSTEMS)) {

                if (entry instanceof SystemDesc) {
                    tmp = (SystemDesc) entry;

                    if (tmp.getName().equals(name)) {
                        tmpSearchResult.add(tmp);
                    }
                }
            }
        }
        if (tmpSearchResult.size() == 1 && !sameWord) {
            Object_OneCoordinate sd = tmpSearchResult.get(0);
            SystemDesc tmp2 = (SystemDesc) sd;
            zoomRectScaled(tmp2.getX1() - 200, tmp2.getY1() - 200, 400, 400);
            //  switchToCoordinates(Math.round(tgetX1getX()), Math.round(tmgetY1etY()));
            lastSearchId = tmp2.getId();
            showPopUp(false);
        } else if (tmpSearchResult.size() > 1 && !sameWord) {

            Object_OneCoordinate sd = tmpSearchResult.get(0);
            SystemDesc tmp2 = (SystemDesc) sd;
            zoomRectScaled(tmp2.getX1() - 200, tmp2.getY1() - 200, 400, 400);
            //switchToCoordinates(Math.roundgetX12.getX()), Math.round(getY1.getY()));
            lastSearchId = tmp2.getId();
            tmp2.setMarked(true);
            searchPopup.setMessage("Found : " + tmpSearchResult.size() + " matches showing (1/" + tmpSearchResult.size() + ")", false);
            lastId = 0;
            relocateSearchPopup(ESearchPopupLocation.TOP);
        } else if (tmpSearchResult.size() == 1 && sameWord) {
            Object_OneCoordinate sd = tmpSearchResult.get(0);
            SystemDesc tmp2 = (SystemDesc) sd;
            zoomRectScaled(tmp2.getX1() - 200, tmp2.getY1() - 200, 400, 400);
            //  switchToCoordinates(Math.rougetX1mp2.getX()), Math.roungetY1p2.getY()));
            lastSearchId = tmp2.getId();
        } else if (tmpSearchResult.size() > 1 && sameWord) {

            SystemDesc tmp1 = (SystemDesc) tmpSearchResult.get(lastId);
            tmp1.setMarked(false);
            int nextId = lastId + 1;
            if (nextId == tmpSearchResult.size()) {
                nextId = 0;
            }

            SystemDesc tmp2 = (SystemDesc) tmpSearchResult.get(nextId);
            tmp2.setMarked(true);
            zoomRectScaled(tmp2.getX1() - 200, tmp2.getY1() - 200, 400, 400);
            //  switchToCoordinates(Math.rgetX1(tmp2.getX()), Math.rogetY1tmp2.getY()));
            searchPopup.setMessage("Found : " + tmpSearchResult.size() + " matches showing (" + (nextId + 1) + "/" + tmpSearchResult.size() + ")", false);
            lastId = nextId;

        } else {
            searchPopup.setMessage("System: " + name + " not found", true);
        }
    }

    public void switchToSystemId(int systemId) {
        SystemDesc tmp;
        SystemDesc found = null;

        System.out.println("Searching");

        for (I_Object_OneCoordinate entry : painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_SYSTEMS)) {
            if (entry instanceof SystemDesc) {
                tmp = (SystemDesc) entry;
                if (tmp.getId() == systemId) {
                    found = tmp;
                    tmp.setMarked(true);
                } else {
                    tmp.setMarked(false);
                }

            }
        }
        System.out.println("found : " + found);
        if (found != null) {
            zoomRectScaled(found.getX1() - 200, found.getY1() - 200, 400, 400);
            // switchToCoordinates(Math.getX1d(found.getX()), Math.rgetY1(found.getY()));
            showPopUp(false);
        } else {
            searchPopup.setMessage("System# " + systemId + " not found", true);
        }
    }

    public void switchToPlanetName(String name) {
        SystemDesc tmp;
        boolean sameWord = false;
        if (tmpSearchWord.equals(name)) {
            sameWord = true;
        } else {
            tmpSearchResult = new ArrayList();
        }
        tmpSearchWord = name;

        if (!sameWord) {

            for (I_Object_OneCoordinate entry : painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_SYSTEMS)) {
                if (entry instanceof SystemDesc) {

                    tmp = (SystemDesc) entry;

                    for (PlanetEntry planetEntry : tmp.getPlanetList()) {
                        if (planetEntry.getName().equals(name)) {
                            tmpSearchResult.add(tmp);

                        }
                    }
                }
            }
        }
        if (tmpSearchResult.size() == 1 && !sameWord) {
            Object_OneCoordinate sd = tmpSearchResult.get(0);
            SystemDesc tmp2 = (SystemDesc) sd;
            zoomRectScaled(tmp2.getX1() - 200, tmp2.getY1() - 200, 400, 400);
            //  switchToCoordinates(MagetX1ound(tmp2.getX()), MatgetY1und(tmp2.getY()));
            lastSearchId = tmp2.getId();
            showPopUp(false);
        } else if (tmpSearchResult.size() > 1 && !sameWord) {
            Object_OneCoordinate sd = tmpSearchResult.get(0);
            SystemDesc tmp2 = (SystemDesc) sd;
            zoomRectScaled(tmp2.getX1() - 200, tmp2.getY1() - 200, 400, 400);
            // switchToCoordinates(getX1.round(tmp2.getX()), MgetY1round(tmp2.getY()));
            lastSearchId = tmp2.getId();
            tmp2.setMarked(true);
            searchPopup.setMessage("Found : " + tmpSearchResult.size() + " matches showing (1/" + tmpSearchResult.size() + ")", false);
            lastId = 0;
            relocateSearchPopup(ESearchPopupLocation.TOP);
        } else if (tmpSearchResult.size() == 1 && sameWord) {
            Object_OneCoordinate sd = tmpSearchResult.get(0);
            SystemDesc tmp2 = (SystemDesc) sd;
            zoomRectScaled(tmp2.getX1() - 200, tmp2.getY1() - 200, 400, 400);
            //  switchToCoordinategetX1th.round(tmp2.getX()),getY1h.round(tmp2.getY()));
            lastSearchId = tmp2.getId();
        } else if (tmpSearchResult.size() > 1 && sameWord) {

            SystemDesc tmp1 = (SystemDesc) tmpSearchResult.get(lastId);
            tmp1.setMarked(false);
            int nextId = lastId + 1;
            if (nextId == tmpSearchResult.size()) {
                nextId = 0;
            }

            SystemDesc tmp2 = (SystemDesc) tmpSearchResult.get(nextId);
            tmp2.setMarked(true);
            zoomRectScaled(tmp2.getX1() - 200, tmp2.getY1() - 200, 400, 400);
            //  switchToCoordinagetX1Math.round(tmp2.getX()getY1ath.round(tmp2.getY()));
            searchPopup.setMessage("Found : " + tmpSearchResult.size() + " matches showing (" + (nextId + 1) + "/" + tmpSearchResult.size() + ")", false);
            lastId = nextId;

        } else {
            searchPopup.setMessage("Planet: " + name + " not found", true);
        }

    }

    public void relocateSearchPopup(ESearchPopupLocation location) {
        if (location.equals(ESearchPopupLocation.CENTER)) {

            searchPopup.pack();
            searchPopup.setLocationRelativeTo(this);
        } else if (location.equals(ESearchPopupLocation.TOP)) {
            grayedOut = false;
            searchPopup.setLocation(this.getPreferredSize().width / 2 - searchPopup.getPreferredSize().width / 2, 50);
            // searchPopup.revalidate();
        }
    }

    // TODO
    public void switchToPlanetId(int planetId) {

        SystemDesc tmp = null;

        for (I_Object_OneCoordinate entry : painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_SYSTEMS)) {
            if (entry instanceof SystemDesc) {

                for (PlanetEntry planetEntry : ((SystemDesc) entry).getPlanetList()) {
                    if (planetEntry.getId() == planetId) {
                        tmp = (SystemDesc) entry;
                        tmp.setMarked(true);
                        break;
                    }
                    ((SystemDesc) entry).setMarked(false);
                }
            }
        }

        if (tmp != null) {

            zoomRectScaled(tmp.getX1() - 200, tmp.getY1() - 200, 400, 400);
            // zoomRect(tmp.getX() - 200, tmp.getY() - 200, tmp.getX() + 200, tmp.getY() + 200);

            // switchToCoordinates(Math.round(tmp.getX()), Math.round(tmp.getY()));
            showPopUp(false);
        } else {
            searchPopup.setMessage("Planet# " + planetId + " not found", true);
        }
    }

    public void setFleet(FleetEntry fe) {
        fleetToSend = fe;
        dataChanged = true;
        main.mainFrame.setInfoVisibility(false);
        mode = EStarmapMode.SEND_FLEET;

    }

    public void setSendSystem(SystemDesc sd) {
        sendSystem = sd;
    }

    @Override
    public void setMode(EStarmapMode mode) {

        if (mode.equals(EStarmapMode.TERRITORY_ADD)) {
            createdTerritory = new ArrayList();
        }
        this.mode = mode;
    }

    public void addPoint(int x, int y) {;
        double starMapX = convertToFloatX(x, y);
        double starMapY = convertToFloatY(x, y);
        int id = 0;
        if (!createdTerritory.isEmpty()) {
            id = createdTerritory.size();
        }
        Point p = new Point(id, String.valueOf(id), (int) starMapX, (int) starMapY);
        createdTerritory.add(p);
    }

    public I_Object_OneCoordinate getLastPoint() {
        if (createdTerritory.isEmpty()) {
            return null;
        }
        return createdTerritory.get(createdTerritory.size() - 1);
    }

    public boolean isIntersecting(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        float starMapX = convertToFloatX(x, y);
        float starMapY = convertToFloatY(x, y);
        I_Object_OneCoordinate iLastPoint = getLastPoint();
        Object_OneCoordinate lastPoint;
        if (iLastPoint != null) {
            lastPoint = (Object_OneCoordinate) iLastPoint;
        } else {
            return false;
        }
        if (createdTerritory.size() <= 2) {
            return false;
        }
        Object_OneCoordinate previousPoint = null;
        int i = 1;
        for (I_Object_OneCoordinate IPoint : createdTerritory) {
            Object_OneCoordinate point = (Object_OneCoordinate) IPoint;
            if (i == createdTerritory.size()) {
                return false;
            }
            if (previousPoint != null) {

                Line2D l1 = new Line2D.Float(previousPoint.getX1(), previousPoint.getY1(), point.getX1(), point.getY1());
                Line2D l2 = new Line2D.Float(lastPoint.getX1(), lastPoint.getY1(), starMapX, starMapY);
                // return (isIntersection((Point) previousPoint, (Point) point, (Point) lastPoint, new Point("comparePoint", (int) starMapX, (int) starMapY)));
                if (l1.intersectsLine(l2)) {
                    return true;
                }
            }
            previousPoint = point;
            i++;
        }
        return false;
    }

    public boolean mooveIntersectionCheck(Point p) {
        List<I_Object_OneCoordinate> entries = getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_TERRITORY);

        if (entries == null) {
            return false;
        }

        for (I_Object_OneCoordinate terr : entries) {
            Territory t = (Territory) terr;

            List<Line2D> lines = new ArrayList();
            List<Line2D> compareLines = new ArrayList();

            List<Point> points = new ArrayList();
            points.addAll(t.getPoints().values());
            int size = points.size();
            for (int i = 0; i < size; i++) {
                Point point = points.get(i);

                Point nextPoint = null;
                // point is last Point take first point
                if (i == size - 1) {
                    nextPoint = points.get(0);
                } else {
                    nextPoint = points.get(i + 1);
                }
                Line2D line = new Line2D.Float(point.getX1(), point.getY1(), nextPoint.getX1(), nextPoint.getY1());

                if (comparePoints(point, p) || comparePoints(nextPoint, p)) {
                    compareLines.add(line);
                    continue;
                }

                lines.add(line);
            }
            for (Line2D cLine : compareLines) {
                for (Line2D line : lines) {
                    if (cLine.getP1().equals(line.getP1()) || cLine.getP1().equals(line.getP2())
                            || cLine.getP2().equals(line.getP1()) || cLine.getP2().equals(line.getP2())) {
                        continue;
                    }
                    if (line.intersectsLine(cLine)) {
                        return true;
                    }

                }
            }

        }
        return false;
    }

    public boolean isIntersection(Point pStart1, Point pEnd1, Point pStart2, Point pEnd2) {
        Line2D l1 = new Line2D.Float(pStart1.getX1(), pStart1.getY1(), pEnd1.getX1(), pEnd1.getY1());
        Line2D l2 = new Line2D.Float(pStart2.getX1(), pStart2.getY1(), pEnd2.getX1(), pEnd2.getY1());
        return l1.intersectsLine(l2);

    }

    public void createTerritory() {
        Territory territory = new Territory(this, true);
        for (I_Object_OneCoordinate points : createdTerritory) {
            Object_OneCoordinate point = (Object_OneCoordinate) points;
            territory.addPoint(point.getId(), point.getId(), points.getIdentifier(), (int) point.getX1(), (int) point.getY1());
        }
        painterData.addDrawObject(AppletConstants.DRAW_LEVEL_TERRITORY, territory);

        mode = EStarmapMode.DEFAULT;
        setDefaultCursor = true;
        foundTerritory.clear();
        createdTerritory.clear();
    }

    public I_Object_OneCoordinate getFirstPoint() {
        if (createdTerritory == null || createdTerritory.isEmpty()) {
            return null;
        } else {
            return createdTerritory.get(0);
        }
    }

    public boolean isStartPoint() {
        if (foundTerritory == null || foundTerritory.isEmpty()) {
            return false;
        }
        I_Object_OneCoordinate iFirstPoint = getFirstPoint();
        Object_OneCoordinate firstPoint = (Object_OneCoordinate) iFirstPoint;
        for (I_Object_OneCoordinate iPoint : foundTerritory) {

            Object_OneCoordinate point = (Object_OneCoordinate) iPoint;
            if (firstPoint.getX1() == point.getX1() && firstPoint.getY1() == point.getY1()) {
                return true;
            }

        }
        return false;
    }

    public boolean isMoved() {
        return moved;
    }

    public void setMoved(boolean moved) {
        this.moved = moved;
    }

    public void removeLastPoint() {
        if (!createdTerritory.isEmpty()) {
            createdTerritory.remove(createdTerritory.size() - 1);
            dataChanged = true;
            forceRefresh();
        }
    }

    public void removeSelectedPoint() {

        if (foundTerritory == null || foundTerritory.isEmpty()) {
            return;
        }
        I_Object_OneCoordinate iFoundPoint = foundTerritory.get(0);
        Object_OneCoordinate foundPoint = (Object_OneCoordinate) iFoundPoint;
        List<I_Object_OneCoordinate> entries = getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_TERRITORY);
        if (entries == null) {
            return;
        }
        boolean found = false;
        for (I_Object_OneCoordinate terr : entries) {
            Territory t = (Territory) terr;
            if (t.getPoints().size() < 3) {
                return;
            }
            TreeMap<Integer, Point> original = (TreeMap<Integer, Point>) t.getPoints().clone();
            for (Map.Entry<Integer, Point> entry : t.getPoints().entrySet()) {
                Point point = entry.getValue();
                if (foundPoint.getX1() == point.getX1() && foundPoint.getY1() == point.getY1()) {
                     t.getPoints().remove(point);
                    found = true;
                    break;
                }
            }
            if (found) {
                boolean intersects = t.hasIntersections();
                if (intersects) {
                    //reroll
                    t.setPoints(original);
                }
                break;
            }

        }

    }

    public LineIntersection findLineIntersection(float mouseX, float mouseY) {
        List<I_Object_OneCoordinate> territoryEntries = getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_TERRITORY);

        if (territoryEntries == null) {
            return null;
        }
        float maxdist = 2;
        float x = convertToFloatX((int) mouseX, (int) mouseY);
        float y = convertToFloatY((int) mouseX, (int) mouseY);

        Rectangle2D selection = new Rectangle2D.Float(x - maxdist, y - maxdist, maxdist * 2, maxdist * 2);

        for (I_Object_OneCoordinate terr : territoryEntries) {
            Territory t = (Territory) terr;
            if (t.getStatus() != 0) {
                continue;
            }
            ArrayList<Point> points = new ArrayList();
            points.addAll(t.getPoints().values());

            Point lastPoint = null;

            for (Point point : points) {
                if (lastPoint == null) {
                    lastPoint = points.get(points.size() - 1);
                }
                Line2D l = new Line2D.Float(lastPoint.getX1(), lastPoint.getY1(), point.getX1(), point.getY1());

                if (selection.intersectsLine(l)) {
                    LineIntersection li = new LineIntersection(t, lastPoint, point);
                    t.setMarked(true);
                    return li;
                }

                lastPoint = point;

            }
            t.setMarked(false);

        }

        return null;

    }

    public Point createPoint(Integer id, String identifier, int x, int y) {

        float conX = convertToFloatX(x, y);
        float conY = convertToFloatY(x, y);
        Point p = new Point(id, identifier, (int) conX, (int) conY);
        return p;

    }

    public ArrayList<I_Object_OneCoordinate> findTerritoryEntry(int x, int y) {
        if (mode.equals(EStarmapMode.TERRITORY_ADD)) {
            List<I_Object_OneCoordinate> entries = createdTerritory;
            if (entries == null) {
                return new ArrayList();
            }
            float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 2 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 2);

            ArrayList<I_Object_OneCoordinate> result = new ArrayList<I_Object_OneCoordinate>();
            for (I_Object_OneCoordinate obj : entries) {

                Object_OneCoordinate point = (Object_OneCoordinate) obj;
                float halfX = Math.round(point.getActCoord_x1());
                float halfY = Math.round(point.getActCoord_y1());

                float dist = ((x - halfX) * (x - halfX) + (y - halfY) * (y - halfY));
                if (dist < maxdist) {
                    result.add(obj);
                }

            }

            return result;
        } else if (mode.equals(EStarmapMode.TERRITORY_MOVE)) {

            List<I_Object_OneCoordinate> entries = getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_TERRITORY);
            if (entries == null) {
                return new ArrayList();
            }
            float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 2 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 2);

            ArrayList<I_Object_OneCoordinate> result = new ArrayList();
            for (I_Object_OneCoordinate terr : entries) {
                boolean found = false;
                Territory t = (Territory) terr;
                if (t.getStatus() != 0) {
                    continue;
                }
                for (Map.Entry<Integer, Point> entry : t.getPoints().entrySet()) {
                    Point point = entry.getValue();
                    float halfX = Math.round(point.getActCoord_x1());
                    float halfY = Math.round(point.getActCoord_y1());

                    float dist = ((x - halfX) * (x - halfX) + (y - halfY) * (y - halfY));
                    if (dist < maxdist) {
                        result.add(point);
                        found = true;
                    }
                }
                t.setMarked(found);

            }
            return result;

        } else if (mode.equals(EStarmapMode.DEFAULT) && ((PainterData) painterData).isShowTerritory()) {

            List<I_Object_OneCoordinate> entries = getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_TERRITORY);
            if (entries == null) {
                return new ArrayList();
            }
            float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 2 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 2);

            ArrayList<I_Object_OneCoordinate> result = new ArrayList();
            for (I_Object_OneCoordinate terr : entries) {
                Territory t = (Territory) terr;
                for (Map.Entry<Integer, Point> entry : t.getPoints().entrySet()) {
                    Point point = entry.getValue();
                    float halfX = Math.round(point.getActCoord_x1());
                    float halfY = Math.round(point.getActCoord_y1());

                    float dist = ((x - halfX) * (x - halfX) + (y - halfY) * (y - halfY));
                    if (dist < maxdist) {
                        result.add(point);
                        drawTerritoryInfo = true;
                        infoTerritory = t;
                        return result;
                    }
                }

            }

            drawTerritoryInfo = false;
            infoTerritory = null;
            return null;

        }
        return new ArrayList();
    }


    private boolean comparePoints(Point p1, Point p2) {
        if (p1.getX1() == p2.getX1() && p1.getY1() == p2.getY1()) {
            return true;
        } else {
            return false;
        }
    }

    public void showPopUp(boolean showPopUp) {
        if (showPopUp && searchPopup == null) {
            searchPopup = new SearchPopup(this, appletData.getLocale());
            searchPopup.pack();
            searchPopup.setLocationRelativeTo(this);
            grayedOut = true;
            forceRefresh();
        } else {
            grayedOut = false;
            searchPopup.setVisible(false);
            searchPopup.dispose();
            searchPopup = null;
            forceRefresh();
        }
    }
}
