/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_TwoCoordinates;
import at.darkdestiny.vismap.dto.IAppletData;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author HorstRabe
 */
public class TransmitterRoute extends Object_TwoCoordinates {

    private int startId;
    private int endId;

    public TransmitterRoute(int id, int startId, int endId, float x1, float y1, float x2, float y2) {
        super(id, (int) x1, (int) y1, (int) x2, (int) y2);
        this.id = id;
        this.startId = startId;
        this.endId = endId;

    }

    @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {


        gc.setColor(Color.ORANGE.darker());
        Graphics2D g2d = (Graphics2D) gc;
        float[] dashPattern = {30, 10, 10, 10};
        int width = 1;
        getZFactorAdjust(width, 2);
        g2d.setStroke(new BasicStroke(width, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10,
                dashPattern, 0));
        g2d.drawLine((int) (actCoord_x),
                (int) (actCoord_y),
                (int) (actCoord_x2),
                (int) (actCoord_y2));
        g2d.setStroke(new BasicStroke());


    }

    @Override
    public String getIdentifier() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getZFactorAdjust(float inValue, int maxSize) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > maxSize) {
            return maxSize;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }
}
