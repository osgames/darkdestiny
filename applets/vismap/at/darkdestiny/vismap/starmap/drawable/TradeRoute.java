/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_TwoCoordinates;
import at.darkdestiny.vismap.dto.IAppletData;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.starmap.enumerations.ETradeRouteStatus;
import at.darkdestiny.vismap.starmap.dto.trade.TradeSystem;
import at.darkdestiny.vismap.starmap.panels.trade.RoutePanel;
import java.util.List;

/**
 *
 * @author Eobane
 */
public class TradeRoute extends Object_TwoCoordinates {

    private int id;
    private int userId;
    private int targetUserId;
    private int startSystemId;
    private int endSystemId;
    private String startSystemName;
    private String endSystemName;
    private double efficiency;
    private int capacity;
    private ETradeRouteStatus status;
    private boolean external;
    private AppletMain mainApplet;
    private float MAXIMG_SIZE = 10;

    private List<TradeSystem> systems;

    public TradeRoute(int x1, int y1, int x2, int y2, AppletMain main){
        super(0, x1, y1, x2, y2);
        this.mainApplet = main;
        systems = new ArrayList();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the targetUserId
     */
    public int getTargetUserId() {
        return targetUserId;
    }

    /**
     * @param targetUserId the targetUserId to set
     */
    public void setTargetUserId(int targetUserId) {
        this.targetUserId = targetUserId;
    }

    /**
     * @return the efficiency
     */
    public double getEfficiency() {
        return efficiency;
    }

    /**
     * @param efficiency the efficiency to set
     */
    public void setEfficiency(double efficiency) {
        this.efficiency = efficiency;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @return the details
     */
    /**
     * @return the startSystemId
     */
    public int getStartSystemId() {
        return startSystemId;
    }

    /**
     * @param startSystemId the startSystemId to set
     */
    public void setStartSystemId(int startSystemId) {
        this.startSystemId = startSystemId;
    }

    /**
     * @return the endSystemId
     */
    public int getEndSystemId() {
        return endSystemId;
    }

    /**
     * @param endSystemId the endSystemId to set
     */
    public void setEndSystemId(int endSystemId) {
        this.endSystemId = endSystemId;
    }

    /**
     * @return the startSystemName
     */
    public String getStartSystemName() {
        return startSystemName;
    }

    /**
     * @param startSystemName the startSystemName to set
     */
    public void setStartSystemName(String startSystemName) {
        this.startSystemName = startSystemName;
    }

    /**
     * @return the endSystemName
     */
    public String getEndSystemName() {
        return endSystemName;
    }

    /**
     * @param endSystemName the endSystemName to set
     */
    public void setEndSystemName(String endSystemName) {
        this.endSystemName = endSystemName;
    }

    /**
     * @return the external
     */
    public boolean isExternal() {
        return external;
    }

    /**
     * @param external the external to set
     */
    public void setExternal(boolean external) {
        this.external = external;
    }

    /**
     * @return the status
     */
    public ETradeRouteStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(ETradeRouteStatus status) {
        this.status = status;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {
        if (external) {
            gc.setColor(Color.blue);
        } else {
            gc.setColor(Color.orange);
        }
        gc.drawLine((int) (this.actCoord_x),
                (int) (this.actCoord_y),
                (int) (this.actCoord_x2),
                (int) (this.actCoord_y2));


        float halfX = Math.round((this.actCoord_x2 + this.actCoord_x) / 2);
        float halfY = Math.round((this.actCoord_y2 + this.actCoord_y) / 2);

        int wx = getZFactorAdjust(15 * delta[0]);
        int wy = getZFactorAdjust(15 * delta[1]);
        int size = Math.min(wx, wy);
        
        gc.setColor(Color.BLACK);
        gc.fillOval(((int) halfX - size / 2) - 1, ((int) halfY - size / 2) - 1, size + 2, size + 2);

        gc.setColor(Color.ORANGE);

        gc.fillOval((int) halfX - size / 2, (int) halfY - size / 2, size, size);

    }

    public String getIdentifier() {
        return "trade-" + id;
    }


    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > MAXIMG_SIZE) {
            return (int) MAXIMG_SIZE;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    @Override
    public JPanel getPanel(){

        return new RoutePanel(this, mainApplet);
    }

    /**
     * @return the systems
     */
    public List<TradeSystem> getSystems() {
        return systems;
    }

    /**
     * @param systems the systems to set
     */
    public void setSystems(ArrayList<TradeSystem> systems) {
        this.systems = systems;
    }
}
