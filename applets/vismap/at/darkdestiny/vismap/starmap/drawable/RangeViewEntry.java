/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.logic.DisplayManagement;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author LAZYPAD
 */
public class RangeViewEntry extends Object_OneCoordinate {

    private int range;

    public RangeViewEntry(int range, int x, int y) {
        super(0, "", x, y, null);
        this.range = range;
    }

    //ovdbg @Override
    @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {
        gc.setColor(DisplayManagement.systemViewRange);
        gc.fillOval(Math.round(this.getActCoord_x1() - delta[0] * range),
                Math.round(this.getActCoord_y1() - delta[1] * range),
                Math.round(delta[0] * 2 * range),
                Math.round(delta[1] * 2 * range));

    }

    @Override
    public String getIdentifier() {
        return ("SystemView");
    }

    @Override
    public JPanel getPanel() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
