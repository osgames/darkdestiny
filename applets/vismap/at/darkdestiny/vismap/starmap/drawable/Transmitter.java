/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Bullet
 */
public class Transmitter extends Object_OneCoordinate {

    private int id;
    private int systemId;
    private int planetId;
    private int userId;
    private List<Integer> targetTransmitterIds;
    private boolean transitAllowed = false;

    public Transmitter(int id, int systemId, int planetId, int userId, int x, int y, boolean transitAllowed,  List<Integer> targetTransmitterIds) {
        super(id, "", x, y, null);
        this.id = id;
        this.systemId = systemId;
        this.planetId = planetId;
        this.userId = userId;
        this.targetTransmitterIds = targetTransmitterIds;
        this.transitAllowed = transitAllowed;
    }

    @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {

        gc.setColor(Color.ORANGE.darker());
        Graphics2D g2d = (Graphics2D) gc;
        float[] dashPattern = {10, 10};
        int width = (int) (30 * delta[0]);
        g2d.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_ROUND, 1,
                dashPattern, 0));
        Ellipse2D.Double circle = new Ellipse2D.Double(this.getActCoord_x1() - width / 2,
                this.getActCoord_y1() - width / 2,
                (int) (width),
                (int) (width));
        g2d.draw(circle);

        g2d.setStroke(new BasicStroke());

    }

    @Override
    public String getIdentifier() {
        return ("triangle");
    }

    @Override
    public JPanel getPanel() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getSystemId() {
        return systemId;
    }

    /**
     * @return the planetId
     */
    public int getPlanetId() {
        return planetId;
    }

    public String toString(){
        return "Transmitter " + id;
    }

    /**
     * @return the targetTransmitterIds
     */
    public List<Integer> getTargetTransmitterIds() {
        return targetTransmitterIds;
    }

    /**
     * @return the transitAllowed
     */
    public boolean isTransitAllowed() {
        return transitAllowed;
    }

    /**
     * @param transitAllowed the transitAllowed to set
     */
    public void setTransitAllowed(boolean transitAllowed) {
        this.transitAllowed = transitAllowed;
    }
}
