/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.starmap.AppletData;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JPanel;

/**
 *
 * @author Admin
 */
public class Territory extends Object_OneCoordinate {

    private Map<Integer, Point> points;
    public String name;
    public String description;
    private int status = 0;
    private int id;
    private boolean isNew = false;
    private int lastId = 0;
    private boolean marked = false;

    public Territory(IPainter painter, boolean isNew) {
        super(0, "", 0, 0, null);
        this.painter = painter;
        this.isNew = isNew;
        points = new TreeMap();
    }

    public Territory(IPainter painter, int id, String description, String name, int status, boolean isNew) {
        super(id, "territory - " + id, 0, 0, null);
        this.isNew = isNew;
        this.painter = painter;
        this.id = id;
        this.status = status;
        this.description = description;
        this.name = name;
        points = new TreeMap();
    }

    public void addPoint(int order, int id, String identifier, int x, int y) {
        if (isNew) {
            lastId += 10;
            getPoints().put(lastId, new Point(lastId, String.valueOf(lastId), x, y));

        } else {

            getPoints().put(order, new Point(id, identifier, x, y));
        }
    }

    @Override
    public String getIdentifier() {
        return "Id - Territory " + id;
    }

    @Override
    public JPanel getPanel() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {
        int x[] = new int[getPoints().size()];
        int y[] = new int[getPoints().size()];
        int i = 0;
        int pos[] = new int[2];
        String territoryImage = "territory.png";
        Image img = DisplayManagement.getGraphic(territoryImage);

        int wx = ((at.darkdestiny.vismap.starmap.Painter) painter).getZFactorAdjust(16 * delta[0], 16);
        int wy = ((at.darkdestiny.vismap.starmap.Painter) painter).getZFactorAdjust(16 * delta[1], 16);

        int width = wx;
        int height = wy;
        for (Map.Entry<Integer, Point> entry : getPoints().entrySet()) {
            Point p = entry.getValue();
            ((at.darkdestiny.vismap.starmap.Painter) painter).convertCoordinates(p, pos);

            x[i] = (int) p.getActCoord_x1();
            y[i] = (int) p.getActCoord_y1();
            i++;
        }
        if (marked) {

            gc.setColor(new Color(0.9f, 0.9f, 0f, 0.2f));
        } else {
            if (status == 0) {
                gc.setColor(new Color(0f, 0.9f, 0f, 0.2f));
            } else {
                Color c = Color.decode(((AppletData) appletData).getRelations().get(status).getColor());
                float[] colors = new float[4];
                c.getRGBColorComponents(colors);
                gc.setColor(new Color(colors[0], colors[1], colors[2], 0.2f));
            }
        }
        Polygon poly = new Polygon(x, y, getPoints().size());
        gc.fillPolygon(poly);
        if (marked) {

            gc.setColor(new Color(0.9f, 0.9f, 0f, 0.8f));
        } else {
            if (status == 0) {
                gc.setColor(new Color(0f, 0.9f, 0f, 0.8f));
            } else {
                Color c = Color.decode(((AppletData) appletData).getRelations().get(status).getColor());
                float[] colors = new float[4];
                c.getRGBColorComponents(colors);
                gc.setColor(new Color(colors[0], colors[1], colors[2], 0.8f));
            }
        }
        gc.drawPolygon(poly);
        for (Map.Entry<Integer, Point> entry : getPoints().entrySet()) {
            Point p = entry.getValue();

            ((at.darkdestiny.vismap.starmap.Painter) painter).convertCoordinates(p, pos);
            gc.drawImage(img, p.getActCoord_x1() - ((at.darkdestiny.vismap.starmap.Painter) painter).getZFactorAdjust(32 * delta[0], 32) / 2, p.getActCoord_y1() - ((at.darkdestiny.vismap.starmap.Painter) painter).getZFactorAdjust(32 * delta[0], 32) / 2, width, height, null);
        }
    }

    /**
     * @return the points
     */
    public TreeMap<Integer, Point> getPoints() {
        return (TreeMap<Integer, Point>)points;
    }

    public void setPoints(Map<Integer, Point> newList) {
        this.points = newList;
    }

    public boolean hasIntersections() {
        List<Line2D> lines = new ArrayList();
        int size = getPoints().size();
        for (int i = 0; i < size; i++) {
            Point point = getPoints().get(i);

            Point nextPoint = null;
            // point is last Point take first point
            if (i == size - 1) {
                nextPoint = getPoints().get(0);
            } else {
                nextPoint = getPoints().get(i + 1);
            }
            Line2D line = new Line2D.Float(point.getX1(), point.getY1(), nextPoint.getX1(), nextPoint.getY1());

            lines.add(line);
        }
        for (Line2D cLine : lines) {
            for (Line2D line : lines) {
                if (cLine.getP1().equals(line.getP1()) || cLine.getP1().equals(line.getP2())
                        || cLine.getP2().equals(line.getP1()) || cLine.getP2().equals(line.getP2())) {
                    continue;
                }
                if (line.intersectsLine(cLine)) {
                    return true;
                }

            }
        }
        return false;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param marked the marked to set
     */
    @Override
    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public void rebuildPoints() {
        TreeMap<Integer, Point> newMap = new TreeMap();
        for (Map.Entry<Integer, Point> entry : points.entrySet()) {
            newMap.put(entry.getValue().getId(), entry.getValue());
        }
        points = newMap;
    }
}
