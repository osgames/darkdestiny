/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_TwoCoordinates;
import at.darkdestiny.vismap.dto.IAppletData;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author HorstRabe
 */
public class Raster extends Object_TwoCoordinates {

boolean inverted = false;
    public Raster(int x1, int y1, int x2, int y2) {
        super(0, x1, y1, x2, y2);

    }


    /**
     * Eine Ebene des Bildes zeichnen
     * @param gc wohin zeichnen
     * @param level welche Ebene Zeichnen (0 - Painter.MAX_PAINTLEVEL)
     * @param delta wie gro� ist ein LJ (d.h. der Positionsabstand 1)
     */

    public void setInverted(boolean inverted){
        this.inverted = inverted;
    }
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {



        gc.setColor(new Color(0.3f, 0.3f, 1f));
        if (this.getX1() != 0) {


            gc.drawString(String.valueOf(this.getX1()), Math.round(this.getActCoord_x1()), 30);
        } else {
            if (this.getX1() == 0) {

                gc.drawString(String.valueOf(this.getX1()), Math.round(this.getActCoord_x1()), 30);

            }
            gc.drawString(String.valueOf(this.getY1()), 20, Math.round(this.getActCoord_y1()));

        }

        if (inverted) {
            gc.setColor(Color.BLACK);
        } else {

            gc.setColor(Color.WHITE);
        }

        gc.drawLine((int) (actCoord_x),
                (int) (actCoord_y),
                (int) (actCoord_x2),
                (int) (actCoord_y2));


    }

    public String getIdentifier() {
        throw new UnsupportedOperationException("Not supported yet.");
    }






}
