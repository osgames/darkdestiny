/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.starmap.dto.LoadingEntry;
import at.darkdestiny.vismap.drawable.Object_TwoCoordinates;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.starmap.dto.ChassisEntry;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.awt.image.LookupOp;
import java.awt.image.LookupTable;
import java.awt.image.ShortLookupTable;
import java.util.ArrayList;
import javax.swing.JPanel;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.starmap.AppletData;
import at.darkdestiny.vismap.starmap.PainterData;
import at.darkdestiny.vismap.starmap.panels.fleet.FleetPanel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author HorstRabe
 */
public class FleetEntry extends Object_TwoCoordinates {

    private String name;
    private String owner;
    private String endPlanet;
    private int endPlanetId;
    private String endSystem;
    private int endSystemId;
    private double speed;
    private int timeRemaining;
    private int status;
    private int offsetX = 0;
    private int scanDuration = 0;
    private boolean scanSystem = false;
    private boolean scanPlanet = false;
    private static int STARPICTURE_SIZE = 32;
    private static final int MAXIMG_SIZE = 32;
    private static final int MAXIMG_WIDTH = 32;
    private static final int MAXIMG_HEIGHT = 32;
    private boolean fleetFormation = false;
    private boolean fleetFormationLeader = false;
    private boolean participating = false;
    private List<ChassisEntry> chassis;
    private List<LoadingEntry> loading;
    boolean mirror = false;
    private int offsetY;
    private boolean targetVisible = false;
    IPainter painter;
    private int range;
    private static Map<Integer, Set<Color>> formationAtPosition = new HashMap();

    public FleetEntry(int startX, int startY, int endX, int endY, String name, int id, String owner, int endPlanetId, String endPlanet, String endSystem, int endSystemId, double speed, int timeRemaining, int status, IPainter painter, boolean fleetFormation, boolean fleetFormationLeader, boolean participating, boolean targetVisible, int range) {
        super(id, startX, startY, endX, endY);

        chassis = new ArrayList<>();
        loading = new ArrayList<>();
        this.fleetFormation = fleetFormation;
        this.fleetFormationLeader = fleetFormationLeader;
        this.participating = participating;
        this.targetVisible = targetVisible;

        this.identifier = AppletConstants.ID_INFORMATIONPANEL_FLEET + id;

        this.name = name;
        this.owner = owner;
        this.endPlanetId = endPlanetId;
        this.endPlanet = endPlanet;
        this.endSystem = endSystem;
        this.endSystemId = endSystemId;
        this.speed = speed;
        this.timeRemaining = timeRemaining;
        this.status = status;
        if (startX < endX) {
            this.mirror = true;
        }
        this.painter = painter;
        this.range = range;
    }

    public FleetEntry(int startX, int startY, int endX, int endY, String name, int id, String owner, String endSystem, int endSystemId, double speed, int timeRemaining, int status, MainPainter painter, boolean fleetFormation, boolean fleetFormationLeader, boolean targetVisible, int range) {
        super(id, startX, startY, endX, endY);

        chassis = new ArrayList<>();
        loading = new ArrayList<>();
        this.fleetFormation = fleetFormation;

        this.identifier = AppletConstants.ID_INFORMATIONPANEL_FLEET + id;

        this.fleetFormationLeader = fleetFormationLeader;
        this.name = name;
        this.owner = owner;
        this.speed = speed;
        this.targetVisible = targetVisible;
        this.timeRemaining = timeRemaining;
        this.endSystem = endSystem;
        this.endSystemId = endSystemId;
        this.status = status;
        if (startX < endX) {
            this.mirror = true;
        }
        this.painter = painter;
        this.range = range;
    }

    public static void clearFormationMap() {
        formationAtPosition.clear();
    }

    @Override
    public JPanel getPanel() {
        return new FleetPanel(this, null);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getEndPlanet() {
        return endPlanet;
    }

    public void setEndPlanet(String endPlanet) {
        this.endPlanet = endPlanet;
    }

    public int getEndPlanetId() {
        return endPlanetId;
    }

    public void setEndPlanetId(int endPlanetId) {
        this.endPlanetId = endPlanetId;
    }

    public String getEndSystem() {
        return endSystem;
    }

    public void setEndSystem(String endSystem) {
        this.endSystem = endSystem;
    }

    public int getEndSystemId() {
        return endSystemId;
    }

    public void setEndSystemId(int endSystemId) {
        this.endSystemId = endSystemId;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getTimeRemaiuning() {
        return timeRemaining;
    }

    public void setTimeRemaiuning(int timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    @Override
    public void init(MainPainter p) {
        this.painter = p;

    }

    @Override
    public void draw(IAppletData appletData, Graphics g, float[] delta) {

        String fleetImage;

        Graphics2D gc = (Graphics2D) g;

        String imgOutline = "Fleet_Outline";
        if (mirror) {
            imgOutline += "_Mirr";
        }

        imgOutline += ".png";
        Image imageOutline = DisplayManagement.getGraphic(imgOutline);
        BufferedImage bufferedImageSource = DisplayManagement.toBufferedImage(imageOutline, imgOutline);
        BufferedImage copyOfImage
                = new BufferedImage(bufferedImageSource.getWidth(), bufferedImageSource.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D cloneBuffer = (Graphics2D) copyOfImage.getGraphics();
        cloneBuffer.drawImage(bufferedImageSource, 0, 0, painter.getPanel());

        if (this.timeRemaining <= 0) {
            Point p = new Point(id, "fleet : " + id, this.getX1(), this.getY1());
            if (!isFleetFormation()) {
                if (((PainterData) painter.getPainterData()).fleetsDrawnContains(p, status)) {
                    return;
                } else {
                    ((PainterData) painter.getPainterData()).addFleetDrawn(p, status);
                }
            }
        }
        if (status == AppletConstants.FLEET_ALLIED || status == 10) {
            gc.setColor(AppletConstants.FLEET_COLOR_ALLIED);
            fleetImage = AppletConstants.FLEET_IMAGE_ALLIED;

            if (mirror) {
                fleetImage += "_Mirr";
                imgOutline += "_Mirr";
            }

            createColorizeOp(AppletConstants.FLEET_COLOR_ALLIED).filter(copyOfImage, copyOfImage);
            fleetImage += ".gif";
            // !Stationary
            Image img = DisplayManagement.getGraphic(fleetImage);
            int wx = getZFactorAdjust(MAXIMG_WIDTH * delta[0]);
            int wy = getZFactorAdjust(MAXIMG_HEIGHT * delta[1]);

            int width = wx;
            int height = wy;

            this.offsetX = getZFactorAdjust(16f * delta[0]);
            this.offsetY = getZFactorAdjust(14f * delta[1], 14);

            if (this.timeRemaining != 0) {

                gc.drawLine((int) (actCoord_x),
                        (int) (actCoord_y),
                        (int) (actCoord_x2),
                        (int) (actCoord_y2));

                if (fleetFormation) {
                    gc.drawImage(copyOfImage, (actCoord_x - width / 2), (actCoord_y - height / 2), width, height, null);
                }

                gc.drawImage(img, (actCoord_x - width / 2), (actCoord_y - height / 2), width, height, null);

            } else {

                // Inner Ring
                gc.setColor(AppletConstants.FLEET_COLOR_ALLIED);
                gc.fillOval((this.getActCoord_x1() - width / 2) + getOffsetX(), (this.getActCoord_y1() - height / 2) + getOffsetY(), width, height);

                if (fleetFormation) {
                    addFormation(getEndSystemId(), gc.getColor());
                }

                // Draw diagonal Balken if fleet formation under other player control but you participate
                if (fleetFormation && participating) {
                    int middleX = ((this.getActCoord_x1() - width / 2) + getOffsetX() - 2) + (width + 4) / 2;
                    int middleY = ((this.getActCoord_y1() - height / 2) + getOffsetY() - 2) + (height + 4) / 2;

                    gc.setColor(AppletConstants.FLEET_COLOR_OWN);

                    int[] xBar = new int[4];
                    int[] yBar = new int[4];

                    xBar[0] = middleX + (int) (-0.5d * ((width + 4d) / 2d));
                    xBar[1] = middleX + (int) (-(Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));
                    xBar[2] = middleX + (int) (0.5d * ((width + 4d) / 2d));
                    xBar[3] = middleX + (int) ((Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));

                    yBar[0] = middleY + (int) (-(Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[1] = middleY + (int) (-0.5d * ((height + 4d) / 2d));
                    yBar[2] = middleY + (int) ((Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[3] = middleY + (int) (0.5d * ((height + 4d) / 2d));

                    gc.fillPolygon(xBar, yBar, 4);
                }

                // Outer Ring
                gc.setColor(Color.BLACK);
                if (fleetFormation || drawFormationStyle(getEndSystemId(), AppletConstants.FLEET_COLOR_ALLIED)) {
                    gc.setColor(AppletConstants.FLEET_COLOR_FLEETFORMATION);
                }

                Ellipse2D e = new Ellipse2D.Float((this.getActCoord_x1() - width / 2) + getOffsetX() - 2, (this.getActCoord_y1() - height / 2) + this.getOffsetY() - 2, width + 4, height + 4);
                Ellipse2D e2 = new Ellipse2D.Float((this.getActCoord_x1() - width / 2) + getOffsetX(), (this.getActCoord_y1() - height / 2) + getOffsetY(), width, height);

                Area shapeOuterRing = new Area(e);
                Area Middle = new Area(e2);

                shapeOuterRing.subtract(Middle);
                gc.fill(shapeOuterRing);

                gc.drawImage(img, (this.getActCoord_x1() - width / 2) + getOffsetX(), (this.getActCoord_y1() - height / 2) + getOffsetY(), width, height, null);

            }

        } else if (status == AppletConstants.FLEET_OWN) {
            gc.setColor(AppletConstants.FLEET_COLOR_OWN);

            fleetImage = AppletConstants.FLEET_IMAGE_OWN;
            if (mirror) {
                fleetImage += "_Mirr";
            }
            fleetImage += ".gif";
            createColorizeOp(AppletConstants.FLEET_COLOR_OWN).filter(copyOfImage, copyOfImage);
            // !Stationary
            Image img = DisplayManagement.getGraphic(fleetImage);
            int wx = getZFactorAdjust(MAXIMG_WIDTH * delta[0]);
            int wy = getZFactorAdjust(MAXIMG_HEIGHT * delta[1]);

            int width = wx;
            int height = wy;

            this.offsetX = getZFactorAdjust(0f * delta[0]);
            this.offsetY = -getZFactorAdjust(30f * delta[1]);

            if (this.timeRemaining != 0) {

                gc.drawLine((int) (actCoord_x),
                        (int) (actCoord_y),
                        (int) (actCoord_x2),
                        (int) (actCoord_y2));
                if (fleetFormation) {
                    gc.drawImage(copyOfImage, (actCoord_x - width / 2), (actCoord_y - height / 2), width, height, null);
                }

                gc.drawImage(img, (actCoord_x - width / 2), (actCoord_y - height / 2), width, height, null);
            } else {

                gc.setColor(Color.BLACK);
                if (fleetFormation) {
                    gc.setColor(AppletConstants.FLEET_COLOR_FLEETFORMATION);
                }

                // Inner Ring
                gc.setColor(AppletConstants.FLEET_COLOR_OWN);
                gc.fillOval((this.getActCoord_x1() - width / 2) + getOffsetX(), (this.getActCoord_y1() - height / 2) + getOffsetY(), width, height);

                if (fleetFormation) {
                    addFormation(getEndSystemId(), gc.getColor());
                }

                // Draw diagonal Balken if fleet formation under other player control but you participate
                if (fleetFormation && participating) {
                    int middleX = ((this.getActCoord_x1() - width / 2) + getOffsetX() - 2) + (width + 4) / 2;
                    int middleY = ((this.getActCoord_y1() - height / 2) + getOffsetY() - 2) + (height + 4) / 2;

                    gc.setColor(AppletConstants.FLEET_COLOR_OWN);

                    int[] xBar = new int[4];
                    int[] yBar = new int[4];

                    xBar[0] = middleX + (int) (-0.5d * ((width + 4d) / 2d));
                    xBar[1] = middleX + (int) (-(Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));
                    xBar[2] = middleX + (int) (0.5d * ((width + 4d) / 2d));
                    xBar[3] = middleX + (int) ((Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));

                    yBar[0] = middleY + (int) (-(Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[1] = middleY + (int) (-0.5d * ((height + 4d) / 2d));
                    yBar[2] = middleY + (int) ((Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[3] = middleY + (int) (0.5d * ((height + 4d) / 2d));

                    gc.fillPolygon(xBar, yBar, 4);
                }

                // Outer Ring
                gc.setColor(Color.BLACK);
                if (fleetFormation || drawFormationStyle(getEndSystemId(), AppletConstants.FLEET_COLOR_OWN)) {
                    gc.setColor(AppletConstants.FLEET_COLOR_FLEETFORMATION);
                }

                Ellipse2D e = new Ellipse2D.Float((this.getActCoord_x1() - width / 2) + getOffsetX() - 2, (this.getActCoord_y1() - height / 2) + this.getOffsetY() - 2, width + 4, height + 4);
                Ellipse2D e2 = new Ellipse2D.Float((this.getActCoord_x1() - width / 2) + getOffsetX(), (this.getActCoord_y1() - height / 2) + getOffsetY(), width, height);

                Area shapeOuterRing = new Area(e);
                Area Middle = new Area(e2);

                shapeOuterRing.subtract(Middle);
                gc.fill(shapeOuterRing);

                gc.drawImage(img, (this.getActCoord_x1() - width / 2) + getOffsetX(), (this.getActCoord_y1() - height / 2) + getOffsetY(), width, height, null);

            }
        } else {

            gc.setColor(AppletConstants.FLEET_COLOR_ENEMY);
            fleetImage = AppletConstants.FLEET_IMAGE_ENEMY;

            if (mirror) {
                fleetImage += "_Mirr";
            }
            fleetImage += ".gif";
            // !Stationary
            Image img = DisplayManagement.getGraphic(fleetImage);
            int wx = getZFactorAdjust(MAXIMG_WIDTH * delta[0]);
            int wy = getZFactorAdjust(MAXIMG_HEIGHT * delta[1]);

            int width = wx;
            int height = wy;

            this.offsetX = -getZFactorAdjust(28f * delta[0], 28);
            this.offsetY = getZFactorAdjust(19f * delta[1], 19);

            if (this.timeRemaining != 0) {

                if (status == 3) {
                    gc.setColor(Color.PINK);
                    createColorizeOp(Color.PINK).filter(copyOfImage, copyOfImage);

                } else {
                    Color c = Color.decode(((AppletData) appletData).getRelations().get(status).getColor());
                    createColorizeOp(c).filter(copyOfImage, copyOfImage);

                    gc.setColor(c);
                }

                gc.drawLine((int) (actCoord_x),
                        (int) (actCoord_y),
                        (int) (actCoord_x2),
                        (int) (actCoord_y2));
                if (fleetFormation) {
                    gc.drawImage(copyOfImage, (actCoord_x - width / 2), (actCoord_y - height / 2), width, height, null);

                }
                gc.drawImage(img, (actCoord_x - width / 2), (actCoord_y - height / 2), width, height, null);
            } else {
                Color c = Color.decode(((AppletData) appletData).getRelations().get(status).getColor());
                Color relColor;

                if (status == 3) {
                    gc.setColor(Color.PINK);
                } else {
                    gc.setColor(c);
                }

                relColor = gc.getColor();

                if (fleetFormation) {
                    addFormation(getEndSystemId(), gc.getColor());
                }

                gc.setColor(Color.BLACK);
                if (fleetFormation) {
                    gc.setColor(AppletConstants.FLEET_COLOR_FLEETFORMATION);
                }

                // Inner Ring
                gc.setColor(c);
                gc.fillOval((this.getActCoord_x1() - width / 2) + getOffsetX(), (this.getActCoord_y1() - height / 2) + getOffsetY(), width, height);

                // Draw diagonal Balken if fleet formation under other player control but you participate
                if (fleetFormation && participating) {
                    int middleX = ((this.getActCoord_x1() - width / 2) + getOffsetX() - 2) + (width + 4) / 2;
                    int middleY = ((this.getActCoord_y1() - height / 2) + getOffsetY() - 2) + (height + 4) / 2;

                    gc.setColor(AppletConstants.FLEET_COLOR_OWN);

                    int[] xBar = new int[4];
                    int[] yBar = new int[4];

                    xBar[0] = middleX + (int) (-0.5d * ((width + 4d) / 2d));
                    xBar[1] = middleX + (int) (-(Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));
                    xBar[2] = middleX + (int) (0.5d * ((width + 4d) / 2d));
                    xBar[3] = middleX + (int) ((Math.sqrt(3d) / 2d) * ((width + 4d) / 2d));

                    yBar[0] = middleY + (int) (-(Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[1] = middleY + (int) (-0.5d * ((height + 4d) / 2d));
                    yBar[2] = middleY + (int) ((Math.sqrt(3d) / 2d) * ((height + 4d) / 2d));
                    yBar[3] = middleY + (int) (0.5d * ((height + 4d) / 2d));

                    gc.fillPolygon(xBar, yBar, 4);
                }

                // Outer Ring
                gc.setColor(Color.BLACK);

                if (fleetFormation || drawFormationStyle(getEndSystemId(), relColor)) {
                    gc.setColor(AppletConstants.FLEET_COLOR_FLEETFORMATION);
                }

               Ellipse2D e = new Ellipse2D.Float((this.getActCoord_x1() - width / 2) + getOffsetX() - 2, (this.getActCoord_y1() - height / 2) + this.getOffsetY() - 2, width + 4, height + 4);
                Ellipse2D e2 = new Ellipse2D.Float((this.getActCoord_x1() - width / 2) + getOffsetX(), (this.getActCoord_y1() - height / 2) + getOffsetY(), width, height);

                Area shapeOuterRing = new Area(e);
                Area Middle = new Area(e2);

                shapeOuterRing.subtract(Middle);
                gc.fill(shapeOuterRing);

                gc.drawImage(img, (this.getActCoord_x1() - width / 2) + getOffsetX(), (this.getActCoord_y1() - height / 2) + getOffsetY(), width, height, null);


            }
        }

    }

    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > MAXIMG_SIZE) {
            return MAXIMG_SIZE;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    private int getZFactorAdjust(float inValue, int maxSize) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > maxSize) {
            return maxSize;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ChassisEntry> getChassis() {
        return chassis;
    }

    public void addChassis(ChassisEntry chassi) {
        this.chassis.add(chassi);

    }

    public List<LoadingEntry> getLoading() {
        return loading;
    }

    public void addLoading(LoadingEntry loadingEntry) {
        this.loading.add(loadingEntry);
    }

    /**
     * @return the offsetX
     */
    public int getOffsetX() {
        return offsetX;
    }

    /**
     * @return the offsetY
     */
    public int getOffsetY() {
        return offsetY;
    }

    /**
     * @return the fleetFormation
     */
    public boolean isFleetFormation() {
        return fleetFormation;
    }

    protected LookupOp createColorizeOp(Color newColor) {
        short[] alpha = new short[256];
        short[] red = new short[256];
        short[] green = new short[256];
        short[] blue = new short[256];

        for (short i = 0; i < 256; i++) {
            alpha[i] = i;
            red[i] = i;
            green[i] = i;
            blue[i] = i;
        }

        red[201] = (short) newColor.getRed();
        green[201] = (short) newColor.getGreen();
        blue[201] = (short) newColor.getBlue();

        short[][] data = new short[][]{
            red, green, blue, alpha
        };
        LookupTable lookupTable = new ShortLookupTable(0, data);
        return new LookupOp(lookupTable, null);
    }

    /**
     * @return the fleetFormationLeader
     */
    public boolean isFleetFormationLeader() {
        return fleetFormationLeader;
    }

    private void addFormation(int system, Color c) {
        if (!formationAtPosition.containsKey(system)) {
            formationAtPosition.put(system, new HashSet<Color>());
        }

        formationAtPosition.get(system).add(c);
    }

    private boolean drawFormationStyle(int system, Color c) {
        Set<Color> tmpMap = formationAtPosition.get(system);
        if (tmpMap == null) {
            return false;
        }

        for (Color cTmp : tmpMap) {
            if (cTmp.equals(c)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return the targetVisible
     */
    public boolean isTargetVisible() {
        return targetVisible;
    }

    /**
     * @return the scanDuration
     */
    public int getScanDuration() {
        return scanDuration;
    }

    /**
     * @param scanDuration the scanDuration to set
     */
    public void setScanDuration(int scanDuration) {
        this.scanDuration = scanDuration;
    }

    /**
     * @return the scanSystem
     */
    public boolean isScanSystem() {
        return scanSystem;
    }

    /**
     * @param scanSystem the scanSystem to set
     */
    public void setScanSystem(boolean scanSystem) {
        this.scanSystem = scanSystem;
    }

    /**
     * @return the scanPlanet
     */
    public boolean isScanPlanet() {
        return scanPlanet;
    }

    /**
     * @param scanPlanet the scanPlanet to set
     */
    public void setScanPlanet(boolean scanPlanet) {
        this.scanPlanet = scanPlanet;
    }

    /**
     * @return the range
     */
    public int getRange() {
        return range;
    }
}
