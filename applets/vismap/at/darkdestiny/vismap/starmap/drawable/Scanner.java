package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import at.darkdestiny.vismap.logic.AppletConstants;

/**
 * Ein System, f�r die Anzeige
 *
 * @author martin
 *
 */
public class Scanner extends Object_OneCoordinate {


    private int status = -1;

    private int range = 0;

    private String identifier;

    public Scanner(int x, int y, int status, int range) {

        super(Math.round(x * y), "Hyperraumscanner", x, y, null);
        this.status = status;
        this.range = range;
        this.identifier = AppletConstants.ID_INFORMATIONPANEL_HYPERSCANNER + x + "|" + y;

    }

    @Override
    public boolean isToShow(int i) {

        return true;

    }

    @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {
        switch (status) {

            case (0): // Derzeit nur Bev�lkerungsdichte f�r Join Mode

                gc.setColor(Color.LIGHT_GRAY);
                gc.fillOval(Math.round(this.getActCoord_x1() - delta[0] * range),
                        Math.round(this.getActCoord_y1() - delta[1] * range),
                        Math.round(delta[0] * 2 * range),
                        Math.round(delta[1] * 2 * range));
                break;
            case (1): // Mark a special system
                gc.setColor(Color.LIGHT_GRAY);
                gc.fillOval(Math.round(this.getActCoord_x1() - delta[0] * range),
                        Math.round(this.getActCoord_y1() - delta[1] * range),
                        Math.round(delta[0] * 2 * range),
                        Math.round(delta[1] * 2 * range));

        }
    }



    @Override
    public String getIdentifier() {
        return this.identifier;
    }

    @Override
    public JPanel getPanel() {

        return new JPanel();

    }
}
