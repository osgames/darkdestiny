/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.starmap.AppletData;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JPanel;
import at.darkdestiny.vismap.logic.AppletConstants;

/**
 *
 * @author Bullet
 */
public class Triangle extends Object_OneCoordinate {

    private int status;
    private int fleetId;
    private static final int MAXIMG_SIZE = 32;
    boolean ownFleetFormation = false;
    IPainter painter;

    public Triangle(int status, int x, int y, int fleetId, IPainter painter, boolean ownFleetFormation) {
        super(0, "", x, y, null);
        this.fleetId = fleetId;
        this.painter = painter;
        this.status = status;
        this.ownFleetFormation = ownFleetFormation;
    }

   @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {

        if (status == AppletConstants.FLEET_OWN) {

            double angle = 240;
            double radius = getZFactorAdjust(40 * delta[1]);

            Polygon poly = new Polygon();
            double anglePerPart = 60;

            double[] point = getPoint(angle, radius);
            int[] point1 = new int[2];
            int[] point2 = new int[2];
            int[] point3 = new int[2];
            int x = this.getActCoord_x1();
            int y = this.getActCoord_y1();

            point1[0] = x;
            point1[1] = y;
            poly.addPoint(point1[0], point1[1]);
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point2[0] = x;
            point2[1] = y;
            angle += anglePerPart;
            gc.setColor(Color.BLACK);
            poly.addPoint(x, y);
            point = getPoint(angle, radius);
            x = this.getActCoord_x1();
            y = this.getActCoord_y1();
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point3[0] = x;
            point3[1] = y;
            poly.addPoint(x, y);

            gc.fillPolygon(poly);
            gc.drawPolygon(poly);

            //Inner color
            angle = 245;
            radius = getZFactorAdjust(40 * delta[1]);

            poly = new Polygon();
            anglePerPart = 50;

            point = getPoint(angle, radius);
            point1 = new int[2];
            point2 = new int[2];
            point3 = new int[2];
            x = this.getActCoord_x1();
            y = this.getActCoord_y1();

            point1[0] = x;
            point1[1] = y - 4;
            poly.addPoint(point1[0], point1[1]);
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point2[0] = x;
            point2[1] = y;
            angle += anglePerPart;
            gc.setColor(Color.BLACK);
            poly.addPoint(x, y);
            point = getPoint(angle, radius);
            x = this.getActCoord_x1();
            y = this.getActCoord_y1();
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point3[0] = x;
            point3[1] = y;
            poly.addPoint(x, y);

            gc.fillPolygon(poly);
            gc.drawPolygon(poly);
            gc.setColor(AppletConstants.FLEET_COLOR_OWN);
            if(ownFleetFormation){
            gc.setColor(AppletConstants.FLEET_COLOR_FLEETFORMATION);
            }

            gc.fillPolygon(poly);
            gc.drawPolygon(poly);
        } else if (status == AppletConstants.FLEET_ALLIED || status == 10) {

            double angle = -5;
            double radius = getZFactorAdjust(20 * delta[1]);

            Polygon poly = new Polygon();
            double anglePerPart = 60;

            double[] point = getPoint(angle, radius);
            int[] point1 = new int[2];
            int[] point2 = new int[2];
            int[] point3 = new int[2];
            int x = this.getActCoord_x1();
            int y = this.getActCoord_y1();

            point1[0] = x;
            point1[1] = y;
            poly.addPoint(point1[0], point1[1]);
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point2[0] = x;
            point2[1] = y;
            angle += anglePerPart;
            gc.setColor(Color.BLACK);
            poly.addPoint(x, y);
            point = getPoint(angle, radius);
            x = this.getActCoord_x1();
            y = this.getActCoord_y1();
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point3[0] = x;
            point3[1] = y;
            poly.addPoint(x, y);

            gc.fillPolygon(poly);
            gc.drawPolygon(poly);

            //Inner color
            angle = 0;
            radius = getZFactorAdjust(20 * delta[1]);

            poly = new Polygon();
            anglePerPart = 50;

            point = getPoint(angle, radius);
            point1 = new int[2];
            point2 = new int[2];
            point3 = new int[2];
            x =this.getActCoord_x1();
            y =this.getActCoord_y1();

            point1[0] = x + 2;
            point1[1] = y + 2;
            poly.addPoint(point1[0], point1[1]);
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point2[0] = x;
            point2[1] = y;
            angle += anglePerPart;
            poly.addPoint(x, y);
            point = getPoint(angle, radius);
            x = this.getActCoord_x1();
            y = this.getActCoord_y1();
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point3[0] = x;
            point3[1] = y;
            poly.addPoint(x, y);

            gc.fillPolygon(poly);
            gc.drawPolygon(poly);
            gc.setColor(AppletConstants.FLEET_COLOR_ALLIED);

            gc.fillPolygon(poly);
            gc.drawPolygon(poly);
        } else {


            double angle = 115;
            double radius = getZFactorAdjust(20 * delta[1]);

            Polygon poly = new Polygon();
            double anglePerPart = 60;

            double[] point = getPoint(angle, radius);
            int[] point1 = new int[2];
            int[] point2 = new int[2];
            int[] point3 = new int[2];
            int x = getActCoord_x1();
            int y = getActCoord_y1();

            point1[0] = x;
            point1[1] = y;
            poly.addPoint(point1[0], point1[1]);
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point2[0] = x;
            point2[1] = y;
            angle += anglePerPart;
            gc.setColor(Color.BLACK);
            poly.addPoint(x, y);
            point = getPoint(angle, radius);
            x = this.getActCoord_x1();
            y = this.getActCoord_y1();
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point3[0] = x;
            point3[1] = y;
            poly.addPoint(x, y);

            gc.fillPolygon(poly);
            gc.drawPolygon(poly);

            //Inner color
            angle = 120;
            radius = getZFactorAdjust(20 * delta[1]);

            poly = new Polygon();
            anglePerPart = 50;

            point = getPoint(angle, radius);
            point1 = new int[2];
            point2 = new int[2];
            point3 = new int[2];
            x = this.getActCoord_x1();
            y = this.getActCoord_y1();

            point1[0] = x - 2;
            point1[1] = y + 2;
            poly.addPoint(point1[0], point1[1]);
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point2[0] = x;
            point2[1] = y;
            angle += anglePerPart;
            gc.setColor(Color.BLACK);
            poly.addPoint(x, y);
            point = getPoint(angle, radius);
            x = this.getActCoord_x1();
            y = this.getActCoord_y1();
            x += Math.round(point[0]);
            y += Math.round(point[1]);
            point3[0] = x;
            point3[1] = y;
            poly.addPoint(x, y);

            gc.fillPolygon(poly);
            gc.drawPolygon(poly);
            Color c = Color.decode(((AppletData)appletData).getRelations().get(status).getColor());
            if (status == 3) {
                gc.setColor(Color.PINK);
            } else {
                gc.setColor(c);
            }

            gc.fillPolygon(poly);
            gc.drawPolygon(poly);
        }


    }

    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > MAXIMG_SIZE) {
            return MAXIMG_SIZE;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    /**
     * @return the fleetId
     */
    public int getFleetId() {
        return fleetId;
    }

    public static double[] getPoint(double angle, double radius) {
        double rads = angle * Math.PI / 180.00;
        double Px = Math.cos(rads) * radius;
        double Py = Math.sin(rads) * radius;
        double[] point = new double[2];
        point[0] = Px;
        point[1] = Py;
        return point;
    }

    @Override
    public String getIdentifier() {
        return ("triangle");
    }

    @Override
    public JPanel getPanel() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
