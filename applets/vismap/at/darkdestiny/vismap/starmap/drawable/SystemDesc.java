package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.starmap.dto.PlanetEntry;
import at.darkdestiny.vismap.enumerations.EVismapType;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.*;
import javax.swing.JPanel;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.gui.AppletMain;

import at.darkdestiny.vismap.logic.HideShowSystems;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.starmap.panels.system.SystemInformationPanel;

/**
 * Ein System, f�r die Anzeige
 *
 * @author martin
 *
 */
public class SystemDesc extends Object_OneCoordinate {

    private int STARPICTURE_SIZE = 30;
    private long lastUpdate = 0l;
    /**
     * Wie weit sind Planeten vom eigenen System aus zu sehen
     */
    private static final int MAXIMG_SIZE = 19;
    /**
     * Wieviele Planeten sind vorhanden, und von Welchem Typ
     */
    private int planetCount = 0;
    private List<PlanetEntry> planetList = new LinkedList<>();
    private boolean marked = false;
    private boolean havingOwnPlanet = false;
    /**
     * Anzeige ob Feind Freund usw;
     */
    private int systemStatus = -1;
    /**
     * Daten f�r farbliche Hinterlegung im Join Modus
     */
    private int popPoints = 0;
    private int scorePoints = 0;
    private AppletMain mainApplet;
    private String identifier;
    private Map<Integer, String> colors;
    private int level;

    public SystemDesc(String sysName, int sysId, int x, int y, int systemStatus, Image imgName, List<PlanetEntry> peList, AppletMain mainApplet, long lastUpdateSystem, Map<Integer, String> colors, boolean hasOwnPlanet) {
        super(sysId, sysName, x, y, imgName);
        this.lastUpdate = lastUpdateSystem;
        this.systemStatus = systemStatus;
        this.planetList = peList;
        this.mainApplet = mainApplet;
        this.havingOwnPlanet = hasOwnPlanet;
        this.colors = colors;
        planetCount = peList.size();
        this.identifier = AppletConstants.ID_INFORMATIONPANEL_SYSTEM + String.valueOf(sysId);
    }

    public SystemDesc(String sysName, int sysId, int x, int y, Image img, AppletMain mainApplet, boolean havingOwnPlanet) {
        super(sysId, sysName, x, y, img);
        this.mainApplet = mainApplet;
        colors = new TreeMap();
        planetList = new LinkedList<>();
        this.havingOwnPlanet = havingOwnPlanet;
        this.identifier = AppletConstants.ID_INFORMATIONPANEL_SYSTEM + String.valueOf(sysId);
    }

    public int getPlanetCount() {
        return planetCount;

    }

    public int getSystemStatus() {
        return systemStatus;
    }

    @Override
    public boolean isToShow(int i) {
        if (i == 0) {
            if (getSystemStatus() == 1 || getSystemStatus() == 3 || getSystemStatus() == 5 || getSystemStatus() == 7) {
                return HideShowSystems.isDrawPlanetVisibleArea();
            }
            return false;
        }
        if (i == 1) {
            return HideShowSystems.isSystemToShow(-1);
        }
        return HideShowSystems.isSystemToShow(getSystemStatus());
    }

    public void draw(IAppletData appletData, Graphics gc, int level, float[] delta) {
        this.level = level;
        draw(appletData, gc, delta);
    }

    @Override

    public void draw(IAppletData appletData, Graphics gc, float[] delta) {
        if (!HideShowSystems.isSystemToShow(systemStatus)) {

            return;
        }
        switch (level) {

            case (1): // Derzeit nur Bev�lkerungsdichte f�r Join Mode
                if (!appletData.getVismapType().equals(EVismapType.JOINMAP)) {
                    break;
                }
                int popDensityValue = (int) Math.ceil(255d / 10d * this.getPopPoints());

                int scoreDensityValue = (int) Math.ceil(this.getScorePoints() / 1000d);
                if (scoreDensityValue > 255) {
                    scoreDensityValue = 255;
                }

                int totalDensity = (int) (Math.floor((popDensityValue + scoreDensityValue) / 4d));

                int sRGBValue = 0;
                sRGBValue += ((int) (totalDensity & 0xFF)) << 24; // alpha
                sRGBValue += ((int) (255 & 0xFF)) << 16; // rot
                sRGBValue += ((int) (255 & 0xFF)) << 8; // gr�n
                sRGBValue += ((int) (0 & 0xFF)); // blau

                int radius = this.getPopPoints() * 10;

                gc.setColor(new Color(sRGBValue, true));
                gc.fillOval(Math.round(this.getActCoord_x1() - delta[0] * radius),
                        Math.round(this.getActCoord_y1() - delta[1] * radius),
                        Math.round(delta[0] * 2 * radius),
                        Math.round(delta[1] * 2 * radius));
                break;
            case (2): // Mark a special system
                if (!appletData.getVismapType().equals(EVismapType.JOINMAP)) {
                    break;
                }
                if (this.getSystemStatus() == 2) {
                    sRGBValue = 0;
                    sRGBValue += ((int) (128 & 0xFF)) << 24; // alpha
                    sRGBValue += ((int) (0 & 0xFF)) << 16; // rot
                    sRGBValue += ((int) (180 & 0xFF)) << 8; // gr�n
                    sRGBValue += ((int) (255 & 0xFF)); // blau

                    gc.setColor(new Color(sRGBValue, true));
                    gc.fillOval(Math.round(this.getActCoord_x1() - delta[0] * 50),
                            Math.round(this.getActCoord_y1() - delta[1] * 50),
                            Math.round(delta[0] * 2 * 50),
                            Math.round(delta[1] * 2 * 50));
                }
                break;
            case 3: //Hier werden die grauen Systeme gezeichnet
            {
                if (isMarked()) {

                    gc.setColor(Color.blue);
                    gc.fillOval(Math.round(this.getActCoord_x1() - delta[0] * 40),
                            Math.round(this.getActCoord_y1() - delta[1] * 40),
                            Math.round(delta[0] * 2 * 40),
                            Math.round(delta[1] * 2 * 40));
                }
                int wx = getZFactorAdjust(STARPICTURE_SIZE * delta[0]);
                int wy = getZFactorAdjust(STARPICTURE_SIZE * delta[1]);
                int size = Math.min(wx, wy);
                gc.drawImage(DisplayManagement.getGraphicForSystemState(-1), this.getActCoord_x1() - size / 2, this.getActCoord_y1() - size / 2, size, size, null);
                break;
            }
            case 8: //Hier werden die Systeme noch mal gezeichnet
            {
                if (isMarked()) {

                    gc.setColor(new Color(0f, 1f, 1f, 0.9f));
                    gc.drawOval(Math.round(this.getActCoord_x1() - getZFactorAdjust(50 * delta[0], 50) / 2),
                            Math.round(this.getActCoord_y1() - getZFactorAdjust(50 * delta[1], 50) / 2),
                            Math.round(getZFactorAdjust(50 * delta[0], 50)),
                            Math.round(getZFactorAdjust(50 * delta[1], 50)));
                }

                int wx = getZFactorAdjust(30 * delta[0]);
                int wy = getZFactorAdjust(30 * delta[1]);
                int size = Math.min(wx, wy);
                //Filter same colors
                ArrayList<String> realColors = new ArrayList();
                if (getColors() != null && !colors.isEmpty()) {
                    for (Map.Entry<Integer, String> entry : getColors().entrySet()) {
                        if (!realColors.contains(entry.getValue())) {
                            realColors.add(entry.getValue());
                        }
                    }
                }
                if (realColors != null && !realColors.isEmpty()) {
                    int ovalSize = (int) (size + (5 * delta[0] * (realColors.size() - 1)));
                    double factor = 1d;
                    double increase = 0.7;
                    factor = factor + (realColors.size() - 1) * increase;
                    for (String color : realColors) {
                        gc.setColor(Color.decode(color));
                        gc.fillOval(this.getActCoord_x1() - ovalSize / 2, this.getActCoord_y1() - ovalSize / 2, ovalSize, ovalSize);

                        ovalSize = (int) Math.floor((double) ovalSize / factor);
                        factor -= increase;
                    }
                }
                break;
            }
            case 9: //Hier werden die Systeme noch mal gezeichnet
            {
                if (isMarked()) {

                    gc.setColor(new Color(0f, 1f, 0f, 0.9f));
                    gc.drawOval(Math.round(this.getActCoord_x1() - getZFactorAdjust(50 * delta[0], 50) / 2),
                            Math.round(this.getActCoord_y1() - getZFactorAdjust(50 * delta[1], 50) / 2),
                            Math.round(getZFactorAdjust(50 * delta[0], 50)),
                            Math.round(getZFactorAdjust(50 * delta[1], 50)));
                }
                int wx = getZFactorAdjust(15 * delta[0]);
                int wy = getZFactorAdjust(15 * delta[1]);
                int size = Math.min(wx, wy);

                gc.drawImage(getImgName(), this.getActCoord_x1() - size / 2, this.getActCoord_y1() - size / 2, size, size, null);
                break;
            }

        }
    }

    private int getZFactorAdjust(float inValue) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > MAXIMG_SIZE) {
            return MAXIMG_SIZE;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    private int getZFactorAdjust(float inValue, int maxSize) {
        boolean down = true;

        // prevent Starflicker
        if (inValue <= 1.5f) {
            return 1;
        } else if ((inValue >= 1.5f) && (inValue <= 2.5f)) {
            if (Math.round(inValue - 1.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 3.5f) && (inValue <= 4.5f)) {
            if (Math.round(inValue - 3.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 5.5f) && (inValue <= 6.5f)) {
            if (Math.round(inValue - 5.5f) == 1) {
                down = false;
            }
        } else if ((inValue >= 7.5f) && (inValue <= 8.5f)) {
            if (Math.round(inValue - 7.5f) == 1) {
                down = false;
            }
        } else if (inValue > maxSize) {
            return maxSize;
        } else {
            return Math.round(inValue);
        }

        if (down) {
            inValue -= 0.5f;
        } else {
            inValue += 0.5f;
        }

        return Math.round(inValue);
    }

    public float getPriority() {
        return ((planetCount > 0) ? getId() : -1);
    }

    public List<PlanetEntry> getPlanetList() {
        return planetList;
    }

    public int getPopPoints() {
        return popPoints;
    }

    public void setPopPoints(int popPoints) {
        this.popPoints = popPoints;
    }

    public int getScorePoints() {
        return scorePoints;
    }

    public void setScorePoints(int scorePoints) {
        this.scorePoints = scorePoints;
    }

    public void setSystemStatus(int systemStatus) {
        this.systemStatus = systemStatus;
    }

    @Override
    public String getIdentifier() {
        return this.identifier;
    }

    @Override
    public JPanel getPanel() {
        SystemInformationPanel sip = new SystemInformationPanel(this, mainApplet);
        new Thread(sip).start();
        return sip;

    }

    /**
     * @return the lastUpdate
     */
    public long getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @param lastUpdate the lastUpdate to set
     */
    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * @return the colors
     */
    public Map<Integer, String> getColors() {

        if (this.colors == null) {

            return new TreeMap();
        }
        return colors;
    }

    public PlanetEntry getPlanet(int planetId) {
        if (planetList != null) {
            for (PlanetEntry pe : planetList) {
                if (pe.getId() == planetId) {
                    return pe;
                }
            }
        }
        return null;
    }

    public PlanetEntry getEndPlanet(int endPlanetId) {
        for (PlanetEntry pe : planetList) {
            if (pe.getId() == endPlanetId) {
                return pe;
            }
        }
        return null;
    }

    /**
     * @return the havingOwnPlanet
     */
    public boolean isHavingOwnPlanet() {
        return havingOwnPlanet;
    }

    /**
     * @param havingOwnPlanet the havingOwnPlanet to set
     */
    public void setHavingOwnPlanet(boolean havingOwnPlanet) {
        this.havingOwnPlanet = havingOwnPlanet;
    }
}
