/**
 *
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.starmap.AppletData;
import java.awt.*;

import at.darkdestiny.vismap.starmap.dto.PlanetEntry;

/**
 * @author martin
 *
 */
public class SystemDesc_ToolTip {

    private int x;
    private int y;
    public SystemDesc sd;
    private int width;
    private int height;
    private boolean xoff = false;
    private boolean yoff = false;
    private int globalWidth;
    private int globalHeight;
    /**
     * @param y
     * @param x
     * @param sd
     *
     */
    public SystemDesc_ToolTip(SystemDesc sd, int x, int y, int globalWidth, int globalHeight) {
        this.x = x;
        this.y = y;
        this.sd = sd;

        this.globalWidth = globalWidth;
        this.globalHeight = globalHeight;

    }
        public SystemDesc_ToolTip(SystemDesc sd, int x, int y) {
        this.x = x;
        this.y = y;
        this.sd = sd;
    }
    public void paint(IAppletData appletData, Graphics g) {

        boolean planetInformations = false;
        int rectWidth = 20;
        int rectHeight = 20;
        String sysName = "System "+sd.getName()+" ("+sd.getId()+":x)";
        int longestLine = g.getFontMetrics().stringWidth(sysName);

        Font normalFont = g.getFont();
        Font boldFont = new Font(normalFont.getFontName(),Font.BOLD,normalFont.getSize());
        Font smallFont = new Font(normalFont.getFontName(),Font.PLAIN,normalFont.getSize()-1);
        Font verySmallFont = new Font(normalFont.getFontName(),Font.PLAIN,normalFont.getSize()-1);

        String planetInfo[] = new String[sd.getPlanetCount()*2];
        if (sd.getPlanetCount() > 0) {
            rectHeight += 17;
            planetInformations = true;
        }

        int lineCount = 0;

        if (planetInformations) {
            for (PlanetEntry pe : sd.getPlanetList()) {
                String ownString = "";
                String planetName = "Planet " + pe.getId();

                if (!pe.getName().equalsIgnoreCase("")) {
                    planetName = pe.getName();
                }

                if (!pe.getOwner().equalsIgnoreCase("")) {
                    ownString = "["+pe.getOwner()+"] ";
                }

                planetInfo[lineCount] = planetName + " " + ownString + "("+sd.getId()+":"+pe.getId()+")";
                g.setFont(smallFont);
                longestLine = Math.max(g.getFontMetrics().stringWidth(planetInfo[lineCount]),longestLine);
                lineCount++;

                planetInfo[lineCount] = "Typ: " + pe.getTypland();
                g.setFont(verySmallFont);
                longestLine = Math.max(g.getFontMetrics().stringWidth(planetInfo[lineCount])+5,longestLine);
                lineCount++;
            }
        }

        // Loop through all textLines and determine longest line
        g.setFont(normalFont);
        int w = longestLine;
        int h = 31 * sd.getPlanetCount();
        this.width = w;
        this.height = h;

        if ((x+(w+rectWidth)) > globalWidth)
            x = x-(w+rectWidth);
        if((y+(h+rectHeight)) > globalHeight) {
            y = y-(h+rectHeight);

            // Check if new y will be lower than zero if yes move down till zero
            if (y < 0) {
                y = y + Math.abs(y);
            }
        }

        g.setFont(boldFont);

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, w+rectWidth, h+rectHeight);
        g.setColor(Color.black);
        g.drawRect(x, y, w+rectWidth, h+rectHeight);

        g.drawString(sysName, x+10, y+15);

        g.setFont(normalFont);

        if(planetInformations){
            int yPos = y+10+2*g.getFontMetrics().getHeight();

            lineCount = 0;
            for (PlanetEntry pe : sd.getPlanetList()) {
                    if(pe.getSide() == 1){
                        g.setColor(new Color(0,140,0));
                        if (pe.isHomesystem()) g.setColor(new Color(150,150,0));
                }else if(pe.getSide() == 14 || pe.getSide() == 15){


                    Color c = Color.decode("#996600");
                    g.setColor(c);
                }else if(pe.getSide() == 11 || pe.getSide() == 12){


                    Color c = Color.decode("#9933CC");
                    g.setColor(c);
                }
                else if(pe.getSide() > 0){


                    Color c = Color.decode(((AppletData)appletData).getRelations().get(pe.getSide()).getColor());
                    g.setColor(c);
                }else{

                g.setColor(Color.decode(pe.getColor()));
                }

                g.setFont(smallFont);
                g.drawString(planetInfo[lineCount],x+10,yPos);
                g.setColor(Color.BLACK);

                lineCount++;

                g.setFont(verySmallFont);
                yPos += g.getFontMetrics().getHeight();
                g.drawString(planetInfo[lineCount],x+20,yPos);
                g.setFont(normalFont);

                lineCount++;

                yPos += g.getFontMetrics().getHeight();
            }
        }
    }

    public void setPos(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public boolean isXoff() {
        return xoff;
    }

    public boolean isYoff() {
        return yoff;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
