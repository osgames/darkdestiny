/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.drawable;

import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IAppletData;
import java.awt.Graphics;
import javax.swing.JPanel;
import at.darkdestiny.vismap.logic.DisplayManagement;

/**
 *
 * @author Bullet
 */
public class Observatory  extends Object_OneCoordinate{

    private int status;

    public Observatory(int status, int x, int y){
        super(0,"",x, y, null);
        this.status = status;
    }


    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

     @Override
    public void draw(IAppletData appletData, Graphics gc, float[] delta) {
            gc.setColor(DisplayManagement.planetVisibleColor);
            gc.fillOval(Math.round(this.getActCoord_x1() - delta[0] * 100),
                    Math.round(this.getActCoord_y1() - delta[1] * 100),
                    Math.round(delta[0] * 2 * 100),
                    Math.round(delta[1] * 2 * 100));

    }

    @Override
        public String getIdentifier() {
        return ("Obervatory");
    }

    @Override
    public JPanel getPanel() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
