/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.system;

import at.darkdestiny.vismap.logic.PlanetToColor;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.JPanel;

/**
 *
 * @author Admin
 */
public class PlanetInformationPic extends JPanel implements ActionListener {

    private final String type;
    private final int diameter;
    public static final String LANDTYPE_A = "A";
    public static final String LANDTYPE_B = "B";
    public static final String LANDTYPE_C = "C";
    public static final String LANDTYPE_E = "E";
    public static final String LANDTYPE_G = "G";
    public static final String LANDTYPE_M = "M";
    public static final String LANDTYPE_L = "L";
    public static final String LANDTYPE_I = "I";
    public static final String LANDTYPE_J = "J";
    int size = 67;
    final String owner;
    final Color relColor;
    final int side;

    public PlanetInformationPic(String type, int diameter, String owner, int side, Color relColor, MouseListener ml) {
        this.type = type;
        this.diameter = diameter;
        this.relColor = relColor;

        this.addMouseListener(ml);
        this.owner = owner;
        this.side = side;
        this.setBackground(Color.BLACK);
        this.setToolTipText("<html>Typ - " + type + "<br>" + "Size - " + getSizeStringLong(type, diameter) + "</html>");
        this.setPreferredSize(new Dimension(size, size));
    }

    public String getSizeStringLong(String landType, int diameter) {
        String sizeString = getSizeString(landType, diameter);
        if (sizeString.equalsIgnoreCase("S")) {
            return "Small";
        }
        if (sizeString.equalsIgnoreCase("M")) {
            return "Medium";
        }
        if (sizeString.equalsIgnoreCase("L")) {
            return "Large";
        } else {
            return "Unknown";
        }
    }

    public String getSizeString(String landType, int diameter) {
        String sizeString = "??";
        // ResultSet rs = stmt1.executeQuery("select * from planet where id="+planetID);

        if (landType.equalsIgnoreCase(LANDTYPE_A)) {
            diameter = (int) Math.round(diameter / 14.5d);
        } else if (landType.equalsIgnoreCase(LANDTYPE_B)) {
            diameter = (int) Math.round(diameter / 9d);
        }

        if (diameter < 10500) {
            sizeString = "S";
        } else if (diameter > 16000) {
            sizeString = "L";
        } else {
            sizeString = "M";
        }



        return sizeString;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Color planetColor = PlanetToColor.getColor(type);
        g.setColor(planetColor);
        int planetSize = 20;

        if (!type.equals("A") && !type.equals("B")) {

            planetSize = diameter / 400;
        } else if (type.equals("A")) {
            planetSize = diameter / 5500;
        } else if (type.equals("B")) {
            planetSize = diameter / 3000;
        }

        int posX = size / 2;
        int posY = size / 2;

        int shiftX = -planetSize / 2;
        int shiftY = -planetSize / 2;

        g.fillOval(posX + shiftX, posY + shiftY, planetSize, planetSize);

        String sizeString = getSizeString(type, diameter);

        g.setColor(new Color(0f, 0f, 0f));
        Font f = new Font("Tahoma", Font.BOLD, 12);
        g.setFont(f);
        int fsize = g.getFontMetrics(f).stringWidth(sizeString);
        g.drawString(sizeString, posX - fsize / 2, posY + 4);

     /*   if (!owner.equals("")) {
            Color c = Color.BLACK;
            if (side == 1) {
                c = Color.GREEN;
            } else if (side > 1) {
                c = relColor;
            }

            g.setColor(c);
            g.drawRect(0, 0, this.getPreferredSize().width, this.getPreferredSize().height - 1);
        }*/
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
