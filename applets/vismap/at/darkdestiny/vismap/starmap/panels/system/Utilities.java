/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.system;

import java.applet.Applet;
import java.awt.Insets;
import java.net.URL;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.logic.DisplayManagement;

/**
 *
 * @author Eobane
 */
public class Utilities {

    private static JButton showSystemButton;
    private static JButton sendFleetButton;
    private static JButton tradeRouteButton;
    private static ImageIcon showSystemIcon;
    private static ImageIcon sendFleetIcon;
    private static ImageIcon tradeRouteIcon; 

    public static JButton getMoveFleetButton(Applet mainApplet) {

        return sendFleetButton;
    }

    public static JButton getShowSystemButton(Applet mainApplet) {


        return showSystemButton;
    }

    
    public static JButton getCancelTradeRouteButton(Applet mainApplet) {
        return showSystemButton;
    }
    public static void setMain(AppletMain mainApplet) {


        try {


            URL file = DisplayManagement.class.getClassLoader().getResource("pics/goSystem.gif");

            URL file2 = DisplayManagement.class.getClassLoader().getResource("pics/movefleet.gif");

            URL file3 = DisplayManagement.class.getClassLoader().getResource("pics/cancel.jpg");

            showSystemIcon = new ImageIcon(mainApplet.getImage(file));
            
            sendFleetIcon = new ImageIcon(mainApplet.getImage(file2));
            tradeRouteIcon = new ImageIcon(mainApplet.getImage(file3));

        } catch (Exception e) {
        }
        
        showSystemButton = new JButton(showSystemIcon);
        sendFleetButton = new JButton(sendFleetIcon);
        tradeRouteButton = new JButton(sendFleetIcon);

        showSystemButton.setMargin(new Insets(0, 0, 0, 0));
        sendFleetButton.setMargin(new Insets(0, 0, 0, 0));
        tradeRouteButton.setMargin(new Insets(0, 0, 0, 0));

    }

    static String formatDate(long lastUpdate) {
           String date = "??-??-????";
            GregorianCalendar gc = new GregorianCalendar();
        if (lastUpdate > 0) {
            date = "";


            gc.setTimeInMillis(lastUpdate);
            int day = gc.get(GregorianCalendar.DAY_OF_MONTH);
            if (day < 10) {
                date += "0";
            }
            date += day;
            date +="-";

            int month = gc.get(GregorianCalendar.MONTH)+1;
            if (month < 10) {
                date += "0";
            }
            date += month;
            date +="-";

            int year = gc.get(GregorianCalendar.YEAR);
            date += year;

            date +="  ";


            int hour = gc.get(GregorianCalendar.HOUR_OF_DAY);
            if (hour < 10) {
                date += "0";
            }
            date += hour;
            date += ":";
            
            int minutes = gc.get(GregorianCalendar.MINUTE);
            if (minutes < 10) {
                date += "0";
            }
            date += minutes;

        }
        int minutes = 10;
        int seconds = minutes * 60;
        int millis = seconds * 1000;

        if((System.currentTimeMillis() - lastUpdate) < millis){
            date = "Aktuell";
        }
        return date;
    }
}
