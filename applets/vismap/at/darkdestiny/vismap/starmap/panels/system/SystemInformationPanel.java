/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.system;

import at.darkdestiny.vismap.buttons.BlueButton;
import at.darkdestiny.vismap.drawable.GraphPanel;
import at.darkdestiny.vismap.interfaces.I_InformationPanel;
import java.applet.AppletContext;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.starmap.PainterData;
import at.darkdestiny.vismap.starmap.dto.PlanetEntry;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import java.awt.Graphics;
import java.awt.event.MouseListener;

/**
 *
 * @author Horst
 */
public class SystemInformationPanel extends JPanel implements I_InformationPanel, ActionListener, Runnable {

    private SpringLayout layout;
    private String identifier;
    private AppletMain mainApplet;
    private SystemDesc systemDesc;
    private BlueButton showSystemButton;
    private GraphPanel suntransmitter;
    private boolean selected = false;
    private JLabel lastUpdate;

    public SystemInformationPanel(final SystemDesc systemDesc, final AppletMain mainApplet) {
        this.mainApplet = mainApplet;
        this.systemDesc = systemDesc;
        this.identifier = systemDesc.getIdentifier();
        setBackground(new Color(0f, 0f, 0f, 0.8f));

        showSystemButton = new BlueButton(15, 15, "goSystem.png", mainApplet.mainFrame.getPainter(), "System anzeigen");
        this.layout = new SpringLayout();
        this.setLayout(layout);

        JLabel systemName = new JLabel(systemDesc.getName());
        lastUpdate = new JLabel(Utilities.formatDate(systemDesc.getLastUpdate()));
        Color fontColor = new Color(0.6f, 0.6f, 1f);
        systemName.setForeground(fontColor);
        lastUpdate.setForeground(fontColor);
        lastUpdate.setFont(new Font("Tahoma", 1, 9));
        systemName.setFont(new Font("Tahoma", 1, 12));
        this.add(systemName);
        this.add(lastUpdate);
        this.add(showSystemButton);
        this.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent e) {
            }

            @Override
            public void mousePressed(java.awt.event.MouseEvent e) {
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent e) {
                if (mainApplet.mainFrame.getPainter().getMode().equals(EStarmapMode.SEND_FLEET)) {
                    mainApplet.mainFrame.sendFleetToSystem(systemDesc.getId(), systemDesc, systemDesc.getName());
                }

            }

            @Override
            public void mouseEntered(java.awt.event.MouseEvent e) {
                if (mainApplet.mainFrame.getPainter().getMode().equals(EStarmapMode.SEND_FLEET)) {
                    selected = true;
                    repaint();
                    mainApplet.mainFrame.repaint();
                }
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent e) {
                selected = false;
                repaint();
                mainApplet.mainFrame.repaint();
            }
        });
        showSystemButton.addActionListener(this);

        layout.putConstraint(SpringLayout.WEST, systemName, 182 / 2 - systemName.getPreferredSize().width / 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, systemName, 2, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, lastUpdate, 182 / 2 - lastUpdate.getPreferredSize().width / 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lastUpdate, 5, SpringLayout.SOUTH, systemName);



        layout.putConstraint(SpringLayout.NORTH, showSystemButton, 2, SpringLayout.NORTH, systemName);
        layout.putConstraint(SpringLayout.WEST, showSystemButton, 5, SpringLayout.EAST, systemName);
        PainterData painterData = (PainterData)mainApplet.mainFrame.getPainter().getPainterData();
        if(painterData.getTransmitterBySystem(systemDesc.getId()) != null){
            suntransmitter = new GraphPanel("suntransmitter15x15.png", 15, 15, "Sonnentransmitter-System");
            this.add(suntransmitter);
        layout.putConstraint(SpringLayout.NORTH, suntransmitter, 0, SpringLayout.NORTH, showSystemButton);
        layout.putConstraint(SpringLayout.WEST, suntransmitter, 5, SpringLayout.EAST, showSystemButton);
        }


        this.setVisible(true);

        int height = 50 + 70 * systemDesc.getPlanetList().size();
        this.setPreferredSize(new Dimension(182, height));
    }

    @Override
    public String getIdentifier() {
        return this.identifier;
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(new Color(0.0f, 0.0f, 0.0f, 0.8f));
        g.fillRect(0, 0, this.getPreferredSize().width, 40);


        super.paint(g);
        g.setColor(new Color(0.6f, 0.6f, 1f));
        g.drawRect(0, 0, this.getPreferredSize().width-1, 40);
        if (selected) {

            g.setColor(Color.RED);

            g.drawRect(0, 0, this.getPreferredSize().width - 1, this.getPreferredSize().height - 1);
        }
    }

    @Override
    public void run() {

        boolean firstPlanet = true;
        JPanel tmpPanel = null;

        for (PlanetEntry planetEntry : systemDesc.getPlanetList()) {
            PlanetInformationPanel pie = new PlanetInformationPanel(planetEntry, systemDesc, mainApplet);

            this.add(pie);

            if (firstPlanet) {
                layout.putConstraint(SpringLayout.WEST, pie, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, pie, 14, SpringLayout.SOUTH, lastUpdate);

                firstPlanet = false;

            } else {
                layout.putConstraint(SpringLayout.WEST, pie, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, pie, 3, SpringLayout.SOUTH, tmpPanel);
            }
            tmpPanel = pie;
            this.revalidate();
            this.repaint();
            try {
                Thread.sleep(10);
            } catch (Exception ex) {
                Logger.getLogger(SystemInformationPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        mainApplet.mainFrame.revalidate();
        mainApplet.mainFrame.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == showSystemButton) {
                AppletContext a = mainApplet.getAppletContext();
                a.showDocument(new URL(mainApplet.getCodeBase() + AppletConstants.LINK_GO_SYSTEM + ((at.darkdestiny.vismap.starmap.Painter) mainApplet.mainFrame.getPainter()).sendSystem.getId()), "_new");
            }
        } catch (Exception ex) {
            System.out.print("Error while hyperlinking:" + ex);
        }
    }
}
