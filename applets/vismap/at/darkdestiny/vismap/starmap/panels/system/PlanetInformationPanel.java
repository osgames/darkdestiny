/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.system;

import at.darkdestiny.vismap.buttons.BlueButton;
import at.darkdestiny.vismap.drawable.GraphPanel;
import java.applet.AppletContext;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.starmap.AppletData;
import at.darkdestiny.vismap.starmap.PainterData;
import at.darkdestiny.vismap.starmap.dto.PlanetEntry;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Horst
 */
public class PlanetInformationPanel extends JPanel implements ActionListener {

    private SpringLayout layout;
    private AppletMain mainApplet;
    private BlueButton showPlanetButton;
    private PlanetEntry planetEntry;
    private boolean selected = false;
    List<JLabel> labels;

    public PlanetInformationPanel(final PlanetEntry planetEntry, final SystemDesc sd, final AppletMain mainApplet) {
        this.mainApplet = mainApplet;
        this.planetEntry = planetEntry;

        labels = new ArrayList();

        setBackground(new Color(0.0f, 0.0f, 0.0f, 0.8f));


        showPlanetButton = new BlueButton(15, 15, "goPlanet.png", mainApplet.mainFrame.getPainter(), "Planet Anzeigen");

        this.layout = new SpringLayout();
        this.setLayout(layout);
        MouseListener ml = new MouseListener() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent e) {
            }

            @Override
            public void mousePressed(java.awt.event.MouseEvent e) {
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent e) {
                if (mainApplet.mainFrame.getPainter().getMode().equals(EStarmapMode.SEND_FLEET)) {
                    mainApplet.mainFrame.sendFleetToPlanet(planetEntry.getId(), sd, planetEntry.getName());
                }

            }

            @Override
            public void mouseEntered(java.awt.event.MouseEvent e) {
                if (mainApplet.mainFrame.getPainter().getMode().equals(EStarmapMode.SEND_FLEET)) {
                    selected = true;
                    repaint();
                    mainApplet.mainFrame.repaint();
                }
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent e) {
                selected = false;
                repaint();
                mainApplet.mainFrame.repaint();
            }
        };
        this.addMouseListener(ml);


        JLabel planetLabel = new JLabel(sd.getId() + ":" + planetEntry.getId());
        JLabel nameLabel = new JLabel(planetEntry.getName());

        String date = Utilities.formatDate(planetEntry.getLastUpdate());
        JLabel typeLandLabel = new JLabel(date);
        JLabel ownerLabel;
        if (!planetEntry.getOwner().equals("")) {

            ownerLabel = new JLabel(planetEntry.getOwner());

        } else {

            ownerLabel = new JLabel(" ");
            
        }
        Color fontColor = new Color(0.6f, 0.6f, 1f);

        planetLabel.setForeground(fontColor);
        nameLabel.setForeground(fontColor);
        typeLandLabel.setForeground(fontColor);
        ownerLabel.setForeground(fontColor);
        ownerLabel.setFont(new Font("Tahoma", 1, 9));
        planetLabel.setFont(new Font("Tahoma", 1, 9));
        nameLabel.setFont(new Font("Tahoma", 1, 9));
        typeLandLabel.setFont(new Font("Tahoma", 1, 9));

        showPlanetButton.addActionListener(this);


        labels.add(ownerLabel);
        labels.add(planetLabel);
        labels.add(nameLabel);
        labels.add(typeLandLabel);

        layout.putConstraint(SpringLayout.WEST, ownerLabel, 0, SpringLayout.WEST, planetLabel);
        layout.putConstraint(SpringLayout.NORTH, ownerLabel, 4, SpringLayout.SOUTH, planetLabel);

        layout.putConstraint(SpringLayout.WEST, nameLabel, 0, SpringLayout.WEST, planetLabel);
        layout.putConstraint(SpringLayout.NORTH, nameLabel, 4, SpringLayout.SOUTH, ownerLabel);

        layout.putConstraint(SpringLayout.WEST, typeLandLabel, 0, SpringLayout.WEST, planetLabel);
        layout.putConstraint(SpringLayout.NORTH, typeLandLabel, 4, SpringLayout.SOUTH, nameLabel);



        layout.putConstraint(SpringLayout.WEST, showPlanetButton, 3, SpringLayout.EAST, ownerLabel);
        layout.putConstraint(SpringLayout.NORTH, showPlanetButton, 2, SpringLayout.SOUTH, planetLabel);

        layout.putConstraint(SpringLayout.WEST, planetLabel, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, planetLabel, 5, SpringLayout.NORTH, this);
        if (planetEntry.getSide() == 1) {

            this.add(showPlanetButton);

        }

        PainterData painterData = (PainterData)mainApplet.mainFrame.getPainter().getPainterData();
        if (painterData.getTransmitterByPlanet(planetEntry.getId()) != null) {
            GraphPanel suntransmitter = new GraphPanel("suntransmitter15x15.png", 15, 15, "Sonnentransmitter-Planet");
            this.add(suntransmitter);
            layout.putConstraint(SpringLayout.NORTH, suntransmitter, 0, SpringLayout.NORTH, showPlanetButton);
            layout.putConstraint(SpringLayout.WEST, suntransmitter, 5, SpringLayout.EAST, showPlanetButton);
        }

        for (JLabel l : labels) {
            this.add(l);
        }

        Color relColor = null;
        try{
            relColor = Color.decode(((AppletData)mainApplet.getAppletData()).getRelations().get(planetEntry.getSide()).getColor());
        }catch(Exception e){

        }
        PlanetInformationPic pic = new PlanetInformationPic(planetEntry.getTypId(), planetEntry.getDiameter(), planetEntry.getOwner(), planetEntry.getSide(), relColor, ml);
        this.add(pic);

      //  this.add(test);
        layout.putConstraint(SpringLayout.NORTH, pic, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.EAST, pic, 0, SpringLayout.EAST, this);



        this.setPreferredSize(new Dimension(182, 67));
        this.setLayout(layout);
        this.setVisible(true);

    }

    @Override
    public void paint(Graphics g) {

        super.paint(g);
       if (!planetEntry.getOwner().equals("")) {
            Color c = Color.BLACK;
            if (planetEntry.getSide() == 1) {
                c = Color.GREEN;
            } else if (planetEntry.getSide() > 1) {
                c = Color.decode(((AppletData)mainApplet.getAppletData()).getRelations().get(planetEntry.getSide()).getColor());
            }

            g.setColor(c);
            g.fillRect(0, 0, 6, this.getPreferredSize().height);
            g.drawRect(0, 0, this.getPreferredSize().width, this.getPreferredSize().height - 1);
        } else {

            g.setColor(new Color(0.6f, 0.6f, 1f));
            g.drawRect(0, 0, this.getPreferredSize().width, this.getPreferredSize().height - 1);
        }

       if (selected) {

            g.setColor(Color.RED);

            g.drawRect(0, 0, this.getPreferredSize().width, this.getPreferredSize().height - 1);
        }
    }

    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == showPlanetButton) {
                AppletContext a = mainApplet.getAppletContext();
                a.showDocument(new URL(mainApplet.getCodeBase() + AppletConstants.LINK_GO_PLANET + this.planetEntry.getId()), "_new");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}