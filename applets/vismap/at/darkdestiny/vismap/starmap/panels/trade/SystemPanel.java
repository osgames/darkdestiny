/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.trade;

import at.darkdestiny.vismap.drawable.GraphPanel;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.starmap.dto.trade.TradePlanet;
import at.darkdestiny.vismap.starmap.dto.trade.TradeSystem;
import java.util.List;

/**
 *
 * @author HorstRabe
 */
public class SystemPanel extends JPanel {

    private String startSystem;
    private String endSystem;
    private SpringLayout layout;
    private RoutePanel routePanel;
    private TopSystemPanel topSystemPanel;
    protected JPanel bottomImage;

    public SystemPanel(TradeSystem tradeSystem, RoutePanel routePanel) {

        this.routePanel = routePanel;

        this.setBackground(Color.BLACK);
        this.startSystem = tradeSystem.getStartSystemName();
        this.endSystem = tradeSystem.getEndSystemName();
        this.layout = new SpringLayout();
        this.setLayout(layout);
        topSystemPanel = new TopSystemPanel(startSystem, endSystem);
        initializeComponents();
        addComponents();
        orderComponents();

        this.addPlanets(tradeSystem.getPlanets());
        updateSize();
  
    }

    private void addComponents() {
        this.add(topSystemPanel);
    }

    private void addPlanets(List<TradePlanet> planets) {

  
        PlanetPanel tmpPanel = null;
        int width = 0;
        double height = topSystemPanel.getPreferredSize().getHeight();
        JPanel tmpLeftImage = null;
        JPanel tmpRightImage = null;
        for (int i = 0; i < planets.size(); i++) {
            PlanetPanel planetPanel = new PlanetPanel(planets.get(i), this);
            this.add(planetPanel);

            JPanel leftSystemTop = new GraphPanel("leftSystemTop.png", 4, 13);
            JPanel rightSystemTop = new GraphPanel("leftSystemTop.png", 4, 13);


            this.add(leftSystemTop);
            this.add(rightSystemTop);

                   layout.putConstraint(SpringLayout.WEST, leftSystemTop, 0, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.NORTH, leftSystemTop, 0, SpringLayout.NORTH, planetPanel);

            layout.putConstraint(SpringLayout.EAST, rightSystemTop, 0, SpringLayout.EAST, this);
            layout.putConstraint(SpringLayout.NORTH, rightSystemTop, 0, SpringLayout.NORTH, planetPanel);

 
           for (int j = 0; j < planets.get(i).getRessources().size(); j++) {

                JPanel leftImage = new GraphPanel("leftSystemRess.png", 4, 24);
                JPanel rightImage = new GraphPanel("rightSystemRess.png", 4, 24);
                this.add(leftImage);
                this.add(rightImage);
                if (j == 0) {
                    layout.putConstraint(SpringLayout.WEST, leftImage, 0, SpringLayout.WEST, this);
                    layout.putConstraint(SpringLayout.NORTH, leftImage, 0, SpringLayout.SOUTH, leftSystemTop);
                    layout.putConstraint(SpringLayout.EAST, rightImage, 0, SpringLayout.EAST, this);
                    layout.putConstraint(SpringLayout.NORTH, rightImage, 0, SpringLayout.SOUTH, rightSystemTop);


                } else {
                    layout.putConstraint(SpringLayout.WEST, leftImage, 0, SpringLayout.WEST, this);
                    layout.putConstraint(SpringLayout.NORTH, leftImage, 0, SpringLayout.SOUTH, tmpLeftImage);
                    layout.putConstraint(SpringLayout.EAST, rightImage, 0, SpringLayout.EAST, this);
                    layout.putConstraint(SpringLayout.NORTH, rightImage, 0, SpringLayout.SOUTH, tmpRightImage);

                }

                tmpLeftImage = leftImage;
                tmpRightImage = rightImage;
            }

            if (i == 0) {
                layout.putConstraint(SpringLayout.WEST, planetPanel, 4, SpringLayout.WEST, tmpLeftImage);
                layout.putConstraint(SpringLayout.NORTH, planetPanel, 0, SpringLayout.SOUTH, topSystemPanel);

            } else {
                layout.putConstraint(SpringLayout.WEST, planetPanel, 4, SpringLayout.WEST, tmpLeftImage);
                layout.putConstraint(SpringLayout.NORTH, planetPanel, 0, SpringLayout.SOUTH, tmpPanel);
            }
            JPanel leftSystemRessBottom = new GraphPanel("leftSystemRessBottom.png", 4, 5);
            JPanel rightSystemRessBottom = new GraphPanel("rightSystemRessBottom.png", 4, 5);

            this.add(leftSystemRessBottom);
            this.add(rightSystemRessBottom);
  layout.putConstraint(SpringLayout.SOUTH, leftSystemRessBottom, 0, SpringLayout.SOUTH, planetPanel);
            layout.putConstraint(SpringLayout.WEST, leftSystemRessBottom, 0, SpringLayout.WEST, this);

            layout.putConstraint(SpringLayout.SOUTH, rightSystemRessBottom, 0, SpringLayout.SOUTH, planetPanel);
            layout.putConstraint(SpringLayout.EAST, rightSystemRessBottom, 0, SpringLayout.EAST, this);
          

            height += planetPanel.getPreferredSize().getHeight();
            tmpPanel = planetPanel;

        }



        bottomImage = new GraphPanel("bottomSystem.png", 182, 9);
        this.add(bottomImage);
        layout.putConstraint(SpringLayout.WEST, bottomImage, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, bottomImage, 0, SpringLayout.SOUTH, tmpPanel);
        height += 12;
   
        this.setPreferredSize(new Dimension(182, (int) height));
    }

    private void initializeComponents() {
    }

    private void orderComponents() {

        layout.putConstraint(SpringLayout.WEST, topSystemPanel, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, topSystemPanel, 0, SpringLayout.NORTH, this);

    }

    public void updateSize() {

        orderComponents();

    }

    /**
     * @return the routePanel
     */
    public RoutePanel getRoutePanel() {
        return routePanel;
    }
}