/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.trade;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;
import at.darkdestiny.vismap.drawable.CentredBackgroundBorder;
import at.darkdestiny.vismap.starmap.dto.LoadingEntry;
import at.darkdestiny.vismap.starmap.dto.TradeRouteDetail;
import at.darkdestiny.vismap.logic.DisplayManagement;

/**
 *
 * @author HorstRabe
 */
public class RessourcePanel extends JPanel {

    private JLabel capacityLabel;
    private String ressourceName;
    private SpringLayout layout;
    private int capacity;

    public RessourcePanel(TradeRouteDetail tradeRessource) {

        this.ressourceName = tradeRessource.getRessId() + " : ";
        this.capacity = tradeRessource.getLastTransport();
        layout = new SpringLayout();
        this.setPreferredSize(new Dimension(142, 24));
        this.setLayout(layout);

        String picName = "";
        switch (tradeRessource.getRessId()) {
            case (1):
                picName = "ironP.png";
                break;
                case (2):
                picName = "steelP.png";
                break;
                case (3):
                picName = "terkonitP.png";
                break;
            case (4):
                picName = "ynkeP.png";
                break;
            case (5):
                picName = "howalP.png";
                break;
            case (9):
                picName = "foodP.png";
                break;


        }

        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic(picName), picName);
        final Border background = new CentredBackgroundBorder(bi);

        this.setBorder(background);
        this.repaint();

        initializeComponents();
        addComponents();
        orderComponents();





    }

    public RessourcePanel(LoadingEntry loadingEntry) {

        this.ressourceName = loadingEntry.getRessourceName() + " : ";
        this.capacity = loadingEntry.getCapacity();
        layout = new SpringLayout();
        this.setPreferredSize(new Dimension(142, 24));
        this.setLayout(layout);

        String picName = "";
        switch (loadingEntry.getRessourceId()) {
            case (1):
                picName = "ironP.png";
                break;
                case (2):
                picName = "steelP.png";
                break;
                case (3):
                picName = "terkonitP.png";
                break;
            case (4):
                picName = "ynkeP.png";
                break;
            case (5):
                picName = "howalP.png";
                break;
            case (9):
                picName = "foodP.png";
                break;


        }

        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic(picName), picName);
        final Border background = new CentredBackgroundBorder(bi);

        this.setBorder(background);
        this.repaint();

        initializeComponents();
        addComponents();
        orderComponents();





    }
    private void addComponents() {
        this.add(capacityLabel);
    }

    private void initializeComponents() {
        capacityLabel = new JLabel(String.valueOf(capacity));
        this.capacityLabel.setForeground(Color.WHITE);
    }

    private void orderComponents() {

        layout.putConstraint(SpringLayout.NORTH, capacityLabel, 2, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.EAST, capacityLabel, -4, SpringLayout.EAST, this);

    }
}
