/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.starmap.panels.trade;

import at.darkdestiny.vismap.starmap.drawable.TradeRoute;
import at.darkdestiny.vismap.starmap.dto.trade.TradeSystem;
import java.awt.Color;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

/**
 *
 * @author HorstRabe
 */
public class TradeRoutePanel extends JPanel{
  
    private String system1;
    private String system2;
    private JLabel system1Label;
    private JLabel system2Label;
    
    public int height = 25;
    
    protected SpringLayout layout;
    
    public TradeRoutePanel(TradeRoute tradeRoute){
        
                
         this.system1 = tradeRoute.getStartSystemName();
        this.system2 = tradeRoute.getEndSystemName();
        
        this.layout = new SpringLayout();
        this.setLayout(layout);
        
        
           initializeComponents();
      addComponents();
      orderComponents();
       addSystems(tradeRoute.getSystems());
      
      updateSize();
        
        
    }

    private void addComponents() {
        this.add(system1Label);
        this.add(system2Label);
    }

    private void addSystems(List<TradeSystem> systems) {
        
    }

    private void initializeComponents() {
      system1Label = new JLabel(system1 + "<=>");
      system2Label = new JLabel(system2);
      system1Label.setForeground(Color.WHITE);
      system2Label.setForeground(Color.WHITE);
    }

    private void orderComponents() {      
        
        layout.putConstraint(SpringLayout.WEST,  system1Label, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST,  system2Label, 0, SpringLayout.EAST, this);
      
    }
    
    public void updateSize(){
        
        orderComponents();
        
    }
}
