/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.trade;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;

import at.darkdestiny.vismap.drawable.CentredBackgroundBorder;
import at.darkdestiny.vismap.logic.DisplayManagement;

/**
 *
 * @author HorstRabe
 */
public class TopSystemPanel extends JPanel {

    SpringLayout layout;
    private JLabel startSystemLabel;
    private JLabel endSystemLabel;
    private String startSystem;
    private String endSystem;

    public TopSystemPanel(String planet1, String planet2) {



        this.startSystem = planet1;
        this.endSystem = planet2;
        this.layout = new SpringLayout();
        this.setLayout(layout);
        this.setPreferredSize(new Dimension(182, 16));
        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic("topSystem.png"), "topSystem.png");
        final Border background = new CentredBackgroundBorder(bi);

        this.setBorder(background);
        initializeComponents();
        addComponents();
        layout.putConstraint(SpringLayout.WEST, startSystemLabel, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, startSystemLabel, 3, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.EAST, endSystemLabel, -4, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, endSystemLabel, 3, SpringLayout.NORTH, this);
        this.repaint();
    }

    private void addComponents() {
        this.add(startSystemLabel);
        this.add(endSystemLabel);
    }

    private void initializeComponents() {
        startSystemLabel = new JLabel(startSystem);
        endSystemLabel = new JLabel(endSystem);
        startSystemLabel.setFont(new Font("Times New Roman", 1, 9));
        startSystemLabel.setForeground(Color.WHITE);
        endSystemLabel.setFont(new Font("Times New Roman", 1, 9));
        endSystemLabel.setForeground(Color.WHITE);
    }
}
