/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.trade;

import at.darkdestiny.vismap.interfaces.I_InformationPanel;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.starmap.drawable.TradeRoute;
import at.darkdestiny.vismap.starmap.panels.trade.TradeRoutePanel;

/**
 *
 * @author HorstRabe
 */
public class TradePanel extends JPanel implements I_InformationPanel{

    private SpringLayout layout;
    private String identifier;
    

    public TradePanel(TradeRoute tradeRoute) {

      
        this.identifier = tradeRoute.getIdentifier();
        this.setBackground(Color.BLACK);
        TradeRoutePanel routePanel = new TradeRoutePanel(tradeRoute);

        this.add(routePanel);
         
        layout.putConstraint(SpringLayout.WEST, routePanel, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.SOUTH, routePanel, 0, SpringLayout.SOUTH, this);
        this.setPreferredSize(routePanel.getPreferredSize());

        this.setLayout(layout);
        this.setVisible(true);
    }

    public String getIdentifier() {
        return this.identifier;
    }



  
}
