/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.trade;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.starmap.drawable.TradeRoute;
import at.darkdestiny.vismap.starmap.dto.trade.TradeSystem;
import java.util.List;

/**
 *
 * @author HorstRabe
 */
public class RoutePanel extends JPanel {

    private String system1;
    private String system2;
    private JLabel system1Label;
    private JLabel between;
    private JLabel system2Label;
    public int height = 25;
    ArrayList<TradeRoute> subRoutes;
    protected SpringLayout layout;
    protected AppletMain mainApplet;
    private TradeRoute tradeRoute;

    public RoutePanel(TradeRoute tradeRoute, AppletMain mainApplet) {

        this.mainApplet = mainApplet;

        this.setBackground(Color.BLACK);
        this.system1 = tradeRoute.getStartSystemName();
        this.system2 = tradeRoute.getEndSystemName();


        this.tradeRoute = tradeRoute;
        this.layout = new SpringLayout();
        this.setLayout(layout);



        initializeComponents();
        addComponents();
        orderComponents();

        addSystems(tradeRoute.getSystems());
        updateSize();


    }

    private void addComponents() {
        this.add(system1Label);
        this.add(system2Label);
        this.add(between);
    }

    private void addSystems(List<TradeSystem> systems) {

        SystemPanel tmpPanel = null;

        int height = 0;
        for (int i = 0; i < systems.size(); i++) {

            SystemPanel systemPanel = new SystemPanel(systems.get(i), this);



            this.add(systemPanel);

            if (i == 0) {
                layout.putConstraint(SpringLayout.WEST, systemPanel, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, systemPanel, 0, SpringLayout.SOUTH, system1Label);

            } else {
                layout.putConstraint(SpringLayout.WEST, systemPanel, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, systemPanel, 0, SpringLayout.SOUTH, tmpPanel);

            }

            height += systemPanel.getPreferredSize().getHeight();
            tmpPanel = systemPanel;

        }
        this.setPreferredSize(new Dimension((int) tmpPanel.getPreferredSize().getWidth(), height));


    }

    private void initializeComponents() {
        system1Label = new JLabel(system1);
        system2Label = new JLabel(system2);
        system1Label.setFont(new Font("Times New Roman", 1, 10));
        system2Label.setFont(new Font("Times New Roman", 1, 10));
        between = new JLabel("<=>");
        between.setFont(new Font("Tahoma", 1, 9));

        system1Label.setForeground(Color.WHITE);
        between.setForeground(Color.WHITE);
        system2Label.setForeground(Color.WHITE);
    }

    private void orderComponents() {

        layout.putConstraint(SpringLayout.NORTH, system1Label, 2, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, system1Label, 3, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, system2Label, 2, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.EAST, system2Label, -4, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, between, 2, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, between, 79, SpringLayout.WEST, this);

    }

    public void updateSize() {

        orderComponents();

    }

    /**
     * @return the tradeRoute
     */
    public TradeRoute getTradeRoute() {
        return tradeRoute;
    }
}
