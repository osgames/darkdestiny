/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.trade;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;
import at.darkdestiny.vismap.drawable.CentredBackgroundBorder;
import at.darkdestiny.vismap.logic.DisplayManagement;

/**
 *
 * @author HorstRabe
 */
public class TopPlanetPanel extends JPanel {

    SpringLayout layout;
    private JLabel startPlanetLabel;
    private JLabel endPlanetLabel;
    private String startPlanet;
    private String endPlanet;

    public TopPlanetPanel(String planet1, String planet2) {

        this.startPlanet = planet1;
        this.endPlanet = planet2;
        this.layout = new SpringLayout();
        this.setLayout(layout);
        this.setPreferredSize(new Dimension(174, 13));
        BufferedImage bi = DisplayManagement.toBufferedImage(DisplayManagement.getGraphic("topPlanet.png"), "topPlanet.png");
        final Border background = new CentredBackgroundBorder(bi);

        this.setBorder(background);
        initializeComponents();
        addComponents();
        layout.putConstraint(SpringLayout.WEST, startPlanetLabel, 4, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, startPlanetLabel, 1, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.EAST, endPlanetLabel, -4, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, endPlanetLabel, 1, SpringLayout.NORTH, this);
         this.repaint();
    }

    private void addComponents() {
        this.add(startPlanetLabel);
        this.add(endPlanetLabel);
    }

    private void initializeComponents() {
        startPlanetLabel = new JLabel(startPlanet);
        endPlanetLabel = new JLabel(endPlanet);
        startPlanetLabel.setFont(new Font("Times New Roman", 1, 9));
        startPlanetLabel.setForeground(Color.WHITE);
        endPlanetLabel.setFont(new Font("Times New Roman", 1, 9));
        endPlanetLabel.setForeground(Color.WHITE);
    }
}
