/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.starmap.AppletData;
import at.darkdestiny.vismap.starmap.dto.PlanetEntry;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

/**
 *
 * @author LensiPensi
 */
public class PlanetIcon extends JPanel {

    private PlanetEntry planetEntry;
    private AppletMain mainApplet;
    int size = 15;

    public PlanetIcon(PlanetEntry planetEntry, AppletMain mainApplet) {
        this.setPreferredSize(new Dimension(size, size));
        this.setOpaque(false);
        setToolTipText("Planet");
        this.planetEntry = planetEntry;
        this.mainApplet = mainApplet;
        SpringLayout layout = new SpringLayout();
        this.setLayout(layout);

        if (planetEntry == null) {
            JLabel questionMark = new JLabel("?");
            questionMark.setForeground(Color.WHITE);
            this.add(questionMark);
            layout.putConstraint(SpringLayout.NORTH, questionMark, 0, SpringLayout.NORTH, this);
            layout.putConstraint(SpringLayout.WEST, questionMark, 4, SpringLayout.WEST, this);
        }

    }

    @Override
    public void paint(Graphics gc) {
        super.paint(gc);
        if (planetEntry == null) {
            return;
        }

        Color c = Color.WHITE;

        if (planetEntry.getSide() == 1) {
            c = Color.GREEN;
        } else if (planetEntry.getSide() > 1) {
            c = Color.decode(((AppletData)mainApplet.getAppletData()).getRelations().get(planetEntry.getSide()).getColor());
        }
        gc.setColor(c);
        gc.fillRect(0, 0, size, size);


    }
}
