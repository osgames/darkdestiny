/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;

/**
 *
 * @author HorstRabe
 */
public class FleetPanel extends JPanel {

    private SpringLayout layout;

    public FleetPanel(FleetEntry fleetEntry, AppletMain am) {
        this.layout = new SpringLayout();
        setBackground(new Color(0f,0f,0f,0.8f));
        this.setLayout(layout);
        TopFleetPanel topFleetPanel = new TopFleetPanel(fleetEntry, this, am);

        this.add(topFleetPanel);
        ShippingPanel shippingPanel = null;
        if (fleetEntry.getLoading().size() > 0) {
            shippingPanel = new ShippingPanel(fleetEntry);
            this.add(shippingPanel);
        }
        ShipsPanel shipsPanel = null;

        shipsPanel = new ShipsPanel(fleetEntry, am);
        this.add(shipsPanel);

        int height = 0;

        layout.putConstraint(SpringLayout.NORTH, topFleetPanel, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, topFleetPanel, 0, SpringLayout.WEST, this);


        layout.putConstraint(SpringLayout.NORTH, shipsPanel, 0, SpringLayout.SOUTH, topFleetPanel);
        layout.putConstraint(SpringLayout.WEST, shipsPanel, 0, SpringLayout.WEST, this);

        if (fleetEntry.getLoading().size() > 0) {
            layout.putConstraint(SpringLayout.NORTH, shippingPanel, 0, SpringLayout.SOUTH, shipsPanel);
            layout.putConstraint(SpringLayout.WEST, shippingPanel, 0, SpringLayout.WEST, this);
            height += shippingPanel.getPreferredSize().getHeight();
        }
        height += shipsPanel.getPreferredSize().getHeight();
        height += topFleetPanel.getPreferredSize().getHeight();
        this.setPreferredSize(new Dimension(182, height));


    }
}
