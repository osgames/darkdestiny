/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import at.darkdestiny.vismap.drawable.GraphPanel;
import at.darkdestiny.vismap.gui.AppletMain;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.starmap.dto.ChassisEntry;
import java.awt.Graphics;
import javax.swing.JComponent;

/**
 *
 * @author HorstRabe
 */
public class ShipPanel extends JPanel {

    private SpringLayout layout;
    private JLabel count;
    private JLabel designName;
    private ChassisEntry ship;

    public ShipPanel(ChassisEntry ship, AppletMain am) {
        this.ship = ship;
        this.layout = new SpringLayout();
        this.setLayout(layout);
        Color fontColor = new Color(0.3f, 0.3f, 1f);
        setBackground(new Color(0f, 0f, 0f, 0.8f));

        count = new JLabel(String.valueOf(ship.getCount()));

        designName = new JLabel(ship.getDesignName());

        JLabel chassisLabel = new ChassisLabel(ship.getChassisId(), am);

        this.add(count);
        this.add(designName);
        this.add(chassisLabel);

        count.setFont(new Font("Tahoma", 1, 9));
        count.setForeground(fontColor);
        designName.setFont(new Font("Tahoma", 1, 9));
        designName.setForeground(fontColor);
        layout.putConstraint(SpringLayout.WEST, chassisLabel, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, chassisLabel, 4, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, designName, 31, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, designName, 4, SpringLayout.NORTH, this);


        layout.putConstraint(SpringLayout.WEST, count, 0, SpringLayout.WEST, designName);
        layout.putConstraint(SpringLayout.NORTH, count, 2, SpringLayout.SOUTH, designName);

        JComponent tmp = chassisLabel;
        boolean first = true;
        for (ChassisEntry ce : ship.getLoadedChassis()) {
            ShipPanel sp = new ShipPanel(ce, am);
            this.add(sp);
            if (first) {
                GraphPanel gp = new GraphPanel("loaded.png", 25, 25, "Geladen", false);
                this.add(gp);
                layout.putConstraint(SpringLayout.WEST, gp, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, gp, 0, SpringLayout.SOUTH, tmp);
                

            }
            layout.putConstraint(SpringLayout.WEST, sp, 27, SpringLayout.WEST, this);
            layout.putConstraint(SpringLayout.NORTH, sp, 0, SpringLayout.SOUTH, tmp);

            tmp = sp;
        }

        this.setPreferredSize(new Dimension(182, 30 + ship.getLoadedChassis().size() * 30));
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(new Color(0.3f, 0.3f, 1f));
        g.drawRect(0, 2, 27, 27);
        g.drawRect(27, 2, this.getPreferredSize().width - 1 - 27, 27);
    }
}
