/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import at.darkdestiny.vismap.gui.AppletMain;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.starmap.dto.ChassisEntry;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import java.util.List;

/**
 *
 * @author HorstRabe
 */
public class ShipsPanel extends JPanel {

    private SpringLayout layout;

    private AppletMain am;
    public ShipsPanel(FleetEntry fleetEntry, AppletMain am) {

        this.layout = new SpringLayout();
        this.setLayout(layout);
        this.am = am;
        setBackground(new Color(0f,0f,0f,0.8f));


        this.layout = new SpringLayout();
        this.setLayout(layout);
        this.addShips(fleetEntry.getChassis());

    }

    private void addShips(List<ChassisEntry> chassis) {
        ShipPanel tmpPanel = null;

        int ressourcesHeight = 0;
        for (int i = 0; i < chassis.size(); i++) {

            ShipPanel chassisPanel = new ShipPanel(chassis.get(i), am);
            this.add(chassisPanel);


            if (i == 0) {
                layout.putConstraint(SpringLayout.WEST, chassisPanel, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, chassisPanel, 0, SpringLayout.NORTH, this);



            } else {
                layout.putConstraint(SpringLayout.WEST, chassisPanel, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, chassisPanel, 0, SpringLayout.SOUTH, tmpPanel);



            }

            ressourcesHeight += chassisPanel.getPreferredSize().getHeight();
            tmpPanel = chassisPanel;
        }


        double height = ressourcesHeight;

        this.setPreferredSize(new Dimension(182, (int) height));
    }
}
