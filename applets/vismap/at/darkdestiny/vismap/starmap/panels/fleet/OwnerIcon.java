/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.starmap.AppletData;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author LensiPensi
 */
public class OwnerIcon extends JPanel {

    private AppletMain mainApplet;
    int size = 15;
    int status;

    public OwnerIcon(int status, AppletMain mainApplet) {
        this.mainApplet = mainApplet;
        this.status = status;
        this.setPreferredSize(new Dimension(size, size));
        this.setOpaque(false);

        setToolTipText("Besitzer");

    }

    @Override
    public void paint(Graphics gc) {
        super.paint(gc);
         if (status == AppletConstants.FLEET_OWN) {
            gc.setColor(AppletConstants.FLEET_COLOR_OWN);
        } else {
             if(status == AppletConstants.FLEET_ALLIED){
                 status = 10;
             }
            Color c = Color.decode(((AppletData)mainApplet.getAppletData()).getRelations().get(status).getColor());
            gc.setColor(c);
        }
        gc.fillRect(0, 0, size, size);


    }
}
