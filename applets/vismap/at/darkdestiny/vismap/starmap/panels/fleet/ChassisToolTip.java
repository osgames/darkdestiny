/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import java.awt.Graphics;
import java.awt.event.MouseEvent;

/**
 *
 * @author Admin
 */
public class ChassisToolTip {
MouseEvent e;
    public ChassisToolTip(MouseEvent e, int chassisSize) {
        this.e = e;
    }

    public void draw(Graphics g) {
        g.drawString("Chassis", e.getX(), e.getY());
    }
    
}
