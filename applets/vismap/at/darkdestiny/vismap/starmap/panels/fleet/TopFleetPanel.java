/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import at.darkdestiny.vismap.buttons.BlueButton;
import at.darkdestiny.vismap.drawable.GraphPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.joinmap.DataLoader;
import at.darkdestiny.vismap.starmap.PainterData;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.drawable.Transmitter;
import java.awt.Component;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HorstRabe
 */
public class TopFleetPanel extends JPanel implements ActionListener {

    private SpringLayout layout;
    private JLabel endSystemValue;
    private JLabel nameValue;
    private JLabel ownerValue;
    private JLabel speedValue;
    private JLabel timeRemainingValue;
    private JLabel planetValue;
    private AppletMain appletMain;
    private FleetEntry fleetEntry;
    private int iconSize = 15;
    private BlueButton sendFleetButton;
    private BlueButton scanButton;
    private FleetPanel fleetPanel;
    private BlueButton transmitter;

    public TopFleetPanel(final FleetEntry fleetEntry, FleetPanel fleetPanel, AppletMain am) {

        setBackground(new Color(0f, 0f, 0f, 0.8f));

        this.appletMain = am;
        this.fleetPanel = fleetPanel;
        this.fleetEntry = fleetEntry;
        this.layout = new SpringLayout();
        this.setLayout(layout);

        //suntransmitter15x15
        sendFleetButton = new BlueButton(15, 15, "movefleet.gif", am.mainFrame.getPainter(), "Flotte versenden");

        sendFleetButton.addActionListener(this);
        if (fleetEntry.getStatus() == AppletConstants.FLEET_OWN && fleetEntry.getTimeRemaiuning() == 0) {
            if (fleetEntry.isFleetFormation() && !fleetEntry.isFleetFormationLeader()) {
                //doNothing
            } else {
                if (am.mainFrame.getPainter().getPainterData().getDisplayObjects().get(AppletConstants.DRAW_LEVEL_TRANSMITTER) != null) {
                    for (I_Object_OneCoordinate o : am.mainFrame.getPainter().getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_TRANSMITTER)) {
                        if (!(o instanceof Transmitter)) {
                            continue;
                        }
                        final Transmitter t = (Transmitter) o;
                        if (t.getSystemId() == fleetEntry.getEndSystemId()) {
                            if (t.isTransitAllowed()) {
                                transmitter = new BlueButton(15, 15, "suntransmitter15x15.png", am.mainFrame.getPainter(), "Transmitter " + t.getId());
                                this.add(transmitter);
                                transmitter.addActionListener(new ActionListener() {

                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        appletMain.mainFrame.sendFleetToTransmitter(fleetEntry, t);
                                    }
                                });
                            } else {
                                transmitter = new BlueButton(15, 15, "suntransmitter15x15_red.png", am.mainFrame.getPainter(), "Transmitter " + t.getId());
                                this.add(transmitter);
                                transmitter.setToolTipText("<html>Keine Kontrolle �ber<BR> den Sonnentransmitter</html>");
                            }
                        }
                    }
                }
                this.add(sendFleetButton);
            }
        }
        if (fleetEntry.getEndSystemId() != 0) {
            endSystemValue = new JLabel(fleetEntry.getEndSystem() + "[" + fleetEntry.getEndSystemId() + "]");
        } else if (!fleetEntry.isTargetVisible()) {
            endSystemValue = new JLabel("Unbekannt");
        } else {
            endSystemValue = new JLabel(fleetEntry.getEndSystem());

        }
        nameValue = new JLabel(fleetEntry.getName());
        ownerValue = new JLabel(fleetEntry.getOwner());
        if (fleetEntry.getSpeed() != 999) {
            speedValue = new JLabel(String.valueOf(fleetEntry.getSpeed()));
        } else {
            speedValue = new JLabel("???");
        }
        timeRemainingValue = new JLabel(String.valueOf(fleetEntry.getTimeRemaiuning()));
        if (!fleetEntry.isTargetVisible()) {
            planetValue = new JLabel("Unbekannt");

        } else if (fleetEntry.getEndPlanetId() == 0) {
            planetValue = new JLabel(" ");
        } else {
            planetValue = new JLabel(fleetEntry.getEndPlanet() + "[" + fleetEntry.getEndPlanetId() + "]");
        }

        if (fleetEntry.getTimeRemaiuning() <= 0) {

            boolean scanPossible = false;
            if (fleetEntry.getScanDuration() != 0) {
                scanPossible = true;
            } else {
                if (fleetEntry.isScanPlanet() && fleetEntry.getEndPlanetId() > 0) {
                    scanPossible = true;
                } else if (fleetEntry.isScanSystem()) {
                    scanPossible = true;

                }
            }
            if (scanPossible) {
                String tooltip = "";
                if (fleetEntry.getEndPlanetId() > 0) {
                    scanButton = new BlueButton(15, 15, "scan_planet.png", am.mainFrame.getPainter(), tooltip);
                    scanButton.addActionListener(this);
                    if (fleetEntry.getScanDuration() > 0) {
                        tooltip = "Scan im Gange : " + fleetEntry.getScanDuration() + " Ticks";
                        scanButton.setEnabled(false);
                    } else if (fleetEntry.getScanDuration() == -1) {
                        tooltip = "Scan gestartet";
                        scanButton.setEnabled(false);
                    } else {
                        tooltip = "Planet scannen";
                    }
                } else {
                    scanButton = new BlueButton(15, 15, "scan_system.png", am.mainFrame.getPainter(), tooltip);
                    scanButton.addActionListener(this);
                    if (fleetEntry.getScanDuration() > 0) {
                        tooltip = "Scan im Gange : " + fleetEntry.getScanDuration() + " Ticks";
                        scanButton.setEnabled(false);
                    } else if (fleetEntry.getScanDuration() == -1) {
                        tooltip = "Scan gestartet";
                        scanButton.setEnabled(false);
                    } else {
                        tooltip = "System scannen";
                    }
                }
                scanButton.setToolTipText(tooltip);

                this.add(scanButton);
            }

        } else {
            sendFleetButton.setVisible(false);
        }

        Font font = new Font("Tahoma", 1, 9);
        if (fleetEntry.getStatus() != 3) {
            this.add(planetValue);
            planetValue.setFont(font);
            planetValue.setForeground(Color.WHITE);
        }

        this.add(endSystemValue);
        endSystemValue.setFont(font);
        endSystemValue.setForeground(Color.WHITE);
        this.add(nameValue);
        nameValue.setFont(font);
        nameValue.setForeground(Color.WHITE);
        this.add(ownerValue);
        ownerValue.setFont(font);
        ownerValue.setForeground(Color.WHITE);
        this.add(speedValue);
        speedValue.setFont(font);
        speedValue.setForeground(Color.WHITE);

        GraphPanel speedIcon = new GraphPanel("speed.png", 15, 15, "Geschwindigkeit", false);
        GraphPanel ticksIcon = new GraphPanel("ticks.png", 15, 15, "Restliche Ticks", false);

        if (fleetEntry.getTimeRemaiuning() > 0) {
            this.add(timeRemainingValue);

            this.add(ticksIcon);
        }
        timeRemainingValue.setFont(font);
        timeRemainingValue.setForeground(Color.WHITE);

        SystemDesc systemDesc = ((PainterData) am.mainFrame.getPainter().getPainterData()).getSystem(fleetEntry.getEndSystemId());

        SystemIcon sip = new SystemIcon(systemDesc);

        PlanetIcon pip = null;

        if (systemDesc != null) {
            pip = new PlanetIcon(systemDesc.getEndPlanet(fleetEntry.getEndPlanetId()), am);
        } else {
            pip = new PlanetIcon(null, am);

        }
        OwnerIcon oip = new OwnerIcon(fleetEntry.getStatus(), am);

        this.add(sip);
        this.add(oip);
        this.add(speedIcon);
        this.add(pip);

        layout.putConstraint(SpringLayout.NORTH, sendFleetButton, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.EAST, sendFleetButton, 0, SpringLayout.EAST, this);

        Component tmp = sendFleetButton;

        if (scanButton != null) {
            layout.putConstraint(SpringLayout.NORTH, scanButton, 0, SpringLayout.NORTH, this);
            layout.putConstraint(SpringLayout.EAST, scanButton, 0, SpringLayout.WEST, sendFleetButton);
            tmp = scanButton;

        }

        if (transmitter != null) {
            layout.putConstraint(SpringLayout.NORTH, transmitter, 0, SpringLayout.NORTH, this);
            layout.putConstraint(SpringLayout.EAST, transmitter, 0, SpringLayout.WEST, tmp);
        }
        layout.putConstraint(SpringLayout.NORTH, oip, iconSize, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, oip, 0, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, sip, 0, SpringLayout.SOUTH, oip);
        layout.putConstraint(SpringLayout.WEST, sip, 0, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, pip, 0, SpringLayout.SOUTH, sip);
        layout.putConstraint(SpringLayout.WEST, pip, 0, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, speedIcon, 0, SpringLayout.SOUTH, pip);
        layout.putConstraint(SpringLayout.WEST, speedIcon, 0, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, ticksIcon, 0, SpringLayout.SOUTH, pip);
        layout.putConstraint(SpringLayout.WEST, ticksIcon, 182 / 2, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.WEST, nameValue, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, nameValue, 2, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, ownerValue, 18, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, ownerValue, 5, SpringLayout.SOUTH, nameValue);

        layout.putConstraint(SpringLayout.WEST, endSystemValue, 0, SpringLayout.WEST, ownerValue);
        layout.putConstraint(SpringLayout.NORTH, endSystemValue, 3, SpringLayout.SOUTH, ownerValue);
        if (fleetEntry.getStatus() != 3) {

            layout.putConstraint(SpringLayout.WEST, planetValue, 0, SpringLayout.WEST, endSystemValue);
            layout.putConstraint(SpringLayout.NORTH, planetValue, 4, SpringLayout.SOUTH, endSystemValue);

        }

        layout.putConstraint(SpringLayout.WEST, speedValue, 0, SpringLayout.WEST, endSystemValue);
        layout.putConstraint(SpringLayout.NORTH, speedValue, 4, SpringLayout.SOUTH, planetValue);

        layout.putConstraint(SpringLayout.WEST, timeRemainingValue, 182 / 2 + 18, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, timeRemainingValue, 0, SpringLayout.NORTH, speedValue);

        this.setPreferredSize(new Dimension(182, iconSize * 5 + 1));

    }

    public void paint(Graphics g) {

        super.paint(g);
        g.setColor(new Color(0.3f, 0.3f, 1f));
        g.drawRect(0, 0, this.getPreferredSize().width - 1, iconSize);
        g.drawRect(0, 0, this.getPreferredSize().width - 1, this.getPreferredSize().height - 1);
        g.drawRect(0, iconSize, iconSize, this.getPreferredSize().height - 1);
        g.drawRect(0, iconSize, iconSize, iconSize);
        g.drawRect(0, iconSize * 2, iconSize, iconSize);
        g.drawRect(0, iconSize * 3, iconSize, iconSize);

        if (fleetEntry.getTimeRemaiuning() > 0) {
            g.drawRect(this.getPreferredSize().width / 2, iconSize * 4, iconSize, iconSize);
        }

        if (fleetEntry.getScanDuration() != 0) {

            g.setColor(Color.RED);
            g.drawRect(this.getPreferredSize().width - 2 * iconSize, 0, iconSize, iconSize);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == sendFleetButton) {
            ((at.darkdestiny.vismap.starmap.Painter) appletMain.mainFrame.getPainter()).setFleet(fleetEntry);
        } else if (e.getSource() == scanButton) {
            String source = appletMain.getParameter("source");
            String fleetId = String.valueOf(fleetEntry.getId());
            String params = "";
            if (fleetEntry.isFleetFormation()) {
                fleetId = "FF" + fleetId;
            }
            params += "?fleetId=" + fleetId;

            if (fleetEntry.getEndPlanetId() > 0) {
                params += "&planetId=" + String.valueOf(fleetEntry.getEndPlanetId());
            } else {
                params += "&systemId=" + String.valueOf(fleetEntry.getEndSystemId());
            }
            try {
                String basePath = source.replace("createStarMapInfoXT.jsp", "FleetScanServlet" + params);
                boolean okay = false;
                String error = "Error";

                URL url = new URL(basePath);
                URLConnection uc = url.openConnection();
                HttpURLConnection conn = (HttpURLConnection) uc;
                conn.setRequestProperty("Cookie", "JSESSIONID=" + appletMain.getParameter("sessionId"));
                //dbg System.out.println("Src : " + source);

                InputStream is = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    line = line.trim();
                    System.out.println(line);
                    if (line.contains("OKAY")) {
                        okay = true;

                        fleetEntry.setScanDuration(-1);
                        scanButton.setEnabled(false);
                        scanButton.setToolTipText("Scan gestartet");
                        break;
                    }
                    if (!line.equals("")) {
                        error = line;
                        System.out.println(error);
                    }
                }

            } catch (Exception x) {
                x.printStackTrace();
                System.out.println("e : " + x);
            }
        }
        // appletMain.mainFrame.updateFleetEntry(fe);
    }
}
