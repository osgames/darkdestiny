/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;

/**
 *
 * @author Aion
 */
public class SelFleetPanel extends JPanel {

    SpringLayout layout;
    private HashMap<Integer, FleetEntry> marked;
    AppletMain am;
    private int id = 0;
    private SystemDesc sd;
    private int type;
    private String name;
    private JLabel targetLabel;
    private JLabel nameLabel;

    public SelFleetPanel(AppletMain am) {
        this.am = am;
        this.layout = new SpringLayout();
        this.setLayout(layout);
        this.setBackground(Color.GRAY);
        marked = new HashMap<Integer, FleetEntry>();

        this.setVisible(true);
    }

    public void addFleetEntry(FleetEntry fe) {
        if (getMarked() == null) {
            marked = new HashMap<Integer, FleetEntry>();
        }
        if (!marked.containsKey(fe.getId())) {
            getMarked().put(fe.getId(), fe);
        }
        rebuild();
    }

    public void rebuild() {
        build();
    }

    public void build() {
        boolean first = true;
        this.removeAll();
        SelFleetEntry tmp = null;

        targetLabel = new JLabel("Ziel: ");
        if (id > 0) {
            nameLabel = new JLabel(name);
        } else {
            nameLabel = new JLabel("Keine Auswahl");
        }
        this.add(targetLabel);
        this.add(nameLabel);
        targetLabel.setFont(new Font("Tahoma", 1, 9));
        targetLabel.setForeground(Color.WHITE);
        nameLabel.setFont(new Font("Tahoma", 1, 9));
        nameLabel.setForeground(Color.WHITE);

        layout.putConstraint(SpringLayout.NORTH, targetLabel, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, targetLabel, 3, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, nameLabel, 0, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, nameLabel, 0, SpringLayout.EAST, targetLabel);

        for (Map.Entry<Integer, FleetEntry> entries : getMarked().entrySet()) {
            FleetEntry fe = entries.getValue();
            SelFleetEntry sfe = new SelFleetEntry(this, fe, id);
            if (first) {
                layout.putConstraint(SpringLayout.NORTH, sfe, 0, SpringLayout.SOUTH, targetLabel);
                first = false;
            } else {
                layout.putConstraint(SpringLayout.NORTH, sfe, 0, SpringLayout.SOUTH, tmp);
            }
            layout.putConstraint(SpringLayout.WEST, sfe, 0, SpringLayout.WEST, this);
            this.add(sfe);
            tmp = sfe;
        }
        if (marked.size() > 0) {
            this.setPreferredSize(new Dimension(tmp.getPreferredSize().width,
                    targetLabel.getPreferredSize().height + tmp.getPreferredSize().height * marked.size()));
            this.setVisible(true);
        } else {
            if(id > 0){
            this.setPreferredSize(new Dimension(180,
                    (int)nameLabel.getPreferredSize().getHeight()));
            this.setVisible(true);
            }else{
            this.setVisible(false);

            }
        }
        this.revalidate();
        this.repaint();
        am.reValidate();
        am.repaint();
    }

    /**
     * @return the marked
     */
    public HashMap<Integer, FleetEntry> getMarked() {
        if(marked == null){
            marked = new HashMap<Integer, FleetEntry>();
        }
        return marked;
    }

    public void removeFleetEntry(FleetEntry fe) {
        if (marked.containsKey(fe.getId())) {
            marked.remove(fe.getId());
        }
        rebuild();
    }

    public void setTarget(int id, SystemDesc sd, int i, String name) {
        this.id = id;
        this.type = i;
        this.name = name;
        this.sd = sd;
        this.id = id;
        this.type = i;
        this.name = name;
        this.sd = sd;
        rebuild();
    }

    void sendFleet(FleetEntry fe) {
    }

    void setError(boolean b) {
        if(b){

        }
    }
}
