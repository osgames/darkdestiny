/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import java.util.List;

/**
 *
 * @author HorstRabe
 */
public class FleetsPanel extends JPanel {

    private SpringLayout layout;

    public FleetsPanel(List<FleetEntry> fleets, AppletMain am) {

        this.layout = new SpringLayout();
        this.setLayout(layout);
        setBackground(new Color(0f,0f,0f,0.8f));
        
        FleetPanel tmpFleetPanel = null;
        int height = 0;
        for (int i = 0; i < fleets.size(); i++) {
            FleetEntry fleetEntry = fleets.get(i);
            FleetPanel fleetPanel = new FleetPanel(fleetEntry, am);
            this.add(fleetPanel);
            if (i == 0) {

                layout.putConstraint(SpringLayout.WEST, fleetPanel, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, fleetPanel, 0, SpringLayout.NORTH, this);
            } else {

                layout.putConstraint(SpringLayout.WEST, fleetPanel, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, fleetPanel, 0, SpringLayout.SOUTH, tmpFleetPanel);
            }

            height += (int)fleetPanel.getPreferredSize().getHeight();
            tmpFleetPanel = fleetPanel;
        }
        this.setPreferredSize(new Dimension(182,height));

    }
}
