/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.logic.DisplayManagement;

/**
 *
 * @author Aion
 */
public class SelFleetEntry extends JPanel implements ActionListener {

    private SpringLayout layout;
    private SelFleetPanel sfp;
    private FleetEntry fe;
    private JButton unmark;
    private JButton send;

    public SelFleetEntry(SelFleetPanel sfp, FleetEntry fe, int id) {
        this.sfp = sfp;
        this.fe = fe;
        this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
        this.layout = new SpringLayout();
        this.setBackground(Color.DARK_GRAY);
        this.setLayout(layout);
        JLabel fleetName = new JLabel(fe.getName());
        fleetName.setFont(new Font("Tahoma", 1, 9));
        fleetName.setForeground(Color.WHITE);
        unmark = new JButton();
        unmark.setForeground(Color.WHITE);
        unmark.setBackground(Color.BLUE);
        unmark.setHorizontalAlignment(SwingConstants.CENTER);
        unmark.setMargin(new Insets(-1, -1, -1, -1));
        unmark.addActionListener(this);
        unmark.setBackground(Color.BLACK);
        send = new JButton();
        send.setForeground(Color.WHITE);
        send.setBackground(Color.BLUE);
        send.setHorizontalAlignment(SwingConstants.CENTER);
        send.setMargin(new Insets(-1, -1, -1, -1));
        send.addActionListener(this);
        send.setBackground(Color.BLACK);
        Image sendImage = DisplayManagement.getGraphic("movefleet.gif");
        ImageIcon sendImageIcon = new ImageIcon(sendImage);

        send.setIcon(sendImageIcon);
        send.setPreferredSize(new Dimension(20, 20));
        this.add(send);

        Image unmarkImage = DisplayManagement.getGraphic("cancelP.png");
        ImageIcon unmarkImageIcon = new ImageIcon(unmarkImage);

        unmark.setIcon(unmarkImageIcon);
        unmark.setPreferredSize(new Dimension(20, 20));
        this.add(unmark);
        if (id > 0) {
            this.setPreferredSize(new Dimension(200, 23));
        } else {
            this.setPreferredSize(new Dimension(180, 23));
        }
        this.add(fleetName);
        layout.putConstraint(SpringLayout.NORTH, fleetName, 6, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, fleetName, 2, SpringLayout.WEST, this);
        if (id > 0) {
            layout.putConstraint(SpringLayout.NORTH, send, 0, SpringLayout.NORTH, this);
            layout.putConstraint(SpringLayout.EAST, send, 0, SpringLayout.EAST, this);

            layout.putConstraint(SpringLayout.NORTH, unmark, 0, SpringLayout.NORTH, this);
            layout.putConstraint(SpringLayout.EAST, unmark, 0, SpringLayout.WEST, send);
            send.setVisible(true);
        } else {

            layout.putConstraint(SpringLayout.NORTH, unmark, 0, SpringLayout.NORTH, this);
            layout.putConstraint(SpringLayout.EAST, unmark, 0, SpringLayout.EAST, this);
            send.setVisible(false);

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == unmark) {
            sfp.removeFleetEntry(fe);
        } else if (e.getSource() == send) {
            sfp.sendFleet(fe);
        }
    }
}
