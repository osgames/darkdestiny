/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.starmap.PainterData;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.drawable.Transmitter;
import at.darkdestiny.vismap.starmap.drawable.Triangle;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;

/**
 *
 * @author Aion
 */
public class FleetMovementPanel extends JPanel implements ActionListener {

public enum ELocationType {
    SYSTEM, PLANET, TRANSIT, TRANSMITTER;
}

    private FleetEntry fe;
    private int id;
    private ELocationType type;
    private String name;
    private SystemDesc sd;
    private JButton yes;
    private JButton no;
    private SpringLayout layout;
    private AppletMain main;
    JTextArea ta;
    private int timeToTarget = 0;

    public FleetMovementPanel(AppletMain mainApplet) {

        this.main = mainApplet;
        this.layout = new SpringLayout();
        this.setLayout(layout);

        this.setVisible(true);

        this.setBackground(Color.DARK_GRAY);
        this.setBorder(BorderFactory.createLineBorder(Color.GREEN, 2));
    }

    public void setValues(final FleetEntry fe, Transmitter transmitter) {
        this.removeAll();

        Integer firstTargetTransmitterId = null;

        List<TransmitterSelect> transmitterStrings = new ArrayList<>();
        for (Integer i : transmitter.getTargetTransmitterIds()) {
            if (firstTargetTransmitterId == null) {
                firstTargetTransmitterId = i;
            }
            transmitterStrings.add(new TransmitterSelect(i));
        }

        final JComboBox<TransmitterSelect> transmitterBox = new JComboBox(transmitterStrings.toArray());

        transmitterBox.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent event) {
                if (event.getStateChange() == ItemEvent.SELECTED) {
                    TransmitterSelect transmitterSelect = (TransmitterSelect) event.getItem();
                    Transmitter t = getTransmitter(transmitterSelect.getId());
                    if (t != null) {
                        SystemDesc sd = ((PainterData) main.mainFrame.getPainter().getPainterData()).getSystem(t.getSystemId());
                        setValues(fe, sd.getId(), sd, ELocationType.TRANSMITTER, sd.getName(), transmitterBox, t.getId());
                    } else {
                        setValues(fe, -1, null, ELocationType.TRANSMITTER, null, transmitterBox, transmitterSelect.getId());
                    }
                }
            }
        });

        this.add(transmitterBox);
        transmitterBox.setPreferredSize(new Dimension(228, 20));
        Transmitter tmpTransmitter = getTransmitter(firstTargetTransmitterId);


        if (tmpTransmitter != null) {

            SystemDesc firstSD = ((PainterData) main.mainFrame.getPainter().getPainterData()).getSystem(tmpTransmitter.getSystemId());
            setValues(fe, firstSD.getId(), firstSD, ELocationType.TRANSMITTER, firstSD.getName(), transmitterBox, firstTargetTransmitterId);
        } else {

            setValues(fe, -1, null, ELocationType.TRANSMITTER, null, transmitterBox, firstTargetTransmitterId);
        }

        //this.setPreferredSize(new Dimension(230, transmitterBox.getPreferredSize().height + 2));
    }

    public Transmitter getTransmitter(int transmitterId) {
        for (I_Object_OneCoordinate o : main.mainFrame.getPainter().getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_TRANSMITTER)) {
            if (o instanceof Transmitter) {
                Transmitter tmpTransmitter = (Transmitter) o;
                if (tmpTransmitter.getId() == transmitterId) {
                    return tmpTransmitter;
                }
            }
        }
        return null;
    }

    public void setValues(FleetEntry fe, int id, SystemDesc sd, ELocationType type, String name, Component transmitterBox, Integer targetTransmitterId) {
        this.removeAll();
        if (transmitterBox != null) {
            this.add(transmitterBox);
        }
        this.fe = fe;
        this.id = id;
        if(targetTransmitterId != null){
            this.id = targetTransmitterId;
        }
        this.type = type;
        this.name = name;
        this.sd = sd;
        timeToTarget = 0;
        if (targetTransmitterId != null) {
            timeToTarget = 1;
        } else {
            double distance = Math.sqrt(Math.pow(fe.getX1() - sd.getX1(), 2d) + Math.pow(fe.getY1() - sd.getY1(), 2d));
            timeToTarget = (int) Math.ceil(distance / fe.getSpeed());
        }

        ta = new JTextArea();
        ta.setBackground(Color.BLACK);
        ta.setForeground(Color.WHITE);
        String msg = "Wollen sie wirklich die Flotte: " + fe.getName() + " ";
        if (type.equals(ELocationType.SYSTEM)) {
            msg += "zum System : ";
        } else if (type.equals(ELocationType.PLANET)) {
            msg += "zum Planeten : ";
        } else if (type.equals(ELocationType.TRANSMITTER)) {
            msg += "zum Transmitter : ";
        }
        if (name == null) {
            msg += "?" + " (?)";
        } else {
            msg += name + " (" + id + ")";
        }
        msg += " senden";

        this.add(ta);
        ta.setText(msg);
        ta.setEditable(false);
        ta.setWrapStyleWord(true);
        ta.setLineWrap(true);
        ta.setRows(3);

        JLabel ticks = new JLabel("Flugzeit : " + timeToTarget);
        ticks.setForeground(Color.YELLOW);
        this.add(ticks);

        yes = new JButton("Ja");
        no = new JButton("Nein");
        yes.addActionListener(this);
        no.addActionListener(this);
        this.add(yes);
        this.add(no);
        yes.setForeground(Color.BLACK);
        yes.setHorizontalAlignment(SwingConstants.CENTER);
        yes.setMargin(new Insets(-1, -1, -1, -1));
        yes.setBackground(Color.WHITE);
        yes.setFont(new Font("Tahoma", 1, 9));
        yes.setPreferredSize(new Dimension(30, 20));
        no.setForeground(Color.BLACK);
        no.setHorizontalAlignment(SwingConstants.CENTER);
        no.setMargin(new Insets(-1, -1, -1, -1));
        no.setBackground(Color.WHITE);
        no.setFont(new Font("Tahoma", 1, 9));
        no.setPreferredSize(new Dimension(30, 20));

        if (transmitterBox != null) {
            layout.putConstraint(SpringLayout.NORTH, ta, 0, SpringLayout.SOUTH, transmitterBox);
        } else {
            layout.putConstraint(SpringLayout.NORTH, ta, 0, SpringLayout.NORTH, this);
        }
        layout.putConstraint(SpringLayout.WEST, ta, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, ta, 0, SpringLayout.EAST, this);

        layout.putConstraint(SpringLayout.NORTH, ticks, 0, SpringLayout.SOUTH, ta);
        layout.putConstraint(SpringLayout.WEST, ticks, 0, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, yes, 1, SpringLayout.NORTH, ticks);
        layout.putConstraint(SpringLayout.WEST, yes, 4, SpringLayout.EAST, ticks);

        layout.putConstraint(SpringLayout.NORTH, no, 1, SpringLayout.NORTH, ticks);
        layout.putConstraint(SpringLayout.WEST, no, 4, SpringLayout.EAST, yes);

        int height = (ta.getPreferredSize().height + ticks.getPreferredSize().height + 15);
        if (transmitterBox != null) {
            height += transmitterBox.getPreferredSize().height;
        }
        this.setPreferredSize(new Dimension(230, height));
        this.revalidate();
        main.mainFrame.getPainter().forceRefresh();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == yes) {
            String source = main.getParameter("source");
            String fleetId = String.valueOf(fe.getId());
            if (fe.isFleetFormation()) {
                fleetId = "FF" + fleetId;
            }
            String params = "?fleetId=" + fleetId;

            params += "&targetType=" +type.toString();
            params += "&targetId=" + id;
            if(type.equals(ELocationType.SYSTEM) || type.equals(ELocationType.TRANSMITTER)){
                params += "&toSystem=" + "true";
            }else{
                params += "&toSystem=" + "false";
            }
            String basePath = source.replace("createStarMapInfoXT.jsp", "new/fleetapplet.jsp" + params);
            System.out.println("Sent request : " + basePath);
            boolean okay = false;
            String error = "Error";
            try {
                URL url = new URL(basePath);
                URLConnection uc = url.openConnection();
                HttpURLConnection conn = (HttpURLConnection) uc;
                conn.setRequestProperty("Cookie", "JSESSIONID=" + main.getAppletData().getSessionId());
                InputStream is = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    line = line.trim();
                    if (line.contains("OKAY")) {
                        okay = true;
                        break;
                    }
                    if (!line.equals("")) {
                        error = line;
                    }
                }

            } catch (Exception x) {
                x.printStackTrace();
            }

            if (okay) {
                main.mainFrame.fmp.setVisible(false);
                fe.setTimeRemaiuning(timeToTarget);
                if (type.equals(ELocationType.SYSTEM)) {
                    fe.setX2(Math.round(sd.getX1()));
                    fe.setY2(Math.round(sd.getY1()));
                    fe.setEndPlanetId(0);
                    fe.setEndSystemId(sd.getId());
                    fe.setEndSystem(sd.getName());

                } else if (type.equals(ELocationType.PLANET)) {
                    fe.setX2(Math.round(sd.getX1()));
                    fe.setY2(Math.round(sd.getY1()));
                    fe.setEndPlanetId(id);
                    fe.setName("Planet #" + id);
                    fe.setEndSystemId(sd.getId());
                    fe.setEndSystem(sd.getName());
                }else if(type.equals(ELocationType.TRANSMITTER)){
                    fe.setX2(fe.getX1());
                    fe.setY2(fe.getX1());
                    fe.setEndPlanetId(0);
                }
                List<I_Object_OneCoordinate> entries = main.mainFrame.getPainter().getPainterData().getDisplayObjects().get(AppletConstants.DRAW_LEVEL_FLEETS);

                for (int i = 0; i < entries.size(); i++) {
                    I_Object_OneCoordinate obj = entries.get(i);
                    if (obj instanceof FleetEntry) {

                        FleetEntry fleetEntry = (FleetEntry) obj;
                        if (fleetEntry.getId() == fe.getId()) {
                            entries.set(i, fe);
                        } else if (obj instanceof Triangle) {

                            Triangle tr = (Triangle) obj;
                            if (tr.getFleetId() == fe.getId()) {
                                entries.remove(i);
                            }
                        }
                    }
                }
                entries = main.mainFrame.getPainter().getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_FLEETS);

                for (int i = 0; i < entries.size(); i++) {
                    I_Object_OneCoordinate obj = entries.get(i);
                    if (obj instanceof FleetEntry) {

                        FleetEntry fleetEntry = (FleetEntry) obj;
                        if (fleetEntry.getId() == fe.getId()) {
                            entries.set(i, fe);
                        }
                    }
                }
                entries = main.mainFrame.getPainter().getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_FLEETS_TRIANGLES);
                for (int i = 0; i < entries.size(); i++) {
                    I_Object_OneCoordinate obj = entries.get(i);
                    if (obj instanceof Triangle) {

                        Triangle tr = (Triangle) obj;
                        if (tr.getFleetId() == fe.getId()) {
                            entries.remove(i);
                        }
                    }
                }
                ((at.darkdestiny.vismap.starmap.Painter) main.mainFrame.getPainter()).sendFleet = false;
                ((at.darkdestiny.vismap.starmap.Painter) main.mainFrame.getPainter()).sendSystem = null;
                main.mainFrame.getPainter().setMode(EStarmapMode.DEFAULT);
                main.mainFrame.getPainter().forceRefresh();

            } else {
                ta.setText(error);
                ta.revalidate();
            }
        } else if (e.getSource() == no) {
            main.mainFrame.fmp.setVisible(false);
        }
    }

    public class TransmitterSelect {

        private int id;

        public TransmitterSelect(int id) {
            this.id = id;
        }

        public String toString() {
            return "Transmitter " + id;
        }

        public int getId() {
            return id;
        }
    }
}
