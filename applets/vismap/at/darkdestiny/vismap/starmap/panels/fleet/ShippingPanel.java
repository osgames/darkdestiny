/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import at.darkdestiny.vismap.starmap.panels.trade.RessourcePanel;
import at.darkdestiny.vismap.drawable.GraphPanel;
import at.darkdestiny.vismap.starmap.panels.trade.SystemPanel;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.starmap.dto.LoadingEntry;
import java.util.List;

/**
 *
 * @author HorstRabe
 */
public class ShippingPanel extends JPanel {

    private SpringLayout layout;
    public SystemPanel systemPanel;
    protected JPanel ressourceTitlePanel;
    protected JPanel bottomImage;

    public ShippingPanel(FleetEntry fleetEntry) {

        ressourceTitlePanel = new GraphPanel("RessourceTitlePanel.png", 182, 19);
        this.setBackground(Color.WHITE);
        this.add(ressourceTitlePanel);
        this.layout = new SpringLayout();
        this.setLayout(layout);

        layout.putConstraint(SpringLayout.WEST, ressourceTitlePanel, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, ressourceTitlePanel, 0, SpringLayout.NORTH, this);

        addRessources(fleetEntry.getLoading());

    }

    private void addRessources(List<LoadingEntry> ressources) {

        RessourcePanel tmpPanel = null;

        int ressourcesHeight = 0;
        for (int i = 0; i < ressources.size(); i++) {

            RessourcePanel ressourcePanel = new RessourcePanel(ressources.get(i));
            this.add(ressourcePanel);

            JPanel leftImage = new GraphPanel("leftPlanet.png", 5, 24);
            JPanel rightImage = new GraphPanel("rightPlanet.png", 4, 24);

            this.add(leftImage);
            this.add(rightImage);

            if (i == 0) {
                layout.putConstraint(SpringLayout.WEST, leftImage, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, leftImage, 0, SpringLayout.SOUTH, ressourceTitlePanel);

                layout.putConstraint(SpringLayout.WEST, ressourcePanel, -1, SpringLayout.EAST, leftImage);
                layout.putConstraint(SpringLayout.NORTH, ressourcePanel, 0, SpringLayout.SOUTH, ressourceTitlePanel);

            } else {
                layout.putConstraint(SpringLayout.WEST, leftImage, 0, SpringLayout.WEST, this);
                layout.putConstraint(SpringLayout.NORTH, leftImage, 0, SpringLayout.SOUTH, tmpPanel);

                layout.putConstraint(SpringLayout.WEST, ressourcePanel, -1, SpringLayout.EAST, leftImage);
                layout.putConstraint(SpringLayout.NORTH, ressourcePanel, 0, SpringLayout.SOUTH, tmpPanel);

            }

            layout.putConstraint(SpringLayout.WEST, rightImage, 0, SpringLayout.EAST, ressourcePanel);
            layout.putConstraint(SpringLayout.NORTH, rightImage, 0, SpringLayout.NORTH, ressourcePanel);

            ressourcesHeight += ressourcePanel.getPreferredSize().getHeight();
            tmpPanel = ressourcePanel;
        }

        bottomImage = new GraphPanel("bottomPlanet.png", 174, 5);
        this.add(bottomImage);
        layout.putConstraint(SpringLayout.WEST, bottomImage, 0, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, bottomImage, 0, SpringLayout.SOUTH, tmpPanel);

        double height = ressourceTitlePanel.getPreferredSize().getHeight() + ressourcesHeight + bottomImage.getPreferredSize().getHeight();

        this.setPreferredSize(new Dimension(182, (int) height));
    }

}
