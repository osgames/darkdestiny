/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

/**
 *
 * @author LensiPensi
 */
public class SystemIcon extends JPanel {

    private Map<Integer, String> colors;
    private SystemDesc systemDesc;
    private int size = 15;

    public SystemIcon(SystemDesc systemDesc) {
        this.setOpaque(false);
        this.setPreferredSize(new Dimension(size,size));
        
        SpringLayout layout = new SpringLayout();
        this.setLayout(layout);

        setToolTipText("System");
        if (systemDesc == null) {
            JLabel questionMark = new JLabel("?");
            questionMark.setForeground(Color.WHITE);
            this.add(questionMark);
            layout.putConstraint(SpringLayout.NORTH, questionMark, 0, SpringLayout.NORTH, this);
            layout.putConstraint(SpringLayout.WEST, questionMark, 4, SpringLayout.WEST, this);
            return;
        }


        this.systemDesc = systemDesc;
        this.colors = systemDesc.getColors();
    }

    @Override
    public void paint(Graphics gc) {
        super.paint(gc);

        if(systemDesc == null)return;
        //Filter same colors
        ArrayList<String> realColors = new ArrayList<String>();
        if (colors != null && !colors.isEmpty()) {
            for (Map.Entry<Integer, String> entry : colors.entrySet()) {
                if (!realColors.contains(entry.getValue())) {
                    realColors.add(entry.getValue());
                }
            }
        }
        int maxOvalSize = size;
        if (realColors != null && !realColors.isEmpty()) {
            int ovalSize = maxOvalSize;
            double factor = 1d;
            double increase = 0.3;
            factor = factor + (realColors.size() - 1) * increase;
            
            for (String color : realColors) {
                gc.setColor(Color.decode(color));
                gc.fillRect(maxOvalSize/2 - ovalSize/2, maxOvalSize/2 - ovalSize/2, ovalSize, ovalSize);

                ovalSize = (int) Math.floor((double) ovalSize / factor);
                factor -= increase;
            }
        }
         gc.drawImage(systemDesc.getImgName(), 0, 0, size+1, size+1, null);
               
    }
}
