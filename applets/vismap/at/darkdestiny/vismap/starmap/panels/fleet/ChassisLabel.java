/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.panels.fleet;

import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.starmap.AppletData;
import java.awt.Image;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Admin
 */
public class ChassisLabel extends JLabel {

    boolean showTooltip = false;
    MouseEvent mouseEvent;
    AppletMain am;
    final int chassisSize;

    public ChassisLabel(final int chassisSize, final AppletMain am) {
        String imageName = "shipBackground.png";
        this.am = am;
        this.chassisSize = chassisSize;
        switch (chassisSize) {
            case (1):
                imageName = "star_fighter.png";
                break;
            case (2):
                imageName = "star_corvette.png";
                break;
            case (3):
                imageName = "star_frigate.png";
                break;

            case (4):
                imageName = "star_destroyer.png";
                break;
            case (5):
                imageName = "star_cruiser.png";
                break;
            case (6):
                imageName = "star_battleship.png";
                break;
            case (8):
                imageName = "star_superbattleship.png";
                break;
            case (10):
                imageName = "star_tender.png";
                break;

        }
        this.setToolTipText(((AppletData) am.getAppletData()).getChassis().get(chassisSize).getName());
        Image i = DisplayManagement.getGraphic(imageName);
        setIcon(new ImageIcon(i));

    }

}
