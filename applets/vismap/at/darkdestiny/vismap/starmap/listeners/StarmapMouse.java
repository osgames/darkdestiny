/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.listeners;

import at.darkdestiny.vismap.starmap.PainterData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc_ToolTip;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.AbstractMouse;
import at.darkdestiny.vismap.starmap.drawable.Point;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.drawable.TradeRoute;
import at.darkdestiny.vismap.starmap.dto.LineIntersection;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class StarmapMouse extends AbstractMouse {

    private PainterData painterData;
    private at.darkdestiny.vismap.starmap.Painter painter;
    private AppletMain main;
    int lastMousePressedX = 0;
    int lastMousePressedY = 0;
    boolean mouseDragged = false;
    boolean mousePressed = true;
    float x_start = Float.NaN;
    float y_start = Float.NaN;
    private int tmpx;
    private int tmpy;
    private int x_int;
    private int y_int;
    private TradeRoute showTradeRoute;
    public boolean moveMap = false;
    public boolean selectedRect = false;
    private List<FleetEntry> showFleetEntry = new ArrayList();
    private static final int TOOLTIP_HEIGHT = 10;
    private static final int TOOLTIP_WIDTH = 10;

    public StarmapMouse(AppletMain main, IPainter painter, IPainterData painterData) {
        this.painterData = (PainterData)painterData;
        this.painter = (at.darkdestiny.vismap.starmap.Painter) painter;
        this.main = main;

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

        int zoom = e.getWheelRotation() * 1;

        painter.mouseWheelChanged(zoom, e);

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (painter.getMode().equals(EStarmapMode.DEFAULT)) {
            if (selectedRect) {
                tmpx = e.getX();
                tmpy = e.getY();
                if (Math.abs(tmpx - x_int) > 2 && Math.abs(tmpy - y_int) > 2) {
                    active = true;
                    painter.repaint();
                }
            } else if (moveMap) {


                painter.setMouseEvent(e);

                tmpx = e.getX();
                tmpy = e.getY();
                int diff_x = x_int - tmpx;
                int diff_y = y_int - tmpy;

                float weight = 0.3f;

                if (Math.abs(diff_x) > 2) {
                    if (diff_x > 0) {
                        painter.moveImage("left", weight);
                        diff_x--;
                    } else if (diff_x < 0) {
                        painter.moveImage("right", weight);
                        diff_x++;
                    }
                }
                if (Math.abs(diff_y) > 2) {
                    if (diff_y > 0) {
                        painter.moveImage("up", weight);
                        diff_y--;
                    } else if (diff_y < 0) {
                        painter.moveImage("down", weight);
                        diff_y++;
                    }
                }
                x_int = painter.getPreferredSize().width / 2;
                y_int = painter.getPreferredSize().height / 2;
                try {
                    Robot r = new java.awt.Robot();
                    r.mouseMove(main.getLocationOnScreen().x + x_int, main.getLocationOnScreen().y + y_int);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (painter.mode.equals(EStarmapMode.TERRITORY_MOVE)) {

            painter.setMouseEvent(e);
            if (painter.foundTerritory != null && !painter.foundTerritory.isEmpty()) {
                Object_OneCoordinate point = (Object_OneCoordinate) painter.foundTerritory.get(0);


                double starMapX = painter.convertToFloatX(e.getX(), e.getY());
                double starMapY = painter.convertToFloatY(e.getX(), e.getY());
                int oldValueX = point.getX1();
                int oldValueY = point.getY1();
                point.setX1((int) starMapX);
                point.setY1((int) starMapY);
                if (painter.mooveIntersectionCheck((Point) point)) {
                    point.setX1(oldValueX);
                    point.setY1(oldValueY);
                }
                painter.forceRefresh();
            }
        }
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public void mouseMoved(MouseEvent e) {

        if (painter.mode.equals(EStarmapMode.SEND_FLEET)) {
            painter.setMouseEvent(e);
            painter.repaint();
        }
        if (painter.mode.equals(EStarmapMode.TERRITORY_ADD_POINT)) {
            painter.setMouseEvent(e);
            LineIntersection li = painter.findLineIntersection(e.getX(), e.getY());
            if (painter.lineIntersection == null && li != null) {
                painter.forceRefresh();
            } else if (painter.lineIntersection != null && li == null) {
                painter.forceRefresh();
            } else {
                painter.repaint();
            }
        }

        if (painter.mode.equals(EStarmapMode.TERRITORY_ADD)) {
            painter.intersecting = painter.isIntersecting(e);
            painter.foundTerritory = painter.findTerritoryEntry(e.getX(), e.getY());
            if (painter.foundTerritory != null && !painter.foundTerritory.isEmpty()) {
                if (painter.isStartPoint()) {
                } else {
                    painter.intersecting = true;
                }
            }
            painter.setMouseEvent(e);
            painter.repaint();
        } else if (painter.mode.equals(EStarmapMode.TERRITORY_MOVE)) {
            if (painter.findTerritoryEntry(e.getX(), e.getY()) != null) {
                painter.setMoved(true);
                painter.forceRefresh();
            } else if (painter.foundTerritory != null) {
                painter.setMoved(true);
                painter.forceRefresh();
            } else {
                if (painter.isMoved()) {
                    painter.setMoved(false);
                    painter.forceRefresh();
                }
            }
        }
        if (painterData.isShowTerritory()) {
            boolean foundTerritory = painter.drawTerritoryInfo;
            painter.findTerritoryEntry(e.getX(), e.getY());
            if (!foundTerritory && painter.drawTerritoryInfo) {
                painter.setMouseEvent(e);
                painter.repaint();
            }
            if (foundTerritory && !painter.drawTerritoryInfo) {
                painter.repaint();
            }
        }
        mouseEvent = e;
        SystemDesc sd = findSystem(e.getX(), e.getY());
        if (sd != null) {
            if ((painter.showToolTipFor == null) || (painter.showToolTipFor.sd != sd)) {
                painter.showToolTipFor = new SystemDesc_ToolTip(sd, e.getX(), e.getY(), painterData.getWidth(), painterData.getHeight());
                painter.repaint();
            } else {
                painter.showToolTipFor.setPos(e.getX(), e.getY());
                painter.repaint();
            }

        } else if (painter.showToolTipFor != null) {
            painter.showToolTipFor = null;
            painter.repaint();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {


        if (painter.mode.equals(EStarmapMode.TERRITORY_ADD)) {
            if (!painter.intersecting) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    if (painter.foundTerritory != null && !painter.foundTerritory.isEmpty()) {
                        if (painter.isStartPoint()) {
                            painter.createTerritory();
                        } else {
                            painter.intersecting = true;
                        }
                    } else {
                        painter.addPoint(e.getX(), e.getY());
                        painter.forceRefresh();
                    }
                }
            }
            if (e.getButton() == MouseEvent.BUTTON3) {
                painter.removeLastPoint();
            }
        } else if (painter.mode.equals(EStarmapMode.TERRITORY_MOVE)) {
            if (e.getButton() == MouseEvent.BUTTON3 && painter.foundTerritory == null) {
                painter.removeSelectedPoint();
            }
            painter.setMoved(false);
            painter.foundTerritory = null;
        } else if (painter.mode.equals(EStarmapMode.TERRITORY_ADD_POINT)) {
            if (painter.lineIntersection != null && painter.findTerritoryEntry(e.getX(), e.getY()).isEmpty()) {

                float conX = painter.convertToFloatX(mouseEvent.getX(), mouseEvent.getY());
                float conY = painter.convertToFloatY(mouseEvent.getX(), mouseEvent.getY());
                painter.lineIntersection.insertPoint((int) conX, (int) conY);
                painter.forceRefresh();
            }
        } else if (painter.mode.equals(EStarmapMode.SEND_FLEET)) {

            if (e.getButton() == MouseEvent.BUTTON3) {
                painter.setMode(EStarmapMode.DEFAULT);
                painter.setSendSystem(null);
                painter.setDefaultCursor(true);
            }
        }

        //Zoom Rect

        if (painter.getMode().equals(EStarmapMode.DEFAULT)) {
            if (selectedRect) {
                float tmpx = painter.convertToFloatX(e.getX(), e.getY());
                float tmpy = painter.convertToFloatY(e.getX(), e.getY());


                if (Math.abs(x_int - e.getX()) > 5 && Math.abs(y_int - e.getY()) > 5) {

                    painter.zoomRect(x_start, y_start, tmpx, tmpy);
                }
                x_start = Float.NaN;
                y_start = Float.NaN;
            }

            active = false;
            moveMap = false;
            selectedRect = false;
            painter.setDefaultCursor(true);
            painter.forceRefresh();

        }

        if ((lastMousePressedX == e.getX()) && (lastMousePressedY == e.getY())) {

            SystemDesc sd = findSystem(e.getX(), e.getY());
            if (painterData.isShowTradeRoutes()) {
                showTradeRoute = findTradeRoute(e.getX(), e.getY());
            }
            if (painterData.isShowFleets()) {
                showFleetEntry = findFleetEntry(e.getX(), e.getY());
            }
            if (showTradeRoute != null && painterData.isShowTradeRoutes()) {

                main.mainFrame.updateInformationPanel(showTradeRoute);

            } else if (showFleetEntry.size() > 0 && painterData.isShowFleets()) {

                main.mainFrame.updateInformationPanel(showFleetEntry);

                mousePressed = false;
            } else if (sd != null) {
                painter.showToolTipFor = null;

                if (e.getButton() == MouseEvent.BUTTON1) {
                    main.mainFrame.updateInformationPanel(sd);

                    painter.setSendSystem(sd);
                } else if (e.getButton() == MouseEvent.BUTTON2) {

                    main.mainFrame.setInfoVisibility(false);

                }

            } else {
                main.mainFrame.setInfoVisibility(false);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mousePressed = true;
        lastMousePressedX = e.getX();
        lastMousePressedY = e.getY();
        if (painter.getMode().equals(EStarmapMode.DEFAULT)) {
            if (e.getButton() == MouseEvent.BUTTON1 && !painter.getMode().equals(EStarmapMode.SEND_FLEET)) {
                x_start = painter.convertToFloatX(e.getX(), e.getY());
                y_start = painter.convertToFloatY(e.getX(), e.getY());
                x_int = e.getX();
                y_int = e.getY();
                selectedRect = true;
            } if (e.getButton() == MouseEvent.BUTTON3) {
                moveMap = true;
                x_int = painter.getPreferredSize().width / 2;
                y_int = painter.getPreferredSize().height / 2;
            }
        } else if (painter.mode.equals(EStarmapMode.TERRITORY_MOVE)) {
            if (e.getButton() == MouseEvent.BUTTON1 && (painter.foundTerritory == null || painter.foundTerritory.isEmpty())) {
                painter.foundTerritory = painter.findTerritoryEntry(e.getX(), e.getY());
                painter.setMoved(true);
            } else if (e.getButton() == MouseEvent.BUTTON1 && !painter.foundTerritory.isEmpty()) {
                painter.foundTerritory.clear();
            }
        }
    }

    protected SystemDesc findSystem(int x, int y) {
        float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 16 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 16);

        SystemDesc res = null;
        float priority = Float.NEGATIVE_INFINITY;
        List<I_Object_OneCoordinate> entries = painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_SYSTEMS);

        int xd = (int) painter.convertToFloatX((int) x, (int) y);
        int yd = (int) painter.convertToFloatY((int) x, (int) y);
        for (I_Object_OneCoordinate obj : entries) {
            Object_OneCoordinate sd = (Object_OneCoordinate) obj;
            float dist = (float) Math.sqrt(Math.pow(x - sd.getActCoord_x1(), 2d) + Math.pow(y - sd.getActCoord_y1(), 2d));

            if ((sd instanceof SystemDesc) && (dist < maxdist)) {


                SystemDesc sys = (SystemDesc) sd;
                if (sys.getPriority() > priority) {
                    priority = sys.getPriority();
                    res = sys;
                }
            }
        }

        return res;
    }

    protected TradeRoute findTradeRoute(int x, int y) {
        List<I_Object_OneCoordinate> entries = painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_TRADE);
        if (entries == null) {
            return null;
        }
        float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 8 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 8);

        for (I_Object_OneCoordinate obj : entries) {
            if (obj instanceof TradeRoute) {
                TradeRoute tradeRoute = (TradeRoute) obj;
                float halfX = Math.round((tradeRoute.getActCoord_x2() + tradeRoute.getActCoord_x1()) / 2);
                float halfY = Math.round((tradeRoute.getActCoord_y2() + tradeRoute.getActCoord_y1()) / 2);

                float dist = ((x - halfX) * (x - halfX) + (y - halfY) * (y - halfY));
                if (dist < maxdist) {
                    return tradeRoute;
                }

            }
        }
        return null;
    }

    protected ArrayList<FleetEntry> findFleetEntry(int x, int y) {
        List<I_Object_OneCoordinate> entries = painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_FLEETS);
        if (entries == null) {
            return new ArrayList<FleetEntry>();
        }
        float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 2 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 2);
        ArrayList<FleetEntry> fleets = new ArrayList<FleetEntry>();

        for (I_Object_OneCoordinate obj : entries) {
            if (obj instanceof FleetEntry) {

                FleetEntry fleetEntry = (FleetEntry) obj;
                float halfX = Math.round(fleetEntry.getActCoord_x1());
                float halfY = Math.round(fleetEntry.getActCoord_y1());
                if (fleetEntry.getTimeRemaiuning() == 0 && fleetEntry.getStatus() == AppletConstants.FLEET_OWN) {
                    halfY -= 35;
                } else if (fleetEntry.getTimeRemaiuning() == 0 && fleetEntry.getStatus() == AppletConstants.FLEET_ALLIED) {
                    halfY += 10;
                    halfX += 30;
                } else if (fleetEntry.getTimeRemaiuning() == 0) {
                    halfY += 10;
                    halfX -= 30;
                }
                float dist = ((x - halfX) * (x - halfX) + (y - halfY) * (y - halfY));
                if (dist < maxdist) {
                    fleets.add(fleetEntry);
                }
            }
        }
        return fleets;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void paint(IPainter painter, Graphics g) {

        if (selectedRect) {
            int w = Math.abs(tmpx - x_int);
            int h = Math.abs(tmpy - y_int);
            g.setColor(new Color(0.3f, 0.3f, 0.8f, 0.3f));
            g.fillRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w, h);

            g.setColor(new Color(0.3f, 0.3f, 1f));
            g.drawRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w, h);
        } else if (moveMap) {
        }
    }

    @Override
    public void update(AppletMain main, MainPainter aThis, IPainterData painterData) {
        this.painterData = (PainterData)painterData;
        this.painter = (at.darkdestiny.vismap.starmap.Painter) painter;
        this.main = main;

    }
}
