/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.buttons;

import at.darkdestiny.vismap.buttons.BlueButton;
import at.darkdestiny.vismap.buttons.BlueToggleButton;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.logic.ReloadThread;
import at.darkdestiny.vismap.starmap.Painter;
import at.darkdestiny.vismap.starmap.PainterData;
import at.darkdestiny.vismap.starmap.drawable.Point;
import at.darkdestiny.vismap.starmap.drawable.Territory;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import java.applet.AppletContext;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.ToolTipManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Admin
 */
public class ToolBox extends JPanel implements ActionListener {

    private BlueToggleButton showFleets = null;
    private BlueToggleButton territory_add_point = null;
    private BlueToggleButton territory_move = null;
    private BlueToggleButton territory_show = null;
    private BlueToggleButton territory_add = null;
    private BlueToggleButton territory_edit = null;
    private BlueToggleButton showRaster = null;
    private BlueToggleButton showTraderoutes = null;
    private BlueToggleButton invertRaster = null;
    private BlueToggleButton rasterSmaller = null;
    private BlueToggleButton rasterBigger = null;
    private BlueToggleButton showHyperScanner = null;
    private BlueButton refresh = null;
    private BlueButton help = null;
    private BlueButton territory_save = null;
    private BlueButton search = null;
    private IPainter painter;
    SpringLayout layout = new SpringLayout();
    List<JComponent> buttons = new ArrayList();
    private AppletMain appletMain;
    private PainterData painterData;

    public ToolBox(IPainter painter, AppletMain appletMain) {
        this.setLayout(layout);
        ToolTipManager ttm = ToolTipManager.sharedInstance();
        ttm.setInitialDelay(100);
        this.appletMain = appletMain;
        this.painter = painter;
        this.painterData = (PainterData)(painter.getPainterData());

        buildPanel();
    }

    private void buildPanel() {
        showFleets = new BlueToggleButton("Zeige Flotten", "showfleet.png", true, 30, 30);
        showFleets.addActionListener(this);



        showRaster = new BlueToggleButton("Raster Anzeigen", "raster.png", false, 30, 30);
        showRaster.addActionListener(this);

        invertRaster = new BlueToggleButton("Raster Invertieren", "raster_invert.png", false, 30, 30);
        invertRaster.addActionListener(this);


        rasterSmaller = new BlueToggleButton("Raster Verkleinern", "raster_smaller.png", false, 30, 30);
        rasterSmaller.addActionListener(this);

        rasterBigger = new BlueToggleButton("Raster Vergrößern", "raster_bigger.png", false, 30, 30);
        rasterBigger.addActionListener(this);

        showTraderoutes = new BlueToggleButton("Handelsrouten anzeigen", "transportroutes.png", false, 30, 30);
        showTraderoutes.addActionListener(this);



        showHyperScanner = new BlueToggleButton("Hyperscanner anzeigen", "hyperscanner.png", false, 30, 30);
        showHyperScanner.addActionListener(this);


        territory_add_point = new BlueToggleButton("Territoriums Punkt hinzufügen", "territory_addpoint.png", false, 30, 30);
        territory_add_point.addActionListener(this);


        territory_show = new BlueToggleButton("Territorium anzeigen", "territory.png",  true, 30, 30);
        territory_show.addActionListener(this);


        territory_edit = new BlueToggleButton("Territorium editieren", "territory_edit.png", false, 30, 30);
        territory_edit.addActionListener(this);


        territory_move = new BlueToggleButton("Territorium verschieben", "territory_move.png", false, 30, 30);
        territory_move.addActionListener(this);

        territory_add = new BlueToggleButton("Territorium erstellen", "territory_add.png", false, 30, 30);
        territory_add.addActionListener(this);

        territory_save = new BlueButton("Änderungen speichern", "territory_save.png", 30, 30, painter);
        territory_save.addActionListener(this);


        refresh = new BlueButton("Refresh", "refresh.png", 30, 30, painter);
        refresh.addActionListener(this);

        help = new BlueButton("Hilfe", "help.png", 30, 30, painter);
        help.addActionListener(this);


        search = new BlueButton("Suche", "search.png", 30, 30, painter);
        search.addActionListener(this);

        buttons.add(showFleets);
        buttons.add(territory_move);
        buttons.add(territory_add);
        buttons.add(territory_edit);
        buttons.add(territory_add_point);
        buttons.add(territory_show);
        buttons.add(territory_save);
        buttons.add(showRaster);
        buttons.add(invertRaster);
        buttons.add(rasterSmaller);
        buttons.add(rasterBigger);
        buttons.add(showTraderoutes);
        buttons.add(showHyperScanner);
        buttons.add(search);
        buttons.add(refresh);
        buttons.add(help);

        invertRaster.setVisible(false);
        rasterSmaller.setVisible(false);
        rasterBigger.setVisible(false);

        territory_add_point.setVisible(false);
        territory_add.setVisible(false);
        territory_move.setVisible(false);
        territory_save.setVisible(false);

        if (buttons.size() > 0) {

            for (JComponent button : buttons) {
                this.add(button);
            }
        }


        layout.putConstraint(SpringLayout.WEST, showFleets, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, showFleets, 2, SpringLayout.NORTH, this);


        layout.putConstraint(SpringLayout.WEST, territory_show, 0, SpringLayout.WEST, showFleets);
        layout.putConstraint(SpringLayout.NORTH, territory_show, 0, SpringLayout.SOUTH, showFleets);

        layout.putConstraint(SpringLayout.WEST, territory_edit, 0, SpringLayout.EAST, territory_show);
        layout.putConstraint(SpringLayout.NORTH, territory_edit, 0, SpringLayout.NORTH, territory_show);

        layout.putConstraint(SpringLayout.WEST, territory_add, 0, SpringLayout.EAST, territory_edit);
        layout.putConstraint(SpringLayout.NORTH, territory_add, 0, SpringLayout.NORTH, territory_edit);

        layout.putConstraint(SpringLayout.WEST, territory_move, 0, SpringLayout.EAST, territory_add);
        layout.putConstraint(SpringLayout.NORTH, territory_move, 0, SpringLayout.NORTH, territory_add);

        layout.putConstraint(SpringLayout.WEST, territory_add_point, 0, SpringLayout.EAST, territory_move);
        layout.putConstraint(SpringLayout.NORTH, territory_add_point, 0, SpringLayout.NORTH, territory_move);

        layout.putConstraint(SpringLayout.WEST, territory_save, 0, SpringLayout.EAST, territory_add_point);
        layout.putConstraint(SpringLayout.NORTH, territory_save, 0, SpringLayout.NORTH, territory_add_point);

        layout.putConstraint(SpringLayout.WEST, showRaster, 0, SpringLayout.WEST, territory_show);
        layout.putConstraint(SpringLayout.NORTH, showRaster, 0, SpringLayout.SOUTH, territory_show);

        layout.putConstraint(SpringLayout.WEST, invertRaster, 0, SpringLayout.EAST, showRaster);
        layout.putConstraint(SpringLayout.NORTH, invertRaster, 0, SpringLayout.NORTH, showRaster);

        layout.putConstraint(SpringLayout.WEST, invertRaster, 0, SpringLayout.EAST, showRaster);
        layout.putConstraint(SpringLayout.NORTH, invertRaster, 0, SpringLayout.NORTH, showRaster);


        layout.putConstraint(SpringLayout.WEST, rasterSmaller, 0, SpringLayout.EAST, invertRaster);
        layout.putConstraint(SpringLayout.NORTH, rasterSmaller, 0, SpringLayout.NORTH, showRaster);


        layout.putConstraint(SpringLayout.WEST, rasterBigger, 0, SpringLayout.EAST, rasterSmaller);
        layout.putConstraint(SpringLayout.NORTH, rasterBigger, 0, SpringLayout.NORTH, showRaster);


        layout.putConstraint(SpringLayout.WEST, showTraderoutes, 0, SpringLayout.WEST, showRaster);
        layout.putConstraint(SpringLayout.NORTH, showTraderoutes, 0, SpringLayout.SOUTH, showRaster);

        layout.putConstraint(SpringLayout.WEST, showHyperScanner, 0, SpringLayout.WEST, showTraderoutes);
        layout.putConstraint(SpringLayout.NORTH, showHyperScanner, 0, SpringLayout.SOUTH, showTraderoutes);

        layout.putConstraint(SpringLayout.WEST, search, 0, SpringLayout.WEST, showHyperScanner);
        layout.putConstraint(SpringLayout.NORTH, search, 20, SpringLayout.SOUTH, showHyperScanner);

        layout.putConstraint(SpringLayout.WEST, refresh, 0, SpringLayout.WEST, search);
        layout.putConstraint(SpringLayout.NORTH, refresh, 20, SpringLayout.SOUTH, search);

        layout.putConstraint(SpringLayout.WEST, help, 0, SpringLayout.WEST, refresh);
        layout.putConstraint(SpringLayout.NORTH, help, 0, SpringLayout.SOUTH, refresh);



        this.setPreferredSize(new Dimension(180, buttons.size() * 30 + 30));
        this.setOpaque(false);
        this.setVisible(true);
    }

    public IPainter getPainter() {
        return painter;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == showFleets) {
            painterData.setShowFleets(!painterData.isShowFleets());
            painter.forceRefresh();
        } else if (e.getSource() == territory_show) {
            painterData.setShowTerritory(!painterData.isShowTerritory());

            territory_edit.setVisible(!territory_edit.isVisible());
            painter.forceRefresh();

        } else if (e.getSource() == territory_edit) {
            territory_add.setVisible(!territory_add.isVisible());
            territory_move.setVisible(!territory_move.isVisible());
            territory_add_point.setVisible(!territory_add_point.isVisible());
            territory_save.setVisible(!territory_save.isVisible());
            if (territory_show.getModel().isPressed()) {
                territory_show.setEnabled(true);
            } else {
                territory_show.setEnabled(false);
            }
            territory_edit.setEnabled(false);
            painter.setDefaultCursor(true);

        } else if (e.getSource() == territory_add) {
            territory_add_point.getModel().setPressed(false);
            territory_move.getModel().setPressed(false);
            territory_move.getModel().setSelected(false);
            territory_add_point.getModel().setSelected(false);
            if (!territory_add.getModel().isPressed()) {
                painter.setMode(EStarmapMode.TERRITORY_ADD);
                painter.forceRefresh();
            } else {
                painter.setMode(EStarmapMode.DEFAULT);
                ((at.darkdestiny.vismap.starmap.Painter) painter).createdTerritory.clear();
                painter.setDefaultCursor(true);
            }
        } else if (e.getSource() == territory_move) {
            painter.setDefaultCursor(true);
            painter.setMode(EStarmapMode.TERRITORY_MOVE);
            territory_add.getModel().setPressed(false);
            territory_add_point.getModel().setPressed(false);
            territory_add.getModel().setSelected(false);
            territory_add_point.getModel().setSelected(false);
            painter.forceRefresh();

        } else if (e.getSource() == territory_add_point) {
            painter.setMode(EStarmapMode.TERRITORY_ADD_POINT);
            territory_add.getModel().setPressed(false);
            territory_move.getModel().setPressed(false);
            territory_add.getModel().setSelected(false);
            territory_move.getModel().setSelected(false);
            painter.forceRefresh();

        } else if (e.getSource() == showHyperScanner) {
            painterData.setShowHyperScanner(!painterData.isShowHyperScanner());
            painter.forceRefresh();
        } else if (e.getSource() == showTraderoutes) {
            painterData.setShowTradeRoutes(!painterData.isShowTradeRoutes());
            painter.forceRefresh();
        } else if (e.getSource() == invertRaster) {
            painter.getPainterData().setRasterInverted(!painter.getPainterData().isRasterInverted());

            painter.forceRefresh();
        } else if (e.getSource() == rasterBigger) {
            painter.setRasterDetail(painter.getRasterDetail() + 100);
            if (painter.getRasterDetail() <= 200) {
                rasterSmaller.setEnabled(false);
            } else {
                rasterSmaller.setEnabled(true);
            }
            if (painter.getRasterDetail() >= 2000) {
                rasterBigger.setEnabled(false);
            } else {

                rasterBigger.setEnabled(true);
            }
            painter.forceRefresh();
        } else if (e.getSource() == rasterSmaller) {
            if (painter.getRasterDetail() <= 200) {
                rasterSmaller.setEnabled(false);
            } else {

                rasterSmaller.setEnabled(true);
            }
            if (painter.getRasterDetail() >= 2000) {
                rasterBigger.setEnabled(false);
            } else {

                rasterBigger.setEnabled(true);
            }
            painter.setRasterDetail(painter.getRasterDetail() - 100);

            painter.forceRefresh();
        } else if (e.getSource() == showRaster) {
            painter.getPainterData().setShowRaster(!painter.getPainterData().isShowRaster());

            invertRaster.setVisible(painter.getPainterData().isShowRaster());
            rasterSmaller.setVisible(painter.getPainterData().isShowRaster());
            rasterBigger.setVisible(painter.getPainterData().isShowRaster());
            painter.forceRefresh();
        } else if (e.getSource() == search) {
            ((at.darkdestiny.vismap.starmap.Painter) painter).showPopUp(true);

        } else if (e.getSource() == territory_save) {

            painter.setMode(EStarmapMode.TERRITORY_ADD_POINT);
            territory_add.setVisible(!territory_add.isVisible());
            territory_move.setVisible(!territory_move.isVisible());
            territory_add_point.setVisible(!territory_add_point.isVisible());
            territory_save.setVisible(!territory_save.isVisible());
            territory_edit.getModel().setPressed(false);
            territory_edit.getModel().setSelected(false);
            territory_show.setEnabled(true);
            territory_edit.setEnabled(true);

            String source = appletMain.getParameter("source");
            Document document = null;
            String docString = "";
            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder builder =
                        factory.newDocumentBuilder();
                document = builder.newDocument();
                Element root = (Element) document.createElement("Territories");


                List<I_Object_OneCoordinate> territories = painter.getPainterData().getDrawObjects().get(AppletConstants.DRAW_LEVEL_TERRITORY);
                for (Object o : territories) {
                    Territory t = (Territory) o;
                    if (t.getStatus() == 0) {
                        Element territory = document.createElement("Territory");
                        territory.setAttribute("id", String.valueOf(t.getId()));
                        for (Map.Entry<Integer, Point> entry : t.getPoints().entrySet()) {
                            Point p = entry.getValue();
                            Element point = document.createElement("Point");
                            point.setAttribute("id", String.valueOf(p.getId()));
                            point.setAttribute("x", String.valueOf(p.getX1()));
                            point.setAttribute("y", String.valueOf(p.getY1()));
                            territory.appendChild(point);
                        }
                        root.appendChild(territory);
                    }
                }
                document.appendChild(root);

                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");

                StreamResult result = new StreamResult(new StringWriter());
                DOMSource domSource = new DOMSource(document);
                transformer.transform(domSource, result);

                docString = result.getWriter().toString();
                System.out.println(docString);
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Error while creating document : " + ex);

            }




            String basePath = source.replace("createStarMapInfoXT.jsp", "TerritoryServlet");

            try {
                URL url = new URL(basePath);
                URLConnection uc = url.openConnection();
                HttpURLConnection connection = (HttpURLConnection) uc;
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");

                connection.setRequestProperty("Content-Length", ""
                        + Integer.toString(docString.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");

                connection.setRequestProperty("Cookie", "JSESSIONID=" + appletMain.getParameter("sessionId"));
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
                // connection.setRequestProperty("Set-Cookie", cookies);

                //Send request
                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());
                wr.writeBytes(docString);
                wr.flush();
                wr.close();


                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response2 = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response2.append(line);
                    response2.append('\r');
                }
                rd.close();

            } catch (Exception x) {
                x.printStackTrace();
            }


            appletMain.getLoader().setVisible(true);
            ReloadThread lt = new ReloadThread(appletMain);
            lt.start();

            painter.setMode(EStarmapMode.DEFAULT);
            painter.setDefaultCursor(true);
            painter.forceRefresh();
        } else if (e.getSource() == refresh) {

            appletMain.getLoader().setVisible(true);
            ReloadThread lt = new ReloadThread(appletMain);
            lt.start();
        } else if (e.getSource() == help) {
            try {
                AppletContext a = appletMain.getAppletContext();
                a.showDocument(new URL("http://www.thedarkdestiny.at/wiki/index.php/Sternenkarte"), "_new");

            } catch (Exception ex) {
                System.out.print("Error while hyperlinking:" + ex);
            }
        }

        if (!((at.darkdestiny.vismap.starmap.Painter) painter).createdTerritory.isEmpty()) {
            ((at.darkdestiny.vismap.starmap.Painter) painter).createdTerritory.clear();
            painter.setMode(EStarmapMode.DEFAULT);
            painter.setDefaultCursor(true);
            territory_add.getModel().setPressed(false);
            territory_add.getModel().setSelected(false);
            painter.forceRefresh();
        }
    }

    private void setEnabled(ArrayList<JComponent> components, boolean enabled) {
        for (JComponent c : components) {
            c.setEnabled(enabled);
        }
    }
}
