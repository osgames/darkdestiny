/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.starmap.dto;

/**
 *
 * @author Dreloc
 */
public class Relation {

    private int id;
    private String color;

    public Relation(int id, String color) {
        this.id = id;
        this.color = color;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }
}
