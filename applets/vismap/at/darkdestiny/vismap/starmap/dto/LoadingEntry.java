/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.dto;

/**
 *
 * @author HorstRabe
 */
public class LoadingEntry {

    private int count;
    private int ressId;
    private String ressName;
    private int ressType;

    public LoadingEntry(int count, int ressId, String ressName, int ressType){
        
        this.count = count;
        this.ressId = ressId;
        this.ressName = ressName;
        this.ressType = ressType;
        
    }
    
    public int getCapacity() {
        return count;
    }

    public int getRessourceId() {
        return ressId;
    }

    public String getRessourceName() {
        return ressName;
    }

    public int getRessourceType() {
        return ressType;
    }
}
