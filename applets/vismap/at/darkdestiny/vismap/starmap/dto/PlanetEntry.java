/*
 * PlanetEntry.java
 *
 * Created on 24. April 2007, 23:27
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.starmap.dto;

import java.awt.Color;

/**
 *
 * @author Stefan
 */
public class PlanetEntry {
    private int id;
    private String name;
    private String owner;
    private boolean homesystem;
    private String typland;
    private int side = 0;
    private String typId;
    private int diameter;
    private long lastUpdate;
    private String color = "#000000";


/**
 *
 * @param id
 * @param name
 * @param typland
 * @param typId
 * @param owner 1 = own, 2 = allied, 3 = enemy
 * @param homesystem
 * @param side
 */
    public PlanetEntry(int id, String name, String typland, String typId, String owner, boolean homesystem, int side, int diameter, long lastUpdate) {

        this.id = id;
        this.name = name;
        this.owner = owner;
        this.typland = typland;
        this.homesystem = homesystem;
        this.side = side;
        this.typId = typId;
        this.diameter = diameter;
        this.lastUpdate = lastUpdate;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isHomesystem() {
        return homesystem;
    }

    public String getTypland() {
        return typland;
    }

    public int getSide() {
        return side;
    }

    public String getOwner() {
        return owner;
    }

    /**
     * @return the diameter
     */
    public int getDiameter() {
        return diameter;
    }

    /**
     * @return the typId
     */
    public String getTypId() {
        return typId;
    }

    /**
     * @return the lastUpdate
     */
    public long getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

}
