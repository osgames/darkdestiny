/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.starmap.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HorstRabe
 */
public class ChassisEntry {
    private String designName;
    private int count;
    private String chassis;
    private int chassisId;
    private List<ChassisEntry> loadedChassis;
    
    public ChassisEntry(String designName, int count, String chassis, int chassisId, ArrayList<ChassisEntry> loadedChassis){
        this.designName = designName;
        this.count = count;
        this.chassis = chassis;
        this.chassisId = chassisId;
        this.loadedChassis = loadedChassis;
    }

    public String getDesignName() {
        return designName;
    }

    public int getCount() {
        return count;
    }

    public String getChassis() {
        return chassis;
    }

    /**
     * @return the chassisId
     */
    public int getChassisId() {
        return chassisId;
    }

    /**
     * @param chassisId the chassisId to set
     */
    public void setChassisId(int chassisId) {
        this.chassisId = chassisId;
    }

    /**
     * @return the loadedChassis
     */
    public List<ChassisEntry> getLoadedChassis() {
        return loadedChassis;
    }

}
