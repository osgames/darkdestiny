/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.starmap.dto.trade;

import java.util.ArrayList;
import at.darkdestiny.vismap.starmap.dto.TradeRouteDetail;
import java.util.List;

/**
 *
 * @author Aion
 */
public class TradePlanet {


    private int startPlanetId;
    private int endPlanetId;
    private String startPlanetName;
    private String endPlanetName;

    private List<TradeRouteDetail> ressources;

    public TradePlanet(){
        ressources = new ArrayList();
    }

    /**
     * @return the ressources
     */
    public List<TradeRouteDetail> getRessources() {
        return ressources;
    }

    /**
     * @param ressources the ressources to set
     */
    public void setRessources(ArrayList<TradeRouteDetail> ressources) {
        this.ressources = ressources;
    }

    /**
     * @return the startPlanetId
     */
    public int getStartPlanetId() {
        return startPlanetId;
    }

    /**
     * @param startPlanetId the startPlanetId to set
     */
    public void setStartPlanetId(int startPlanetId) {
        this.startPlanetId = startPlanetId;
    }


    /**
     * @return the startPlanetName
     */
    public String getStartPlanetName() {
        return startPlanetName;
    }

    /**
     * @param startPlanetName the startPlanetName to set
     */
    public void setStartPlanetName(String startPlanetName) {
        this.startPlanetName = startPlanetName;
    }

    /**
     * @return the endPlanetName
     */
    public String getEndPlanetName() {
        return endPlanetName;
    }

    /**
     * @param endPlanetName the endPlanetName to set
     */
    public void setEndPlanetName(String endPlanetName) {
        this.endPlanetName = endPlanetName;
    }

    /**
     * @return the endPlanetId
     */
    public int getEndPlanetId() {
        return endPlanetId;
    }

    /**
     * @param endPlanetId the endPlanetId to set
     */
    public void setEndPlanetId(int endPlanetId) {
        this.endPlanetId = endPlanetId;
    }

}
