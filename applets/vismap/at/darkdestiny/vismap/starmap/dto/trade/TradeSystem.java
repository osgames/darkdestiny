/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.starmap.dto.trade;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aion
 */
public class TradeSystem {


    private int startSystemId;
    private String startSystemName;
    private int endSystemId;
    private String endSystemName;

   private List<TradePlanet> planets;

   public TradeSystem(){
       planets = new ArrayList<TradePlanet>();
   }

    /**
     * @return the planets
     */
    public List<TradePlanet> getPlanets() {
        return planets;
    }

    /**
     * @param planets the planets to set
     */
    public void setPlanets(ArrayList<TradePlanet> planets) {
        this.planets = planets;
    }

    /**
     * @return the startSystemId
     */
    public int getStartSystemId() {
        return startSystemId;
    }

    /**
     * @param startSystemId the startSystemId to set
     */
    public void setStartSystemId(int startSystemId) {
        this.startSystemId = startSystemId;
    }

    /**
     * @return the startSystemName
     */
    public String getStartSystemName() {
        return startSystemName;
    }

    /**
     * @param startSystemName the startSystemName to set
     */
    public void setStartSystemName(String startSystemName) {
        this.startSystemName = startSystemName;
    }

    /**
     * @return the endSystemId
     */
    public int getEndSystemId() {
        return endSystemId;
    }

    /**
     * @param endSystemId the endSystemId to set
     */
    public void setEndSystemId(int endSystemId) {
        this.endSystemId = endSystemId;
    }

    /**
     * @return the endSystemName
     */
    public String getEndSystemName() {
        return endSystemName;
    }

    /**
     * @param endSystemName the endSystemName to set
     */
    public void setEndSystemName(String endSystemName) {
        this.endSystemName = endSystemName;
    }

}
