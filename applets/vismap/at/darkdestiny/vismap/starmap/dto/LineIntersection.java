/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.dto;

import at.darkdestiny.vismap.starmap.drawable.Point;
import at.darkdestiny.vismap.starmap.drawable.Territory;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class LineIntersection {

    private Territory territory;
    private Point lineStart;
    private Point lineEnd;

    public LineIntersection(Territory territory, Point lineStart, Point lineEnd) {
        this.territory = territory;
        this.lineStart = lineStart;
        this.lineEnd = lineEnd;
    }

    public void insertPoint(int x, int y) {
        Map<Integer, Point> newList = new TreeMap();
        int lastId = lineStart.getId();
        int newId = lastId + 1;
        boolean foundStart = false;
        boolean shift = false;
        for (Map.Entry<Integer, Point> entry : territory.getPoints().entrySet()) {
            Point point = entry.getValue();
            if (foundStart) {

                //Oh noes we have no id left between the points start shifting by 1
                if (point.getId() == newId) {
                    shift = true;
                }
            }
            if (shift) {
                point.setId(point.getId() + 1);

            }

            if (!foundStart && comparePoints(point, lineStart)) {
                foundStart = true;
            }

        }
        //Rebuild Treemap with new Ids;
        if (shift) {
            territory.rebuildPoints();
        }
        //insert the point
        for (Map.Entry<Integer, Point> entry : territory.getPoints().entrySet()) {
            Point point = entry.getValue();
            if (comparePoints(point, lineStart)) {
                newList.put(point.getId(), point);
                Point p = new Point(newId, "Point", x, y);
                newList.put(p.getId(), p);
            } else {
                newList.put(point.getId(), point);
            }
        }
        territory.setPoints(newList);
    }

    private boolean comparePoints(Point p1, Point p2) {
        if (p1.getX1() == p2.getX1() && p1.getY1() == p2.getY1()) {
            return true;
        } else {
            return false;
        }
    }
}
