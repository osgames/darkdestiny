/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap.dto;

/**
 *
 * @author Admin
 */
public class Chassis {
    private int id;

    public Chassis(int id, String name) {
        this.id = id;
        this.name = name;
    }
    private String name;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
}
