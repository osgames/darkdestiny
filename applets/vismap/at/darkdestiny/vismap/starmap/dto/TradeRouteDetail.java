/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at.darkdestiny.vismap.starmap.dto;


/**
 *
 * @author Stefan
 */
public class TradeRouteDetail{
    private Boolean reversed;
    private Integer ressId;
    private Integer lastTransport;

    /**
     * @return the reversed
     */
    public Boolean getReversed() {
        return reversed;
    }

    /**
     * @param reversed the reversed to set
     */
    public void setReversed(Boolean reversed) {
        this.reversed = reversed;
    }

    /**
     * @return the ressId
     */
    public Integer getRessId() {
        return ressId;
    }

    /**
     * @param ressId the ressId to set
     */
    public void setRessId(Integer ressId) {
        this.ressId = ressId;
    }

    /**
     * @return the lastTransport
     */
    public Integer getLastTransport() {
        return lastTransport;
    }

    /**
     * @param lastTransport the lastTransport to set
     */
    public void setLastTransport(Integer lastTransport) {
        this.lastTransport = lastTransport;
    }

}
