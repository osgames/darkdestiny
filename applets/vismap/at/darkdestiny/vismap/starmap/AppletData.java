/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap;

import at.darkdestiny.vismap.dto.AbstractAppletData;
import at.darkdestiny.vismap.enumerations.EVismapType;
import at.darkdestiny.vismap.joinmap.drawable.Galaxy;
import at.darkdestiny.vismap.starmap.dto.Chassis;
import at.darkdestiny.vismap.starmap.dto.Relation;
import java.util.HashMap;
import java.util.Locale;
import java.util.TreeMap;

/**
 *
 * @author Admin
 */
public class AppletData extends AbstractAppletData{

    private Locale locale;
    private EVismapType vismapType;
    private TreeMap<Integer, Relation> relations;
    private TreeMap<String, Integer> viewSystemRange;
    private HashMap<Integer, Chassis> chassis;
    private HashMap<Integer, Galaxy> galaxies;
    private HashMap<Integer, String> troops;
    private String sessionId;

    public AppletData() {
        relations = new TreeMap<Integer, Relation>();
        troops = new HashMap<Integer, String>();
        chassis = new HashMap<Integer, Chassis>();
        viewSystemRange = new TreeMap<String, Integer>();
        galaxies = new HashMap<>();
    }

    public void addTroop(Integer id, String name) {
        troops.put(id, name);
    }

    public void addViewSystemRange(String type, int range) {
        getViewSystemRange().put(type, range);
    }

    public void addChassis(Integer id, Chassis c) {
        chassis.put(id, c);
    }

    public void addRelation(Integer id, Relation relation) {
        getRelations().put(id, relation);
    }
    public void addGalaxy(Integer id, Galaxy galaxy) {
        getGalaxies().put(id, galaxy);
    }

    /**
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * @param locale the locale to set
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * @return the relations
     */
    public TreeMap<Integer, Relation> getRelations() {
        return relations;
    }

    /**
     * @return the troops
     */
    public HashMap<Integer, String> getTroops() {
        return troops;
    }

    /**
     * @return the chassis
     */
    public HashMap<Integer, Chassis> getChassis() {
        return chassis;
    }

    /**
     * @return the vismapType
     */
    public EVismapType getVismapType() {
        return vismapType;
    }

    /**
     * @param vismapType the vismapType to set
     */
    public void setVismapType(EVismapType vismapType) {
        this.vismapType = vismapType;
    }

    /**
     * @return the viewSystemRange
     */
    public TreeMap<String, Integer> getViewSystemRange() {
        return viewSystemRange;
    }

    public void setSessionId(String sessionId) {
      this.sessionId = sessionId;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @return the galaxies
     */
    public HashMap<Integer, Galaxy> getGalaxies() {
        return galaxies;
    }
}
