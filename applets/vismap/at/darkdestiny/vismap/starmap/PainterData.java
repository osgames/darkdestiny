/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.starmap;

import at.darkdestiny.vismap.dto.AbstractPainterData;
import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.joinmap.drawable.Tile;
import at.darkdestiny.vismap.planetlogviewer.PlanetLogThread;
import at.darkdestiny.vismap.planetlogviewer.dto.PlanetLog;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.starmap.drawable.Point;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.drawable.Territory;
import at.darkdestiny.vismap.starmap.drawable.TradeRoute;
import at.darkdestiny.vismap.starmap.drawable.Transmitter;
import at.darkdestiny.vismap.starmap.drawable.Triangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class PainterData extends AbstractPainterData {

    private Map<Integer, SystemDesc> systems;
    private Map<Integer, Transmitter> transmitters;
    private Map<Integer, Integer> systemsToTransmitter;
    private Map<Integer, Integer> planetsToTransmitter;
    ;
    private Map<Integer, String> users;
    private List<I_Object_OneCoordinate> scanners;
    private List<PlanetLog> planetLog;
    private SystemDesc homeSystem;
    private boolean showFleets = true;
    private boolean showTradeRoutes = false;
    private boolean showHyperScanner = false;
    private boolean rasterInverted = false;
    private boolean showRaster = false;
    private boolean showTerritory = true;
    private boolean showTiles = true;
    private Map<Point, List<Integer>> fleetsDrawn;
    private PlanetLogThread planetLogThread;

    public PainterData() {
        scanners = new ArrayList();
        systems = new HashMap();
        fleetsDrawn = new HashMap();
        transmitters = new HashMap();
        systemsToTransmitter = new HashMap();
        planetsToTransmitter = new HashMap();
    }

    public boolean isContinueOnObject(I_Object_OneCoordinate ob, MainPainter mainPainter, Object data) {
        Boolean sendFleet = (Boolean) data;
        if (ob instanceof FleetEntry && (!isShowFleets() && !sendFleet)) {
            return true;
        }
        if (ob instanceof TradeRoute && (!isShowTradeRoutes() && !sendFleet)) {
            return true;
        }
        if (ob instanceof Tile && !isShowTiles()) {
            return true;
        }

        if (ob instanceof Triangle && (!isShowFleets() && !sendFleet)) {
            return true;
        }

        if (ob instanceof Territory && (!isShowTerritory())) {
            return true;
        }

        return false;
    }

    public void addScanner(I_Object_OneCoordinate scanner) {
        getScanners().add(scanner);
    }

    public void addPlanetLog(PlanetLog pl) {
        if (planetLog == null) {
            planetLog = new ArrayList<>();
        }
        planetLog.add(pl);
    }

    public List<PlanetLog> getPlanetLog() {
        return planetLog;
    }

    public void clearFleetsDrawn() {
        fleetsDrawn.clear();
    }

    public void addPlanetToTransmitterEntry(int planetId, int transmitterId) {
        planetsToTransmitter.put(planetId, transmitterId);
    }

    public Integer getTransmitterByPlanet(int planetId) {
        return planetsToTransmitter.get(planetId);
    }

    public void addSystemToTransmitterEntry(int systemId, int transmitterId) {
        systemsToTransmitter.put(systemId, transmitterId);
    }

    public Integer getTransmitterBySystem(int systemId) {
        return systemsToTransmitter.get(systemId);
    }

    public boolean fleetsDrawnContains(Point point, int type) {
        boolean foundPoint = false;
        boolean foundType = false;
        for (Map.Entry<Point, List<Integer>> entry : fleetsDrawn.entrySet()) {
            Point p = entry.getKey();
            if (p.getX1() == point.getX1() && p.getY1() == point.getY1()) {
                List<Integer> types = entry.getValue();
                if (types == null) {
                    types = new ArrayList<Integer>();

                    types.add(type);
                    fleetsDrawn.put(p, types);
                } else {
                    for (Integer i : types) {
                        if (i.equals(type)) {
                            return true;
                        }
                    }
                    if (!foundType) {
                        return false;
                    }
                }
            }
        }
        if (!foundPoint) {
            return false;
        }
        return false;
    }

    public void addFleetDrawn(Point point, int type) {
        boolean foundPoint = false;
        boolean foundType = false;
        for (Map.Entry<Point, List<Integer>> entry : fleetsDrawn.entrySet()) {
            Point p = entry.getKey();
            if (p.getX1() == point.getX1() && p.getY1() == point.getY1()) {
                List<Integer> types = entry.getValue();
                if (types == null) {
                    types = new ArrayList<Integer>();

                    types.add(type);
                    fleetsDrawn.put(p, types);
                } else {
                    for (Integer i : types) {
                        if (i.equals(type)) {
                            return;
                        }
                    }
                    if (!foundType) {

                        types.add(type);
                        fleetsDrawn.put(p, types);
                    }
                }
            }
        }
        if (!foundPoint) {
            ArrayList<Integer> types = new ArrayList<Integer>();
            types.add(type);
            fleetsDrawn.put(point, types);
        }

    }

    /**
     * @return the homeSystem
     */
    public SystemDesc getHomeSystem() {
        return homeSystem;
    }

    /**
     * @param homeSystem the homeSystem to set
     */
    public void setHomeSystem(SystemDesc homeSystem) {
        this.homeSystem = homeSystem;
    }

    /**
     * @return the scanners
     */
    public List<I_Object_OneCoordinate> getScanners() {
        return scanners;
    }

    /**
     * @return the showFleets
     */
    public boolean isShowFleets() {
        return showFleets;
    }

    /**
     * @param showFleets the showFleets to set
     */
    public void setShowFleets(boolean showFleets) {
        this.showFleets = showFleets;
    }

    /**
     * @return the showTradeRoutes
     */
    public boolean isShowTradeRoutes() {
        return showTradeRoutes;
    }

    /**
     * @param showTradeRoutes the showTradeRoutes to set
     */
    public void setShowTradeRoutes(boolean showTradeRoutes) {
        this.showTradeRoutes = showTradeRoutes;
    }

    /**
     * @return the showHyperScanner
     */
    public boolean isShowHyperScanner() {
        return showHyperScanner;
    }

    /**
     * @param showHyperScanner the showHyperScanner to set
     */
    public void setShowHyperScanner(boolean showHyperScanner) {
        this.showHyperScanner = showHyperScanner;
    }

    /**
     * @return the rasterInverted
     */
    public boolean isRasterInverted() {
        return rasterInverted;
    }

    /**
     * @param rasterInverted the rasterInverted to set
     */
    public void setRasterInverted(boolean rasterInverted) {
        this.rasterInverted = rasterInverted;
    }

    public boolean isShowRaster() {
        return showRaster;
    }

    /**
     * @param showRaster the showRaster to set
     */
    public void setShowRaster(boolean showRaster) {
        this.showRaster = showRaster;
    }

    public void addSystem(int id, SystemDesc sd) {
        systems.put(id, sd);
    }

    public void addTransmitter(int id, Transmitter t) {
        transmitters.put(id, t);
    }

    public Transmitter getTransmitter(int id) {
        return transmitters.get(id);
    }

    public SystemDesc getSystem(int id) {
        return systems.get(id);
    }

    /**
     * @return the showTerritory
     */
    public boolean isShowTerritory() {
        return showTerritory;
    }

    /**
     * @param showTerritory the showTerritory to set
     */
    public void setShowTerritory(boolean showTerritory) {
        this.showTerritory = showTerritory;
    }

    public void addUser(int userId, String userName) {
        if (users == null) {
            users = new HashMap<>();
        }
        users.put(userId, userName);
    }

    public String getUserName(int userId) {
        if (users.get(userId) == null) {
            return "Unbekannt";
        }
        return users.get(userId);
    }

    /**
     * @return the planetLogThread
     */
    public PlanetLogThread getPlanetLogThread() {
        return planetLogThread;
    }

    /**
     * @param planetLogThread the planetLogThread to set
     */
    public void setPlanetLogThread(PlanetLogThread planetLogThread) {
        this.planetLogThread = planetLogThread;
    }

    public ArrayList<SystemDesc> getSystems() {
        ArrayList<SystemDesc> values = new ArrayList<>();
        values.addAll(systems.values());
        return values;
    }

    public void switchShowTiles() {

        this.showTiles = !this.showTiles;
    }

    public boolean isShowTiles() {
        return showTiles;
    }
}
