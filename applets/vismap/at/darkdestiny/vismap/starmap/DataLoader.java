package at.darkdestiny.vismap.starmap;

import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.starmap.dto.Relation;
import at.darkdestiny.vismap.starmap.enumerations.ETradeRouteStatus;
import at.darkdestiny.vismap.gui.LoaderScreen;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.AbstractDataLoader;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.ArrayList;

import at.darkdestiny.vismap.starmap.dto.ChassisEntry;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.starmap.dto.LoadingEntry;
import at.darkdestiny.vismap.starmap.drawable.Observatory;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.dto.PlanetEntry;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.starmap.drawable.Point;
import at.darkdestiny.vismap.starmap.drawable.RangeViewEntry;
import at.darkdestiny.vismap.starmap.drawable.Scanner;
import at.darkdestiny.vismap.starmap.drawable.Territory;
import at.darkdestiny.vismap.starmap.drawable.TradeRoute;
import at.darkdestiny.vismap.starmap.drawable.Transmitter;
import at.darkdestiny.vismap.starmap.drawable.TransmitterRoute;
import at.darkdestiny.vismap.starmap.dto.TradeRouteDetail;
import at.darkdestiny.vismap.starmap.drawable.Triangle;
import at.darkdestiny.vismap.starmap.dto.Chassis;
import at.darkdestiny.vismap.starmap.dto.TroopEntry;
import at.darkdestiny.vismap.starmap.dto.ViewSystemRange;
import at.darkdestiny.vismap.starmap.dto.trade.TradePlanet;
import at.darkdestiny.vismap.starmap.dto.trade.TradeSystem;
import at.darkdestiny.vismap.xml.XMLMemo;
import java.net.HttpURLConnection;
import java.net.URLConnection;

/**
 * Das DataLoader dient dazu den XML Stream auszulesen für - Systeme u.
 * Planeten - Flotten - Interne Handelsflotten - Hyperraumscanner
 *
 *
 * @author martin
 *
 */
public class DataLoader extends AbstractDataLoader {

    private static AppletMain main;
    private static XMLMemo landCoding;
    private static boolean joinMode = false;
    private static int marked = 0;
    private static String basePath = "";
    private static LoaderScreen loader;
    private static int timer = 2;

    int height;
    int width;
    public static final boolean showViewSystemCircles = false;

    static at.darkdestiny.vismap.starmap.AppletData appletData;
    static at.darkdestiny.vismap.starmap.PainterData painterData;

    public DataLoader(AppletMain main, IAppletData appletData, IPainterData painterData, LoaderScreen l) {
        DataLoader.appletData = (at.darkdestiny.vismap.starmap.AppletData) appletData;
        DataLoader.painterData = (at.darkdestiny.vismap.starmap.PainterData) painterData;

        loader = l;

        DataLoader.main = main;
    }

    public void init() {

        loadXMLData(main);
    }

    private static SystemDesc parseSystem(XMLMemo sys) {
        String name = sys.getAttribute("name");
        int id = Integer.parseInt(sys.getAttribute("id"));
        int x = Integer.parseInt(sys.getAttribute("x"));
        int y = Integer.parseInt(sys.getAttribute("y"));

        boolean hasOwnPlanet = false;
        long lastUpdateSystem = Long.parseLong(sys.getAttribute("lastupdate").trim());
        TreeMap<Integer, String> colors = new TreeMap<Integer, String>();
        if (sys.getAttribute("sysstatus") != null) {
            int sysstatus = -1;
            try {
                sysstatus = Integer.parseInt(sys.getAttribute("sysstatus"));
            } catch (Exception e) {
            }

            if ((id == marked) && joinMode) {
                sysstatus = 2;
            }

            Image img = DisplayManagement.getGraphicForSystemState(sysstatus);
            if (sysstatus != -1) {
                SystemDesc sysDesc = null;
                boolean userHomeSystem = false;
                if (!joinMode) {
                    LinkedList<PlanetEntry> peList = new LinkedList<>();
                    if (sys.getElements("Planets").size() > 0) {
                        XMLMemo planet = (XMLMemo) sys.getElements("Planets").get(0);

                        peList = new LinkedList<>();
                        List<XMLMemo> planetList = planet.getElements("Planet");

                        for (XMLMemo currPlanet : planetList) {
                            String owner = "";
                            int side = 0;
                            boolean homesystem = false;

                            int planetId = Integer.parseInt(currPlanet.getAttribute("id"));
                            int diameter = 10;
                            String typId = "";

                            if (currPlanet.getAttribute("typ") != null) {

                                typId = currPlanet.getAttribute("typ");
                            }
                            if (currPlanet.getAttribute("diameter") != null) {

                                diameter = Integer.parseInt(currPlanet.getAttribute("diameter"));
                            }

                            String typland = landCoding.getAttribute(currPlanet.getAttribute("typ"));

                            if (currPlanet.getAttribute("owner") != null) {
                                owner = currPlanet.getAttribute("owner");
                                side = Integer.parseInt(currPlanet.getAttribute("pStatus"));
                                String color2 = "#FFFFFF";
                                if (side == 1) {
                                    hasOwnPlanet = true;
                                    color2 = "#00FF00";
                                } else {
                                    color2 = appletData.getRelations().get(side).getColor();
                                }
                                if (!colors.containsKey(side)) {
                                    colors.put(side, color2);
                                }
                            }
                            if (currPlanet.getAttribute("homesystem") != null) {
                                homesystem = Boolean.valueOf(currPlanet.getAttribute("homesystem"));
                                if (side == 1 && homesystem) {
                                    userHomeSystem = true;
                                }
                                // override image
                                if (homesystem) {
                                    //dbg//dbg System.out.println("homeSystem : found");
                                    img = DisplayManagement.getGraphicForSystemState(-2);
                                }
                            }

                            long lastUpdatePlanet = Long.parseLong(currPlanet.getAttribute("lastupdate"));
                            String planetName = currPlanet.getAttribute("name");
                            PlanetEntry pe = new PlanetEntry(planetId, planetName, typland, typId, owner, homesystem, side, diameter, lastUpdatePlanet);
                            peList.add(pe);
                        }
                    }
                    sysDesc = new SystemDesc(name, id, x, y, sysstatus, img, peList, main, lastUpdateSystem, colors, hasOwnPlanet);

                    if (userHomeSystem) {
                        painterData.setHomeSystem(sysDesc);
                    }
                }

                if (joinMode) {
                    sysDesc = new SystemDesc(name, id, x, y, img, main, hasOwnPlanet);
                    sysDesc.setSystemStatus(sysstatus);

                    if (id == marked) {
                        //dbg//dbg System.out.println("System marked -> " + id);
                        sysDesc.setSystemStatus(2);
                    }
                }

                if (joinMode && (sysstatus == 4)) {
                    XMLMemo score = (XMLMemo) sys.getElements("Score").get(0);
                    sysDesc.setPopPoints(Integer.parseInt(score.getAttribute("popPoints")));
                    sysDesc.setScorePoints(Integer.parseInt(score.getAttribute("statPoints")));
                }

                return sysDesc;
            }
        }

        return new SystemDesc(name, id, x, y, DisplayManagement.getGraphicForSystemState(-1), main, hasOwnPlanet);
    }

    public void reload(AppletMain main) {

        this.painterData = (PainterData) main.mainFrame.getPainter().getPainterData();

        boolean showRaster = painterData.isShowRaster();
        boolean rasterInverted = painterData.isRasterInverted();
        boolean territoryShown = painterData.isShowTerritory();
        boolean fleetShown = painterData.isShowFleets();
        boolean hyperScanner = painterData.isShowHyperScanner();
        boolean tradeRoutes = painterData.isShowTradeRoutes();

        painterData = new PainterData();
        loadXMLData(main);
        //p.fitView(height, width);
        for (int i = timer; i >= 0; i--) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            loader.addLine("Rebuilding UI in : " + i + " seconds");
        }
        painterData.setShowRaster(showRaster);
        painterData.setRasterInverted(rasterInverted);
        painterData.setShowTerritory(territoryShown);
        painterData.setShowFleets(fleetShown);
        painterData.setShowHyperScanner(hyperScanner);
        painterData.setShowTradeRoutes(tradeRoutes);

        main.mainFrame.getPainter().setPainterData(painterData);

    }

    private static XMLMemo getXMLfromFile(InputStream is, AppletMain main) {
        String source = main.getParameter("source");
        String join = main.getParameter("join");

        if (join != null) {
            //dbg//dbg System.out.println("Join Mode active");
            joinMode = true;
        } else {
            joinMode = false;
        }

        if (source == null) {
            // joinMode = true;
            source = "c:\\xml\\starmap.xml";
            //dbg//dbg System.out.println("SOURCE=" + source);

            loader.addLine("Getting XML file from: " + source);
            try {
                is = new FileInputStream(source);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                //dbg//dbg System.out.println("e2=" + e);
                return null;
            }
        } else {
            URL url;

            // Get base path
            if (joinMode) {
                basePath = source.replace("createJoinMapInfo.jsp", "");
            } else {
                basePath = source.replace("createStarMapInfo.jsp", "");

            }
            loader.addLine("Getting XML file from: " + basePath);
            String sessionId = main.getParameter("sessionId");
            main.getAppletData().setSessionId(sessionId);
            loader.addLine("SessionId: " + sessionId);
            try {

                long t1 = System.currentTimeMillis();

                url = new URL(basePath);
                URLConnection uc = url.openConnection();
                HttpURLConnection conn = (HttpURLConnection) uc;
                conn.setRequestProperty("Cookie", "JSESSIONID=" + sessionId);

                loader.addLine("Loading Data - Requesting StarMapInfo - Done in " + (System.currentTimeMillis() - t1) + " ms");

                is = conn.getInputStream();
                loader.addLine("Loading Data - Parsing Inputstream - Done in " + (System.currentTimeMillis() - t1) + " ms");

            } catch (IOException e) {
                e.printStackTrace();
                //dbg//dbg System.out.println("e4=" + e);
                return null;
            }
        }

        //dbg//dbg System.out.println("mem");
        XMLMemo in;
        try {
            long t1 = System.currentTimeMillis();
            //     loader.addLine("Loading Data - Parse to XML - Start");
            in = XMLMemo.parse(is);
            loader.addLine("Loading Data - Parse to XML - Done in " + (System.currentTimeMillis() - t1) + " ms");
            //dbg//dbg System.out.println("is");
        } catch (Exception e) {
            e.printStackTrace();
            //dbg//dbg System.out.println("e5=" + e);
            loader.addLine("Could not read XML - Mostlikely your session has expired. Relogin and retry pls");
            return null;
        }
        try {

            is.close();
            //dbg//dbg System.out.println("closed Stream");
        } catch (Exception e) {
            e.printStackTrace();
            //dbg//dbg System.out.println("Cant close Stream : " + e);
        }
        return in;
    }

    //Load Data from XML
    private static void loadXMLData(AppletMain main) {
        InputStream is = null;
        XMLMemo in = getXMLfromFile(is, main);
        long t1 = System.currentTimeMillis();

        //Possible Errors
        boolean landCodeError = false;
        boolean groundTroopsError = false;
        boolean languageError = false;
        boolean relationsError = false;
        boolean systemsError = false;
        boolean tradeRouteError = false;
        boolean fleetError = false;
        boolean hyperScannerError = false;
        boolean loadConstantsError = false;
        boolean observatoriesError = false;
        boolean chassisError = false;

        ArrayList<Exception> exceptions = new ArrayList<Exception>();
        try {
            loadLandCode(in);
            loader.addLine("Loading Landcode - Done in " + (System.currentTimeMillis() - t1) + " ms");
        } catch (Exception e) {
            exceptions.add(e);
            landCodeError = true;
        }
        try {

            XMLMemo constants = in.getElements("Constants").get(0);
            System.currentTimeMillis();

            t1 = System.currentTimeMillis();
            loader.addLine("Loading Constants - Done in " + (System.currentTimeMillis() - t1) + " ms");

            try {
                loadGroundTroops(constants);
                loader.addLine("Loading Groundtroops - Done in " + (System.currentTimeMillis() - t1) + " ms");

            } catch (Exception e) {
                exceptions.add(e);
                groundTroopsError = true;
            }

            try {
                loadViewSystemRanges(constants);
                loader.addLine("Loading ViewSystemRanges - Done in " + (System.currentTimeMillis() - t1) + " ms");

            } catch (Exception e) {
                exceptions.add(e);
                groundTroopsError = true;
            }

            try {
                loadChassis(constants);
                loader.addLine("Loading Chasis - Done in " + (System.currentTimeMillis() - t1) + " ms");

            } catch (Exception e) {
                exceptions.add(e);
                chassisError = true;
            }
            if (!joinMode) {
                landCoding = ((XMLMemo) in.getElements("Codierung").get(0)).getElements("LandCode").get(0);
            }

            t1 = System.currentTimeMillis();
            XMLMemo Language = constants.getElements("Language").get(0);
            try {
                parseLanguage(Language);
                loader.addLine("Loading Language - Done in " + (System.currentTimeMillis() - t1) + " ms");
            } catch (Exception e) {
                exceptions.add(e);
                languageError = true;
            }

            try {

                XMLMemo UniverseConstants = constants.getElements("UniverseConstants").get(0);
                AppletConstants.UNIVERSE_SIZE_X = Integer.parseInt(UniverseConstants.getAttribute("width"));
                AppletConstants.UNIVERSE_SIZE_Y = Integer.parseInt(UniverseConstants.getAttribute("height"));
                AppletConstants.MAXXDIFF = AppletConstants.UNIVERSE_SIZE_Y * 2 * 4;
                AppletConstants.MAXYDIFF = AppletConstants.UNIVERSE_SIZE_X * 2 * 4;

            } catch (Exception e) {
                e.printStackTrace();
            }

            //      loader.addLine("Loading Relationtypes - Start");
            t1 = System.currentTimeMillis();
            XMLMemo Relations = constants.getElements("Relations").get(0);
            try {
                loadRelationTypes(Relations);
                loader.addLine("Loading Relationtypes - Done in " + (System.currentTimeMillis() - t1) + " ms");
            } catch (Exception e) {
                exceptions.add(e);
                relationsError = true;
            }
        } catch (Exception e) {

            exceptions.add(e);
            loadConstantsError = true;
        }

        t1 = System.currentTimeMillis();

        try {
            loadSystems(in);
            loader.addLine("Loading Systems - Done in " + (System.currentTimeMillis() - t1) + " ms");

        } catch (Exception e) {
            exceptions.add(e);
            systemsError = true;
        }
        t1 = System.currentTimeMillis();
        try {
            loadSunTransmitters(in);
            loader.addLine("Loading SunTransmitter - Done in " + (System.currentTimeMillis() - t1) + " ms");

        } catch (Exception e) {
            exceptions.add(e);
        }
        t1 = System.currentTimeMillis();
        try {
            loadTradeRoutes(in);
            loader.addLine("Loading TradeRoutes - Done in " + (System.currentTimeMillis() - t1) + " ms");
        } catch (Exception e) {
            exceptions.add(e);
            tradeRouteError = true;
        }

        t1 = System.currentTimeMillis();
        try {
            loadFleets(in);
            loader.addLine("Loading Fleets - Done in " + (System.currentTimeMillis() - t1) + " ms");
        } catch (Exception e) {
            exceptions.add(e);
            fleetError = true;
        }

        t1 = System.currentTimeMillis();
        try {
            loadHyperScanner(in);
            loader.addLine("Loading Hyperscanner - Done in " + (System.currentTimeMillis() - t1) + " ms");
        } catch (Exception e) {
            exceptions.add(e);
            hyperScannerError = true;
        }

        t1 = System.currentTimeMillis();
        try {
            loadObservatories(in);
            loader.addLine("Loading Observatories - Done in " + (System.currentTimeMillis() - t1) + " ms");
        } catch (Exception e) {
            exceptions.add(e);
            observatoriesError = true;
        }
        t1 = System.currentTimeMillis();
        try {
            loadTerritories(in);
            loader.addLine("Loading Territories - Done in " + (System.currentTimeMillis() - t1) + " ms");
        } catch (Exception e) {
            exceptions.add(e);
            observatoriesError = true;
        }

        if (observatoriesError || hyperScannerError || systemsError || languageError || relationsError || fleetError || groundTroopsError
                || tradeRouteError || landCodeError || loadConstantsError) {
            loader.addLine(">>>>>>>>>>>>>>>>>An Error occured");

            if (observatoriesError && hyperScannerError && systemsError && languageError && relationsError && fleetError && groundTroopsError
                    && tradeRouteError && landCodeError && loadConstantsError) {

                loader.addLine(">>>>>>>>>>>>>No data could be read this is mostlikely because your session expired! Re-Login and retry");
            }
            if (landCodeError) {
                loader.addLine("LandCode - Error: " + landCodeError);
            }
            if (loadConstantsError) {
                loader.addLine("No Constants found - Error: " + loadConstantsError);
            }
            if (observatoriesError) {
                loader.addLine("Observatories - Error: " + observatoriesError);
            }
            if (hyperScannerError) {
                loader.addLine("Hyperscanner - Error: " + hyperScannerError);
            }
            if (systemsError) {
                loader.addLine("Systems - Error: " + systemsError);
            }
            if (languageError) {
                loader.addLine("Language - Error: " + languageError);
            }
            if (relationsError) {
                loader.addLine("Relation - Error: " + relationsError);
            }
            if (fleetError) {
                loader.addLine("Fleet - Error: " + fleetError);
            }
            if (groundTroopsError) {
                loader.addLine("GroundTroop - Error: " + groundTroopsError);
            }
            if (tradeRouteError) {
                loader.addLine("TradeRoute - Error: " + tradeRouteError);
            }
            for (Exception e : exceptions) {
                e.printStackTrace();
                loader.addLine("Errorline: " + e.getStackTrace()[0].getLineNumber() + " - " + e.toString());
            }
            main.lt.error = true;
        }

        loader.addLine("== Done Loading Data ==");
    }

    private static Scanner parseScanner(XMLMemo memo) {
        int status = Integer.parseInt(memo.getAttribute("status"));
        int x = Integer.parseInt(memo.getAttribute("x"));
        int y = Integer.parseInt(memo.getAttribute("y"));
        int range = Integer.parseInt(memo.getAttribute("range"));
        //dbg//dbg System.out.println("ADDING NEW sCANNER" + x + " : " + y);
        return new Scanner(x, y, status, range);

    }

    private static TradeRoute parseTradeRoute(List<I_Object_OneCoordinate> obj, XMLMemo tradeRoute) {

        int id = Integer.parseInt(tradeRoute.getAttribute("id"));
        int capacity = Integer.parseInt(tradeRoute.getAttribute("capacity"));
        int startX = Integer.parseInt(tradeRoute.getAttribute("startX"));
        int startY = Integer.parseInt(tradeRoute.getAttribute("startY"));
        int startPlanetId = Integer.parseInt(tradeRoute.getAttribute("startPlanetId").trim());
        int startSystemId = Integer.parseInt(tradeRoute.getAttribute("startSystemId").trim());
        String startPlanet = tradeRoute.getAttribute("startPlanet").trim();
        String startSystem = tradeRoute.getAttribute("startSystem").trim();
        int endX = Integer.parseInt(tradeRoute.getAttribute("endX"));
        int endY = Integer.parseInt(tradeRoute.getAttribute("endY"));
        int endPlanetId = Integer.parseInt(tradeRoute.getAttribute("endPlanetId").trim());
        int endSystemId = Integer.parseInt(tradeRoute.getAttribute("endSystemId").trim());
        String endPlanet = tradeRoute.getAttribute("endPlanet").trim();
        String endSystem = tradeRoute.getAttribute("endSystem").trim();
        ETradeRouteStatus status = ETradeRouteStatus.valueOf(tradeRoute.getAttribute("status"));
        boolean external = Boolean.parseBoolean(tradeRoute.getAttribute("external"));

        TradeRoute tr = null;

        if (obj == null) {
            obj = new ArrayList<I_Object_OneCoordinate>();
        }
        //dbg System.out.println("TradeRoute : " + startSystemId + " <=> " + endSystemId);

        for (I_Object_OneCoordinate route : obj) {
            if (route instanceof TradeRoute) {
                TradeRoute trTmp = (TradeRoute) route;
                if ((trTmp.getStartSystemId() == startSystemId && trTmp.getEndSystemId() == endSystemId)
                        || trTmp.getStartSystemId() == endSystemId && trTmp.getEndSystemId() == startSystemId) {
                    tr = trTmp;
                    //dbg System.out.println("Found Existing route");
                    break;
                }

            }
        }

        if (tr == null) {
            //dbg System.out.println("Creating new Route");
            tr = new TradeRoute(startX, startY, endX, endY, main);
            tr.setStartSystemId(startSystemId);
            tr.setStartSystemName(startSystem);
            tr.setEndSystemId(endSystemId);
            tr.setEndSystemName(endSystem);

            tr.setStatus(status);
            tr.setId(id);
            tr.setCapacity(capacity);
            tr.setExternal(external);

        }

        boolean reversed = false;

        for (Iterator i = tradeRoute.getElements("TradeRouteDetail").iterator(); i.hasNext();) {
            //dbg//dbg System.out.println("In TradeRouteDetail");
            XMLMemo trdXml = (XMLMemo) i.next();
            int lastTransport = Integer.parseInt(trdXml.getAttribute("lastTransport"));
            int ressId = Integer.parseInt(trdXml.getAttribute("ressId").trim());
            reversed = Boolean.parseBoolean(trdXml.getAttribute("reversed").trim());
            //dbg System.out.println("rev = " + reversed);
            TradeRouteDetail trdNormal = null;
            TradeRouteDetail trdReversed = null;
            if (!reversed) {

                TradeSystem tsNormal = null;

                //dbg System.out.println("Searching for Normal System : " + startSystemId + " => " + endSystemId);
                for (TradeSystem tsTmp : tr.getSystems()) {
                    if (tsTmp.getStartSystemId() == startSystemId && tsTmp.getEndSystemId() == endSystemId) {
                        tsNormal = tsTmp;
                        //dbg System.out.println("Found Existing Normal System");
                    }
                }
                if (tsNormal == null) {
                    //dbg System.out.println("Create new normal System");
                    tsNormal = new TradeSystem();
                    tsNormal.setStartSystemId(startSystemId);
                    tsNormal.setStartSystemName(startSystem);
                    tsNormal.setEndSystemId(endSystemId);
                    tsNormal.setEndSystemName(endSystem);
                    tr.getSystems().add(tsNormal);
                }
                TradePlanet tpNormal = null;

                //dbg System.out.println("Searching for Normal Planets : " + startPlanetId + " => " + endPlanetId);
                for (TradePlanet tpTmp : tsNormal.getPlanets()) {
                    if (tpTmp.getStartPlanetId() == startPlanetId && tpTmp.getEndPlanetId() == endPlanetId) {
                        tpNormal = tpTmp;
                        //dbg System.out.println("Found existing normal planet");
                    }
                }

                if (tpNormal == null) {
                    //dbg System.out.println("create new normal planet");
                    tpNormal = new TradePlanet();
                    tpNormal.setEndPlanetId(endPlanetId);
                    tpNormal.setEndPlanetName(endPlanet);
                    tpNormal.setStartPlanetId(startPlanetId);
                    tpNormal.setStartPlanetName(startPlanet);
                    tsNormal.getPlanets().add(tpNormal);
                }

                //dbg System.out.println("Searching for Normal ressources");
                for (TradeRouteDetail trdTmp : tpNormal.getRessources()) {
                    if (trdTmp.getRessId() == ressId) {
                        trdNormal = trdTmp;
                        //dbg System.out.println("found ress normal");
                    }
                }
                if (trdNormal == null) {
                    //dbg System.out.println("create ress normal : " + ressId);
                    trdNormal = new TradeRouteDetail();
                    trdNormal.setLastTransport(lastTransport);
                    trdNormal.setRessId(ressId);
                    trdNormal.setReversed(reversed);
                    tpNormal.getRessources().add(trdNormal);
                }

            } else {
                TradeSystem tsReversed = null;
                //dbg System.out.println("Searching for REV System : " + startSystemId + " => " + endSystemId);
                for (TradeSystem tsTmp : tr.getSystems()) {
                    if (tsTmp.getStartSystemId() == endSystemId && tsTmp.getEndSystemId() == startSystemId) {
                        tsReversed = tsTmp;
                        //dbg System.out.println("Found Existing Normal System");
                    }
                }
                if (tsReversed == null) {
                    //dbg System.out.println("Create new REV System");
                    tsReversed = new TradeSystem();
                    tsReversed.setStartSystemId(endSystemId);
                    tsReversed.setStartSystemName(endSystem);
                    tsReversed.setEndSystemId(startSystemId);
                    tsReversed.setEndSystemName(startSystem);
                    tr.getSystems().add(tsReversed);
                }
                //dbg System.out.println("Searching for REV Planets : " + startPlanetId + " => " + endPlanetId);

                TradePlanet tpReversed = null;
                for (TradePlanet tpTmp : tsReversed.getPlanets()) {
                    if (tpTmp.getStartPlanetId() == endPlanetId && tpTmp.getEndPlanetId() == startPlanetId) {
                        tpReversed = tpTmp;
                        //dbg System.out.println("Found existing REV planet");
                    }
                }

                if (tpReversed == null) {
                    //dbg System.out.println("create new REV planet");
                    tpReversed = new TradePlanet();
                    tpReversed.setEndPlanetId(startPlanetId);
                    tpReversed.setEndPlanetName(startPlanet);
                    tpReversed.setStartPlanetId(endPlanetId);
                    tpReversed.setStartPlanetName(endPlanet);
                    tsReversed.getPlanets().add(tpReversed);
                }

                //dbg System.out.println("Searching for REV ressources");
                //<TradeRouteDetail lastTransport="6933" ressId="1" reversed="false" />
                for (TradeRouteDetail trdTmp : tpReversed.getRessources()) {
                    if (trdTmp.getRessId() == ressId) {
                        trdReversed = trdTmp;
                        //dbg System.out.println("found ress REV");
                    }
                }
                if (trdReversed == null) {
                    //dbg System.out.println("create ress REV " + ressId);
                    trdReversed = new TradeRouteDetail();
                    trdReversed.setLastTransport(lastTransport);
                    trdReversed.setRessId(ressId);
                    trdReversed.setReversed(reversed);
                    tpReversed.getRessources().add(trdReversed);

                }
            }
            // main.getmainPaintCanvas().drawObjects.add(tr);

        }

        return tr;
    }

    /**
     * Enemyfleet hat im Gegensatz zur normalen Flotte keine Designnamen sowie
     * Informationen über die Beladung
     *
     * @param fleet
     * @return
     */
    private static FleetEntry parseEnemyFleet(XMLMemo fleet) {

        //dbg//dbg System.out.println("Parsing an enemy Fleet");
        String name = fleet.getAttribute("name");
        String endPlanet = null;
        try {
            endPlanet = fleet.getAttribute("endPlanet").trim();
        } catch (Exception e) {
        }
        int endPlanetId = 0;
        try {
            endPlanetId = Integer.parseInt(fleet.getAttribute("endPlanetId").trim());
        } catch (Exception e) {
        }
        int id = Integer.parseInt(fleet.getAttribute("id"));

        String owner = fleet.getAttribute("owner");
        int startX = Integer.parseInt(fleet.getAttribute("startX"));
        int startY = Integer.parseInt(fleet.getAttribute("startY"));
        int endX = Integer.parseInt(fleet.getAttribute("endX"));
        int endY = Integer.parseInt(fleet.getAttribute("endY"));
        String endSystem = fleet.getAttribute("endSystem").trim();
        int endSystemId = Integer.parseInt(fleet.getAttribute("endSystemId"));
        int timeRemaining = 0;
        try {

            timeRemaining = Integer.parseInt(fleet.getAttribute("time"));
        } catch (Exception e) {
            //dbg//dbg System.out.println("Fetter Error: " + e);
        }

        int status = Integer.parseInt(fleet.getAttribute("status"));
        String fleetFormation = fleet.getAttribute("ff");

        boolean isFF = false;
        if (fleetFormation.equals("1")) {
            isFF = true;
        }
        boolean isFFLeader = false;
        boolean isParticipating = false;

        if (isFF) {
            String fleetFormationLeader = fleet.getAttribute("ffLeader");
            if (fleetFormationLeader.equals("1")) {
                isFFLeader = true;
            }

            String participating = fleet.getAttribute("participating");
            if ((participating != null) && (participating.equalsIgnoreCase("1"))) {
                isParticipating = true;
            }
        }
        double speed = Double.parseDouble(fleet.getAttribute("speed"));
        boolean targetVisible = Boolean.parseBoolean(fleet.getAttribute("targetVisible"));
        FleetEntry fleetEntry = new FleetEntry(startX, startY, endX, endY, name, id, owner, endPlanetId, endPlanet, endSystem, endSystemId, speed, timeRemaining, status, main.mainFrame.getPainter(), isFF, isFFLeader, isParticipating, targetVisible, 0);

        XMLMemo ships = fleet.getElements("Ships").get(0);

        try {
            XMLMemo chassis = ships.getElements("ShipChassis").get(0);

            for (Iterator i = chassis.getElements("ChassisDetail").iterator(); i.hasNext();) {
                XMLMemo chassisEntry = (XMLMemo) i.next();
                fleetEntry.addChassis(parseChassisEntry(chassisEntry));
                //printTradeRoutes(main.getmainPaintCanvas().getTradeRoutes());
            }

        } catch (Exception e) {
            //dbg//dbg System.out.println("ERROR Y");
        }

        return fleetEntry;
    }

    private static FleetEntry parseFleet(XMLMemo fleet) {

        String name = fleet.getAttribute("name");

        int id = Integer.parseInt(fleet.getAttribute("id"));

        String owner = fleet.getAttribute("owner");
        int startX = Integer.parseInt(fleet.getAttribute("startX"));
        int startY = Integer.parseInt(fleet.getAttribute("startY"));

        int endX = Integer.parseInt(fleet.getAttribute("endX"));
        int endY = Integer.parseInt(fleet.getAttribute("endY"));
        int endPlanetId = Integer.parseInt(fleet.getAttribute("endPlanetId").trim());
        String endPlanet = fleet.getAttribute("endPlanet").trim();
        String endSystem = fleet.getAttribute("endSystem").trim();
        int endSystemId = Integer.parseInt(fleet.getAttribute("endSystemId"));
        double speed = Double.parseDouble(fleet.getAttribute("speed"));
        speed = Math.round(speed * 100d) / 100d;
        String fleetFormation = fleet.getAttribute("ff");

        boolean isFF = false;
        if (fleetFormation.equals("1")) {
            isFF = true;
        }
        boolean isFFLeader = false;
        boolean isParticipating = false;

        if (isFF) {

            String fleetFormationLeader = fleet.getAttribute("ffLeader");
            if (fleetFormationLeader.equals("1")) {
                isFFLeader = true;
            }

            String participating = fleet.getAttribute("participating");
            if ((participating != null) && (participating.equalsIgnoreCase("1"))) {
                isParticipating = true;
            }
        }

        int timeRemaining = 0;

        try {
            timeRemaining = Integer.parseInt(fleet.getAttribute("time"));
        } catch (NumberFormatException nfe) {
            timeRemaining = -1;
        }

        int status = Integer.parseInt(fleet.getAttribute("status"));
        int range = 0;
        try {
            range = Integer.parseInt(fleet.getAttribute("range"));
        } catch (Exception e) {

        }

        FleetEntry fleetEntry = new FleetEntry(startX, startY, endX, endY, name, id, owner, endPlanetId, endPlanet, endSystem, endSystemId, speed, timeRemaining, status, main.mainFrame.getPainter(), isFF, isFFLeader, isParticipating, true, range);

        if (fleet.getAttribute("scanDuration") != null) {
            int scanDuration = Integer.parseInt(fleet.getAttribute("scanDuration"));
            fleetEntry.setScanDuration(scanDuration);
        }

        if (fleet.getAttribute("canScanSystem") != null) {
            boolean canScanSystem = Boolean.parseBoolean(fleet.getAttribute("canScanSystem"));
            fleetEntry.setScanSystem(canScanSystem);
        }

        if (fleet.getAttribute("canScanPlanet") != null) {
            boolean canScanPlanet = Boolean.parseBoolean(fleet.getAttribute("canScanPlanet"));
            fleetEntry.setScanPlanet(canScanPlanet);
        }

        XMLMemo ships = fleet.getElements("Ships").get(0);

        try {
            XMLMemo loading = ships.getElements("Loading").get(0);

            for (Iterator i = loading.getElements("LoadingEntry").iterator(); i.hasNext();) {
                XMLMemo loadingEntry = (XMLMemo) i.next();

                fleetEntry.addLoading(parseLoading(loadingEntry));
            }

        } catch (Exception e) {
            //dbg//dbg System.out.println("NO LOADING");
        }

        try {
            XMLMemo chassis = ships.getElements("ShipChassis").get(0);

            for (Iterator i = chassis.getElements("ChassisDetail").iterator(); i.hasNext();) {
                XMLMemo chassisEntry = (XMLMemo) i.next();

                fleetEntry.addChassis(parseChassisEntry(chassisEntry));
            }

        } catch (Exception e) {
            //dbg//dbg System.out.println("ERROR X:" + e);
            e.printStackTrace();
        }

        return fleetEntry;
    }

    private static Observatory parseObservatory(XMLMemo loading) {

        int status = Integer.parseInt(loading.getAttribute("status"));
        int x = Integer.parseInt(loading.getAttribute("x"));
        int y = Integer.parseInt(loading.getAttribute("y"));

        return new Observatory(status, x, y);
    }

    private static LoadingEntry parseLoading(XMLMemo loading) {

        String name = loading.getAttribute("name");
        int count = Integer.parseInt(loading.getAttribute("count"));
        int type = Integer.parseInt(loading.getAttribute("type"));
        int id = Integer.parseInt(loading.getAttribute("id"));

        return new LoadingEntry(count, id, name, type);
    }

    private static ChassisEntry parseChassisEntry(XMLMemo chassisEntry) {
        String designName = "Unbekannt";
        if (chassisEntry.getAttribute("designname") != null) {
            designName = chassisEntry.getAttribute("designname");
        }
        String chassis = chassisEntry.getAttribute("chassisname");
        int chassisId = Integer.parseInt(chassisEntry.getAttribute("chassisid"));
        int count = Integer.parseInt(chassisEntry.getAttribute("count"));

        ArrayList<ChassisEntry> entries = new ArrayList<ChassisEntry>();
        try {
            XMLMemo loadedChassis = chassisEntry.getElements("LoadedChassis").get(0);
            for (Iterator i = loadedChassis.getElements("ChassisDetail").iterator(); i.hasNext();) {
                XMLMemo loadedChassisEntry = (XMLMemo) i.next();
                entries.add(parseChassisEntry(loadedChassisEntry));
                //printTradeRoutes(main.getmainPaintCanvas().getTradeRoutes());
            }
        } catch (Exception e) {
        }
        return new ChassisEntry(designName, count, chassis, chassisId, entries);
    }

    public static String getBasePath() {
        return basePath;
    }

    private static Chassis parseChassis(XMLMemo chassis) {
        String name = chassis.getAttribute("name");
        int id = Integer.parseInt(chassis.getAttribute("id"));
        Chassis c = new Chassis(id, name);
        return c;
    }

    private static Transmitter parseTransmitter(XMLMemo transmitter) {

        int id = Integer.parseInt(transmitter.getAttribute("id"));
        int systemId = Integer.parseInt(transmitter.getAttribute("systemId"));
        int planetId = Integer.parseInt(transmitter.getAttribute("planetId"));
        int userId = Integer.parseInt(transmitter.getAttribute("userId"));
        boolean transitAllowed = Boolean.parseBoolean(transmitter.getAttribute("transitAllowed"));

        List<Integer> targetTransmitters = parseTargetTransmitters(transmitter);

        SystemDesc sd = ((PainterData) main.mainFrame.getPainter().getPainterData()).getSystem(systemId);
        if (sd != null) {
            return new Transmitter(id, systemId, planetId, userId, sd.getX1(), sd.getY1(), transitAllowed, targetTransmitters);
        } else {
            return null;
        }
    }

    private static TransmitterRoute parseTransmitterRoute(XMLMemo transmitterRoute) {

        int id = Integer.parseInt(transmitterRoute.getAttribute("id"));
        int startId = Integer.parseInt(transmitterRoute.getAttribute("startId"));
        int endId = Integer.parseInt(transmitterRoute.getAttribute("endId"));

        Transmitter startTransmitter = ((PainterData) main.mainFrame.getPainter().getPainterData()).getTransmitter(startId);
        Transmitter endTransmitter = ((PainterData) main.mainFrame.getPainter().getPainterData()).getTransmitter(endId);

        try {

            SystemDesc startSystem = ((PainterData) main.mainFrame.getPainter().getPainterData()).getSystem(startTransmitter.getSystemId());
            SystemDesc endSystem = ((PainterData) main.mainFrame.getPainter().getPainterData()).getSystem(endTransmitter.getSystemId());

            return new TransmitterRoute(id, startId, endId, startSystem.getX1(), startSystem.getY1(), endSystem.getX1(), endSystem.getY1());
        } catch (Exception e) {
            e.printStackTrace();
            //System may not yet be visisble
        }
        return null;
    }

    private static ViewSystemRange parseViewSystemRange(XMLMemo troop) {
        String type = troop.getAttribute("type");
        int range = Integer.parseInt(troop.getAttribute("range"));
        ViewSystemRange vsr = new ViewSystemRange(type, range);
        return vsr;
    }

    private static TroopEntry parseTroops(XMLMemo troop) {
        String name = troop.getAttribute("name");
        int id = Integer.parseInt(troop.getAttribute("id"));
        TroopEntry t = new TroopEntry(id, name);
        return t;
    }

    private static Territory parseTerritory(XMLMemo territory) {
        int id = Integer.parseInt(territory.getAttribute("id"));
        int status = Integer.parseInt(territory.getAttribute("status"));
        String description = territory.getAttribute("description");
        String name = territory.getAttribute("name");
        Territory t = new Territory(main.mainFrame.getPainter(), id, description, name, status, false);
        for (Iterator i = territory.getElements("Point").iterator(); i.hasNext();) {
            XMLMemo point = (XMLMemo) i.next();
            Point p = parsePoint(point);

            t.addPoint(p.getId(), p.getId(), p.getIdentifier(), p.getX1(), p.getY1());

        }
        return t;
    }

    private static void parseLanguage(XMLMemo Language) {
        String locale = Language.getAttribute("Locale");
        String language = locale.substring(0, locale.indexOf("_"));
        String country = locale.substring(locale.indexOf("_") + 1, locale.length());
        Locale l = new Locale(language, country);
        appletData.setLocale(l);
    }

    private static Point parsePoint(XMLMemo point) {

        int id = Integer.parseInt(point.getAttribute("id"));
        int x = Integer.parseInt(point.getAttribute("x"));
        int y = Integer.parseInt(point.getAttribute("y"));

        Point p = new Point(id, "Point - " + id, x, y);
        return p;

    }

    private static void loadChassis(XMLMemo constants) {

        XMLMemo groundTroops = constants.getElements("ChassisList").get(0);
        for (Iterator i = groundTroops.getElements("Chassis").iterator(); i.hasNext();) {
            XMLMemo chassis = (XMLMemo) i.next();
            Chassis c = parseChassis(chassis);
            appletData.addChassis(c.getId(), c);
        }
    }

    private static void loadViewSystemRanges(XMLMemo constants) {

        XMLMemo ranges = constants.getElements("ViewSystemRanges").get(0);
        for (Iterator i = ranges.getElements("ViewSystemRange").iterator(); i.hasNext();) {
            XMLMemo range = (XMLMemo) i.next();
            ViewSystemRange vsr = parseViewSystemRange(range);
            appletData.addViewSystemRange(vsr.getType(), vsr.getRange());
        }
    }

    private static void loadGroundTroops(XMLMemo constants) {

        XMLMemo groundTroops = constants.getElements("Groundtroops").get(0);
        for (Iterator i = groundTroops.getElements("Troop").iterator(); i.hasNext();) {
            XMLMemo troop = (XMLMemo) i.next();
            TroopEntry t = parseTroops(troop);
            appletData.addTroop(t.getId(), t.getName());
        }
    }

    private static void loadTerritories(XMLMemo territories) {
        if (territories.getElements("Territories").size() > 0) {
            XMLMemo allSystems = territories.getElements("Territories").get(0);

            for (Iterator i = allSystems.getElements("Territory").iterator(); i.hasNext();) {
                XMLMemo territory = (XMLMemo) i.next();
                Territory t = parseTerritory(territory);
                painterData.addDrawObject(AppletConstants.DRAW_LEVEL_TERRITORY, t);
            }
        }
    }

    private static void loadSystems(XMLMemo in) {

        //Reading all Systems
        XMLMemo allSystems = in.getElements("Systems").get(0);

        for (Iterator i = allSystems.getElements("System").iterator(); i.hasNext();) {
            XMLMemo sys = (XMLMemo) i.next();
            SystemDesc sd = parseSystem(sys);

            if (showViewSystemCircles) {
                if (sd.isHavingOwnPlanet()) {
                    painterData.addDrawObject(AppletConstants.DRAW_LEVEL_RANGE_VIEW_ENTRY, new RangeViewEntry(appletData.getViewSystemRange().get("SYSTEM"), sd.getX1(),
                            sd.getY1()));
                }
            }
            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_SYSTEMS_OVALS, sd);
            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_SYSTEMS, sd);
            painterData.addDisplayObject(AppletConstants.DRAW_LEVEL_SYSTEMS, sd);
            painterData.addSystem(sd.getId(), sd);

        }
    }

    private static void loadTradeRoutes(XMLMemo in) {
        //Reading TradeRoutes
        try {
            XMLMemo traderoutes = in.getElements("TradeRoutes").get(0);

            XMLMemo usertraderoutes = traderoutes.getElements("UserTradeRoutes").get(0);

            for (Iterator i = usertraderoutes.getElements("TradeRoute").iterator(); i.hasNext();) {

                XMLMemo trXML = (XMLMemo) i.next();

                TradeRoute tr = parseTradeRoute(painterData.getDrawObjects().get(AppletConstants.DRAW_LEVEL_TRADE), trXML);

                painterData.addDrawObject(AppletConstants.DRAW_LEVEL_TRADE, tr);
                painterData.addDisplayObject(AppletConstants.DRAW_LEVEL_TRADE, tr);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void loadFleets(XMLMemo in) {
        //Reading Fleets
        try {
            XMLMemo fleets = in.getElements("Fleets").get(0);

            try {
                XMLMemo ownFleets = fleets.getElements("ownFleets").get(0);

                for (Iterator i = ownFleets.getElements("Fleet").iterator(); i.hasNext();) {
                    XMLMemo fleet = (XMLMemo) i.next();
                    FleetEntry fe = parseFleet(fleet);

                    if (showViewSystemCircles) {
                        if (fe.getTimeRemaiuning() == 0) {
                            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_RANGE_VIEW_ENTRY, new RangeViewEntry(appletData.getViewSystemRange().get("FLEET_STATIONARY"), fe.getX1(),
                                    fe.getY1()));
                        } else {
                            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_RANGE_VIEW_ENTRY, new RangeViewEntry(appletData.getViewSystemRange().get("FLEET_INFLIGHT"), fe.getX1(),
                                    fe.getY1()));
                        }
                    }
                    addFleetToPainterData(fe);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                XMLMemo alliedFleets = fleets.getElements("alliedFleets").get(0);

                for (Iterator i = alliedFleets.getElements("Fleet").iterator(); i.hasNext();) {
                    XMLMemo fleet = (XMLMemo) i.next();
                    FleetEntry fe = parseFleet(fleet);

                    addFleetToPainterData(fe);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                XMLMemo enemyFleets = fleets.getElements("enemyFleets").get(0);

                for (Iterator i = enemyFleets.getElements("Fleet").iterator(); i.hasNext();) {

                    XMLMemo fleet = (XMLMemo) i.next();
                    FleetEntry fe = parseEnemyFleet(fleet);

                    addFleetToPainterData(fe);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void loadHyperScanner(XMLMemo in) {
        //Reading Hyperscanner
        try {
            XMLMemo hyperScanner = in.getElements("Hyperscanner").get(0);

            for (Iterator i = hyperScanner.getElements("Scanner").iterator(); i.hasNext();) {

                XMLMemo scanner = (XMLMemo) i.next();
                painterData.addScanner(parseScanner(scanner));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void loadObservatories(XMLMemo in) {

        //main.getmainPaintCanvas().resetObservatories();
        //Reading Observatories
        try {
            XMLMemo observatories = in.getElements("Observatorys").get(0);
            for (Iterator i = observatories.getElements("Observatory").iterator(); i.hasNext();) {
                XMLMemo observatory = (XMLMemo) i.next();
                Observatory ob = parseObservatory(observatory);
                if (showViewSystemCircles) {
                    painterData.addDrawObject(AppletConstants.DRAW_LEVEL_RANGE_VIEW_ENTRY, new RangeViewEntry(appletData.getViewSystemRange().get("OBSERVATORY"), ob.getX1(),
                            ob.getY1()));
                }

                painterData.addDrawObject(AppletConstants.DRAW_LEVEL_OBSERVATORY, ob);

            }
        } catch (Exception e) {
        }
    }

    private static void loadSunTransmitters(XMLMemo in) {

        //main.getmainPaintCanvas().resetObservatories();
        //Reading Observatories
        try {
            if (in.getElements("SunTransmitters") != null && !in.getElements("SunTransmitters").isEmpty()) {
                XMLMemo suntransmitters = in.getElements("SunTransmitters").get(0);
                for (Iterator i = suntransmitters.getElements("SunTransmitter").iterator(); i.hasNext();) {
                    XMLMemo suntransmitter = (XMLMemo) i.next();
                    Transmitter trans = parseTransmitter(suntransmitter);

                    if (trans != null) {
                        painterData.addTransmitter(trans.getId(), trans);
                        painterData.addSystemToTransmitterEntry(trans.getSystemId(), trans.getId());
                        painterData.addPlanetToTransmitterEntry(trans.getPlanetId(), trans.getId());
                        painterData.addDrawObject(AppletConstants.DRAW_LEVEL_TRANSMITTER, trans);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (in.getElements("SunTransmitterRoutes") != null && !in.getElements("SunTransmitterRoutes").isEmpty()) {
                XMLMemo suntransmitterroutes = in.getElements("SunTransmitterRoutes").get(0);
                for (Iterator i = suntransmitterroutes.getElements("SunTransmitterRoute").iterator(); i.hasNext();) {
                    XMLMemo suntransmitterroute = (XMLMemo) i.next();
                    TransmitterRoute transRoute = parseTransmitterRoute(suntransmitterroute);

                    if (transRoute == null) {
                        continue;
                    }
                    painterData.addDrawObject(AppletConstants.DRAW_LEVEL_TRANSMITTER, transRoute);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void loadLandCode(XMLMemo in) {
        landCoding = ((XMLMemo) in.getElements("Codierung").get(0)).getElements("LandCode").get(0);

    }

    private static void loadRelationTypes(XMLMemo Relations) {
        for (Iterator i = Relations.getElements("Relation").iterator(); i.hasNext();) {
            XMLMemo relation = (XMLMemo) i.next();
            Relation r = parseRelation(relation);
            appletData.addRelation(r.getId(), r);
        }
    }

    private static Relation parseRelation(XMLMemo memo) {

        int id = Integer.parseInt(memo.getAttribute("id"));
        String color = (memo.getAttribute("color"));
        //dbg//dbg System.out.println("ADDING NEW sCANNER" + x + " : " + y);
        return new Relation(id, color);

    }

    public IPainterData getPainterData() {
        return painterData;
    }

    private static void addFleetToPainterData(FleetEntry fleetEntry) {

        painterData.addInformationData("f-" + fleetEntry.getId(), fleetEntry);

        painterData.addDrawObject(AppletConstants.DRAW_LEVEL_FLEETS, fleetEntry);
        painterData.addDisplayObject(AppletConstants.DRAW_LEVEL_FLEETS, fleetEntry);

        if (fleetEntry.getTimeRemaiuning() == 0) {
            Triangle tria = new Triangle(fleetEntry.getStatus(), fleetEntry.getX1(), fleetEntry.getY1(), fleetEntry.getId(), main.mainFrame.getPainter(), fleetEntry.isFleetFormationLeader());

            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_FLEETS_TRIANGLES, tria);
        }
    }

    @Override
    public IAppletData getAppletData() {
        return appletData;
    }

    private static List<Integer> parseTargetTransmitters(XMLMemo transmitter) {
        XMLMemo targetTransmitters = transmitter.getElements("TargetTransmitters").get(0);
        List<Integer> result = new ArrayList<>();
        for (Iterator i = targetTransmitters.getElements("TargetTransmitter").iterator(); i.hasNext();) {
            XMLMemo targetTransmitter = (XMLMemo) i.next();
            result.add(Integer.parseInt(targetTransmitter.getAttribute("id")));

        }
        return result;
    }
}
