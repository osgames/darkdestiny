package at.darkdestiny.vismap.xml;

import java.io.IOException;

import at.darkdestiny.vismap.xml.XMLTokenizer.NAME_TOKEN;
import at.darkdestiny.vismap.xml.XMLTokenizer.XML_ATTR_EQUAL;
import at.darkdestiny.vismap.xml.XMLTokenizer.XML_END_TAG;
import at.darkdestiny.vismap.xml.XMLTokenizer.XML_TAG_BEGIN;
import at.darkdestiny.vismap.xml.XMLTokenizer.XML_TAG_END;
import at.darkdestiny.vismap.xml.XMLTokenizer.XML_TEXT;

/**
 * Hiermit wird genau ein Tag mit allen Unter-Tags gelesen
 * @author martin
 */
public class XMLTagReader {

	public static XMLMemo readTag(XMLTokenizer tok) throws XMLMalFormatedException, IOException {
		if (!(tok.nextToken(false) instanceof XMLTokenizer.XML_TAG_BEGIN))
			throw new XMLMalFormatedException(tok, "< erwartet");
		
		if (tok.previewToken(false) instanceof XMLTokenizer.XML_END_TAG)
			return null;
		
		XMLToken n = tok.nextToken(false);
		if (!(n instanceof XMLTokenizer.NAME_TOKEN))
			throw new XMLMalFormatedException(tok, "Name erwartet");
		
		XMLMemo ret = new XMLMemo(n.getText());
		
		readAttribs(tok, ret);
		n = tok.nextToken(false);
		if (n instanceof XMLTokenizer.XML_TAG_END)
		{
			XMLMemo t = ret;
			while (t != null)
			{
				if (tok.previewToken(true) instanceof XML_TEXT)
					ret.addTextChild(tok.nextToken(true));
				if (tok.previewToken(true) instanceof XML_TAG_BEGIN)
				{
					t = readTag(tok);
					if (t != null)
						ret.addChild(t);
				}
			}
			if (!(tok.nextToken(false) instanceof XML_END_TAG))
				throw new XMLMalFormatedException(tok, "/ expected");
			n = tok.nextToken(false);
			if (!(n instanceof XMLTokenizer.NAME_TOKEN))
				throw new XMLMalFormatedException(tok, "Name erwartet");
			if (!n.getText().equals(ret.getName()))
				throw new XMLMalFormatedException(tok, "der Name des schließenden Tags stimmt nicht mit dem des öffnenden Überein");
			if (!(tok.nextToken(false) instanceof XML_TAG_END))
				throw new XMLMalFormatedException(tok, "> expected");
		}
		else if (n instanceof XMLTokenizer.XML_END_TAG)
		{
			if (!(tok.nextToken(false) instanceof XMLTokenizer.XML_TAG_END))
				throw new XMLMalFormatedException(tok, "> erwartet");
		}
		else throw new XMLMalFormatedException(tok, "> erwartet");
		return ret;
	}

	private static void readAttribs(XMLTokenizer tok, XMLMemo ret) throws IOException, XMLMalFormatedException {
		while (tok.previewToken(false) instanceof NAME_TOKEN)
		{
			String name = tok.nextToken(false).getText();
			XMLToken t = tok.nextToken(false);
			if (!(t instanceof XML_ATTR_EQUAL))
				throw new XMLMalFormatedException(tok,"= erwartet");
			
			//Hier den Wert lesen
			String r = tok.readAttribValue();
			ret.addAttribute(name, r);
		}
		
	}

}
