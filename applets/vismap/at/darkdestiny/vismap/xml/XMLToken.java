/**
 * 
 */
package at.darkdestiny.vismap.xml;
/**
 * @author martin
 *
 */
public class XMLToken {

	private String text;

	public XMLToken(String text)
	{
		this.text = text;
	}
	
	public String getText() {
		return text;
	}

}
