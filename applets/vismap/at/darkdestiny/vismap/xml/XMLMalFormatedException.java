package at.darkdestiny.vismap.xml;

public class XMLMalFormatedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3685730287006522977L;

	public XMLMalFormatedException(XMLTokenizer tok, String message) {
		super("Error at "+tok.getPosition()+": "+message);
	}
}
