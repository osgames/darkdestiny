package at.darkdestiny.vismap.xml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

/**
 * Dieser XML-Reader unterstützt nur einen kleinen Teil der XML-Spezifikation
 *
 *
 * @author martin
 */
public class XMLMemo {
    
    private final String name;
    
    /**
     *
     * @param text der Name des XML-Elements
     */
    public XMLMemo(String text) {
        this.name = text;
    }
    
    /**
     * Schreibt das komplette XML-file mit dem PrintWriter ;-)
     * @param w
     */
    public void write(PrintWriter w) {
        write("\n",w,"\t");
        w.println();
    }
    
    public void write(String indent, PrintWriter w,String NEXTINDENT) {
        //erste Leerzeile unterbinden ;-)
        if (!"\n".equalsIgnoreCase(indent))
            w.print(indent);
        w.print("<"+name);
        
        for (Iterator<String> i = attributes.keySet().iterator(); i.hasNext();) {
            String n = i.next();
            w.print(" "+n+"=\""+attributes.get(n)+"\"");
        }
        if ((children.size() == 0) && (texts.length() == 0)) {
            w.print(" />");
            return;
        }
        w.print(">");
        if (texts.length() > 0)
            w.print(indent+texts);
        for (Iterator<XMLMemo> i = children.iterator(); i.hasNext();) {
            XMLMemo tmp = i.next();
            tmp.write(indent+NEXTINDENT, w, NEXTINDENT);
        }
        w.print(indent+"</"+name+">");
    }
    
    public static XMLMemo parse(InputStream is) throws XMLMalFormatedException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        XMLTokenizer tok = new XMLTokenizer(br);
        XMLMemo tmp = XMLTagReader.readTag(tok);
        
        return tmp;
    }
    
    public List<XMLMemo> getElements(String string) {
        List<XMLMemo> res = new ArrayList<XMLMemo>();
        
        for (Iterator<XMLMemo> i = children.iterator(); i.hasNext();) {
            XMLMemo tmp = i.next();
            if (tmp.getName().equals(string))
                res.add(tmp);
        }
        
        return res;
    }
    
    public String getName() {
        return name;
    }
    
    String texts = "";
    
    public void addTextChild(XMLToken token) {
        texts = texts + token.getText();
    }
    
    List<XMLMemo> children = new ArrayList<XMLMemo>();
    public void addChild(XMLMemo t) {
        children.add(t);
    }
    
    TreeMap<String, String> attributes = new TreeMap<String, String>();
    
    public String toString() {
        return getName() + attributes + children;
    }
    
    public void addAttribute(String name2, String val) {
        attributes.put(name2, val);
    }
    
    public String getAttribute(String string) {
        Object o = attributes.get(string);
        if (o == null)
            return null;
        return ""+o;
    }
    
}
