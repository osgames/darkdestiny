/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.planetlogviewer;

import at.darkdestiny.vismap.dto.AbstractAppletData;
import at.darkdestiny.vismap.joinmap.drawable.Galaxy;
import at.darkdestiny.vismap.starmap.dto.Relation;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class AppletData extends AbstractAppletData {

    private Map<Integer, Relation> relations;
    private Map<Integer, Galaxy> galaxies;

    public AppletData() {
        relations = new HashMap();
        galaxies = new HashMap();
    }

    public void addRelation(Integer id, Relation relation) {
        getRelations().put(id, relation);
    }

    /**
     * @return the relations
     */
    public Map<Integer, Relation> getRelations() {
        return relations;
    }
}
