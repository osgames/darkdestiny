/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.planetlogviewer.dto;

import at.darkdestiny.vismap.planetlogviewer.enumerations.EPlanetLogType;

/**
 *
 * @author Admin
 */
public class PlanetLog {

    private final Integer id;
    private final Integer planetId;
    private final Integer userId;
    private final Integer time;
    private final EPlanetLogType type;

    public PlanetLog(Integer id, Integer planetId, Integer userId, Integer time, EPlanetLogType type) {
        this.id = id;
        this.planetId = planetId;
        this.userId = userId;
        this.time = time;
        this.type = type;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the planetId
     */
    public Integer getPlanetId() {
        return planetId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @return the time
     */
    public Integer getTime() {
        return time;
    }

    /**
     * @return the type
     */
    public EPlanetLogType getType() {
        return type;
    }

}
