/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.planetlogviewer.buttons;

import at.darkdestiny.vismap.buttons.BlueButton;
import at.darkdestiny.vismap.buttons.BlueToggleButton;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.logic.ReloadThread;
import at.darkdestiny.vismap.planetlogviewer.PainterData;
import java.applet.AppletContext;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.ToolTipManager;

/**
 *
 * @author Admin
 */
public class ToolBox extends JPanel implements ActionListener {

    private BlueButton forward = null;
    private BlueButton backward = null;
    private BlueButton pause = null;
    private BlueButton play = null;
    private BlueButton faster = null;
    private BlueButton slower = null;
    private BlueToggleButton showRaster = null;

    private BlueToggleButton invertRaster = null;
    private BlueToggleButton rasterSmaller = null;
    private BlueToggleButton rasterBigger = null;

    private BlueButton refresh = null;
    private BlueButton help = null;

    private BlueButton search = null;
    private IPainter painter;
    SpringLayout layout = new SpringLayout();
    ArrayList<JComponent> buttons = new ArrayList<JComponent>();
    private AppletMain appletMain;

    public ToolBox(IPainter painter, AppletMain appletMain) {
        this.setLayout(layout);
        ToolTipManager ttm = ToolTipManager.sharedInstance();
        ttm.setInitialDelay(100);
        this.appletMain = appletMain;
        this.painter = painter;

        buildPanel();
    }

    private void buildPanel() {

        play = new BlueButton("Play", "play.png", 30, 30, painter);
        play.addActionListener(this);
        pause = new BlueButton("Pause", "pause.png", 30, 30, painter);
        pause.addActionListener(this);
        backward = new BlueButton("Zur�ck", "backward.png", 20, 30, painter);
        backward.addActionListener(this);
        forward = new BlueButton("Nach vorne", "forward.png", 30, 30, painter);
        forward.addActionListener(this);

        faster = new BlueButton("Schneller", "faster.png", 30, 30, painter);
        faster.addActionListener(this);
        slower = new BlueButton("Langsamer", "slower.png", 30, 30, painter);
        slower.addActionListener(this);
        showRaster = new BlueToggleButton("Raster Anzeigen", "raster.png", false, 30, 30);
        showRaster.addActionListener(this);

        invertRaster = new BlueToggleButton("Raster Invertieren", "raster_invert.png", false, 30, 30);
        invertRaster.addActionListener(this);


        rasterSmaller = new BlueToggleButton("Raster Verkleinern", "raster_smaller.png", false, 30, 30);
        rasterSmaller.addActionListener(this);

        rasterBigger = new BlueToggleButton("Raster Vergr��ern", "raster_bigger.png", false, 30, 30);
        rasterBigger.addActionListener(this);




        refresh = new BlueButton("Refresh", "refresh.png", 30, 30, painter);
        refresh.addActionListener(this);

        help = new BlueButton("Hilfe", "help.png", 30, 30, painter);
        help.addActionListener(this);


        search = new BlueButton("Suche", "search.png", 30, 30, painter);
        search.addActionListener(this);

        buttons.add(play);
        buttons.add(pause);
        buttons.add(backward);
        //buttons.add(forward);
        buttons.add(faster);
        buttons.add(slower);

        buttons.add(showRaster);
        buttons.add(invertRaster);
        buttons.add(rasterSmaller);
        buttons.add(rasterBigger);
        buttons.add(search);
        buttons.add(refresh);
        buttons.add(help);

        invertRaster.setVisible(false);
        rasterSmaller.setVisible(false);
        rasterBigger.setVisible(false);


        //TODO
     //   territory_show.setEnabled(false);
      //  territory_show.setToolTipText("Under Construction");


        if (buttons.size() > 0) {

            for (JComponent button : buttons) {
                this.add(button);
            }
        }


        System.out.println(appletMain.getWidth()/2);
        layout.putConstraint(SpringLayout.WEST, backward, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, backward, 2, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.WEST, pause, 0, SpringLayout.WEST, backward);
        layout.putConstraint(SpringLayout.NORTH, pause, 0, SpringLayout.SOUTH, backward);

        layout.putConstraint(SpringLayout.WEST, play, 0, SpringLayout.WEST, pause);
        layout.putConstraint(SpringLayout.NORTH, play, 0, SpringLayout.SOUTH, pause);

        layout.putConstraint(SpringLayout.WEST, forward, 0, SpringLayout.WEST, play);
        layout.putConstraint(SpringLayout.NORTH, forward, 0, SpringLayout.SOUTH, play);

        layout.putConstraint(SpringLayout.WEST, faster, 0, SpringLayout.WEST, forward);
        layout.putConstraint(SpringLayout.NORTH, faster, 0, SpringLayout.SOUTH, forward);

        layout.putConstraint(SpringLayout.WEST, slower, 0, SpringLayout.WEST, faster);
        layout.putConstraint(SpringLayout.NORTH, slower, 0, SpringLayout.SOUTH, faster);

        layout.putConstraint(SpringLayout.WEST, showRaster, 0, SpringLayout.WEST, slower);
        layout.putConstraint(SpringLayout.NORTH, showRaster, 0, SpringLayout.SOUTH, slower);


        layout.putConstraint(SpringLayout.WEST, invertRaster, 0, SpringLayout.EAST, showRaster);
        layout.putConstraint(SpringLayout.NORTH, invertRaster, 0, SpringLayout.SOUTH, showRaster);


        layout.putConstraint(SpringLayout.WEST, rasterSmaller, 0, SpringLayout.EAST, invertRaster);
        layout.putConstraint(SpringLayout.NORTH, rasterSmaller, 0, SpringLayout.NORTH, showRaster);


        layout.putConstraint(SpringLayout.WEST, rasterBigger, 0, SpringLayout.EAST, rasterSmaller);
        layout.putConstraint(SpringLayout.NORTH, rasterBigger, 0, SpringLayout.NORTH, showRaster);

        layout.putConstraint(SpringLayout.WEST, search, 0, SpringLayout.WEST, showRaster);
        layout.putConstraint(SpringLayout.NORTH, search, 20, SpringLayout.SOUTH, showRaster);

        layout.putConstraint(SpringLayout.WEST, refresh, 0, SpringLayout.WEST, search);
        layout.putConstraint(SpringLayout.NORTH, refresh, 20, SpringLayout.SOUTH, search);

        layout.putConstraint(SpringLayout.WEST, help, 0, SpringLayout.WEST, refresh);
        layout.putConstraint(SpringLayout.NORTH, help, 0, SpringLayout.SOUTH, refresh);



        this.setPreferredSize(new Dimension(180, buttons.size() * 30 + 30));
        this.setOpaque(false);
        this.setVisible(true);
    }

    public IPainter getPainter() {
        return painter;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        PainterData painterData = (PainterData)painter.getPainterData();
        if (e.getSource() == play) {
            if(!painterData.getPlanetLogThread().isAlive()){
            painterData.getPlanetLogThread().start();
            }else if(painterData.getPlanetLogThread().isPause()){
                painterData.getPlanetLogThread().unpause();
            }
        }  else if (e.getSource() == backward) {
               painterData.getPlanetLogThread().backward();
        }  else if (e.getSource() == pause) {
               painterData.getPlanetLogThread().pause();
        }   else if (e.getSource() == faster) {
                painterData.getPlanetLogThread().faster();
        }  else if (e.getSource() == slower) {
                painterData.getPlanetLogThread().slower();
        }   else if (e.getSource() == invertRaster) {
            painter.getPainterData().setRasterInverted(!painter.getPainterData().isRasterInverted());

            painter.forceRefresh();
        } else if (e.getSource() == rasterBigger) {
            painter.setRasterDetail(painter.getRasterDetail() + 100);
            if (painter.getRasterDetail() <= 200) {
                rasterSmaller.setEnabled(false);
            } else {
                rasterSmaller.setEnabled(true);
            }
            if (painter.getRasterDetail() >= 2000) {
                rasterBigger.setEnabled(false);
            } else {

                rasterBigger.setEnabled(true);
            }
            painter.forceRefresh();
        } else if (e.getSource() == rasterSmaller) {
            if (painter.getRasterDetail() <= 200) {
                rasterSmaller.setEnabled(false);
            } else {

                rasterSmaller.setEnabled(true);
            }
            if (painter.getRasterDetail() >= 2000) {
                rasterBigger.setEnabled(false);
            } else {

                rasterBigger.setEnabled(true);
            }
            painter.setRasterDetail(painter.getRasterDetail() - 100);

            painter.forceRefresh();
        } else if (e.getSource() == showRaster) {
            painter.getPainterData().setShowRaster(!painter.getPainterData().isShowRaster());

            invertRaster.setVisible(painter.getPainterData().isShowRaster());
            rasterSmaller.setVisible(painter.getPainterData().isShowRaster());
            rasterBigger.setVisible(painter.getPainterData().isShowRaster());
            painter.forceRefresh();
        } else if (e.getSource() == search) {
            ((at.darkdestiny.vismap.starmap.Painter) painter).showPopUp(true);

        }else if (e.getSource() == refresh) {

            appletMain.getLoader().setVisible(true);
            ReloadThread lt = new ReloadThread(appletMain);
            lt.start();
        } else if (e.getSource() == help) {
            try {
                AppletContext a = appletMain.getAppletContext();
                a.showDocument(new URL("http://www.thedarkdestiny.at/wiki/index.php/Sternenkarte"), "_new");

            } catch (Exception ex) {
                System.out.print("Error while hyperlinking:" + ex);
            }
        }
    }

    private void setEnabled(ArrayList<JComponent> components, boolean enabled) {
        for (JComponent c : components) {
            c.setEnabled(enabled);
        }
    }
}
