/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.planetlogviewer.listeners;

import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc_ToolTip;
import at.darkdestiny.vismap.starmap.drawable.FleetEntry;
import at.darkdestiny.vismap.drawable.Object_OneCoordinate;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.I_Object_OneCoordinate;
import at.darkdestiny.vismap.logic.AbstractMouse;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class PlanetLogMouse extends AbstractMouse {

    private IPainterData painterData;
    private at.darkdestiny.vismap.planetlogviewer.Painter painter;
    private AppletMain main;
    int lastMousePressedX = 0;
    int lastMousePressedY = 0;
    boolean mouseDragged = false;
    boolean mousePressed = true;
    float x_start = Float.NaN;
    float y_start = Float.NaN;
    private int tmpx;
    private int tmpy;
    private int x_int;
    private int y_int;
    public boolean moveMap = false;
    public boolean selectedRect = false;
    private List<FleetEntry> showFleetEntry = new ArrayList();
    private static final int TOOLTIP_HEIGHT = 10;
    private static final int TOOLTIP_WIDTH = 10;

    public PlanetLogMouse(AppletMain main, IPainter painter, IPainterData painterData) {
        this.painterData = painterData;
        this.painter = (at.darkdestiny.vismap.planetlogviewer.Painter) painter;
        this.main = main;

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {

        int zoom = e.getWheelRotation() * 1;

        painter.mouseWheelChanged(zoom, e);

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (painter.getMode().equals(EStarmapMode.DEFAULT)) {
            if (selectedRect) {
                tmpx = e.getX();
                tmpy = e.getY();
                if (Math.abs(tmpx - x_int) > 2 && Math.abs(tmpy - y_int) > 2) {
                    active = true;
                    painter.repaint();
                }
            } else if (moveMap) {


                painter.setMouseEvent(e);

                tmpx = e.getX();
                tmpy = e.getY();
                int diff_x = x_int - tmpx;
                int diff_y = y_int - tmpy;

                float weight = 0.3f;

                if (Math.abs(diff_x) > 2) {
                    if (diff_x > 0) {
                        painter.moveImage("left", weight);
                        diff_x--;
                    } else if (diff_x < 0) {
                        painter.moveImage("right", weight);
                        diff_x++;
                    }
                }
                if (Math.abs(diff_y) > 2) {
                    if (diff_y > 0) {
                        painter.moveImage("up", weight);
                        diff_y--;
                    } else if (diff_y < 0) {
                        painter.moveImage("down", weight);
                        diff_y++;
                    }
                }
                x_int = painter.getPreferredSize().width / 2;
                y_int = painter.getPreferredSize().height / 2;
                try {
                    Robot r = new java.awt.Robot();
                    r.mouseMove(main.getLocationOnScreen().x + x_int, main.getLocationOnScreen().y + y_int);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

    }

    public boolean isActive() {
        return active;
    }

    @Override
    public void mouseMoved(MouseEvent e) {


        mouseEvent = e;
        SystemDesc sd = findSystem(e.getX(), e.getY());
        if (sd != null) {
            if ((painter.showToolTipFor == null) || (painter.showToolTipFor.sd != sd)) {
                painter.showToolTipFor = new SystemDesc_ToolTip(sd, e.getX(), e.getY(), painterData.getWidth(), painterData.getHeight());
                painter.repaint();
            } else {
                painter.showToolTipFor.setPos(e.getX(), e.getY());
                painter.repaint();
            }

        } else if (painter.showToolTipFor != null) {
            painter.showToolTipFor = null;
            painter.repaint();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //Zoom Rect

        if (painter.getMode().equals(EStarmapMode.DEFAULT)) {
            if (selectedRect) {
                float tmpx = painter.convertToFloatX(e.getX(), e.getY());
                float tmpy = painter.convertToFloatY(e.getX(), e.getY());


                if (Math.abs(x_int - e.getX()) > 5 && Math.abs(y_int - e.getY()) > 5) {

                    painter.zoomRect(x_start, y_start, tmpx, tmpy);
                }
                x_start = Float.NaN;
                y_start = Float.NaN;
            }

            active = false;
            moveMap = false;
            selectedRect = false;
            painter.setDefaultCursor(true);
            painter.forceRefresh();

        }

        if ((lastMousePressedX == e.getX()) && (lastMousePressedY == e.getY())) {


            
                main.mainFrame.setInfoVisibility(false);
            
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mousePressed = true;
        lastMousePressedX = e.getX();
        lastMousePressedY = e.getY();
        if (painter.getMode().equals(EStarmapMode.DEFAULT)) {
            if (e.getButton() == MouseEvent.BUTTON1 && !painter.getMode().equals(EStarmapMode.SEND_FLEET)) {
                x_start = painter.convertToFloatX(e.getX(), e.getY());
                y_start = painter.convertToFloatY(e.getX(), e.getY());
                x_int = e.getX();
                y_int = e.getY();
                selectedRect = true;
            } if (e.getButton() == MouseEvent.BUTTON3) {
                moveMap = true;
                x_int = painter.getPreferredSize().width / 2;
                y_int = painter.getPreferredSize().height / 2;
            }
        }

    }

    protected SystemDesc findSystem(int x, int y) {
        float maxdist = (TOOLTIP_WIDTH * TOOLTIP_WIDTH / 16 + TOOLTIP_HEIGHT * TOOLTIP_HEIGHT / 16);

        SystemDesc res = null;
        float priority = Float.NEGATIVE_INFINITY;
        List<I_Object_OneCoordinate> entries = painterData.getDisplayObjects().get(AppletConstants.DRAW_LEVEL_SYSTEMS);

        for (I_Object_OneCoordinate obj : entries) {
            Object_OneCoordinate sd = (Object_OneCoordinate) obj;
            float dist = (float) Math.sqrt(Math.pow(x - sd.getActCoord_x1(), 2d) + Math.pow(y - sd.getActCoord_y1(), 2d));

            if ((sd instanceof SystemDesc) && (dist < maxdist)) {


                SystemDesc sys = (SystemDesc) sd;
                if (sys.getPriority() > priority) {
                    priority = sys.getPriority();
                    res = sys;
                }
            }
        }

        return res;
    }


    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void paint(IPainter painter, Graphics g) {

        if (selectedRect) {
            int w = Math.abs(tmpx - x_int);
            int h = Math.abs(tmpy - y_int);
            g.setColor(new Color(0.3f, 0.3f, 0.8f, 0.3f));
            g.fillRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w, h);

            g.setColor(new Color(0.3f, 0.3f, 1f));
            g.drawRect(Math.min(x_int, tmpx), Math.min(y_int, tmpy), w, h);
        } else if (moveMap) {
        }
    }

    @Override
    public void update(AppletMain main, MainPainter aThis, IPainterData painterData) {
        this.painterData = painterData;
        this.painter = (at.darkdestiny.vismap.planetlogviewer.Painter) painter;
        this.main = main;

    }
}
