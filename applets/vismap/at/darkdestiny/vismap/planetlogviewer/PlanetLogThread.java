/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.planetlogviewer;

import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.planetlogviewer.dto.PlanetLog;
import at.darkdestiny.vismap.planetlogviewer.enumerations.EPlanetLogType;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.starmap.dto.PlanetEntry;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Admin
 */
public class PlanetLogThread extends Thread {

    int firstTick = Integer.MAX_VALUE;
    int lastTick = Integer.MIN_VALUE;
    int actTick = 0;
    private boolean pause = false;
    PainterData painterData;
    IPainter painter;
    Map<Integer, ArrayList<PlanetLogChange>> changes;
    Map<Integer, Integer> planetToSystemMapping;
    Set<Integer> users = new HashSet<>();
    Map<Integer, String> userColorMapping = new HashMap<>();

    long sleepTime = 10;

    PlanetLogThread(PainterData painterData, IPainter painter) {
        this.painter = painter;
        this.painterData = painterData;
        changes = new HashMap<>();
        planetToSystemMapping = new HashMap<>();
        for (SystemDesc sd : painterData.getSystems()) {
            for (PlanetEntry pe : sd.getPlanetList()) {
                planetToSystemMapping.put(pe.getId(), sd.getId());
            }
        }
        for (PlanetLog pl : painterData.getPlanetLog()) {
            firstTick = Math.min(firstTick, pl.getTime());
            lastTick = Math.max(lastTick, pl.getTime());
            ArrayList<PlanetLogChange> change = changes.get(pl.getTime());
            if (change == null) {
                change = new ArrayList<>();
            }
            if (!users.contains(pl.getUserId())) {
                users.add(pl.getUserId());
            }
            change.add(new PlanetLogChange(pl.getUserId(), pl.getType(), pl.getPlanetId()));
            changes.put(pl.getTime(), change);
        }
        System.out.println("changes : " + changes.size());
        System.out.println("first tick " + firstTick + " lastTick : " + lastTick);
        int userSize = users.size();
        int count = 0;
        for (Integer u : users) {

            int maxValue = 256;
            int colorSpace = (int) Math.pow(maxValue, 3);
            int colorPerUser = Math.round((float) colorSpace / (float) userSize);

            int color = count * colorPerUser;

            Color c = new Color(color);
            userColorMapping.put(u, "#" + Integer.toHexString(c.getRGB()).substring(2));
            count++;
        }
        //normalize

        actTick = firstTick;

    }

    @Override
    public void run() {
        while (true) {
            goToTick(actTick - 1, actTick);
            painter.forceRefresh();
            try {
                sleep(sleepTime);
            } catch (Exception e) {
                System.out.println("Err in run : " + e);
            }
            if (!isPause()) {
                actTick++;
            }

        }
    }

    public void pause() {
        pause = true;
    }

    public void unpause() {
        pause = false;
    }

    public void backward() {
       actTick = firstTick;
       for (SystemDesc sd : painterData.getSystems()) {
           sd.getColors().clear();
           for(PlanetEntry pe : sd.getPlanetList()){
               pe.setOwner("");
               pe.setColor("#000000");
           }
        }
    }

    public void forward() {
        actTick = lastTick;
    }

    public void goToTick(int prevTick, int tick) {

        //Backward
        if (tick < prevTick) {
            int count = prevTick;
            while (tick < prevTick) {
                if (changes.get(actTick) != null) {
                    for (PlanetLogChange plc : changes.get(actTick)) {
                        SystemDesc sd = painterData.getSystem(planetToSystemMapping.get(plc.getPlanetId()));
                        sd.getColors().put(1, userColorMapping.get(plc.getUserId()));
                        if (plc.getType().equals(EPlanetLogType.COLONIZED) && (prevTick - 1 != tick)) {
                            if (sd.getPlanet(plc.getPlanetId()) != null) {
                                sd.getPlanet(plc.getPlanetId()).setOwner("");
                                sd.getPlanet(plc.getPlanetId()).setColor("#000000");
                            }
                        }
                    }
                }
                count--;
            }
        }
        if (prevTick + 1 == tick) {
            if (changes.get(actTick) != null) {
                for (PlanetLogChange plc : changes.get(actTick)) {
                    SystemDesc sd = painterData.getSystem(planetToSystemMapping.get(plc.getPlanetId()));
                    sd.getColors().put(plc.getPlanetId(), userColorMapping.get(plc.getUserId()));
                    if (plc.getType().equals(EPlanetLogType.COLONIZED) || plc.getType().equals(EPlanetLogType.INVADED)) {
                        if (sd.getPlanet(plc.getPlanetId()) != null) {
                            sd.getPlanet(plc.getPlanetId()).setOwner(painterData.getUserName(plc.getUserId()));
                            sd.getPlanet(plc.getPlanetId()).setColor(userColorMapping.get(plc.getUserId()));
                        }
                    }
                }
            }
        }

    }

    /**
     * @return the pause
     */
    public boolean isPause() {
        return pause;
    }

    public int getNormalizedTick() {
        return actTick - firstTick;
    }
    public String getNormalizedDate() {
        int hour = 0;
        int tick = getNormalizedTick();
        hour = Math.round(tick/6);

        int day =  Math.round(tick/144);

        int week =  Math.round(tick/(144 * 7));

        int month = Math.round(tick/(144 * 30));

        return month + "m-" + week + "w-"  + day + "d-" + hour + "h";
    }

    public void faster() {
        sleepTime = Math.max(1, sleepTime - 1);
    }
    public void slower() {
        sleepTime = sleepTime + 1;
    }

    public long getSleepTime() {
        return sleepTime;
    }


    class PlanetLogChange {

        private int userId;
        private EPlanetLogType type;
        private int planetId;

        public PlanetLogChange(int userId, EPlanetLogType type, int planetId) {
            this.userId = userId;
            this.type = type;
            this.planetId = planetId;
        }

        /**
         * @return the userId
         */
        public int getUserId() {
            return userId;
        }

        /**
         * @return the type
         */
        public EPlanetLogType getType() {
            return type;
        }

        /**
         * @return the planetId
         */
        public int getPlanetId() {
            return planetId;
        }
    }
}
