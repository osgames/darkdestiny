/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.planetlogviewer;

import at.darkdestiny.vismap.dto.AbstractPainterData;
import at.darkdestiny.vismap.planetlogviewer.dto.PlanetLog;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class PainterData extends AbstractPainterData{

    private final Map<Integer, SystemDesc> systems;
    private Map<Integer, String> users;
    private List<PlanetLog> planetLog;
    private PlanetLogThread planetLogThread;

    public PainterData() {
        systems = new HashMap();
    }

    public void addPlanetLog(PlanetLog pl){
        if(planetLog == null){
            planetLog = new ArrayList<>();
        }
        planetLog.add(pl);
    }

    public List<PlanetLog> getPlanetLog(){
        return planetLog;
    }

    public void addSystem(int id, SystemDesc sd) {
        systems.put(id, sd);
    }

    public SystemDesc getSystem(int id) {
        return systems.get(id);
    }


    public void addUser(int userId, String userName){
        if(users == null){
            users = new HashMap<>();
        }
        users.put(userId, userName);
    }

    public String getUserName(int userId){
        if(users.get(userId) == null){
            return "Unbekannt";
        }
        return users.get(userId);
    }

    /**
     * @return the planetLogThread
     */
    public PlanetLogThread getPlanetLogThread() {
        return planetLogThread;
    }

    /**
     * @param planetLogThread the planetLogThread to set
     */
    public void setPlanetLogThread(PlanetLogThread planetLogThread) {
        this.planetLogThread = planetLogThread;
    }

    public List<SystemDesc> getSystems() {
        ArrayList<SystemDesc> values = new ArrayList<>();
        values.addAll(systems.values());
        return values;
    }


}
