package at.darkdestiny.vismap.planetlogviewer;

import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.logic.AppletConstants;
import at.darkdestiny.vismap.gui.LoaderScreen;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.logic.AbstractDataLoader;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.ArrayList;

import at.darkdestiny.vismap.starmap.drawable.SystemDesc;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.planetlogviewer.dto.PlanetLog;
import at.darkdestiny.vismap.planetlogviewer.enumerations.EPlanetLogType;
import at.darkdestiny.vismap.starmap.dto.PlanetEntry;
import at.darkdestiny.vismap.xml.XMLMemo;
import java.awt.Image;
import java.net.HttpURLConnection;
import java.net.URLConnection;

/**
 * Das DataLoader dient dazu den XML Stream auszulesen für - Systeme u.
 * Planeten - Flotten - Interne Handelsflotten - Hyperraumscanner
 *
 *
 * @author martin
 *
 */
public class DataLoader extends AbstractDataLoader {

    private static AppletMain main;
    private static final String basePath = "";
    private static LoaderScreen loader;
    private static final int timer = 2;
    private static XMLMemo landCoding;
    int height;
    int width;

    static AppletData appletData;
    static PainterData painterData;

    public DataLoader(AppletMain main, IAppletData appletData, IPainterData painterData, LoaderScreen l) {

        DataLoader.appletData = (at.darkdestiny.vismap.planetlogviewer.AppletData) appletData;
        DataLoader.painterData = (at.darkdestiny.vismap.planetlogviewer.PainterData) painterData;

        loader = l;

        DataLoader.main = main;
    }

    @Override
    public void init() {

        loadXMLData(main);
        painterData.setPlanetLogThread(new PlanetLogThread(painterData, main.mainFrame.getPainter()));
    }

    private static SystemDesc parseSystem(XMLMemo sys) {
        String name = sys.getAttribute("name");
        int id = Integer.parseInt(sys.getAttribute("id"));
        int x = Integer.parseInt(sys.getAttribute("x"));
        int y = Integer.parseInt(sys.getAttribute("y"));
        long lastUpdateSystem = Long.parseLong(sys.getAttribute("lastupdate").trim());

        Map<Integer, String> colors = new TreeMap();
        Image img = DisplayManagement.getGraphicForSystemState(-1);
        List<PlanetEntry> peList = new LinkedList();
        if (sys.getElements("Planets").size() > 0) {
            XMLMemo planet = (XMLMemo) sys.getElements("Planets").get(0);

            peList = new LinkedList();
            List<XMLMemo> planetList = planet.getElements("Planet");

            for (XMLMemo currPlanet : planetList) {
                String owner = "";
                int side = 0;
                boolean homesystem = false;

                int planetId = Integer.parseInt(currPlanet.getAttribute("id"));
                int diameter = 10;
                String typId = "";

                if (currPlanet.getAttribute("typ") != null) {

                    typId = currPlanet.getAttribute("typ");
                }
                if (currPlanet.getAttribute("diameter") != null) {

                    diameter = Integer.parseInt(currPlanet.getAttribute("diameter"));
                }

                String typland = landCoding.getAttribute(currPlanet.getAttribute("typ"));

                long lastUpdatePlanet = Long.parseLong(currPlanet.getAttribute("lastupdate"));
                String planetName = currPlanet.getAttribute("name");
                PlanetEntry pe = new PlanetEntry(planetId, planetName, typland, typId, owner, homesystem, side, diameter, lastUpdatePlanet);
                peList.add(pe);
            }
        }
        return new SystemDesc(name, id, x, y, -1, img, peList, main, lastUpdateSystem, colors, false);
    }

    @Override
    public void reload(AppletMain main) {

        this.painterData = (PainterData) main.mainFrame.getPainter().getPainterData();
        boolean showRaster = painterData.isShowRaster();
        boolean rasterInverted = painterData.isRasterInverted();
        IPainter p = main.mainFrame.getPainter();
        painterData = new PainterData();
        loadXMLData(main);
        //p.fitView(height, width);
        for (int i = timer; i >= 0; i--) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            loader.addLine("Rebuilding UI in : " + i + " seconds");
        }
        painterData.setShowRaster(showRaster);
        painterData.setRasterInverted(rasterInverted);

        main.mainFrame.getPainter().setPainterData(painterData);

    }

    private static XMLMemo getXMLfromFile(InputStream is, AppletMain main) {

        String source = "http://www.thedarkdestiny.at/portal/planetlog/planetlogviewer.xml";

        try {
            URL url = new URL(source);
            URLConnection uc = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uc;

            //       loader.addLine("Loading Data - Parsing Inputstream - Start");
            is = conn.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //dbg//dbg System.out.println("mem");
        XMLMemo in;
        try {
            long t1 = System.currentTimeMillis();
            //     loader.addLine("Loading Data - Parse to XML - Start");
            in = XMLMemo.parse(is);
            loader.addLine("Loading Data - Parse to XML - Done in " + (System.currentTimeMillis() - t1) + " ms");
            //dbg//dbg System.out.println("is");
        } catch (Exception e) {
            e.printStackTrace();
            //dbg//dbg System.out.println("e5=" + e);
            loader.addLine("Could not read XML - Mostlikely your session has expired. Relogin and retry pls");
            return null;
        }
        try {

            is.close();
            //dbg//dbg System.out.println("closed Stream");
        } catch (Exception e) {
            e.printStackTrace();
            //dbg//dbg System.out.println("Cant close Stream : " + e);
        }
        return in;
    }

    //Load Data from XML
    private static void loadXMLData(AppletMain main) {
        InputStream is = null;
        loader.addLine("Loading XML Data");
        XMLMemo in = getXMLfromFile(is, main);
        loader.addLine("Done XML Data");
        long t1 = System.currentTimeMillis();

        //Possible Errors
        boolean landCodeError = false;
        boolean languageError = false;
        boolean systemsError = false;
        boolean loadConstantsError = false;

        List<Exception> exceptions = new ArrayList<Exception>();
        try {
            loadLandCode(in);
            loader.addLine("Loading Landcode - Done in " + (System.currentTimeMillis() - t1) + " ms");
        } catch (Exception e) {
            exceptions.add(e);
            landCodeError = true;
        }
        try {

            XMLMemo constants = in.getElements("Constants").get(0);
            System.currentTimeMillis();
            loadUsers(in);
            t1 = System.currentTimeMillis();
            loader.addLine("Loading Constants - Done in " + (System.currentTimeMillis() - t1) + " ms");

            landCoding = ((XMLMemo) in.getElements("Codierung").get(0)).getElements("LandCode").get(0);

            t1 = System.currentTimeMillis();
            XMLMemo Language = constants.getElements("Language").get(0);
            try {
                parseLanguage(Language);
                loader.addLine("Loading Language - Done in " + (System.currentTimeMillis() - t1) + " ms");
            } catch (Exception e) {
                exceptions.add(e);
                languageError = true;
            }

            try {

                XMLMemo UniverseConstants = constants.getElements("UniverseConstants").get(0);
                AppletConstants.UNIVERSE_SIZE_X = Integer.parseInt(UniverseConstants.getAttribute("width"));
                AppletConstants.UNIVERSE_SIZE_Y = Integer.parseInt(UniverseConstants.getAttribute("height"));
                AppletConstants.MAXXDIFF = AppletConstants.UNIVERSE_SIZE_Y * 2 * 4;
                AppletConstants.MAXYDIFF = AppletConstants.UNIVERSE_SIZE_X * 2 * 4;

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {

            exceptions.add(e);
            loadConstantsError = true;
        }

        try {
            loadPlanetLog(in);
            loader.addLine("Loading PlanetLog - Done in " + (System.currentTimeMillis() - t1) + " ms");

        } catch (Exception e) {
            exceptions.add(e);
            systemsError = true;
        }
        try {
            loadSystems(in);
            loader.addLine("Loading Systems - Done in " + (System.currentTimeMillis() - t1) + " ms");

        } catch (Exception e) {
            exceptions.add(e);
            systemsError = true;
        }

        if (systemsError || languageError
                || landCodeError || loadConstantsError) {
            loader.addLine(">>>>>>>>>>>>>>>>>An Error occured");

            if (systemsError && languageError
                    && landCodeError && loadConstantsError) {

                loader.addLine(">>>>>>>>>>>>>No data could be read this is mostlikely because your session expired! Re-Login and retry");
            }
            if (landCodeError) {
                loader.addLine("LandCode - Error: " + landCodeError);
            }
            if (loadConstantsError) {
                loader.addLine("No Constants found - Error: " + loadConstantsError);
            }
            if (systemsError) {
                loader.addLine("Systems - Error: " + systemsError);
            }
            if (languageError) {
                loader.addLine("Language - Error: " + languageError);
            }
            for (Exception e : exceptions) {
                e.printStackTrace();
                loader.addLine("Errorline: " + e.getStackTrace()[0].getLineNumber() + " - " + e.toString());
            }
            main.lt.error = true;
        }

        loader.addLine("== Done Loading Data ==");
    }

    public static String getBasePath() {
        return basePath;
    }

    private static void loadUsers(XMLMemo in) {
        try {
            XMLMemo users = in.getElements("Users").get(0);

            for (Iterator i = users.getElements("user").iterator(); i.hasNext();) {

                XMLMemo user = (XMLMemo) i.next();
                int userId = Integer.parseInt(user.getAttribute("id"));
                String userName = user.getAttribute("userName");
                painterData.addUser(userId, userName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void loadPlanetLog(XMLMemo in) {
        //Reading Hyperscanner
        try {
            XMLMemo planetLogs = in.getElements("PlanetLogs").get(0);

            for (Iterator i = planetLogs.getElements("PlanetLog").iterator(); i.hasNext();) {

                XMLMemo planetLog = (XMLMemo) i.next();
                painterData.addPlanetLog(parsePlanetLog(planetLog));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void loadLandCode(XMLMemo in) {
        landCoding = ((XMLMemo) in.getElements("Codierung").get(0)).getElements("LandCode").get(0);

    }

    private static PlanetLog parsePlanetLog(XMLMemo memo) {

        int id = Integer.parseInt(memo.getAttribute("id"));
        int planetId = Integer.parseInt(memo.getAttribute("planetId"));
        int userId = Integer.parseInt(memo.getAttribute("userId"));
        int time = Integer.parseInt(memo.getAttribute("time"));
        EPlanetLogType type = EPlanetLogType.valueOf(memo.getAttribute("type"));
        return new PlanetLog(id, planetId, userId, time, type);

    }

    private static void parseLanguage(XMLMemo Language) {
        String locale = Language.getAttribute("Locale");
        String language = locale.substring(0, locale.indexOf("_"));
        String country = locale.substring(locale.indexOf("_") + 1, locale.length());
        Locale l = new Locale(language, country);
        appletData.setLocale(l);
    }

    private static void loadSystems(XMLMemo in) {

        //Reading all Systems
        XMLMemo allSystems = in.getElements("Systems").get(0);

        for (Iterator i = allSystems.getElements("System").iterator(); i.hasNext();) {
            XMLMemo sys = (XMLMemo) i.next();
            SystemDesc sd = parseSystem(sys);

            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_SYSTEMS_OVALS, sd);
            painterData.addDrawObject(AppletConstants.DRAW_LEVEL_SYSTEMS, sd);
            painterData.addDisplayObject(AppletConstants.DRAW_LEVEL_SYSTEMS, sd);
            painterData.addSystem(sd.getId(), sd);

        }
    }

    @Override
    public IPainterData getPainterData() {
        return painterData;
    }

    @Override
    public IAppletData getAppletData() {
        return appletData;
    }

}
