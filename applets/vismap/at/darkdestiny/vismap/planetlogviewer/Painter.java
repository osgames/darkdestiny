package at.darkdestiny.vismap.planetlogviewer;

import at.darkdestiny.vismap.gui.MainPainter;
import at.darkdestiny.vismap.gui.AppletMain;

import at.darkdestiny.vismap.logic.AppletConstants;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import at.darkdestiny.vismap.logic.DisplayManagement;
import at.darkdestiny.vismap.starmap.drawable.SystemDesc_ToolTip;
import at.darkdestiny.vismap.starmap.enumerations.EStarmapMode;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.image.MemoryImageSource;

public class Painter extends MainPainter {

    private static final long serialVersionUID = 4354204667927554366L;

    float[] deltaBase = new float[2];
    float[] offSetBase = new float[2];

    public SystemDesc_ToolTip showToolTipFor;

    private int lastId = -1;
    boolean moved = false;

    public Painter(AppletMain main) {

        super(main);

        setCursor(getDefaultCursor());
    }

    @Override
    public void paint(Graphics g) {

        super.prePaint(g);

        float[] res = new float[2];
        float[] delta = new float[2]; //Die L�nge von jeweils einem LJ (d.h. 1), jeweils in Richtung X und Y

        convertCoordinates(0, 0, res);
        convertCoordinates(1, 1, delta);
        delta[0] -= res[0];
        delta[1] -= res[1];

        if (dataChanged) {

            if (backBuffer == null) {
                backBuffer = createImage(getWidth(), getHeight());
            }
            Graphics gc = backBuffer.getGraphics();

            gc.setColor(Color.black);
            gc.fillRect(0, 0, this.getWidth(), this.getHeight());

            //Main Draw procedure Loop all items
            super.paint(g);

            dataChanged = false;

        }

        g.drawImage(backBuffer, 0, 0, getWidth(), getHeight(), null);

        if (showToolTipFor != null) {
            showToolTipFor.paint(appletData, g);
        }
        g.setColor(Color.black);
        g.fillRect(getWidth() / 2, 30, 150, 45);

        g.setColor(Color.red);
        g.setFont(new Font("Tahoma", Font.BOLD, 15));
        g.drawString("Tick " + ((PainterData) painterData).getPlanetLogThread().getNormalizedTick(), getWidth() / 2, 40);
        g.drawString("Zeit " + ((PainterData) painterData).getPlanetLogThread().getNormalizedDate(), getWidth() / 2, 55);
        g.drawString("Sleeptime (ms) " + ((PainterData) painterData).getPlanetLogThread().getSleepTime(), getWidth() / 2, 70);

        globalRatio = (float) getWidth() / (float) getHeight();

        if (mode.equals(EStarmapMode.DEFAULT) && ((at.darkdestiny.vismap.planetlogviewer.listeners.PlanetLogMouse) mouse).moveMap && mouseEvent != null) {
            String territoryImage = "arr_move.png";

            Image img = DisplayManagement.getGraphic(territoryImage);

            int width = 40;
            int height = 40;

            g.drawImage(img, this.getPreferredSize().width / 2, (this.getPreferredSize().height / 2), width, height, null);

            int[] pixels = new int[16 * 16];
            Image image = Toolkit.getDefaultToolkit().createImage(
                    new MemoryImageSource(16, 16, pixels, 0, 16));
            Cursor transparentCursor
                    = Toolkit.getDefaultToolkit().createCustomCursor(image, new java.awt.Point(0, 0), "invisibleCursor");
            setCursor(transparentCursor);

        }

        if (setDefaultCursor) {
            setCursor(getDefaultCursor());
            setDefaultCursor = false;
        }
        super.postPaint(g);

        if (mouse.active) {
            mouse.paint(this, g);
        }

        if (!main.isInitialized() && !main.isReloaded()) {

            switchToCoordinates(AppletConstants.UNIVERSE_SIZE_X / 2, AppletConstants.UNIVERSE_SIZE_Y / 2);
            main.setInitialized(true);
            main.setReloaded(false);
            dataChanged = true;
            repaint();

        }

    }

    public Cursor getDefaultCursor() {

        if (defaultCursor != null) {
            return defaultCursor;
        }

        defaultCursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);

        return defaultCursor;
    }

    public boolean isMoved() {
        return moved;
    }

    public void setMoved(boolean moved) {
        this.moved = moved;
    }

}
