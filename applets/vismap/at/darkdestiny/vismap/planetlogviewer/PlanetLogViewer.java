/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.darkdestiny.vismap.planetlogviewer;

import at.darkdestiny.vismap.buttons.NavigationPanel;
import at.darkdestiny.vismap.dto.IAppletData;
import at.darkdestiny.vismap.dto.IPainterData;
import at.darkdestiny.vismap.gui.AppletMain;
import at.darkdestiny.vismap.gui.LoaderScreen;
import at.darkdestiny.vismap.interfaces.IDataLoader;
import at.darkdestiny.vismap.interfaces.IPainter;
import at.darkdestiny.vismap.interfaces.IVisMapPlugin;
import at.darkdestiny.vismap.logic.AbstractMouse;
import at.darkdestiny.vismap.planetlogviewer.buttons.ToolBox;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLayeredPane;
import javax.swing.SpringLayout;

/**
 *
 * @author Admin
 */
public class PlanetLogViewer implements IVisMapPlugin {

    private IPainter painter;
    private AbstractMouse mouse;

    @Override
    public IPainter getPainter(AppletMain main) {
        if (painter == null) {
            painter = new Painter(main);
        }
        return painter;
    }

    @Override
    public IDataLoader getDataLoader(AppletMain main, IAppletData appletData, IPainterData painterData, LoaderScreen l) {
        return new at.darkdestiny.vismap.planetlogviewer.DataLoader(main, appletData, painterData, l);
    }

    @Override
    public List<String> getImages() {
        List<String> images = new ArrayList<>();
        images.add("cancelP.png");
        images.add("mainstar.gif");
        images.add("Enemy_Fleet.gif");
        images.add("Own_Fleet.gif");
        images.add("Allied_Fleet.gif");
        images.add("Enemy_Fleet_Mirr.gif");
        images.add("Own_Fleet_Mirr.gif");
        images.add("Allied_Fleet_Mirr.gif");
        images.add("movefleet.gif");
        images.add("cancel.jpg");
        images.add("foodP.png");
        images.add("howalP.png");
        images.add("ynkeP.png");
        images.add("terkonitP.png");
        images.add("steelP.png");
        images.add("ironP.png");
        images.add("topPlanet.png");
        images.add("topSystem.png");
        images.add("zoomOut.png");
        images.add("zoomIn.png");
        images.add("arrright.png");
        images.add("arrleft.png");
        images.add("arrup.png");
        images.add("arrdown.png");
        images.add("RessourceTitlePanel.png");
        images.add("leftPlanet.png");
        images.add("rightPlanet.png");
        images.add("bottomPlanet.png");
        images.add("leftSystemTop.png");
        images.add("leftSystemRess.png");
        images.add("rightSystemRess.png");
        images.add("leftSystemRessBottom.png");
        images.add("rightSystemRessBottom.png");
        images.add("bottomSystem.png");
        images.add("Fleet_Outline.png");
        images.add("Fleet_Outline_Mirr.png");
        images.add("Treaty_Fleet.png");
        images.add("Treaty_Fleet_Mirr.png");
        images.add("10.jpg");
        return images;
    }

    @Override
    public void addPanels(JLayeredPane panel, AppletMain appletMain, IPainter painter) {

        ToolBox tb = new ToolBox(painter, appletMain);
        SpringLayout layout = (SpringLayout) panel.getLayout();
        panel.add(tb, 2, 1);
        layout.putConstraint(SpringLayout.NORTH, tb, 15, SpringLayout.NORTH, panel);
        layout.putConstraint(SpringLayout.WEST, tb, 15, SpringLayout.WEST, panel);


        NavigationPanel np = new NavigationPanel(painter);
        panel.add(np, 6, 1);
        layout.putConstraint(SpringLayout.SOUTH, np, -20, SpringLayout.SOUTH, panel);
        layout.putConstraint(SpringLayout.WEST, np, panel.getPreferredSize().width / 2 - np.getPreferredSize().width / 2, SpringLayout.WEST, panel);
    }

    @Override
    public AbstractMouse getMouse(AppletMain main, IPainter p, IPainterData painterData) {
        mouse = new at.darkdestiny.vismap.planetlogviewer.listeners.PlanetLogMouse(main, p, painterData);
        return mouse;
    }

    @Override
    public AbstractMouse getMouse() {

        return mouse;
    }
}
