/*
 * Connection.java
 *
 * Created on 03. Juli 2008, 19:26
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Chat.Network;

import Chat.AppletManagement;
import Chat.CommandParser;
import Chat.ParameterEntry;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketPermission;
import java.security.MessageDigest;
import sun.misc.*;
/**
 *
 * @author Stefan
 */
public class Connection extends Thread {
    private Socket s = null;
    private boolean noError = true;
    private static boolean loggedIn = false;
    private static PrintWriter out;
    private static BufferedReader in;
    public static String lastError = "";
    
    /** Creates a new instance of Connection */
    public Connection() {
        try { 
            // s = new Socket("localhost", 10500);         
            s = new Socket("www.thedarkdestiny.at",10500);
            s.setKeepAlive(true);
            establishStreams();
        } catch (Exception e) {
            System.out.println("Communication failure3: " + e);
        }                
    }
    
    public void run() {
        try {
            while (noError) {
                String response = in.readLine();
                ParameterEntry pe = CommandParser.parseCommand(response);                              
                
                switch(pe.command) {
                    case(CommandParser.MSG):
                        AppletManagement.addTextToChatWindow(pe.pars.get(0),pe.pars.get(1),pe.pars.get(2));
                        break;
                    case(CommandParser.MSGPRIVATE):
                        break;
                    case(CommandParser.OPENCHAN):
                        AppletManagement.addChatWindow(pe.pars.get(0),0);
                        break;
                    case(CommandParser.ADDUSER):
                        AppletManagement.addUserToChatWindow(pe.pars.get(0),pe.pars.get(1));
                        break;
                    case(CommandParser.REMOVEUSER):
                        AppletManagement.removeUserFromChatWindows(pe.pars.get(0));
                        break;
                    case(CommandParser.PING):
                        out.println("PONG");
                        out.flush();
                        break;
                    case(CommandParser.INVALID):
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Communication failure1: " + e);
        }
    }
    
    private void establishStreams() {
        try {
            out = new PrintWriter(s.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));     
            
            String response = in.readLine();
        } catch (Exception e) {
            System.out.println("Communication failure2: " + e);
        }
    }
    
    public static void sendCommand(String command) {
        out.println(command);
        out.flush();
    }
    
    public static boolean processLogin(String username, String password) {
        try {                        
            out.println("LOGIN|"+username+"|"+encryptPassword(password));
            out.flush();

            boolean timeout = false;
            int counter = 0;
            
            while (true) {                                
                String response = in.readLine();            
                if (!response.equalsIgnoreCase("LOGINOK")) {                
                    ParameterEntry pe = CommandParser.parseCommand(response);
                    
                    if (pe.command == CommandParser.LOGINFAILED) {
                        lastError = pe.pars.get(0);
                        return false;
                    } else if (pe.command == CommandParser.PING) {
                        Connection.sendCommand("PONG");
                        continue;
                    } else {
                        lastError = "Ungueltiger Befehl ("+response+")";
                        return false;
                    }
                } else {                
                    loggedIn = true;
                    return true;
                }    
            }
        } catch (Exception e) {
            System.out.println("ERROR IN LOGIN: " + e);
            return false;
        }
    }
    
    private static String encryptPassword(String password) {
        String encrypted = "";
        
        byte[] orgPwd = password.getBytes();
        
        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(orgPwd);
            byte messageDigest[] = algorithm.digest();
            
            // String s = new BASE64Encoder().encode(messageDigest);
            String s = "";
            for (int i=0;i<messageDigest.length;i++) {
                String currentHex = Integer.toHexString(0xFF & messageDigest[i]);
                if (currentHex.length() == 1) currentHex = "0" + currentHex;
                s += currentHex;
            }
            encrypted = s;
        } catch (Exception e) {
            
        }
        
        return encrypted;
    }        
}
