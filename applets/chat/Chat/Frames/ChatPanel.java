/*
 * ChatPanel.java
 *
 * Created on 03. Juli 2008, 19:04
 */

package Chat.Frames;

import Chat.Network.Connection;

/**
 *
 * @author  Stefan
 */
public class ChatPanel extends javax.swing.JPanel {
    private String windowName;
    
    /** Creates new form ChatPanel */
    public ChatPanel(String windowName) {
        this.windowName = windowName;
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jScrollPane1 = new javax.swing.JScrollPane();
        chatField = new javax.swing.JTextPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        chatUserList = new javax.swing.JList();
        chatInput = new javax.swing.JTextField();
        sendText = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        chatField.setBackground(new java.awt.Color(0, 0, 0));
        chatField.setForeground(new java.awt.Color(204, 255, 255));
        jScrollPane1.setViewportView(chatField);

        chatUserList.setBackground(new java.awt.Color(0, 0, 0));
        chatUserList.setForeground(new java.awt.Color(102, 204, 255));
        jScrollPane2.setViewportView(chatUserList);

        chatInput.setBackground(new java.awt.Color(0, 0, 0));
        chatInput.setForeground(new java.awt.Color(102, 204, 255));
        chatInput.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                chatInputSendLine(evt);
            }
        });

        sendText.setBackground(new java.awt.Color(255, 255, 255));
        sendText.setText("Senden");
        sendText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendTextActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, chatInput, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(sendText, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(sendText)
                    .add(chatInput, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void chatInputSendLine(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_chatInputSendLine
        if (evt.getKeyCode() == evt.VK_ENTER) {        
            Connection.sendCommand("MSG|"+windowName+"|"+this.chatInput.getText());
            chatInput.setText("");
        }
    }//GEN-LAST:event_chatInputSendLine

    private void sendTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendTextActionPerformed
        Connection.sendCommand("MSG|"+windowName+"|"+this.chatInput.getText());
        chatInput.setText("");
    }//GEN-LAST:event_sendTextActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextPane chatField;
    private javax.swing.JTextField chatInput;
    public javax.swing.JList chatUserList;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton sendText;
    // End of variables declaration//GEN-END:variables
    
}
