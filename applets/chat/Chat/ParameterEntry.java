/*
 * ParameterEntry.java
 *
 * Created on 04. Juli 2008, 00:40
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Chat;

import java.util.ArrayList;

/**
 *
 * @author Stefan
 */
public class ParameterEntry {
    public int command;
    public ArrayList<String> pars;
    
    /** Creates a new instance of ParameterEntry */
    public ParameterEntry(int command, ArrayList<String> pars) {
        this.command = command;
        this.pars = pars;
    }    
}
