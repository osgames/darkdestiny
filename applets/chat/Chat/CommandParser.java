/*
 * CommandParser.java
 *
 * Created on 04. Juli 2008, 00:39
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Chat;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author Stefan
 */
public class CommandParser {
    public static final int MSG = 1;
    public static final int MSGPRIVATE = 2;
    public static final int OPENCHAN = 3;
    public static final int ADDUSER = 4;
    public static final int REMOVEUSER = 5;
    public static final int PING = 6;
    public static final int LOGINFAILED = 7;
    public static final int INVALID = 999;
    
    /** Creates a new instance of CommandParser */
    public CommandParser() {
        
    }
    
    public static ParameterEntry parseCommand(String response) {
        System.out.println("Incoming message: " + response);
        
        ParameterEntry pe = null;
                        
        StringTokenizer st = new StringTokenizer(response,"|");
        String[] elem = new String[st.countTokens()];
        
        int count = 0;
        while (st.hasMoreElements()) {
            elem[count] = (String)st.nextElement();
            count++;
        }
        
        // String[] elem = response.split("a");
        
        System.out.println("Command Elements: " + elem.length);
        
        if (elem.length == 0) {
            return new ParameterEntry(INVALID,null);
        }
        
        if (elem[0].equalsIgnoreCase("MSG")) {
            System.out.println("MSG identified");
            if (elem.length < 4) {
                pe = new ParameterEntry(INVALID,null);
            } else {
                ArrayList<String> pars = new ArrayList<String>();
                
                pars.add(elem[1]);
                pars.add(elem[2]);
                
                String lastPar = "";
                for (int i=3;i<elem.length;i++) {
                   lastPar += elem[i];
                }
                pars.add(lastPar);
                
                pe = new ParameterEntry(MSG,pars);
            }
        } else if (elem[0].equalsIgnoreCase("MSGPRIVATE")) {
            System.out.println("MSGPRIVATE identified");
            
            if (elem.length < 3) {
                pe = new ParameterEntry(INVALID,null);
            } else {
                ArrayList<String> pars = new ArrayList<String>();
                
                pars.add(elem[1]);
                String lastPar = "";
                for (int i=2;i<elem.length;i++) {
                   lastPar += elem[i];
                }
                pars.add(lastPar);
                
                pe = new ParameterEntry(MSGPRIVATE,pars);
            }            
        } else if (elem[0].equalsIgnoreCase("OPENCHAN")) {
            System.out.println("OPENCHAN identified");
            
            if (elem.length < 2) {
                pe = new ParameterEntry(INVALID,null);
            } else {
                ArrayList<String> pars = new ArrayList<String>();
                
                System.out.println("Name = " + elem[1]);
                pars.add(elem[1]);                
                
                pe = new ParameterEntry(OPENCHAN,pars);      
            }
        } else if (elem[0].equalsIgnoreCase("ADDUSER")) {
            System.out.println("ADDUSER identified");
            
            if (elem.length < 3) {
                pe = new ParameterEntry(INVALID,null);
            } else {
                ArrayList<String> pars = new ArrayList<String>();
                
                pars.add(elem[1]);
                pars.add(elem[2]);
                
                pe = new ParameterEntry(ADDUSER,pars);      
            }            
        } else if (elem[0].equalsIgnoreCase("REMOVEUSER")) {
            System.out.println("REMOVEUSER identified");
            
            if (elem.length < 2) {
                pe = new ParameterEntry(INVALID,null);
            } else {
                ArrayList<String> pars = new ArrayList<String>();
                
                pars.add(elem[1]);
                
                pe = new ParameterEntry(REMOVEUSER,pars);      
            }       
        } else if (elem[0].equalsIgnoreCase("LOGINFAILED")) {
            System.out.println("LOGINFAILED identified");
            
            if (elem.length < 2) {
                pe = new ParameterEntry(INVALID,null);
            } else {
                ArrayList<String> pars = new ArrayList<String>();
                
                pars.add(elem[1]);
                
                pe = new ParameterEntry(LOGINFAILED,pars);      
            }   
        } else if (elem[0].equalsIgnoreCase("PING")) {
            pe = new ParameterEntry(PING,null);
        } else {
            System.out.println("Invalid Command identified");
            pe = new ParameterEntry(INVALID,null);
        }
        
        return pe;
    } 
}
