/*
 * AppletManagement.java
 *
 * Created on 03. Juli 2008, 18:50
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package Chat;

import Chat.Frames.ChatPanel;
import Chat.Frames.LoginPanel;
import Chat.Network.Connection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.ListModel;

/**
 *
 * @author Stefan
 */
public class AppletManagement {
    public static AppletMain applet;
    private static HashMap<String,ChatPanel> chatWindows = new HashMap<String,ChatPanel>();
    private static Thread conn;
    
    /** Creates a new instance of AppletManagement */
    public AppletManagement() {
         conn = new Connection();
    }

    public void init(AppletMain app) {
        this.applet = app;
                
        JMenu menuEntry = app.jMenuBar1.getMenu(0);
        JMenuItem menuItem = new JMenuItem("Login");
        menuItem.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) {
                openLoginWindow();
            }
        });
                    
        menuEntry.add(menuItem);
                
        // conn.start();
    }
    
    public void openLoginWindow() {        
        // applet.jLayeredPane1.moveToFront(lp);
        applet.jTabbedPane1.add("Login",new LoginPanel());
        applet.jMenuBar1.getMenu(0).getItem(0).setEnabled(false);
    }
    
    public static boolean processLogin(String username, String password) {
        boolean success = Connection.processLogin(username,password);
                
        System.out.println("SUCCESS = " + success);
        if (success) {
            applet.jTabbedPane1.remove(0);
            conn.start();
        }
        
        return success;
    }
    
    public static void addChatWindow(String name, int type) {
        ChatPanel panelTmp = new ChatPanel(name);
        applet.jTabbedPane1.addTab(name,panelTmp);
        chatWindows.put(name,panelTmp);
    }
    
    public static void addTextToChatWindow(String channel, String name, String text) {
        ChatPanel panelTmp = chatWindows.get(channel);
        panelTmp.chatField.setText(panelTmp.chatField.getText() + name + "> " + text + "\n");
        panelTmp.chatField.setCaretPosition(panelTmp.chatField.getText().length());
    }
    
    public static void addUserToChatWindow(String name, String username) {
        ChatPanel panelTmp = chatWindows.get(name);
        ListModel lm = panelTmp.chatUserList.getModel();
        
        String[] users = new String[lm.getSize()+1];
        for (int i=0;i<lm.getSize();i++) {
            users[i] = (String)lm.getElementAt(i);
        }
        
        users[lm.getSize()] = username;
        
        DefaultListModel alm = new javax.swing.DefaultListModel();
 
        alm.setSize(users.length);
        for (int i = 0; i < users.length; i++) {
            alm.set(i, users[i]);
        }
        
        panelTmp.chatUserList.setModel(alm);  
        
        addTextToChatWindow(name,"Server",username + " betritt den Raum");
    }
    
    public static void removeUserFromChatWindows(String username) {
        for (Map.Entry<String,ChatPanel> me : chatWindows.entrySet()) {
            ChatPanel panelTmp = me.getValue();
            String roomName = me.getKey();
            ListModel lm = panelTmp.chatUserList.getModel();

            String[] users = new String[lm.getSize()];
            for (int i=0;i<lm.getSize();i++) {          
                users[i] = (String)lm.getElementAt(i);
                if (users[i].equalsIgnoreCase(username)) {
                    users[i] = null;
                }
            }       

            DefaultListModel alm = new javax.swing.DefaultListModel();

            alm.setSize(users.length - 1);

            int elemCounter = 0;

            for (int i = 0; i < users.length; i++) {
                if (users[i] != null) {
                    alm.add(elemCounter,users[i]);
                    elemCounter++;
                }           
            }

            panelTmp.chatUserList.setModel(alm);          
            addTextToChatWindow(roomName,"Server",username + " verl�sst den Raum");
        }
    }
}
