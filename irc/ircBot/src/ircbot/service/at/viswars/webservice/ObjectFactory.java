
package at.viswars.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.viswars.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _QuestionList_QNAME = new QName("http://webservice.viswars.at/", "questionList");
    private final static QName _Quiz_QNAME = new QName("http://webservice.viswars.at/", "quiz");
    private final static QName _Question_QNAME = new QName("http://webservice.viswars.at/", "question");
    private final static QName _QuizResult_QNAME = new QName("http://webservice.viswars.at/", "quizResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.viswars.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QuestionList }
     * 
     */
    public QuestionList createQuestionList() {
        return new QuestionList();
    }

    /**
     * Create an instance of {@link Quiz }
     * 
     */
    public Quiz createQuiz() {
        return new Quiz();
    }

    /**
     * Create an instance of {@link Question }
     * 
     */
    public Question createQuestion() {
        return new Question();
    }

    /**
     * Create an instance of {@link QuizResult }
     * 
     */
    public QuizResult createQuizResult() {
        return new QuizResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuestionList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.viswars.at/", name = "questionList")
    public JAXBElement<QuestionList> createQuestionList(QuestionList value) {
        return new JAXBElement<QuestionList>(_QuestionList_QNAME, QuestionList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Quiz }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.viswars.at/", name = "quiz")
    public JAXBElement<Quiz> createQuiz(Quiz value) {
        return new JAXBElement<Quiz>(_Quiz_QNAME, Quiz.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Question }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.viswars.at/", name = "question")
    public JAXBElement<Question> createQuestion(Question value) {
        return new JAXBElement<Question>(_Question_QNAME, Question.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuizResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.viswars.at/", name = "quizResult")
    public JAXBElement<QuizResult> createQuizResult(QuizResult value) {
        return new JAXBElement<QuizResult>(_QuizResult_QNAME, QuizResult.class, null, value);
    }

}
