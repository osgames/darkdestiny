/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot;

import at.viswars.webservice.IRC;
import ircbot.commands.ECommand;
import ircbot.commands.ICommand;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public interface IBot {

    void sendWelcomeMessage();

    ArrayList<User> getUsers();

    User searchUser(String nick);

    void updateUserCount();

    void sendMessage(String message);

    public HashMap<ECommand, ICommand> getCommands();

    public IRC getIrcSrv();
}
