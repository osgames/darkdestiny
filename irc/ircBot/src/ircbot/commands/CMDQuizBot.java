/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.commands;

import ircbot.Bot;
import ircbot.bot.quizbot.DarkBot;

/**
 *
 * @author Admin
 */
public class CMDQuizBot extends AbstractCommand {

    public CMDQuizBot(Bot bot) {
        super(bot, new String[]{"!quizbot"}, "Zeigt dieses Men�");

    }

    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {
        if (startsWith(message)) {
                String msg = message.replace(getCommands()[0], "");
                msg = msg.trim();
                if (msg.startsWith(EQuizBotCommand.start.toString())) {
                    if(bot instanceof DarkBot){
                    ((DarkBot)bot).startQuizBot();
                }
                } else if (msg.startsWith(EQuizBotCommand.stop.toString())) {
                    
                    ((DarkBot)bot).stopQuizBot();
                    
                }
        }
    }
    
    
    @Override
    public void printDescription(String sender) {
        for (EQuizBotCommand cmd : EQuizBotCommand.values()) {
            String desc = "";
            if (cmd.equals(EQuizBotCommand.start)) {
                desc = "startet den Quizbot";
            } else if (cmd.equals(EQuizBotCommand.stop)) {
                desc = "den Quizbot";
            } 
            bot.sendMessage(sender, getCommands()[0] + " " + cmd + " " + desc);
        }
    }

    enum EQuizBotCommand{
        start, stop
    }
}
