/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.commands;

import ircbot.Bot;
import ircbot.User;

/**
 *
 * @author Admin
 */
public class CMDListUsers extends AbstractCommand {

    public CMDListUsers(Bot bot) {
        super(bot, "!listusers", "Zeigt alle Benutzer im Raum");

    }

    @Override
    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {
        if (isTriggered(message)) {
            for (User u : bot.getUsers()) {
                if (!u.getNick().equals(bot.getName())) {
                    bot.sendMessage("Hallo " + u.getNick());
                }
            }
        }
    }
}
