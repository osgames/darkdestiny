/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.commands;

import ircbot.Bot;
import ircbot.User;

/**
 *
 * @author Admin
 */
public class CMDPoints extends AbstractCommand {

    public CMDPoints(Bot bot) {
        super(bot, "!points", " <Spielername>  Zeigt Namen und Rang des Spielers");

    }

    @Override
    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {
        if (startsWith(message)) {
            String gameName = message.replace(getCommands()[0], "");
            gameName = gameName.trim();
            
            Integer points = bot.getIrcSrv().getPoints(gameName);
            Integer rank = bot.getIrcSrv().getRank(gameName);
            if(points >= 0 && rank  >= 0){
                bot.sendMessage(gameName + " | Rang : " + rank + " Punkte : " + points);
            }else{
                bot.sendMessage(gameName + " nicht in den Statistiken gefunden ");
            }
        }
    }
}
