/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.commands;

import ircbot.Bot;

/**
 *
 * @author Admin
 */
public class CMDGreeting extends AbstractCommand {

    public CMDGreeting(Bot bot) {
        super(bot, new String[]{"Hi", "Hello", "Hallo", "Guten Tag", "Guten Morgen"}, "Kills the Bot");

    }

    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {
        if (startsWith(message) && message.toLowerCase().contains("darkbot") && !sender.toLowerCase().contains("bot")) {
            int rnd = (int)(Math.random() * 5);
            String greeting = "Hi";
            switch(rnd){
                case(0):
                    greeting = "Hallo";
                    break;
                case(1):
                    greeting = "Ol�";
                    break;
                case(2):
                    greeting = "Sal�t";
                    break;
                case(3):
                    greeting = "Guten Tag";
                    break;
                case(4):
                    greeting = "Habe die Ehre";
                    break;
                case(5):
                    greeting = "K�ss die Hand";
                    break;
            }
                bot.sendMessage(greeting + " " + sender);
        }
    }
}
