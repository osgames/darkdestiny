/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.commands;

import ircbot.Bot;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class CMDQA extends AbstractCommand {

    HashMap<Integer, Question> questions = new HashMap<Integer, Question>();
    int lastQuestionId = 0;

    public CMDQA(Bot bot) {
        super(bot, "!q&a", "");

    }

    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {
        try {
            if (startsWith(message)) {
                String msg = message.toLowerCase().replace(getCommands()[0].toLowerCase(), "");
                msg = msg.trim();
                if (msg.startsWith(EQuestionCommand.list.toString())) {
                    if (questions.isEmpty()) {
                        bot.sendMessage("Keine Fragen vorhanden");
                    }
                    for (Map.Entry<Integer, Question> entry : questions.entrySet()) {
                        bot.sendMessage("<" + entry.getKey() + "> " + entry.getValue().question + " | " + entry.getValue().answer);
                    }
                } else if (msg.startsWith(EQuestionCommand.add.toString())) {
                    Question q = new Question(msg.replace(EQuestionCommand.add.toString(), "").trim());
                    questions.put(q.id, q);
                    bot.sendMessage("Frage hinzugef�gt : " + "<" + q.id + "> " + q.question);
                } else if (msg.startsWith(EQuestionCommand.solve.toString())) {
                    msg = msg.replace(EQuestionCommand.solve.toString(), "").trim();
                    String idString = msg.substring(0, msg.indexOf(" ")).trim();
                    msg = msg.substring(msg.indexOf(" ")).trim();
                    int id = Integer.parseInt(idString);
                    Question q = questions.get(id);
                    if (q != null) {
                        q.answer = msg;
                        bot.sendMessage("Antwort hinzugef�gt : " + "<" + q.id + "> " + q.question + " | " + q.answer);
                    }

                } else if (msg.startsWith(EQuestionCommand.delete.toString())) {
                    msg = msg.replace(EQuestionCommand.delete.toString(), "").trim();
                    String idString = msg.substring(0, msg.length()).trim();
                    int id = Integer.parseInt(idString);
                    Question q = questions.get(id);
                    if (q != null) {
                        bot.sendMessage("Frage gel�scht : " + "<" + q.id + "> " + q.question + " | " + q.answer);
                        questions.remove(id);
                    }

                }
            }
        } catch (Exception e) {
                        bot.sendMessage("Der Syntax ist wohlm�glich falsch");
            e.printStackTrace();
        }
    }

    @Override
    public void printDescription(String sender) {
        for (EQuestionCommand cmd : EQuestionCommand.values()) {
            String desc = "";
            if (cmd.equals(EQuestionCommand.add)) {
                desc = "<frage> F�gt eine Frage hinzu";
            } else if (cmd.equals(EQuestionCommand.list)) {
                desc = "Zeigt alle Fragen und Antworten";
            } else if (cmd.equals(EQuestionCommand.solve)) {
                desc = "<id> <antwort> beantwortet eine Frage";
            } else if (cmd.equals(EQuestionCommand.delete)) {
                desc = "<id> l�scht eine Frage";
            }
            bot.sendMessage(sender, getCommands()[0] + " " + cmd + " " + desc);
        }
    }

    enum EQuestionCommand {

        list, add, solve, delete
    }

    class Question {

        int id = 0;
        final String question;
        String answer = "";

        public Question(String question) {
            int newId = lastQuestionId + 1;
            id = newId;
            this.question = question;

            lastQuestionId = newId;
        }
    }
}
