/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.commands;

/**
 *
 * @author Admin
 */
public interface ICommand {
    public abstract void onMessage(String channel, String sender,
            String login, String hostname, String message);
    
    public void printDescription(String sender);
    public String[] getCommands();
}
