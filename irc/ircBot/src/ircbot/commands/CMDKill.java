/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.commands;

import ircbot.Bot;
import ircbot.bot.quizbot.DarkBot;

/**
 *
 * @author Admin
 */
public class CMDKill extends AbstractCommand {

    public CMDKill(Bot bot) {
        super(bot, "!kill", "Beendet den Bot");

    }

    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {
        if (isTriggered(message)) {
            if (isOp(sender)) {
                if(bot instanceof DarkBot){
                    ((DarkBot)bot).stopQuizBot();
                }
                
                
                bot.sendMessage(bot.getName() + " geht schlafen");
                try{
                    Thread.sleep(2000);
                }catch(Exception e){
                    
                }
                bot.disconnect();
                bot.quitServer();
                bot.dispose();
            }else{
                bot.sendMessage(sender + " hat nicht genug Rechte");
            }
        }
    }
}
