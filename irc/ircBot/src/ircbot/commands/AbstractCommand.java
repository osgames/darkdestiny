/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.commands;

import ircbot.Bot;
import ircbot.IBot;
import ircbot.User;
import org.jibble.pircbot.PircBot;

/**
 *
 * @author Admin
 */
public abstract class AbstractCommand implements ICommand{
    
    protected String[] commands;
    protected String description;
    protected Bot bot;
    
    
    public AbstractCommand(Bot bot, String command, String description){
        commands = new String[1];
        commands[0] = command;
        this.bot = bot;
        this.description = description;
    }
    
    public AbstractCommand(Bot bot, String[] commands, String description){
        this.commands = commands;
        this.bot = bot;
        this.description = description;
    }
    
    protected boolean isOp(String nick){
        for(User u : bot.getUsers()){
            if(u.getNick().equals(("!" + nick))){
                return true;
            }
        }
        return false;
    }
    
    public boolean isTriggered(String command){
        for(String s : commands){
            if(command.equals(s)){
                return true;
            }
        }
        return false;
    }
    public boolean startsWith(String command){
        for(String s : commands){
            if(command.startsWith(s)){
                return true;
            }
        }
        return false;
    }

    /**
     * @return the command
     */
    public String[] getCommands() {
        return commands;
    }

    /**
     * @return the description
     */
    public void printDescription(String sender) {
                    String help = "";
                    boolean first = true;
                    for (String s : getCommands()) {
                        if (first) {
                            help += s;
                            first = false;
                        } else {
                            help += " " + s;
                        }
                    }
                    System.out.println("Sending : " + sender + " help message");
                bot.sendMessage(sender, help + " - " + description);
    }
}
