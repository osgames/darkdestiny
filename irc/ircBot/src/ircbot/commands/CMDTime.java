/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.commands;

import ircbot.Bot;

/**
 *
 * @author Admin
 */
public class CMDTime extends AbstractCommand{
    
    
    
    public CMDTime(Bot bot){
        super(bot, "!time", "Zeigt die Uhrzeit an");
        
    }
    
    
    
    public void onMessage(String channel, String sender,
            String login, String hostname, String message){
         if (isTriggered(message)) {
            String time = new java.util.Date().toString();
            bot.sendMessage(sender + ": Die Zeit ist " + time);
        }
    }
}
