/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.bot.quizbot;

/**
 *
 * @author Admin
 */
import at.viswars.webservice.Question;
import ircbot.*;
import ircbot.bot.quizbot.commands.CMDQuestion;
import ircbot.bot.quizbot.commands.CMDQuiz;
import ircbot.bot.quizbot.commands.CMDQuizBotHelp;
import ircbot.commands.ECommand;
import ircbot.commands.ICommand;
import ircbot.thread.QuizThread;
import java.util.ArrayList;
import java.util.Map;

public class QuizBot extends Bot implements IBot{

    private QuizThread quizThread = null;

    public QuizBot(String name, String channel) {
        super(name, channel);
        commands.put(ECommand.KILL, new CMDQuiz(this));
        commands.put(ECommand.HELP, new CMDQuizBotHelp(this));
        commands.put(ECommand.QUESTION, new CMDQuestion(this));

    }

    public void sendWelcomeMessage() {

        sendMessage(getName() + " zu ihrer Stelle - Pleeep");
    }

    public void startQuiz(int quizId, int amount, int seconds) {
        if (quizThread == null) {
            System.out.println(quizId + " - " +  amount + " - " +  seconds);
            ArrayList<Question> questions = (ArrayList<Question>) getIrcSrv().getQuestionList(quizId, amount, 0, true).getQuestions();
            System.out.println("got : " + questions.toString() + " from the server");
            quizThread = new QuizThread(this, quizId, questions, seconds);
            quizThread.start();
            sendMessage("QuizThread Started");
        }else{
            sendMessage("Es ist bereits eine Quiz Runde im Gange");
        }
    }
    public void stopQuiz() {
        if (quizThread != null) {
            quizThread.setCancel(true);
            quizThread.interrupt();
            quizThread = null;
        }else{
            sendMessage("Es ist keine Runde im Gange");
        }
    }

    @Override
    public void onJoin(String channel, String sender, String login, String hostname) {
        if (!sender.trim().equalsIgnoreCase(getName().trim())) {

            User u = new User(sender, sender);
            users.put(sender, u);
            updateUserCount();
        }

    }

    @Override
    public void onPart(String channel, String sender, String login, String hostname) {
        try {
            users.remove(sender);
        } catch (Exception e) {
            System.out.println("User : " + sender + " was not on list");
        }
        updateUserCount();

    }

    @Override
    public void onNickChange(String oldNick, String login, String hostname, String newNick) {
        try {
            users.remove(oldNick);
            User u = new User(newNick, newNick);
            users.put(newNick, u);
        } catch (Exception e) {
            e.printStackTrace();
        }
        updateUserCount();

    }

    @Override
    public void onUserList(String channel,
            org.jibble.pircbot.User[] tmpUsers) {
        for (org.jibble.pircbot.User u : tmpUsers) {
            users.put(u.getNick(), new User(u.getNick(), u.getPrefix()));
        }
    }

    @Override
    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {

        for (Map.Entry<ECommand, ICommand> entry : getCommands().entrySet()) {
            entry.getValue().onMessage(channel, sender, login, hostname, message);
        }

        if(quizThread != null){
            quizThread.onMessage(channel, sender, login, hostname, message);
        }


    }

    @Override
    public void onPrivateMessage(String sender, String login, String hostname, String message) {
    }
}
