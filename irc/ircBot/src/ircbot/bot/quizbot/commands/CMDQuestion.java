/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.bot.quizbot.commands;

import at.viswars.webservice.QuestionList;
import at.viswars.webservice.Quiz;
import at.viswars.webservice.QuizResult;
import ircbot.commands.*;
import ircbot.Bot;

/**
 *
 * @author Admin
 */
public class CMDQuestion extends AbstractCommand {

    public CMDQuestion(Bot bot) {
        super(bot, new String[]{"!quizbot"}, "Kills the Bot");

    }

    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {
        if (startsWith(message)) {
            String msg = message.replace(getCommands()[0], "");
            msg = msg.trim();
            if (msg.startsWith(EQuestionCommand.qlist.toString())) {
                
            } else if (msg.startsWith(EQuestionCommand.qadd.toString())) {

            } else if (msg.startsWith(EQuestionCommand.qdelete.toString())) {

            }
        }
    }

    public void printDescription(String sender) {

        for (EQuestionCommand cmd : EQuestionCommand.values()) {
            String desc = "";
            if (cmd.equals(EQuestionCommand.qlist)) {
                desc = "<quizId> Listet alle Fragen f�r ein bestimmtes Quiz";
            } else if (cmd.equals(EQuestionCommand.qadd)) {
                desc = "<quizId> <name> | <topic> F�gt einem Quiz eine Frage hinzu";
            } else if (cmd.equals(EQuestionCommand.qdelete)) {
                desc = "<quizId> <frageId> L�scht eine Frage";
            }
            bot.sendMessage(sender, getCommands()[0] + " " + cmd + " " + desc);
        }
    }

    enum EQuestionCommand {

        qlist, qadd, qdelete
    }
}
