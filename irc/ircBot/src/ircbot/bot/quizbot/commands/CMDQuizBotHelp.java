/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.bot.quizbot.commands;

import ircbot.commands.*;
import ircbot.Bot;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class CMDQuizBotHelp extends AbstractCommand {

    public CMDQuizBotHelp(Bot bot) {
        super(bot, new String[]{"!quizbot hilfe", "!quizbot help", "!quizbot commands", "!quizbot cmds", "!quizbot info"}, "Zeigt dieses Men�");

    }

    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {
        if (isTriggered(message)) {
            bot.sendMessage(sender, "Folgende Befehle sind verf�gbar");
            bot.sendMessage(sender, "/help Ruft die IRC Hilfe auf");
            for (Map.Entry<ECommand, ICommand> cmd : bot.getCommands().entrySet()) {
                //If startswith ! 
                if (cmd.getValue().getCommands()[0].startsWith("!")) {
                    cmd.getValue().printDescription(sender);
                }
            }
        }
    }
}
