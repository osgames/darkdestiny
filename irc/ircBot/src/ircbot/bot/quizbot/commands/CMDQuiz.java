/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.bot.quizbot.commands;

import at.viswars.webservice.Quiz;
import at.viswars.webservice.QuizResult;
import ircbot.commands.*;
import ircbot.Bot;
import ircbot.bot.quizbot.QuizBot;

/**
 *
 * @author Admin
 */
public class CMDQuiz extends AbstractCommand {

    public CMDQuiz(Bot bot) {
        super(bot, new String[]{"!quizbot"}, "Kills the Bot");

    }

    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {
        if (startsWith(message)) {
            String msg = message.replace(getCommands()[0], "");
            msg = msg.trim();
            if (msg.startsWith(EQuizCommand.list.toString())) {
                QuizResult qr = bot.getIrcSrv().getQuizList();
                if (qr.getQuizList().isEmpty()) {
                    bot.sendMessage("Keine Quiz vorhanden");
                }
                for (Quiz q : qr.getQuizList()) {

                    int count = bot.getIrcSrv().getQuestionCount(q.getId());

                    bot.sendMessage("<" + q.getId() + "> " + q.getName() + " | Topic : " + q.getTopic() + " [" + count + "] Fragen");
                }
            } else if (msg.startsWith(EQuizCommand.add.toString())) {

                msg = msg.replace(EQuizCommand.add.toString(), "").trim();
                String name = msg.substring(0, msg.indexOf("|") - 1).trim();
                msg = msg.substring(msg.indexOf("|") + 1).trim();
                String topic = msg;
                bot.getIrcSrv().addQuiz(name, topic);
                bot.sendMessage("Quiz hinzugef�gt : " + name + " Topic : " + topic);
            } else if (msg.startsWith(EQuizCommand.delete.toString())) {

                msg = msg.replace(EQuizCommand.delete.toString(), "").trim();
                String idString = msg.trim();
                int id = Integer.parseInt(idString);

                int done = bot.getIrcSrv().removeQuiz(id);
                if (done > 0) {
                    bot.sendMessage("Quiz mit id : <" + id + "> gel�scht");
                } else {
                    bot.sendMessage("Quiz mit id : <" + id + "> konnte nicht gefunden werden");
                }
            } else if (msg.startsWith(EQuizCommand.create.toString())) {

                msg = msg.replace(EQuizCommand.create.toString(), "").trim();
                String idString = msg.substring(0, msg.indexOf(" ")).trim();
                int id = Integer.parseInt(idString);
                msg = msg.substring(msg.indexOf(" ")).trim();
                String amountString = msg.substring(0, msg.indexOf(" ")).trim();
                int amount = Integer.parseInt(amountString);
                msg = msg.substring(msg.indexOf(" ")).trim();
                String secondString = msg.trim();
                int seconds = Integer.parseInt(secondString);
                
                System.out.println("id : " + id + " amount " + amount + " seconds " + seconds);
                
                if(bot instanceof QuizBot){
                    ((QuizBot)bot).startQuiz(id, amount, seconds);
                }
                
            }else if (msg.startsWith(EQuizCommand.cancel.toString())) {

                if(bot instanceof QuizBot){
                    ((QuizBot)bot).stopQuiz();
                }
                
            }
        }
    }

    public void printDescription(String sender) {

        for (EQuizCommand cmd : EQuizCommand.values()) {
            String desc = "";
            if (cmd.equals(EQuizCommand.list)) {
                desc = "Listet alle Quiz";
            } else if (cmd.equals(EQuizCommand.add)) {
                desc = "<name> | <topic> F�gt ein quiz hinzu";
            } else if (cmd.equals(EQuizCommand.delete)) {
                desc = "<id> L�scht ein Quiz";
            } else if (cmd.equals(EQuizCommand.create)) {
                desc = "<id> <anzahl fragen> <zeit zu antworten in sekunden> Erstellt eine neue Quiz Runde mit den gew�nschten Parametern";
            } else if (cmd.equals(EQuizCommand.cancel)) {
                desc = "Stoppt das aktuelle Quiz";
            }
            bot.sendMessage(sender, getCommands()[0] + " " + cmd + " " + desc);
        }
    }

    enum EQuizCommand {

        list, add, delete, create, cancel
    }
}
