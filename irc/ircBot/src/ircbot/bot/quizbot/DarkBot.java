/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.bot.quizbot;

/**
 *
 * @author Admin
 */
import ircbot.Bot;
import ircbot.User;
import ircbot.commands.CMDGreeting;
import ircbot.commands.CMDHelp;
import ircbot.commands.CMDKill;
import ircbot.commands.CMDListUsers;
import ircbot.commands.CMDPoints;
import ircbot.commands.CMDQA;
import ircbot.commands.CMDQuizBot;
import ircbot.commands.CMDTime;
import ircbot.commands.ECommand;
import ircbot.commands.ICommand;
import ircbot.thread.BanThread;
import java.util.ArrayList;
import java.util.Map;

public class DarkBot extends Bot {

    //Nick => User
    private ArrayList<BanThread> banThreads = new ArrayList<BanThread>();
    private QuizBot quizBot = null;

    public DarkBot(String name, String channel) {
        super(name, channel);

        commands.put(ECommand.KILL, new CMDKill(this));
        commands.put(ECommand.HELP, new CMDHelp(this));
       // commands.put(ECommand.LISTUSERS, new CMDListUsers(this));
        commands.put(ECommand.TIME, new CMDTime(this));
        commands.put(ECommand.POINTS, new CMDPoints(this));
        commands.put(ECommand.GREETING, new CMDGreeting(this));
        commands.put(ECommand.QUIZBOT, new CMDQuizBot(this));
        commands.put(ECommand.QA, new CMDQA(this));
    }

    public void sendWelcomeMessage() {
        sendMessage(getName() + " zu ihrer Stelle - Pleeep");
    }

    public void startQuizBot() {
        if (getQuizBot() == null) {
            quizBot = new QuizBot("QuizBot", channel);

            quizBot.setVerbose(true);
            try {
                quizBot.setEncoding("UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("Starte Quizbot");
            //For testing purpose
            //String server = "irc.freenode.net";
            //String channel = "#pircbot";
            try {
                // Connect to the IRC server.
                getQuizBot().connect("irc.euirc.net");
            } catch (Exception ex) {
                sendMessage("Quizbot konnte nicht gestartet werden : " + ex);
                quizBot = null;
                ex.printStackTrace();
            }

            // Join the #pircbot channel.
            //quizBot.sendMessage("NickServ", "IDENTIFY " + password);

            try {
                Thread.sleep(3000);
            } catch (Exception e) {
            }
            getQuizBot().joinChannel(channel);


            getQuizBot().sendWelcomeMessage();
        } else {
            sendMessage("Der QuizBot rennt bereits");
        }
    }

    public void stopQuizBot() {
        if (getQuizBot() != null) {

            getQuizBot().stopQuiz();
            getQuizBot().sendMessage(getQuizBot().getName() + " geht schlafen");
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
            }
            getQuizBot().disconnect();
            getQuizBot().quitServer();
            getQuizBot().dispose();

            quizBot = null;
        }
    }
    

    @Override
    public void onJoin(String channel, String sender, String login, String hostname) {
        if (!sender.trim().equalsIgnoreCase(getName().trim())) {

            User u = new User(sender, sender);

            // if (getIrcSrv().isUserRegistered(sender)) {
            if (false) {
                sendMessage("Hallo " + sender + "! Dein Name ist im Spiel DarkDestiny registriert.");
                sendMessage("Bitte identifiziere dich mittels folgendem befehl /msg DarkBot IDENTIFY <password>");
                BanThread banThread = new BanThread(this, sender);
                banThread.start();
                banThreads.add(banThread);
            } else {
                sendMessage("Hallo " + sender + "! Ich bin der Bot hier. Um Hilfe zu erhalten f�hre den Befehl !hilfe aus");
                String gameName = null;
                if (getIrcSrv().isUserRegistered(sender)) {
                    gameName = sender;
                } else if (getIrcSrv().isUserRegistered(sender.substring(1, sender.length()))) {
                    gameName = sender.substring(1, sender.length());

                }
                if (gameName != null) {

                    Integer points = getIrcSrv().getPoints(gameName);
                    Integer rank = getIrcSrv().getRank(gameName);
                    if (points >= 0 && rank >= 0) {
                        sendMessage(gameName + " | Rang : " + rank + " Punkte : " + points);
                    } else {
                        sendMessage(gameName + " nicht in den Statistiken gefunden ");
                    }
                }

            }

            users.put(sender, u);
        usersOnline++;
            updateUserCount();
        }

    }

    @Override
    public void onNickChange(String oldNick, String login, String hostname, String newNick){
        
        removeUser("NICKCHANGE", oldNick);
        users.put(newNick, new User(newNick, ""));
    }
    
    @Override
    public void onPart(String channel, String sender, String login, String hostname) {
       
        removeUser("PART", sender);
        usersOnline--;
        updateUserCount();

    }

    private void removeUser(String event, String sender){
        try {
         System.out.println("Got " + event + " by : " + sender + " try to decrement from : " + users.size());
           if (users.containsKey(sender)) {
                users.remove(sender);
            } if (users.containsKey("!"+sender)) {
                users.remove("!"+sender);
            }
             } catch (Exception e) {
            System.out.println("User : " + sender + " was not on list");
        }
         System.out.println("New UserSize : " + users.size());
    }
    @Override
    public void onQuit(String channel, String sender, String login, String hostname) {
        System.out.println("Sender : " + sender + " login : " + login + " hostname " + hostname);
        usersOnline--;
        removeUser("QUIT", sender);
        updateUserCount();

    }

    public void identifyName() {
    }

    @Override
    public void onUserList(String channel,
            org.jibble.pircbot.User[] tmpUsers) {
        for (org.jibble.pircbot.User u : tmpUsers) {
            users.put(u.getNick(), new User(u.getNick(), u.getPrefix()));
            usersOnline = users.size() -1;
        }
        updateUserCount();
    }

    @Override
    public void onMessage(String channel, String sender,
            String login, String hostname, String message) {

        updateBanThreads();
        for (Map.Entry<ECommand, ICommand> entry : getCommands().entrySet()) {

            entry.getValue().onMessage(channel, sender, login, hostname, message);
        }

        for (BanThread bt : banThreads) {
            if (bt.isAlive()) {
                bt.onMessage(channel, sender, login, hostname, message);
            }
        }

    }

    @Override
    public void onPrivateMessage(String sender, String login, String hostname, String message) {


        for (BanThread bt : banThreads) {
            if (bt.isAlive()) {
                bt.onMessage(channel, sender, login, hostname, message);
            }
        }

    }

    /**
     * @return the quizBot
     */
    public QuizBot getQuizBot() {
        return quizBot;
    }
}
