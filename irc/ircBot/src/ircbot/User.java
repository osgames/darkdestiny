/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot;

/**
 *
 * @author Admin
 */
public class User {
    
    private String nick;
    private String prefix;
    
    public User(String nick, String prefix){
        this.nick = nick;
        this.prefix = prefix;
    }

    /**
     * @return the nick
     */
    public String getNick() {
        return nick;
    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }
}
