    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.thread;

import at.viswars.webservice.Question;
import ircbot.IBot;
import ircbot.bot.quizbot.QuizBot;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class QuizThread extends Thread {

    private int quizId;
    private ArrayList<Question> questions;
    private long milis;
    private IBot bot;
    private boolean answered;
    private String answer;
    private HashMap<String, Integer> ranking = new HashMap<String, Integer>();
    private String solver;
    private boolean cancel = false;

    public QuizThread(IBot bot, int quizId, ArrayList<Question> questions, int seconds) {
        this.bot = bot;
        this.quizId = quizId;
        this.questions = questions;
        this.milis = seconds * 1000;
    }

    public void run() {
        for (Question q : questions) {
            answered = false;
            answer = q.getAnswer();
            bot.sendMessage("###" + q.getQuestion());
            int time = 0;

            milis = answer.length() * 2000l + 10000l;

            double points = 10d;
            double pointsPerTick = 10000d / milis;

            int lastLength = 0;
            while (!answered && time < milis) {

                try {
                    sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(QuizThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                time += 1000;
                points -= pointsPerTick;


                if (time > 10000) {

                    int lengthToReveal = (int) Math.floor((time - 10000) / 2000);

                    if (lengthToReveal > q.getAnswer().length()) {
                        break;
                    }
                    if (lastLength != lengthToReveal) {
                        if (lengthToReveal > 0) {
                            bot.sendMessage("###Tipp : " + q.getAnswer().substring(0, lengthToReveal));
                            lastLength = lengthToReveal;
                        }

                    }
                }



                if (points < 0) {
                    points = 0;
                }
            }
            if(cancel){
                break;
            }
            if (!answered) {
                bot.sendMessage("###Antwort : " + q.getAnswer());
            } else {
                bot.sendMessage("###" + solver + " hat die richtige L�sung: " + q.getAnswer() + "! +" + (int) Math.round(points) + " Punkte");
                if (ranking.containsKey(solver)) {
                    ranking.put(solver, ranking.get(solver) + (int) Math.round(points));
                } else {
                    ranking.put(solver, (int) Math.round(points));
                }
            }

        }
        bot.sendMessage("###Das Quiz ist vorbei");
        for (Map.Entry<String, Integer> entry : ranking.entrySet()) {

            bot.sendMessage("###" + entry.getKey() + " hat " + entry.getValue() + " Punkte");
        }
        this.interrupt();
        if (bot instanceof QuizBot) {
            ((QuizBot) bot).stopQuiz();
        }
    }

    public void onMessage(String channel, String sender, String login, String hostname, String message) {
        if (message != null && !message.equals("") && answer != null && !answer.equals("")) {
            if (message.trim().equals(answer.trim())) {
                answered = true;
                solver = sender;
            }

        }

    }

    /**
     * @param cancel the cancel to set
     */
    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }
}
