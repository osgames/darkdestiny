/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot.thread;

import ircbot.Bot;
import ircbot.commands.ICommand;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class BanThread extends Thread implements ICommand {

    Bot bot;
    String nick;
    boolean identified = false;

    public BanThread(Bot bot, String nick) {
        this.bot = bot;
        this.nick = nick;
    }

    public void nickChange(String oldNick, String newNick) {
        if (nick.equals(oldNick)) {
            nick = newNick;
        }
    }

    @Override
    public void run() {
        int time = 0;
        int maxTime = 60;
        while (time < maxTime) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(BanThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            time++;

        }
        boolean tmp = bot.getIrcSrv().isUserRegistered("!"+nick);
        System.out.println("identified : " + identified + " userRegistered " + bot.getIrcSrv().isUserRegistered(nick) + " ! userregistered " + tmp);
        if (!identified && bot.getIrcSrv().isUserRegistered(nick)) {
            bot.sendMessage(nick + " wurde vom Server entfernt (Name gesch�tzt)");
            bot.kick("#darkdestiny", nick, "Dieser Name ist gesch�tzt");
        }


    }

    @Override
    public void onMessage(String channel, String sender, String login, String hostname, String message) {

        System.out.println("Compare : " + sender + " to : " + nick + " and " + sender + " to : !" + nick);
        System.out.println("Received message : " + message);
        if ((sender.equals(nick) || sender.equals("!" + nick)) && !identified) {
            String password = message.replace("IDENTIFY", "");
            identified = bot.getIrcSrv().isUserIdentified(sender, password.trim());
            bot.sendMessage(sender, "Danke f�r die Identifizierung " + sender);
        }
    }

    @Override
    public void printDescription(String sender) {
    }

    @Override
    public String[] getCommands() {
        return null;
    }
}
