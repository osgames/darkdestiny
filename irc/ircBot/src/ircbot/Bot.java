/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ircbot;

/**
 *
 * @author Admin
 */
import at.viswars.webservice.IRC;
import at.viswars.webservice.IRCService;
import ircbot.commands.CMDGreeting;
import ircbot.commands.CMDHelp;
import ircbot.commands.CMDKill;
import ircbot.commands.CMDListUsers;
import ircbot.commands.CMDPoints;
import ircbot.commands.CMDQA;
import ircbot.commands.CMDTime;
import ircbot.commands.ECommand;
import ircbot.commands.ICommand;
import ircbot.thread.BanThread;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.jibble.pircbot.PircBot;

public abstract class Bot extends PircBot {

    protected String channel;
    protected HashMap<ECommand, ICommand> commands = new HashMap<ECommand, ICommand>();
    //Nick => User
    protected HashMap<String, User> users = new HashMap<String, User>();
    private IRCService service = new IRCService();
    private IRC ircSrv = service.getIRCPort();
    private ArrayList<BanThread> banThreads = new ArrayList<BanThread>();

    protected int usersOnline = 0;
    
    public Bot(String name, String channel) {
        this.setName(name);
        this.channel = channel;
        service = new IRCService();
        ircSrv = service.getIRCPort();
    }

    public void sendWelcomeMessage() {

        sendMessage(getName() + " zu ihrer Stelle - Pleeep");
    }

    public User searchUser(String nick) {

        for (Map.Entry<String, User> entry : users.entrySet()) {
            User u = entry.getValue();
            if (u.getNick().equals(nick) || (u.getNick()).equals("!" + nick)) {
                return u;
            }
        }
        return null;

    }


    public void updateBanThreads() {

        ArrayList<BanThread> toRemove = new ArrayList<BanThread>();
        for (BanThread bt : banThreads) {
            if (!bt.isAlive()) {
                toRemove.add(bt);
            }
        }
        for (BanThread bt : toRemove) {
            banThreads.remove(bt);
        }
    }



    public void updateUserCount() {
        try {
            getIrcSrv().setUserCount(usersOnline);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void sendMessage(String message) {
        sendMessage(channel, message);
    }

    public ArrayList<User> getUsers() {
        ArrayList<User> tmp = new ArrayList<User>();
        tmp.addAll(users.values());
        return tmp;
    }

    /**
     * @return the commands
     */
    public HashMap<ECommand, ICommand> getCommands() {
        return commands;
    }

    /**
     * @return the ircSrv
     */
    public IRC getIrcSrv() {
        return ircSrv;
    }
}
