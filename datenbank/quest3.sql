/*
Navicat MySQL Data Transfer

Source Server         : dd
Source Server Version : 50515
Source Host           : localhost:3306
Source Database       : dd_database

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2012-04-14 10:47:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `quest`
-- ----------------------------
DROP TABLE IF EXISTS `quest`;
CREATE TABLE `quest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `author` varchar(255) NOT NULL DEFAULT '',
  `accepted` tinyint(4) NOT NULL DEFAULT '0',
  `type` enum('REPEAT','ONETIME_USER','ONETIME_ALLIANCE') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of quest
-- ----------------------------
INSERT INTO `quest` VALUES ('2', 'quest_2_name', 'quest_2_description', 'Dreloc', '0', 'REPEAT');
INSERT INTO `quest` VALUES ('3', 'quest_3_name', 'quest_3_description', 'Rayden', '0', 'ONETIME_USER');

-- ----------------------------
-- Table structure for `questrequirement`
-- ----------------------------
DROP TABLE IF EXISTS `questrequirement`;
CREATE TABLE `questrequirement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `working` tinyint(4) NOT NULL DEFAULT '0',
  `methodName` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of questrequirement
-- ----------------------------
INSERT INTO `questrequirement` VALUES ('1', 'questrequirement_1_name', 'questrequirement_1_description', '1', '');
INSERT INTO `questrequirement` VALUES ('2', 'questrequirement_2_name', 'questrequirement_2_description', '1', '');
INSERT INTO `questrequirement` VALUES ('3', 'questrequirement_3_name', 'questrequirement_3_description', '1', '');
INSERT INTO `questrequirement` VALUES ('4', 'questrequirement_4_name', 'questrequirement_4_description', '1', '');
INSERT INTO `questrequirement` VALUES ('5', 'questrequirement_5_name', 'questrequirement_5_description', '1', '');
INSERT INTO `questrequirement` VALUES ('6', 'questrequirement_6_name', 'questrequirement_6_description', '1', '');
INSERT INTO `questrequirement` VALUES ('7', 'questrequirement_7_name', 'questrequirement_7_description', '1', '');
INSERT INTO `questrequirement` VALUES ('8', 'questrequirement_8_name', 'questrequirement_8_description', '1', '');
INSERT INTO `questrequirement` VALUES ('9', 'questrequirement_9_name', 'questrequirement_9_description', '0', '');
INSERT INTO `questrequirement` VALUES ('10', 'questrequirement_10_name', 'questrequirement_10_description', '0', '');
INSERT INTO `questrequirement` VALUES ('11', 'questrequirement_11_name', 'questrequirement_11_description', '1', '');
INSERT INTO `questrequirement` VALUES ('12', 'questrequirement_12_name', 'questrequirement_12_description', '0', '');

-- ----------------------------
-- Table structure for `questrequirementmap`
-- ----------------------------
DROP TABLE IF EXISTS `questrequirementmap`;
CREATE TABLE `questrequirementmap` (
  `questRelationId` int(11) NOT NULL,
  `questRequirementId` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`questRelationId`,`questRequirementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of questrequirementmap
-- ----------------------------

-- ----------------------------
-- Table structure for `questresult`
-- ----------------------------
DROP TABLE IF EXISTS `questresult`;
CREATE TABLE `questresult` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `working` tinyint(4) NOT NULL DEFAULT '0',
  `methodName` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of questresult
-- ----------------------------
INSERT INTO `questresult` VALUES ('1', 'questresult_1_name', 'questresult_1_description', '1', '');
INSERT INTO `questresult` VALUES ('2', 'questresult_2_name', 'questresult_2_description', '1', '');
INSERT INTO `questresult` VALUES ('3', 'questresult_3_name', 'questresult_3_description', '0', '');

-- ----------------------------
-- Table structure for `questresultmap`
-- ----------------------------
DROP TABLE IF EXISTS `questresultmap`;
CREATE TABLE `questresultmap` (
  `questRelationId` int(11) NOT NULL,
  `questResultId` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`questRelationId`,`questResultId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of questresultmap
-- ----------------------------

-- ----------------------------
-- Table structure for `queststep`
-- ----------------------------
DROP TABLE IF EXISTS `queststep`;
CREATE TABLE `queststep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `message` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of queststep
-- ----------------------------
INSERT INTO `queststep` VALUES ('1', '2', 'queststep_1_name', 'queststep_1_description', 'queststep_1_message');
INSERT INTO `queststep` VALUES ('2', '2', 'queststep_2_name', 'queststep_2_description', 'queststep_2_message');
INSERT INTO `queststep` VALUES ('3', '2', 'queststep_3_name', 'queststep_3_description', 'queststep_3_message');

-- ----------------------------
-- Table structure for `queststeprelation`
-- ----------------------------
DROP TABLE IF EXISTS `queststeprelation`;
CREATE TABLE `queststeprelation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stepId` int(11) NOT NULL,
  `previousStepId` int(11) NOT NULL DEFAULT '0',
  `type` enum('OTHER','RANDOM_DRIVEN','USER_DRIVEN') NOT NULL DEFAULT 'OTHER',
  `value` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of queststeprelation
-- ----------------------------
INSERT INTO `queststeprelation` VALUES ('1', '1', '0', 'RANDOM_DRIVEN', '1');
INSERT INTO `queststeprelation` VALUES ('2', '2', '1', 'USER_DRIVEN', 'null');
INSERT INTO `queststeprelation` VALUES ('3', '3', '1', 'USER_DRIVEN', 'null');
INSERT INTO `queststeprelation` VALUES ('4', '2', '0', 'USER_DRIVEN', 'null');
INSERT INTO `queststeprelation` VALUES ('5', '2', '0', 'OTHER', 'null');
