***Dark Destiny Version 0.61***

Dark Destiny is a open source browsergame developed on Sourceforge since 2005. 

Please note that for the installation and any alteration of the game you might need a basic understanding of:
.)Java
.)Mysql
.)Apache Tomcat

-License:
Please note that this game is published under GNU General Public License (GPL). For further details visit http://en.wikipedia.org/wiki/GNU_General_Public_License

-Language:
The game is developed in German and later (not yet fully) translated into English. Please note that if there are more requests we will focus on this part.
Feel free to extend the game with other languages: Add a properties file(Bundle_<yourlangage>_<yourcountry>.properites) and insert a database entry in the language-table.
We would appreciate if you send us a copy of it :)

-Installation:
.) As Server

Take all the shitty code and put it onto your tomcat!

There are two ways of injecting the database and creating your universe:
1.) The first possibility is to run the setup.jsp and follow the guideline. You have to set the mysql settings as well as galaxyparameters and gameparameters.
After the galaxy has been created you have to inject the game constants. The "Constants.sql" provides you with all standard constructions,
researches, weapons and so on.
2.) The second way is to inject a full database and set the properties files for mysql, the galaxy and the game manually. Therefore modify the game.properties, db.properties and universe.properties.

.) For Programers with Netbeans
Note that there is a PDF "Installation.pdf" which explains how to check out the newest code and how to create
a new project with netbeans. Furthermore useful routine tasks are listed.


If you have any questions or suggestions feel free to ask in our forums:
http://www.thedarkdestiny.at/board/index.php
